//chart initialization
$(document).ready(function() {

    /* ********************************************************************************************************************* */
    // Age Sex Distrubution

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    var mainContainer = am4core.create("age-distribution", am4core.Container);
    mainContainer.width = am4core.percent(100);
    mainContainer.height = am4core.percent(100);
    mainContainer.layout = "horizontal";


    var maleChart = mainContainer.createChild(am4charts.XYChart);
    maleChart.paddingRight = 0;
    maleChart.dataSource.url = baseurl + "/profile/agesexdistribution/" + txt_municipalityid;
    maleChart.dataSource.parser = new am4core.JSONParser();
    // maleChart.data = JSON.parse(JSON.stringify(usData));


    // Create axes
    var maleCategoryAxis = maleChart.yAxes.push(new am4charts.CategoryAxis());
    maleCategoryAxis.dataFields.category = "age";
    maleCategoryAxis.renderer.grid.template.location = 0;
    //maleCategoryAxis.renderer.inversed = true;
    maleCategoryAxis.renderer.minGridDistance = 15;
    maleCategoryAxis.renderer.inside = true;

    var maleValueAxis = maleChart.xAxes.push(new am4charts.ValueAxis());
    maleValueAxis.renderer.inversed = true;
    maleValueAxis.min = 0;
    maleValueAxis.max = 15;
    maleValueAxis.strictMinMax = true;

    maleValueAxis.numberFormatter = new am4core.NumberFormatter();
    maleValueAxis.numberFormatter.numberFormat = "#.#'%'";

    // Create series
    var maleSeries = maleChart.series.push(new am4charts.ColumnSeries());
    maleSeries.dataFields.valueX = "male";
    maleSeries.dataFields.valueXShow = "percent";
    maleSeries.calculatePercent = true;
    maleSeries.dataFields.categoryY = "age";
    maleSeries.interpolationDuration = 1000;
    if(getlocale == 'np'){
    maleSeries.columns.template.tooltipText = "पुरुष, {categoryY}: {valueX} ({valueX.percent.formatNumber('#.0')}%)";
    }else{
    maleSeries.columns.template.tooltipText = "Males, {categoryY}: {valueX} ({valueX.percent.formatNumber('#.0')}%)";
    }
    //maleSeries.sequencedInterpolation = true;

    maleSeries.tooltip.autoTextColor = false;
    maleSeries.tooltip.label.fill = am4core.color("#FFFFFF");




    var femaleChart = mainContainer.createChild(am4charts.XYChart);
    femaleChart.paddingLeft = 0;
    femaleChart.dataSource.url = baseurl + "/profile/agesexdistribution/" + txt_municipalityid;
    femaleChart.dataSource.parser = new am4core.JSONParser();

    // Create axes
    var femaleCategoryAxis = femaleChart.yAxes.push(new am4charts.CategoryAxis());
    femaleCategoryAxis.renderer.opposite = true;
    femaleCategoryAxis.dataFields.category = "age";
    femaleCategoryAxis.renderer.grid.template.location = 0;
    femaleCategoryAxis.renderer.minGridDistance = 15;
    femaleCategoryAxis.renderer.inside = true;

    var femaleValueAxis = femaleChart.xAxes.push(new am4charts.ValueAxis());
    femaleValueAxis.min = 0;
    femaleValueAxis.max = 15;
    femaleValueAxis.strictMinMax = true;
    femaleValueAxis.numberFormatter = new am4core.NumberFormatter();
    femaleValueAxis.numberFormatter.numberFormat = "#.#'%'";
    femaleValueAxis.renderer.minLabelPosition = 0.01;

    // Create series
    var femaleSeries = femaleChart.series.push(new am4charts.ColumnSeries());
    femaleSeries.dataFields.valueX = "female";
    femaleSeries.dataFields.valueXShow = "percent";
    femaleSeries.calculatePercent = true;
    femaleSeries.fill = femaleChart.colors.getIndex(4);
    femaleSeries.stroke = femaleSeries.fill;

    femaleSeries.tooltip.autoTextColor = false;
    femaleSeries.tooltip.label.fill = am4core.color("#FFFFFF");

    //femaleSeries.sequencedInterpolation = true;
    if(getlocale == 'np'){
    femaleSeries.columns.template.tooltipText = "महिलाहरु, {categoryY}: {valueX} ({valueX.percent.formatNumber('#.0')}%)";
    }else{
    femaleSeries.columns.template.tooltipText = "Females, {categoryY}: {valueX} ({valueX.percent.formatNumber('#.0')}%)";
    }
    femaleSeries.dataFields.categoryY = "age";
    femaleSeries.interpolationDuration = 1000;


    maleChart.legend = new am4charts.Legend();
    if(getlocale == 'np'){
    maleChart.legend.valueLabels.template.text = "पुरुष";
    }else{
    maleChart.legend.valueLabels.template.text = "Male";
    }


    femaleChart.legend = new am4charts.Legend();
    if(getlocale == 'np'){
    femaleChart.legend.valueLabels.template.text = "महिला";
    }else{
    femaleChart.legend.valueLabels.template.text = "Female";
    }

const { forEach } = require("lodash");

try {
              // Themes begin
// Using default theme
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("chartdiv", am4charts.XYChart);

// Add data
chart.data = [{"Ward":1,"Male":155,"Female":1401,"Other":5},{"Ward":2,"Male":924,"Female":1079,"Other":0},{"Ward":3,"Male":844,"Female":907,"Other":3},{"Ward":4,"Male":1795,"Female":1836,"Other":1},{"Ward":5,"Male":1419,"Female":1546,"Other":2},{"Ward":6,"Male":1098,"Female":1163,"Other":1},{"Ward":7,"Male":1136,"Female":1222,"Other":1},{"Ward":8,"Male":712,"Female":787,"Other":0},{"Ward":9,"Male":743,"Female":873,"Other":1},{"Ward":10,"Male":1230,"Female":1396,"Other":0},{"Ward":11,"Male":853,"Female":917,"Other":2},{"Ward":12,"Male":1480,"Female":1644,"Other":1}];;



// Create axes
var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "Ward";
categoryAxis.title.text = "Ward Number";
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.renderer.minGridDistance = 20;
categoryAxis.renderer.cellStartLocation = 0.1;
categoryAxis.renderer.cellEndLocation = 0.9;

var  valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.min = 0;
valueAxis.title.text = "जनसंख्या";

// Create series
function createSeries(field, name, stacked) {
  var series = chart.series.push(new am4charts.ColumnSeries());
  series.dataFields.valueY = field;
  series.dataFields.categoryX = "Ward";
  series.name = name;
  series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
  series.stacked = stacked;
  series.columns.template.width = am4core.percent(95);
}

createSeries("Male", "पुरुष", false);
createSeries("Female", "महिला", false);
createSeries("Other", "अन्य लिङ्ग", false);
createSeries("test", "अन्य ", false);

// Add legend
chart.legend = new am4charts.Legend();            }
            catch( e ) {
              console.log( e );
            }

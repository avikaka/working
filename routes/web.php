<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/geojson','Admin\CollegeController@geoJson');

// Auth::routes();
Route::get('d-admin', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('d-admin', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
]);
Route::post('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);


Route::get('forgetpassword', 'Admin\UserController@forget');
Route::post('changepassword', 'Admin\UserController@changepassword');
Route::get('changepassword/{id}', 'Admin\UserController@linkpassword');
Route::post('/newpasswordforget/{id}', 'Admin\UserController@newpasswordforget');
Route::get('validatepassword/{slug}', 'Admin\UserController@validation');
Route::post('/newpassword/{slug}', 'Admin\UserController@newpassword');




Route::get('/home-test', 'DashboardController@homeTest');
Route::get('/admin', 'DashboardController@index');
Route::get('/', 'DashboardController@index', ['ward' => null]);
Route::get('/dashboard', 'DashboardController@index');
Route::get('/dashboard_ajax_bank', 'DashboardController@index_ajax_bank');
Route::get('/dashboard_ajax_college', 'DashboardController@index_ajax_college');
Route::get('/test', 'DashboardController@test');

// route::get('admin',function(){
//     return view('admin/user/login');
// });
//called via ajax
Route::get('/bankdetails/{type}', 'Admin\BankController@banktype');
Route::get('/collegedetails/{type}', 'Admin\CollegeController@collegetype');
Route::get('/healthdetails/{type}', 'Admin\HealthInstutionController@healthtype');
Route::get('/cooperativedetails/{type}', 'Admin\CooperativeController@cooperartivetype');


Route::get('/dashboard/ward-{ward}', 'DashboardController@dashboard');
Route::get('/update-ward', 'DashboardController@updateWard');
// Route::get('/person', 'PersonController@index');
// Route::get('/ws', 'PersonController@wardWise');
// Route::get('/report', 'PersonController@personsReport');
// Route::get('/json', 'PersonController@json');
Route::get('/age-group', 'PersonController@jsonAgeGroup');
Route::get('/gbc', 'PersonController@getByCaste');
Route::get('/value', 'ValueController@index');
Route::get('/ags', 'PersonController@ageGroupSex');

//Ajax Call routers
Route::prefix('ajax')->name('ajax.')->group(function () {
    Route::get('dashboard/ward-{ward}', 'DashboardController@dashboard')->name('dashboard');
});

//jsonfile
Route::prefix('json')->name('json.')->group(function () {
    Route::get('file', 'JsonController@makejson');
});

Route::prefix('household')->name('household.')->group(function () {
    Route::get('/', 'HouseholdController@index')->name('dashboard');
    // Route::get('/hhget', 'HouseholdController@getData');
    // Route::view('/agroup', 'reports.age-group');
    Route::get('house-ownership', 'HouseholdController@houseOwnership')->name('house.ownership');
    Route::get('land-ownership', 'HouseholdController@landOwnership')->name('land.ownership');
    Route::get('house-type', 'HouseholdController@householdType')->name('house.type');
    Route::get('water-source', 'HouseholdController@waterSource')->name('water.source');
    Route::get('cooking-fuel-source', 'HouseholdController@cookingFuelSource')->name('cooking.fuel');
    Route::get('light-fuel-source', 'HouseholdController@lightFuelSource')->name('light.fuel');
    Route::get('access-road', 'HouseholdController@accessRoad')->name('access.road');
    Route::get('female-ownership', 'HouseholdController@womanOwnership')->name('female.ownership');
    Route::get('bank-account', 'HouseholdController@bankaccount')->name('bank.account');
    Route::get('amenities', 'HouseholdController@houseequipment')->name('amenities');
    Route::get('toilet-status', 'HouseholdController@toilet')->name('toilet');
    Route::get('garbage-management', 'HouseholdController@garbage')->name('garbage');
    Route::get('woman-ownership-in-assets', 'HouseholdController@womenOwnership')->name('women.assets');
});

//Population
Route::prefix('population')->name('population.')->group(function () {
    Route::get('gender', 'PersonController@genderwise')->name('gender');
    Route::get('gender/{ward}', 'PersonController@genderwise');
    Route::get('agewise', 'PersonController@agewise')->name('agewise');
    Route::get('religion', 'PersonController@religion')->name('religion');
    Route::get('caste', 'PersonController@caste')->name('caste');
    Route::get('motherTongue', 'PersonController@motherTongue')->name('mother.tongue');
    Route::get('disability', 'PersonController@disability')->name('disability');
    Route::get('marital-status', 'PersonController@maritalStatus');
    Route::get('marital-status-current-age', 'PersonController@ageAtFirstMarriage')->name('marital.status.current.age');
    Route::get('marital-status', 'PersonController@maritalStatus')->name('marital.status');
    Route::get('age-at-first-marriage', 'PersonController@ageAtFirstMarriage')->name('first.marriage.age');
    Route::get('literacy', 'PersonController@literacy')->name('literacy');
    Route::get('birthplace', 'PersonController@birthplace')->name('birth.place');
});

//Education
Route::prefix('education')->name('education.')->group(function () {
    Route::get('literacy', 'PersonController@literacy')->name('literacy');
    Route::get('level', 'EducationController@educationLevel')->name('level');
    Route::get('not-attending-reason', 'EducationController@reason')->name('school.not.attending.reasons');
    Route::get('school-attend', 'EducationController@attend')->name('school.attending');
    Route::get('skill', 'EducationController@skill')->name('skills');
    Route::get('training', 'EducationController@training')->name('training');
});

//Health
Route::prefix('health')->name('health.')->group(function () {
    Route::get('life-long-disease', 'HealthController@lifelong')->name('life.long.disease');
    Route::get('vaccination', 'HealthController@vaccination')->name('vaccination');
    Route::get('safe-delivery', 'HealthController@safe')->name('safe');
    Route::get('birth', 'HealthController@birth')->name('birth');
    Route::get('total-birth', 'HealthController@totalBirth')->name('total.birth');
    Route::get('child-weight', 'HealthController@childWeight')->name('child.weight');
    Route::get('delivery-place', 'HealthController@delivery')->name('delivery.place');
    Route::get('mother-age', 'HealthController@motherAge')->name('mother.age');
    Route::get('emergency-disease', 'HealthController@emergency')->name('emergency.disease');
});

//Absentees
Route::prefix('absentees')->name('absentees.')->group(function () {
    Route::get('gender', 'AbsenteesController@gender')->name('gender');
    Route::get('agewise', 'AbsenteesController@agewise')->name('agewise');
    Route::get('education', 'AbsenteesController@education')->name('education');
    Route::get('reason', 'AbsenteesController@reason')->name('reasons');
    Route::get('currently-staying', 'AbsenteesController@current')->name('currently.stayiing');
});

Route::get('communityBuilding', 'InstitutionalController@communityBuilding');
Route::get('communityForest', 'InstitutionalController@communityForest');
Route::get('publicToilets', 'InstitutionalController@publicToilets');
Route::get('waterLands', 'InstitutionalController@waterLands');
Route::get('irrigation', 'InstitutionalController@irrigation');
Route::get('agroFarms', 'InstitutionalController@agroFarms');
Route::get('agroGroups', 'InstitutionalController@agroGroups');
Route::get('tourisms', 'InstitutionalController@tourisms');
Route::get('religious', 'InstitutionalController@religious');
Route::get('agroPocket', 'InstitutionalController@agroPocket');
Route::get('roads', 'InstitutionalController@roads');
Route::get('bridge', 'InstitutionalController@bridge');
Route::get('drinkingWater', 'InstitutionalController@drinkingWater');
Route::get('haatBazar', 'InstitutionalController@haatBazar');
Route::get('tlo', 'InstitutionalController@tlo');
Route::get('cooperatives', 'InstitutionalController@cooperatives');
Route::get('banks', 'InstitutionalController@banks');
Route::get('healthInstitutions', 'InstitutionalController@healthInstitutions');
Route::get('playground', 'InstitutionalController@playground');
Route::get('school', 'InstitutionalController@school');
Route::get('childrenClubs', 'InstitutionalController@childrenClubs');
Route::get('youthClubs', 'InstitutionalController@communityForest');
Route::get('publicPlaces', 'InstitutionalController@publicPlaces');
Route::get('crematorioum', 'InstitutionalController@crematorioum');
Route::get('womensGroup', 'InstitutionalController@womensGroup');
Route::get('socialHome', 'InstitutionalController@socialHome');
Route::get('colleges', 'InstitutionalController@colleges');



//Admin Area
Route::group(['middleware' => ['auth']], function () {

    Route::get('/admin/home', 'HomeController@index');
    Route::post('household/search', ['as' => 'search-house', 'uses' => 'HomeController@search']);

    Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function () {

        Route::get('/insertuser', 'UserController@createuser');
        Route::post('/storeuser', 'UserController@storeuser');
        Route::get('/userdeleteform/{id}', 'UserController@deleteuser');
        Route::get('/viewuser', 'UserController@viewuser');
        Route::get('/changePassword/{id}', 'UserController@showChangePasswordForm');
        Route::put('/changePassworduser/{id}', 'UserController@changePassworduser')->name('changePassworduser');

        Route::get('school', 'SchoolController@index');

        //Family
        // Route::resource('housing_household', 'HousingHouseholdController');

        Route::get('housing_household', 'HousingHouseholdController@index');
        Route::post('housing_household', 'HousingHouseholdController@update');

        Route::get('PersonalInof', 'PersonalInformationController@index');
        Route::post('PersonalInof', 'PersonalInformationController@update');

        Route::resource('IndividualInfo', 'GeneralindividualinformationController');

        Route::get('cover', 'GeneralindividualinformationController@index');
        Route::get('cover_create', 'GeneralindividualinformationController@create');
        Route::get('search', 'GeneralindividualinformationController@getData');

        // Route::get('details', 'viewdetailsController@getDetails');
        Route::resource('details', 'viewdetailsController');

        //College
        Route::resource('college', 'CollegeController');

        //ChildrenClub
        Route::resource('children-club', 'ChildrenClubController');

        //SocialHome
        Route::resource('social-home', 'SocialHomeController');

        //WomenGroup
        Route::resource('women-group', 'WomenGroupController');

        //AgroFarm
        Route::resource('agro-farm', 'AgroFarmController');

        //AgroVet
        Route::resource('agro-vet', 'AgroVetController');

        //Agro Vet pocket
        Route::resource('agro-vet-pocket', 'AgroVetPocketController');

        //PublicPlace
        Route::resource('public-place', 'PublicPlaceController');

        //Playground
        Route::resource('playground', 'PlaygroundController');

        //Club
        Route::resource('youth-club', 'ClubController');

        //CommunityBuildings
        Route::resource('community-building', 'CommunityBuildingController');

        Route::resource('drinking-water', 'DrinkWaterProjectController');

        //Bank
        Route::resource('bank', 'BankController');
        Route::post('t/search', ['as' => 'search-bank', 'uses' => 'BankController@search']);

        //Religious
        Route::resource('religious', 'ReligiousController');

        //Tourism
        Route::resource('tourism', 'TourismController');

        //TLO
        Route::resource('tlo', 'TLOController');

        //Cooperative
        Route::resource('cooperative', 'CooperativeController');

        //HealthInstitution
        Route::resource('health-institution', 'HealthInstutionController');

        Route::get('school', 'SchoolController@index');

        //Public Toilet

        Route::resource('public-toilet', 'PublicToiletController');

        //Bridge
        Route::resource('bridge', 'BridgeController');

        //irrigation_projects
        Route::resource('irrigation', 'IrrigationController');

        //Haat Bazar
        Route::resource('haat-bazar', 'HaatBazarController');

        //Ponds Lakes Weland
        Route::resource('pond-lake-wetland', 'PondsLakesWetlandController');

        //Comunity Forest
        Route::resource('community-forest', 'CommunityForestController');

        //Road_Networks
        Route::resource('road-network', 'RoadNetworkController');

        //Drinking Water Route
        Route::resource('drinking-water-projects', 'DrinkWaterProjectController');

        //Crematorium
        Route::resource('crematorium', 'CrematoriumController');

        //Impoertantcontact.......
        Route::get('/insertcontact', 'ImportantContactController@createcontact');
        Route::post('/storecontact', 'ImportantContactController@storecontact');
        Route::get('/viewcontact', 'ImportantContactController@viewcontact');
        Route::get('/changecontactStatus/{id}', 'ImportantContactController@changeStatus');
        Route::get('/contactedit/{id}', 'ImportantContactController@editcontact');
        Route::put('/updatecontact/{id}', 'ImportantContactController@updatecontact');
        Route::delete('/contactdeleteform/{id}', 'ImportantContactController@deletecontact');

        //carousel.......
        Route::get('/insertcarousel', 'CarouselController@createcarousel');
        Route::post('/storecarousel', 'CarouselController@storecarousel');
        Route::get('/viewcarousel', 'CarouselController@viewcarousel');
        Route::get('/changecarouselStatus/{id}', 'CarouselController@changeStatus');
        Route::get('/carouseledit/{id}', 'CarouselController@editcarousel');
        Route::put('/updatecarousel/{id}', 'CarouselController@updatecarousel');
        Route::delete('/carouseldeleteform/{id}', 'CarouselController@deletecarousel');

        //logo.......
        Route::get('/insertlogo', 'LogoController@createlogo');
        Route::post('/storelogo', 'LogoController@storelogo');
        Route::get('/viewlogo', 'LogoController@viewlogo');
        Route::get('/changelogoStatus/{id}', 'LogoController@changeStatus');
        Route::get('/logoedit/{id}', 'LogoController@editlogo');
        Route::put('/updatelogo/{id}', 'LogoController@updatelogo');
        Route::delete('/logodeleteform/{id}', 'LogoController@deletelogo');

        //Description.......
        Route::get('/insertdescription', 'DescriptionController@createdescription');
        Route::post('/storedescription', 'DescriptionController@storedescription');
        Route::get('/viewdescription', 'DescriptionController@viewdescription');
        Route::get('/changedescriptionStatus/{id}', 'DescriptionController@changeStatus');
        Route::get('/descriptionedit/{id}', 'DescriptionController@editdescription');
        Route::put('/updatedescription/{id}', 'DescriptionController@updatedescription');
        Route::delete('/descriptiondeleteform/{id}', 'DescriptionController@deletedescription');

        //Birth.......

        // Route::get('/qwerty', 'BirthcertificationController@index');
        Route::get('/cover_ajax', 'BirthcertificationController@cover_ajax');
        Route::post('bank/search', ['as' => 'search-posts', 'uses' => 'BirthcertificationController@search']);
        Route::get('/householdsearch', 'BirthcertificationController@index');
        Route::post('household/search', ['as' => 'search-house', 'uses' => 'BirthcertificationController@search']);
        Route::get('/insertbirthcertification/{household_id}', 'BirthcertificationController@createbirth');
        Route::post('/storebirthcertification', 'BirthcertificationController@storebirth');

        //Death.......
        Route::get('/person_ajax', 'BirthcertificationController@person_ajax');
        Route::get('/personsearch', 'BirthcertificationController@view');
        Route::post('person/search', ['as' => 'search-person', 'uses' => 'BirthcertificationController@person']);
        Route::get('/insertdeath/{id}/{hI}', 'BirthcertificationController@createdeath');
        Route::post('/storedeath/{id}/{hI}', 'BirthcertificationController@storedeath');

        Route::post('/personfilter', ['as' => 'filter-search', 'uses' => 'BirthcertificationController@filterSearch']);

        Route::get('/personsdetail/{id}', 'BirthcertificationController@personsdetail');

        Route::post('/getDetail/{id}',['as'=> 'getDetail', 'uses'=>'BirthcertificationController@getDetail']);

        //Question
    });
    // Route::get('/question', 'QuestionController@index');
});


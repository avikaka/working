<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Crematorium extends Model
{
    protected $table="crematoriums";

    public $timestamps = false;
    public static function getcrematorium($ward=null){
        if(is_null($ward) || $ward<1 || $ward>TOTAL_WARD || !is_numeric($ward)){         
            return self::all();
        }else{
            // return "Else Part of Playground";
            return self::where('ward','=',$ward)->get();
        }
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absentee extends Model
{
    public function household(){
        return $this->belongsTo(Household::class);
    }

    public static function getAbsentees($ward=null){
        if (getWardFlag($ward) || $ward==0){
            $result = self::selectRaw('C04, count(*) number')->groupBy('C04')->orderBy('C04')->orderBy('C04')->get();
        }else{
            $result = self::selectRaw('C04, count(*) number')->where('WARD','=',$ward)->groupBy('C04')->orderBy('C04')->orderBy('C04')->get();
        }
        // return $result;
        $data['total']=0;
        foreach($result as $row){
            if($row->C04 == 1):
                $data['male']=$row->number;
                $data['total']+=$row->number;
            elseif($row->C04 ==2):
                $data['female']=$row->number;
                $data['total']+=$row->number;
            elseif($row->C04 ==3):
                $data['other']=$row->number;
                $data['total']+=$row->number;
            else:
                $data['not_stated']=$row->number;
                $data['total']+=$row->number;
            endif;
        }
        return $data;
    }
}

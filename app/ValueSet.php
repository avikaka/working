<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValueSet extends Model
{
    // protected $primaryKey = '';
    public function values()
    {
        return $this->hasMany(Value::class);
    }
    public function fields()
    {
        return $this->hasMany(Field::class);
    }
}

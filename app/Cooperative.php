<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cooperative extends Model
{
    public $timestamps = false;

    public static function getCooperative($ward=null){
        if(is_null($ward) || $ward<1 || $ward>TOTAL_WARD || !is_numeric($ward)){
            return self::orderBy('ward')->get();
        }else{
            return self::where('ward','=',$ward)->get();
        }
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use App\Household;

class Person extends Model
{
    //Overriding the default table name
    protected $table = 'persons';
    public $timestamps = false;

    public function household()
    {
        return $this->belongsTo(Household::class);
    }

    public static function getPopulation($ward = null)
    {
        if (getWardFlag($ward) || $ward == 0) {
            $result = self::selectRaw('SEX, count(*) number')->groupBy('SEX')->orderBy('SEX')->orderBy('SEX')->get();
        } else {
            $result = self::selectRaw('SEX, count(*) number')->where('WARD', '=', $ward)->groupBy('SEX')->orderBy('SEX')->orderBy('SEX')->get();
        }
        // return $result;
        $data['total'] = 0;
        foreach ($result as $row) {
            if ($row->SEX == 1) :
                $data['male'] = $row->number;
                $data['total'] += $row->number;
            elseif ($row->SEX == 2) :
                $data['female'] = $row->number;
                $data['total'] += $row->number;
            elseif ($row->SEX == 3) :
                $data['other'] = $row->number;
                $data['total'] += $row->number;
            else :
                $data['not_stated'] = $row->number;
                $data['total'] += $row->number;
            endif;
        }
        return $data;
    }
}

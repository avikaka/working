<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cover extends Model
{
    protected $table = 'cover';
    public $timestamps = false;
    //one to one relationship with
    public function household()
    {
        return $this->belongsTo(Household::class);
    }

    public static function getHousehold($ward = null)
    {
        if (is_null($ward) || $ward < 1 || $ward > TOTAL_WARD) {
            return self::selectRaw('COUNT(*) AS number')->whereIn('USE_OF_HOUSEHOLD', [1, 2])->first();
        } else {
            return self::selectRaw('COUNT(*) AS number')->where('WARD', '=', is_null($ward) ? 0 : $ward)->whereIn('USE_OF_HOUSEHOLD', [1, 2])->first();
        }
    }

    public static function getLocation()
    {
        $data = self::selectRaw('household_id, RESPONDENT AS name, WARD as ward, LATITUDE AS lat, LONGITUDE AS lon')->where('ward', '=', 5)->limit(6)->get();
        return $data;
    }
}

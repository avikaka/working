<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use App\Logo;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('components.card', 'card');
        Schema::defaultStringLength(191);
        if (file_exists("upload/json/logo_file.json")) {
            $test = json_decode(file_get_contents("upload/json/logo_file.json"), true);
            $logo = $test['0'];
            // dd($logo['image']);

            view()->share('logoimage', $logo['image']);
            view()->share('logoname', $logo['name']);
        }
    }
}

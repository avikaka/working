<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicPlace extends Model
{
    public $timestamps = false;

    public static function getPublicPlace($ward = null)
    {
        if (is_null($ward) || $ward < 1 || $ward > TOTAL_WARD || !is_numeric($ward)) {
            // return "Ward No : ".$ward;
            // return "True Playground";
            return self::all();
        } else {
            // return "Else Part of Playground";
            return self::where('ward', '=', $ward)->get();
        }
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndividualInformation extends Model
{
    protected $table = "cover";
    public $timestamps = false;
}
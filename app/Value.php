<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ValueSet;

class Value extends Model
{
    public function valueSet()
    {
        return $this->belongsTo(ValueSet::class);
    }
}

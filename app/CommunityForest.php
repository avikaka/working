<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommunityForest extends Model
{

    protected $table = 'community_forests';


    // protected $fillable = ['name', 'ward', 'forest_area', 'occupied_area', 'member_household', 'latitude', 'longitude'];
    // protected $guarded = ['null'];
}

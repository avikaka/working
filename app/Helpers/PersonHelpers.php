<?php

use Illuminate\Support\Facades\DB;
use App\Person;
use App\Field;

define('TOTAL_WARD', 5);

// define('publicToilet',json_decode(file_get_contents("upload/json/roadnetworkdetails_file.json"), true));

function itemsCount($file_name){
    $getItems = json_decode(file_get_contents("upload/json/$file_name.json"), true);
    if(count($getItems)==0){
        return '-';
    }else{
        return count($getItems);
    };
}

function getItems($file_name){
    $getItems = json_decode(file_get_contents("upload/json/$file_name.json"), true);
    if($getItems){
        return $getItems;
    }else{
        return '-';
    };
}

function geoJson($data,$fileName){
    $college = $data::all();

    $emptyArray = '';

    foreach($college as $row){
        $features = "{
                'type': 'Feature',
                'properties': {
                    'name' : '". $row->name ."',
                    'address' : '". $row->address ."'
                },
                'geometry': {
                'type': 'Point',
                'coordinates': [" . $row->longitude . ", " . $row->latitude ."]
                }
            },";

            $emptyArray .= $features;
            }

            $alternative =
            "{
                'type': 'FeatureCollection',
                'features': [
                ${emptyArray}
                ]
            }";

        $file = 'geo_'. $fileName .'' . '_file.json';
        $destinationPath = public_path() . "/upload/geo/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $alternative);
    }

$not_stated = null;
function getAgeGroup($value = null, $lang = 'np')
{
    $ageGroups = array(
        1 => ['en' => '0 to 4', 'ne' => "० देखि ४"],
        2 => ['en' => '5 to 9', 'ne' => "५ देखि 9"],
        3 => ['en' => '10 to 14', 'ne' => "१० देखि 14"],
        4 => ['en' => '15 to 19', 'ne' => "१५ देखि 19"],
        5 => ['en' => '20 to 24', 'ne' => "२० देखि 24"],
        6 => ['en' => '25 to 29', 'ne' => "२५ देखि 29"],
        7 => ['en' => '30 to 34', 'ne' => "३० देखि 34"],
        8 => ['en' => '35 to 39', 'ne' => "३५ देखि 39"],
        9 => ['en' => '40 to 44', 'ne' => "४० देखि 44"],
        10 => ['en' => '45 to 49', 'ne' => "४५ देखि 49"],
        11 => ['en' => '50 to 54', 'ne' => "५० देखि 54"],
        12 => ['en' => '55 to 59', 'ne' => "५५ देखि 59"],
        13 => ['en' => '60 to 64', 'ne' => "६० देखि 64"],
        14 => ['en' => '65 to 69', 'ne' => "६५ देखि 69"],
        15 => ['en' => '70 to 74', 'ne' => "७० देखि 74"],
        16 => ['en' => '75 to 79', 'ne' => "७५ देखि 79"],
        17 => ['en' => '80 to 84', 'ne' => "८० देखि 84"],
        18 => ['en' => '85 and above', 'ne' => "८५ र भन्दा माथि"],
        0 => ['en' => "Not stated", 'ne' => 'उल्लेख नगरिएको']
    );
    return count($ageGroups);

    if (is_null($value))
        // return $ageGroup['ne'];

        return  $ageGroups[$value][$lang];
}

// function validateSex($sex=null){
//     if (is_null($sex) || !$sex){
//         return $sex;
//     }elseif(is_numeric($sex) && ($sex >=1 && $sex <= 3)){
//         return $sex;
//     }else{
//         $tSex = lcasee(trim($sex));
//         if ($tSex=="male"){
//             return 1;
//         }
//         elseif ($tSex=="female"){
//             return 2;
//         }elseif($tSex=="other" || $tSex == "third gender"){
//             return 3;
//         }else{
//             return null;
//         }
//     }
// }
/*
    return age_group-wise, ward-wise population and group by sex
    parameters:
    -- $ward = numeric; optional; default value is null which means all wards
                if ward is beyond its value or null returns all wards data
    -- $age_group = numeric; optional; default value is null,
                if $age_group is beyond its value or null returns all age group data
    -- $sex = boolean, optional; default is false
                if true is passed then sex-wise data also
*/
function getByAgeGroup($ward = null, $age_group = null, $sex = false)
{
    //Find all the age_group which have value
    $distinct = Person::select('age_group')->distinct()->orderBy('age_group')->get();
    $persons = null;

    // $sex = validateSex($sex);
    // if $sex parameter is suppied with valid value i.e. 1 to 3
    if ($sex) {
        if ($age_group > 18 or $age_group < 0 || is_null($age_group)) :
            if (getWardFlag($ward)) {
                $persons = Person::selectRaw('`WARD` as ward, age_group, SEX sex, count(*) as number')
                    ->groupBy('ward', 'age_group', 'sex')
                    ->orderByRaw('ward, age_group, sex')
                    ->get();
            } else {
                $persons = Person::selectRaw('`WARD` as ward, age_group, SEX sex, count(*) as number')
                    ->where('ward', '=', $ward)
                    ->groupBy('ward', 'age_group', 'sex')
                    ->orderByRaw('ward, age_group, sex')
                    ->get();
            }
        else :
            if (getWardFlag($ward)) {
                $persons = Person::selectRaw('`WARD` as ward, age_group, SEX sex, count(*) as number')
                    ->where('age_group', '=', $age_group)
                    ->groupBy('ward', 'age_group', 'sex')
                    ->orderByRaw('ward, age_group, sex')
                    ->get();
            } else {
                $persons = Person::selectRaw('`WARD` as ward, age_group, SEX sex, count(*) as number')
                    ->where('age_group', '=', $age_group)->where('ward', '=', $ward)
                    ->groupBy('ward', 'age_group', 'sex')
                    ->orderByRaw('ward, age_group, sex')
                    ->get();
            }
        endif; //age_group

    } else { //else part of sex
        if ($age_group > 18 || $age_group < 0 || is_null($age_group)) :
            if (getWardFlag($ward)) {
                $persons = Person::selectRaw('`WARD` as ward, age_group, count(*) as number')
                    ->groupBy('ward', 'age_group')
                    ->orderByRaw('ward, age_group')
                    ->get();
            } else {
                $persons = Person::selectRaw('`WARD` as ward, age_group, count(*) as number')
                    ->where('ward', '=', $ward)
                    ->groupBy('ward', 'age_group')
                    ->orderByRaw('ward, age_group')
                    ->get();
            }
        else : //$age_group else part
            if (getWardFlag($ward)) {
                $persons = Person::selectRaw('`WARD` as ward, age_group, count(*) as number')
                    ->where('age_group', '=', $age_group)
                    ->groupBy('ward', 'age_group')
                    ->orderByRaw('ward, age_group')
                    ->get();
            } else {
                $persons = Person::selectRaw('`WARD` as ward, age_group, count(*) as number')
                    ->where('age_group', '=', $age_group)->where('ward', '=', $ward)
                    ->groupBy('ward', 'age_group')
                    ->orderByRaw('ward, age_group')
                    ->get();
            }
        endif; //age_group
    } //endif sex

    //if valid ward is supplied then processed the
    if (getWardFlag($ward)) {
        foreach ($distinct as $d) {
            $data['age_group'] = $d->age_group;
            for ($i = 1; $i <= TOTAL_WARD; $i++) {
                // $a['ward'] = $i;
                foreach ($persons as $row) {
                    if ($d->age_group == $row->age_group) {
                        if ($i == $row->ward) {
                            $data['ward' . $i] = $row->number;
                        }
                    } else {
                        // $data['ward'.$i] = 0;
                    }
                }
                // $data['data'][]=$a;
            }
            $table[] = $data;
        }

        foreach ($table as $row) {
            $new_data['age_group'] = $row['age_group'];
            for ($i = 1; $i <= TOTAL_WARD; $i++) {
                if (isset($row['ward' . $i])) {
                    $new_data['ward' . $i] = $row['ward' . $i];
                } else {
                    $new_data['ward' . $i] = 0;
                }
            }
            $table2[] = $new_data;
        }
    } else { //wardFlag -- ward is not supplied or invalid
        foreach ($persons as $row) {
            $data['age_group'] = $row->age_group;
            $data['number'] = $row->number;
            $table2[] = $data;
        }
    }
    // dd($persons);
    // return $table2;
    return $persons;
}
function generateAgeGroupTable()
{
}

function createDataFolder()
{
    if (!file_exists('Data')) {
        mkdir('Data');
    }
}

//get field values name
/*
    getFieldValues function returns an array of two keys namely 'values' with values of the given field provided by $field_name argument of the given table provided by $table_name and 'title'. Both the values and title also are arrays having corresponding values and title in both of the arrays.
*/
function getFieldValues($table_name, $field_name, $title_language = 'NE', $col_list = array('Value', 'label_EN', 'label_NE'))
{
    $rows = Field::where([['table_name', '=', $table_name], ['field_name', '=', $field_name]])->first()
        ->valueSet->values()->select($col_list)->get()->toArray();
    // dd($rows);
    // $i = 1;
    foreach ($rows as $row) :
        $values[$row['Value']] = strtolower($row['label_EN']);

        $title[$values[$row['Value']]] = $row['label_' . $title_language];
        $i = $row['Value'] + 1;
    endforeach;
    $values[$i] = "not_stated";
    $title['not_stated'] = $title_language == 'NE' ? "उल्लेख नगरिएको" : 'Not Stated';
    $GLOBALS['not_stated'] = $i;
    //    dd($values);
    return array('values' => $values, 'title' => $title);
}
/*
    getFieldName function returns
*/
function getFieldName($table_name, $field_name, $lang = 'EN')
{
    // $field='null';
    $field_order = DB::table($table_name)->select($field_name)->groupBy($field_name)->orderByRaw('COUNT(*) DESC')->get()->toArray();

    // foreach($field_order as $fo){
    //     echo is_null($fo->RELIGION)?'[null], ':$fo->RELIGION.', ';
    // }
    $fv = getFieldValues($table_name, $field_name, $lang);
    // return $field_order;
    // return array ('field_order' => $field_order, 'values' => $fv['values'], 'title' =>$fv['title']);
    foreach ($field_order as $fo) :
        foreach ($fv['values'] as $fv_key => $fv_value) {
            if ($fo->$field_name == $fv_key) :
                $field[$fv_value] = $fo->$field_name;
                break;
            endif;
        }
    endforeach;
    return $field;
}

/*********************************************
 * Returns Ward-wise $field_name total population data from the given $table_name
 * if the argument $with_total is true then it return with the total values in the last row
 * if valid $ward_no is supplied then return the specified ward data only.
 * $condition is supplied the field_name data is retrieved based on the condition
 * $caption_lang is the language that the label want to be displayed 'EN' or 'NE'

 ************************************************/
function getTabularReport($table_name, $field_name, $condition = null, $with_total = true, $ward_no = null, $without_notstated = true, $caption_lang = 'NE')
{
    $ward_flag = getWardFlag($ward_no);
    // dd($table_name);
    if ($table_name == "persons") {
        if ($ward_flag) {
            // $result = Person::selectRaw("WARD, $field_name, COUNT(*) AS `number`")->groupBy('WARD',$field_name)->orderBy('WARD')->orderBy($field_name)->get()->toArray();
            if (is_null($condition)) {
                $result = DB::table($table_name)->select(DB::raw("WARD, $field_name, COUNT(*) AS `number`"))->where('death', '=', 0)->groupBy('WARD', $field_name)->orderBy('WARD')->orderBy($field_name)->get();
            } else {
                $result = DB::table($table_name)->select(DB::raw("WARD, $field_name, COUNT(*) AS `number`"))->where('death', '=', 0)->whereRaw($condition)->groupBy('WARD', $field_name)->orderBy('WARD')->orderBy($field_name)->get();
            }
            // return gettype($result);
        } else {
            if (is_null($condition)) { //addition $condition is null
                $result = DB::table($table_name)->select(DB::raw("`WARD`, $field_name, COUNT(*) AS `number`"))->where('death', '=', 0)->where('WARD', '=', $ward_no)->groupBy('WARD', $field_name)->orderBy('WARD')->orderBy($field_name)->get();
            } else { //additional $condition is not null
                $result = DB::table($table_name)->select(DB::raw("`WARD`, $field_name, COUNT(*) AS `number`"))->where('death', '=', 0)->whereRaw("WARD={$ward_no} AND {$condition}")->groupBy('WARD', $field_name)->orderBy('WARD')->orderBy($field_name)->get();
            }
        }
    } else {
        if ($ward_flag) {
            // $result = Person::selectRaw("WARD, $field_name, COUNT(*) AS `number`")->groupBy('WARD',$field_name)->orderBy('WARD')->orderBy($field_name)->get()->toArray();
            if (is_null($condition)) {
                $result = DB::table($table_name)->select(DB::raw("WARD, $field_name, COUNT(*) AS `number`"))->groupBy('WARD', $field_name)->orderBy('WARD')->orderBy($field_name)->get();
            } else {
                $result = DB::table($table_name)->select(DB::raw("WARD, $field_name, COUNT(*) AS `number`"))->whereRaw($condition)->groupBy('WARD', $field_name)->orderBy('WARD')->orderBy($field_name)->get();
            }
            // return gettype($result);
        } else {
            if (is_null($condition)) { //addition $condition is null
                $result = DB::table($table_name)->select(DB::raw("`WARD`, $field_name, COUNT(*) AS `number`"))->where('WARD', '=', $ward_no)->groupBy('WARD', $field_name)->orderBy('WARD')->orderBy($field_name)->get();
            } else { //additional $condition is not null
                $result = DB::table($table_name)->select(DB::raw("`WARD`, $field_name, COUNT(*) AS `number`"))->whereRaw("WARD={$ward_no} AND {$condition}")->groupBy('WARD', $field_name)->orderBy('WARD')->orderBy($field_name)->get();
            }
        }
    }

    // return $result;
    $field_meta = getFieldValues($table_name, $field_name);
    foreach ($field_meta['title'] as $key => $values) {
        $title_key[$key] = 0;
    }
    // dd($field_meta);
    // dd($result);
    // // $result = (array) $result;
    //iterating throught ward 1 to TotalWard
    if ($ward_flag) {
        $count = count($field_meta['values']);
        for ($i = 1; $i <= TOTAL_WARD; $i++) {
            $data['total'] = 0;
            foreach ($result as $value) {
                if ($i == $value->WARD) {
                    $data['ward'] = $value->WARD;

                    if ($without_notstated) {
                        $value->$field_name =  $value->$field_name;
                        if (is_null($value->$field_name)) {
                            unset($value->number);
                            $value->number = 0;
                        } else {
                            // dd($value->$field_name);
                            $data[$field_meta['values'][$value->$field_name]] = $value->number;
                            $test = $value->number;
                        }
                    } else {
                        // dd('with');

                        $value->$field_name = is_null($value->$field_name) ? $GLOBALS['not_stated'] : $value->$field_name;

                        $data[$field_meta['values'][$value->$field_name]] = $value->number;
                    }

                    if ($with_total) {
                        $data['total'] += $value->number;
                    }

                    // dd($data);
                }
                // dd($data);
                $ward['ward_' . $i] = $data;
            } //endforeach

            if ($without_notstated) {
                foreach ($ward['ward_' . $i] as $key => $value) {
                    unset($ward['ward_' . $i]['not_stated']);
                }
                array_pop($title_key);
            }

            // dd($ward['ward_' . $i]);

            $final_data[] = array_merge($title_key, $ward['ward_' . $i]);

            // dd($ward);
        }

        //addition of every different array of ward data
        $columadd = array();
        foreach ($final_data as $value) {
            foreach ($value as $key => $secondValue) {
                if (!isset($columadd[$key])) {
                    $columadd[$key] = 0;
                }
                $columadd[$key] += $secondValue;
            }
        }

        //    removing the title keys if there is 0 in total of colunm data and removing ward total

        foreach ($columadd as $key => $value) {
            unset($columadd['ward']);
            if ($value == 0) {

                unset($columadd[$key]);
                unset($field_meta['title'][$key]);
                // dd('im here');
            }
        }

        // calculating percentage
        $per = array();

        foreach ($columadd as $key => $secondValue) {
            if (!isset($per[$key])) {
                $per[$key] = 0;
            }
            // dd($columadd);
            $per[$key] = round($secondValue / $columadd['total'] * 100, 2);
        }

        //    dd($per);

        $test = array();
        $counttitle = count($field_meta['title']);
        foreach ($field_meta['title'] as $tkey => $tvalue) {

            foreach ($columadd as $key => $value) {
                if ($tkey == $key) {
                    $test[] = array(
                        "title" => $tvalue,
                        "total" => $value
                    );
                }
            }
        }
        array_pop($test);
        if ($without_notstated) {
            array_pop($field_meta['title']);
        }

        // dd($field_meta['title']);
    } else {
        foreach ($result as $value) {
            $data['ward'] = $value->WARD;
            $value->$field_name = is_null($value->$field_name) ? 'not_stated' : $value->$field_name;
            $data[$field_meta['values'][$value->$field_name]] = $value->number;
            $ward['ward_' . $ward_no] = $data;
        }
    }
    // dd(($final_data));
    return array('data' => $ward, 'title' => $field_meta['title'], 'final_data' => $final_data, 'total' => $columadd, 'percentage' => $per, 'pie' => $test);
}

//get whether the ward parameter is supplied or not, if supplied the ward is in range within the municipality ward.
function getWardFlag($ward_no)
{
    if (is_null($ward_no) || !is_numeric($ward_no) || ($ward_no < 1 || $ward_no > TOTAL_WARD))
        return true;
    else
        return false;
}

function getTabularIndex($table_name, $field_name, $with_total = true, $ward_no = null, $caption_lang = 'NE')
{
    $ward_flag = getWardFlag($ward_no);

    if ($ward_flag) {
        $result = DB::table($table_name)->select(DB::raw("WARD, $field_name, COUNT(*) AS `number`"))->groupBy('WARD', $field_name)->orderBy('WARD')->orderBy($field_name)->get();
    } else {
        $result = DB::table($table_name)->select(DB::raw("`WARD`, $field_name, COUNT(*) AS `number`"))->where('WARD', '=', $ward_no)->groupBy('WARD', $field_name)->orderBy('WARD')->orderBy($field_name)->get();
    }

    $field_meta = getFieldValues($table_name, $field_name);


    // $field_order = DB::table($table_name)->select($field_name)->groupBy($field_name)->orderByRaw('COUNT(*) DESC')->get();
    // return $result->sum('number');
    // list($val, $head) = $field_meta;
    // dd($field_meta['values']);
    //SELECT p.`CASTE` FROM persons AS p GROUP BY p.`CASTE` ORDER BY COUNT(*) DESC;
    // $result = (array) $result;
    //iterating throught ward 1 to TotalWard
    if ($ward_flag) {
        for ($i = 1; $i <= TOTAL_WARD; $i++) {
            $data['total'] = 0;
            foreach ($result as $value) {
                if ($i == $value->WARD) {
                    $data['ward'] = $value->WARD;
                    $value->$field_name = is_null($value->$field_name) ? null : $value->$field_name;
                    $data[$field_meta['values'][$value->$field_name]] = $value->number;
                    if ($with_total) {
                        $data['total'] += $value->number;
                    }
                }
                $ward['ward_' . $i] = $data;
            }
        }
    } else {
        foreach ($result as $value) {
            $data['ward'] = $value->WARD;
            $value->$field_name = is_null($value->$field_name) ? $GLOBALS['not_stated'] : $value->$field_name;

            $data[$field_meta['values'][$value->$field_name]] = $value->number;
            $ward['ward_' . $ward_no] = $data;
        }
    }
    return array('data' => $ward, 'title' => $field_meta['title']);
}

/*  It generates a Select html element with all wards number
    Arguments
    $element_name = optional parameters - name and id attribute of the html select element
            default value is `ward`
    $class = optional parameter class for the select element
    $selected_ward = optional parameter - if supplied with numeric value within the range of the ward the
*/
function getWardCombo($selected_ward = null, $element_name = "ward", $class = "form-control")
{
    $html_element = "<select class=\"$class\" id=\"$element_name\" name=\"$element_name\">";
    $html_element .= "<option value=\"\"selected=\"selected\">वडा छान्नुहोस्</option>";
    if (getWardFlag($selected_ward)) {
        for ($i = 1; $i <= TOTAL_WARD; $i++) {
            $html_element .= "<option value=\"$i\">वडा $i</option>";
        }
    } else {
        for ($i = 1; $i <= TOTAL_WARD; $i++) {
            if ($selected_ward == $i) {
                $html_element .= "<option value=\"$i\" selected=\"selected\">वडा $i</option>";
            }
            $html_element .= "<option value=\"$i\">वडा $i</option>";
        }
    }
    $html_element .= "</select>";
    return $html_element;
}

function getCaste($selected_ward = null, $element_name = "caste", $class = "form-control")
{
    $html_element = "<select class=\"$class\" id=\"$element_name\" name=\"$element_name\">";
    $html_element .= "<option value=\"\"selected=\"selected\">जात छान्नुहोस्</option>";
    $shipments = json_decode(file_get_contents("upload/json/caste_file.json"), true);
    $title = $shipments['title'];
    $keys = array_values($title);
    array_unshift($keys,"");
    unset($keys[0]);
    // dd(count($keys));
    // if (getWardFlag($selected_ward)) {
        for ($i = 1; $i <= count($keys); $i++) {
            $html_element .= "<option value=\"$i\">$keys[$i]</option>";
        }
    // }
    // else {
    //     for ($i = 1; $i <= TOTAL_WARD; $i++) {
    //         if ($selected_ward == $i) {
    //             $html_element .= "<option value=\"$i\" selected=\"selected\">वडा $i</option>";
    //         }
    //         $html_element .= "<option value=\"$i\">वडा $i</option>";
    //     }
    // }
    $html_element .= "</select>";
    return $html_element;
}


function getReligion($selected_ward = null, $element_name = "religion", $class = "form-control")
{
    $html_element = "<select class=\"$class\" id=\"$element_name\" name=\"$element_name\">";
    $html_element .= "<option value=\"\"selected=\"selected\">धर्म छान्नुहोस्</option>";
    $shipments = json_decode(file_get_contents("upload/json/religion_file.json"), true);
    $title = $shipments['title'];
    $keys = array_values($title);
    array_unshift($keys,"");
    unset($keys[0]);
    // dd(($keys));
    // if (getWardFlag($selected_ward)) {
        for ($i = 1; $i <= count($keys); $i++) {
            $html_element .= "<option value=\"$i\">$keys[$i]</option>";
        }
    // }
    // else {
    //     for ($i = 1; $i <= TOTAL_WARD; $i++) {
    //         if ($selected_ward == $i) {
    //             $html_element .= "<option value=\"$i\" selected=\"selected\">वडा $i</option>";
    //         }
    //         $html_element .= "<option value=\"$i\">वडा $i</option>";
    //     }
    // }
    $html_element .= "</select>";
    return $html_element;
}

/* <label class="col-lg-3 col-sm-4 col-form-label" for="name">विद्यालयको नाम</label>
    <div class="col-lg-9 col-sm-8">
    <input type="text" id="name" class="form-control" name="name" />
    </div>
*/

function getEducation($selected_ward = null, $element_name = "education", $class = "form-control")
{
    $html_element = "<select class=\"$class\" id=\"$element_name\" name=\"$element_name\">";
    $html_element .= "<option value=\"\"selected=\"selected\">शिक्षा स्तर छान्नुहोस्</option>";
    $shipments = json_decode(file_get_contents("upload/json/educationlevel_file.json"), true);
    $title = $shipments['title'];
    $keys = array_values($title);
    array_unshift($keys,"");
    unset($keys[0]);
    // dd(count($keys));
    // if (getWardFlag($selected_ward)) {
        for ($i = 1; $i <= count($keys); $i++) {
            $html_element .= "<option value=\"$i\">$keys[$i]</option>";
        }
    // }
    // else {
    //     for ($i = 1; $i <= TOTAL_WARD; $i++) {
    //         if ($selected_ward == $i) {
    //             $html_element .= "<option value=\"$i\" selected=\"selected\">वडा $i</option>";
    //         }
    //         $html_element .= "<option value=\"$i\">वडा $i</option>";
    //     }
    // }
    $html_element .= "</select>";
    return $html_element;
}

function getInputField($name = "name", $label = "नाम", $placeholder = "",  $value = "", $div_class = "col-lg-9 col-sm-8", $input_class = "form-control", $label_class = "col-lg-3 col-sm-4 col-form-label")
{
    $html = "<label class=\"$label_class\" for=\"$name\">$label</label>
    <div class=\"col-lg-9 col-sm-8\">
    <input type=\"text\" id=\"$name\" name=\"$name\" value=\"$value\" class=\"$input_class\"";
    $placeholder = $placeholder == "" ? "" : "placeholder=\"$placeholder\"";
    $html .= $placeholder . " required/> </div>";
    return $html;
}


function getlatlong($name = "name", $type = "type", $label = "नाम", $placeholder = "",  $value = "", $div_class = "col-lg-9 col-sm-8", $input_class = "form-control", $label_class = "col-lg-3 col-sm-4 col-form-label")
{
    $html = "<label class=\"$label_class\" for=\"$name\">$label</label>
    <div class=\"col-lg-9 col-sm-8\">
    <input type=\"text\" id=\"$type\" name=\"$name\" value=\"$value\" class=\"$input_class\"";
    $placeholder = $placeholder == "" ? "" : "placeholder=\"$placeholder\"";
    $html .= $placeholder . " required/> </div>";
    return $html;
}


function getfieldinput($name = "name", $label = "नाम", $placeholder = "",  $value = "", $div_class = "col-lg-9 col-sm-8", $input_class = "form-control", $label_class = "col-lg-3 col-sm-4 col-form-label")
{
    $html = "<label class=\"$label_class\" for=\"$name\">$label</label>
    <div class=\"col-lg-9 col-sm-8\">
    <input type=\"text\" id=\"$name\" name=\"$name\" value=\"$value\" class=\"$input_class\"";
    $placeholder = $placeholder == "" ? "" : "placeholder=\"$placeholder\"";
    $html .= $placeholder . " /> </div>";
    return $html;
}

function getLocationField($data)
{
    return `<div class="row">
                          <div class="col-md-6">
                            <div class="form-group row">

                              {!! getInputField('latitude','Latitude','Latitude',isset($data)?$data->latitude:old('latitude')) !!}
                             @error('latitude') <span style="color:red;  margin-left:130px"> {{$message}} </span> @Enderror


                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group row">
                            {!! getInputField('longitude','Longitude','Longitude',isset($data)?$data->longitude:old('longitude')) !!}
                            @error('longitude') <span style="color:red;  margin-left:130px"> {{$message}} </span> @Enderror
                            </div>
                          </div>
                        </div>`;
}


function getPasswordInputField($name = "name", $label = "नाम", $placeholder = "", $div_class = "col-lg-9 col-sm-8", $input_class = "form-control", $label_class = "col-lg-3 col-sm-4 col-form-label")
{
    $html = "<label class=\"$label_class\" for=\"$name\">$label</label>
    <div class=\"col-lg-9 col-sm-8\">
    <input type=\"password\" id=\"$name\" name=\"$name\" class=\"$input_class\"";
    $placeholder = $placeholder == "" ? "" : "placeholder=\"$placeholder\"";
    $html .= $placeholder . "/> </div>";
    return $html;
}

/*
<label class="col-sm-3 col-form-label">Category</label>
    <div class="col-sm-9">
    <select class="form-control">
                 </select>
*/
function getSelectField($name = "name", $label = "नाम", $options = [], $placeholder = "", $selected = null, $div_class = "col-lg-9 col-sm-8", $input_class = "form-control", $label_class = "col-lg-3 col-sm-4 col-form-label")
{

    $html = "<label class=\"$label_class\" for=\"$name\">$label</label>\n";
    $html .= "<div class=\"col-lg-9 col-sm-8\">\n\t";
    $html .= "<select class=\"$input_class\" id=\"$name\" name=\"$name\">\n\t";

    $opt = "";
    if (gettype($options) == 'array') {
        foreach ($options as $key => $value) {
            if (!is_null($selected) && $selected == $key) {
                $opt .= "<option value=\"$value\" selected=\"selected\">$value</option>\n";
            } else {
                $opt .= "<option value=\"$value\">$value</option>\n";
            }
        }
    } elseif (gettype($options) == "string") {
        $opt = "<option value=\"$options\">$options</option>\n";
    }
    $html .= $opt;
    $html .= "</select>\n</div>";
    return $html;
}



/*
<label class="col-sm-3 col-form-label">Membership</label>
    <div class="col-sm-4">
    <div class="form-radio">
        <label class="form-check-label">
        <input type="radio" class="form-check-input" name="membershipRadios" id="membershipRadios1" value="" checked> Free
        </label>
    </div>
    </div>
    <div class="col-sm-5">
    <div class="form-radio">
        <label class="form-check-label">
        <input type="radio" class="form-check-input" name="membershipRadios" id="membershipRadios2" value="option2"> Professional
        </label>
    </div>
    </div>
*/
//SELECT p.`WARD`, p.`SEX`, COUNT(*) number FROM persons p GROUP BY p.`WARD`, p.`SEX`  ORDER BY p.`WARD`, p.`SEX`;
function getPopulation($ward = null)
{
    if (getWardFlag($ward)) {
        $result = Person::selectRaw('SEX, count(*) number')->groupBy('SEX')->orderBy('SEX')->orderBy('SEX')->get();
    } else {
        $result = Person::selectRaw('SEX, count(*) number')->where('WARD', '=', $ward)->groupBy('SEX')->orderBy('SEX')->orderBy('SEX')->get();
    }
    $data['total'] = 0;
    foreach ($result as $row) {
        if ($row->SEX == 1) :
            $data['male'] = $row->number;
            $data['total'] += $row->number;
        elseif ($row->SEX == 2) :
            $data['female'] = $row->number;
            $data['total'] += $row->number;
        elseif ($row->SEX == 3) :
            $data['other'] = $row->number;
            $data['total'] += $row->number;
        else :
            $data['not_stated'] = $row->number;
            $data['total'] += $row->number;
        endif;
    }
    return $data;
}


function getArea($ward = null)
{
    if (getWardFlag($ward)) {
        $result = Person::selectRaw('SEX, count(*) number')->groupBy('SEX')->orderBy('SEX')->orderBy('SEX')->get();
    } else {
        $result = Person::selectRaw('SEX, count(*) number')->where('WARD', '=', $ward)->groupBy('SEX')->orderBy('SEX')->orderBy('SEX')->get();
    }
}
function getLiteracy($ward = null)
{
    if (getWardFlag($ward)) {
        $result = Person::selectRaw('WARD, SEX, count(*) number')->groupBy('WARD', 'SEX')->orderBy('WARD')->orderBy('SEX')->get();
    } else {
        $result = Person::selectRaw('SEX, count(*) number')->where('WARD', '=', $ward)->groupBy('SEX')->orderBy('SEX')->orderBy('SEX')->get();
    }
    return $result;
}

function updateWardData()
{
    $data = DB::select('SELECT c.`WARD`, COUNT(*) AS number FROM household h JOIN cover c ON h.id=c.`household_id` WHERE c.`USE_OF_HOUSEHOLD` IN (1,2) GROUP BY c.WARD;');
    $total_affected = 0;
    $total_household = 0;
    foreach ($data as $row) {
        $affected = DB::table('wards')->where('ward', $row->WARD)->update(['households' => $row->number]);
        $total_affected += $affected;
        $total_household += $row->number;
    }
    $affected = DB::table('wards')->where('ward', 0)->update(['households' => $total_household]);
    return $total_affected + $affected;
}

<?php

use Illuminate\Support\Facades\DB;
// use App\Person;

function selectForm($question = "", $value = "", $option = [], $dclass = "browser-default custom-select form-control col-6")
{
    $values = array('cat', 'dog', 'mouse');

    // foreach ($values as $vall) {
    $select = "<label for=\"question\" class=\"form-label-lg mt-4 col-12\"><h3>$question</h3></label>";
    $select .= "<select class=\"$dclass\"><option value=\"$value\">$option</option></select>";
    $html = "<div class=\"form-group mb-3\">$select</div>";
    // }
    // dd($html);
    return $html;
}

function checkBox($question, $value, $option = [])
{
    $select = "<label for=\"question\" class=\"form-label-lg mt-4 col-12\"><h3>$question</h3></label>";
    $input = "<input class=\"form-check-input ml-2\" type=\"checkbox\" value=\"$value\">";
    $input .= "<label class=\"form-check-label ml-3\">$option</label>";
    // dd($option);
    $div = "<fieldset class=\"form-group\"><div class =\"row\"><div class=\"col-6 form-check\">$input</div></div></fieldset>";
    $html = "<div class=\"form-group mb-3\">$select $div</div>";
    // dd($html);
    return $html;
}


// function radioBtn($question, $value)
// {
//     $option = array("cat", "dog", "mouse");
//     // dd(count($option));

//     $select = "<label for=\"question\" class=\"form-label-lg mt-4 col-12\"><h3>$question</h3></label>";
//     for ($i = 0; $i < count($option); $i++) {

//         // dd($option[$i]);
//         $input = "<div class=\"col-6 form-check\"><input class=\"form-check-input ml-2\" type=\"radio\" value=\"$value\">";
//         $input .= "<label class=\"form-check-label ml-3\">$option[$i]</label></div>";
//         // dd($html);
//     }
//     $div = "<fieldset class=\"form-group\"><div class =\"row\">$input</div></fieldset>";
//     $html = "<div class=\"form-group mb-3\">$select $div</div>";
//     echo ($html);
// }

function radioBtn($question = "", $value)
{
    $option = array("a", "b", "c");
    // $question = DB::select('SELECT Question FROM `fields` GROUP BY Question; ');
    // dd($question[2]);
    // foreach ($question as $key => $valuee) {
    //     echo "$key: $valuee";
    // }
    for ($i = 0; $i < count($option); $i++) {
        $qn = "<label for=\"question\" class=\"form-label-lg mt-4 col-12\"><h3>$question</h3></label>";
        $input = "<fieldset class=\"form-group\"><div class =\"row\"><div class=\"col-6 form-check\"><input class=\"form-check-input ml-2\" type=\"radio\" value=\"$value\">";
        $input .= "<label class=\"form-check-label ml-3\">$option[$i]</label></div></div></fieldset>";
        $html = "<div class=\"form-group mb-3\">$qn $input</div>";
        echo $html;
        // dd($question);
    }
}

function getWard($selected_ward = null, $element_name = "ward", $class = "form-control")
{
    $html_element = "<select class=\"$class\" id=\"$element_name\" name=\"$element_name\">";
    $html_element .= "<option selected=\"selected\">वडा छान्नुहोस्</option>";
    if (getWardFlag($selected_ward)) {
        for ($i = 1; $i <= TOTAL_WARD; $i++) {
            $html_element .= "<option value=\"$i\">वडा $i</option>";
        }
    } else {
        for ($i = 1; $i <= TOTAL_WARD; $i++) {
            if ($selected_ward == $i) {
                $html_element .= "<option value=\"$i\" selected=\"selected\">वडा $i</option>";
            }
            $html_element .= "<option value=\"$i\">वडा $i</option>";
        }
    }
    $html_element .= "</select>";
    return $html_element;
}

function inputField($name = "name", $label = "नाम", $placeholder = "",  $value = "", $div_class = "col-lg-9 col-sm-8", $input_class = "form-control")
{
    $html = "<div class=\"form-group\"><label class=\"control-label\" for=\"$name\">$label</label>
             <input type=\"text\" id=\"$name\" name=\"$name\" value=\"$value\" class=\"form-control\"";
    $placeholder = $placeholder == "" ? "" : "placeholder=\"$placeholder\"";
    $html .= $placeholder . "/></div>";
    // dd($html);
    return $html;
}

function latField($name = "", $label = "", $placeholder = "",  $value = "",$id="", $div_class = "col-lg-9 col-sm-8", $input_class = "form-control")
{
    $html = "<div class=\"form-group\"><label class=\"control-label\" for=\"$name\">$label</label>
             <input type=\"text\" id=\"$id\" name=\"$name\" value=\"$value\" class=\"form-control\"";
    $placeholder = $placeholder == "" ? "" : "placeholder=\"$placeholder\"";
    $html .= $placeholder . "/></div>";
    // dd($html);
    return $html;
}
// function imageField()

function selectField($name = "name", $label = "नाम", $options = [], $placeholder = "", $selected = null, $div_class = "col-lg-9 col-sm-8", $input_class = "form-control", $label_class = "col-lg-3 col-sm-4 col-form-label")
{
    $html = "<div class=\"form-group\"><label class=\"control-label\" for=\"$name\">$label</label>\n";
    // $html .= "<div class=\"col-lg-9 col-sm-8\">\n\t";

    $html .= "<select class=\"form-control\" data-placeholder=\"छान्नुहोस\" id=\"$name\" name=\"$name\">\n\t";
    $html .= "<option value=\"\"selected=\"selected\">$label छान्नुहोस्</option>";
    $opt = "";
    if (gettype($options) == 'array') {
        foreach ($options as $key => $value) {
            if (!is_null($selected) && $selected == $key) {
                // $opt .= "<option selected=\"selected\">छान्नुहोस्</option>";
                $opt .= "<option value=\"$value\" selected=\"selected\">$value</option>\n";
            } else {
                // $opt .= "<option selected=\"selected\">छान्नुहोस्</option>";
                $opt .= "<option value=\"$value\">$value</option>\n";
            }
        }

    } elseif (gettype($options) == "string") {
        $opt = "<option value=\"$options\">$options</option>\n";
    }

    $html .= $opt;
    $html .= "</select>\n</div>";
    return $html;
}

function getValueLabel($field_name, $value)
{
    if (!is_null($value)) {
        $sql = "SELECT `label_NE` FROM `values` WHERE value_set_id=(SELECT value_set_id FROM fields WHERE field_name='$field_name') and Value=$value";
        $data = DB::select($sql);
        // dd($data);
        return $data[0]->label_NE;
    }
}

function getQuestions()
{
    $sql = "SELECT question, field_name FROM `fields` WHERE table_name = 'household' AND question != ''";
    $data = DB::select($sql);
    return $data;
}
// function getQuestionswithAns($value,$field_name)
// {
//     $value = getValueLabel($field_name, $value);
//     dd($value);
//     return $value;
// }
function getQuestionHtml()
{
    $data = getQuestions();
    // dd($data);
    $html = "";
    foreach ($data as $row) {
        $html .= "<tr>";
        $html .= "<td>$row->question</td> <td>$row->field_name</td>";
        $html .= "</tr>";
    }
    // dd($html);
    return $html;
}

function getQuestionText($table_name, $field_name)
{
    $sql = "SELECT question FROM `fields` WHERE table_name='$table_name' AND field_name='$field_name'";
    $data = DB::select($sql);
    return $data[0]->question;
}

function getQuestionAnswer($table_name, $field_name, $value)
{

    $sql = "SELECT question, field_name, field_type FROM `fields` WHERE table_name='$table_name' AND field_name='$field_name'";
    $data = DB::select($sql);
    $html = "<tr><td>{$data[0]->field_name} {$data[0]->question}</td>";
    if ($data[0]->field_type == 'option' || $data[0]->field_type == 'select') {
        $html .= "<td>" . getValueLabel($field_name, $value) . "</td>";
    } else {
        $html .= "<td>{$value}</td>";
    }
    $html .= '</tr>';

    return $html;
}

function getPersonData($household_id)
{
    $sql = "SELECT * FROM `persons` WHERE household_id = '$household_id'";
    $data = DB::select($sql);
    return $data;
}

if (!function_exists('hasError')) {
    /**
     * Check for the existence of an error message and return a class name
     *
     * @param  string  $key
     * @return string
     */
    function hasError($key)
    {
        $errors = session()->get('errors') ?: new \Illuminate\Support\ViewErrorBag;
        return $errors->has($key) ? 'is-invalid' : '';
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bridge extends Model
{
    
    public $timestamps = false;
    public static function getBridge($ward=null){
        if(is_null($ward) || $ward<1 || $ward>TOTAL_WARD || !is_numeric($ward)){
            return self::all();
        }else{            
            return self::where('ward','=',$ward)->get();
        }
    }
    
}
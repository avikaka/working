<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicToilet extends Model
{
    protected $table = 'public_toilets';
    public $timestamps = false;

    public static function getPublicToilet($ward=null){
        if(is_null($ward) || $ward<1 || $ward>TOTAL_WARD || !is_numeric($ward)){            
            return self::orderBy('ward')->get();
        }else{            
            return self::where('ward','=',$ward)->get();
        }
    }

}

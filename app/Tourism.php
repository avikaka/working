<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tourism extends Model
{
    public $timestamps = false;

    // protected $guarded = [];
    public static function getTourism($ward = null)
    {
        if (is_null($ward) || $ward < 1 || $ward > TOTAL_WARD || !is_numeric($ward)) {
            // return "Ward No : ".$ward;
            // return "True Playground";
            return self::all();
        } else {
            // return "Else Part of Playground";
            return self::where('ward', '=', $ward)->get();
        }
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToleLevelOrganization extends Model
{
    protected $table = "tole_level_organizations";
    public $timestamps = false;

    public static function getTLO($ward=null){
        if(is_null($ward) || $ward<1 || $ward>TOTAL_WARD || !is_numeric($ward)){
            // return "Ward No : ".$ward;
            return self::all();
        }else{
            return self::where('ward','=',$ward)->get();
        }
    }
}
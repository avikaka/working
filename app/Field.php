<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    public function valueSet(){
        return $this->belongsTo(ValueSet::class);
    }
}

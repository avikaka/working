<?php

namespace App\Http\Controllers;

use App\Person;
use App\Household;
use App\Ward;
use App\ValueSet;
use App\Cover;
use App\Absentee;
use App\Playground;
use App\ToleLevelOrganization;
use App\CommunityBuilding;
use App\PublicToilet;
use App\Crematorium;
use App\Cooperative;
use App\AgroFarm;
use App\AgroVetGroup;
use App\Bank;
use App\ChildrenClub;
use App\College;
use App\HealthInstitution;
use App\Religious;
use App\SocialHome;
use App\Tourism;
use App\WomenGroup;
use App\DrinkWaterProject;
use App\Bridge;
use App\RoadNetwork;
use App\CommunityForest;
use App\IrrigationProject;
use App\HaatBazar;
use App\AgroVetPocket;
use App\Club;
use App\PondsLakesWetland;
use App\PublicPlace;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index($ward = null)
    {

        $gender = json_decode(file_get_contents("upload/json/sex_file.json"), true);
        $drinkwater = json_decode(file_get_contents("upload/json/watersource_file.json"), true);
        $firewood = json_decode(file_get_contents("upload/json/cookingfuel_file.json"), true);
        $lifelong = json_decode(file_get_contents("upload/json/lifelong_file.json"), true);
        $vaccination = json_decode(file_get_contents("upload/json/vaccination_file.json"), true);
        $delivery = json_decode(file_get_contents("upload/json/delivery_file.json"), true);
        $absentees = json_decode(file_get_contents("upload/json/asex_file.json"), true);
        $genderbar = $gender['final_data'];
        $gendert = $gender['total'];
        $gendertotal = $gendert['total'];
        $gendertitle = $gender['title'];
        $genderdata = $gender['pie'];
        $maleFemale = array_slice($genderdata, 0, 2);
        $religion = json_decode(file_get_contents("upload/json/religion_file.json"), true);
        $literacy = json_decode(file_get_contents("upload/json/literacy_file.json"), true);
        $literacynumb = $literacy['percentage'];
        $house = json_decode(file_get_contents("upload/json/householdowner_file.json"), true);
        $housenumber = $house['total'];
        $bank = $this->paginate(json_decode(file_get_contents("upload/json/bankdetails_file.json"), true));
        $college = $this->paginate(json_decode(file_get_contents("upload/json/collegedetails_file.json"), true));
        $disease = json_decode(file_get_contents("upload/json/lifelong_file.json"), true);
        // dd($disease);
        $importantcontact = json_decode(file_get_contents("upload/json/importantcontact_file.json"), true);
        $avgfamily = round($gendertotal / $housenumber['total'], 2);
        $carsouel = json_decode(file_get_contents("upload/json/carousel_file.json"), true);
        $agewise = json_decode(file_get_contents("upload/json/agewise_file.json"), true);
        $description = json_decode(file_get_contents("upload/json/description_file.json"), true);
        // dd($agewise);

        // For Dashboard Items Count
        $hospital=json_decode(file_get_contents("upload/json/healthinstutiondetails_file.json"), true);
        $hp = 0;
        $hos = 0;
        $sk = 0;
        $clinic = 0;
        $birthC = 0;
        // $t = -;
        foreach($hospital as $item){
            if($item['type']=="स्वास्थ्य चाैकी"){
                $hp++;
            }elseif($item['type']=="सामान्य अस्पताल" ){
                $hos++;
            }elseif($item['type']=="प्राथमिक स्वास्थ्य केन्द्र"){
                $sk++;
            }elseif($item['type']=='निजी अस्पताल'){
                $clinic++;
            }elseif($item['type']=="केन्द्रीय अस्पताल"){
                $birthC++;
            }
        }

        $school = json_decode(file_get_contents("upload/json/collegedetails_file.json"), true);
        $ad = 0;
        $pra = 0;
        $ma = 0;
        $priv = 0;
        $mad = 0;
        $col = 0;
        $uni = 0;

        foreach($school as $item){
            if($item['type']=="आधारभुत"){
                $ad++;
            }elseif($item['type']=="प्राथमिक" ){
                $pra++;
            }elseif($item['type']=="माध्यमिक"){
                $ma++;
            }elseif($item['type']=='निजी'){
                $priv++;
            }elseif($item['type']=="मदरसा"){
                $mad++;
            }elseif($item['type']=="विश्वविद्यालय"){
                $uni++;
            }elseif($item['type']=="क्याम्पस"){
                $col++;
            }
        }

        $banking = json_decode(file_get_contents("upload/json/bankdetails_file.json"), true);
        $ban = 0;
        $fin = 0;
        foreach($banking as $item){
            if($item['type']=="वाणिज्य" || 'विकास'){
                $ban++;
            }elseif($item['type']=="फाइनान्स" ){
                $fin++;
            }
        }


        return view('dashboard-main', compact('ban','fin','ad','pra','ma','priv','mad','uni','col','birthC','clinic','sk','hos','hp','absentees','delivery','vaccination','lifelong','firewood','drinkwater', 'description', 'agewise', 'maleFemale', 'genderdata', 'gendertotal', 'religion', 'gendertitle', 'genderbar', 'literacy', 'bank', 'college', 'disease', 'importantcontact', 'literacynumb', 'housenumber', 'avgfamily', 'carsouel'));
    }


    public function index_ajax_bank(Request $request)
    {
        // $bank = (json_decode(file_get_contents("upload/json/bankdetails_file.json"), true));
        if ($request->ajax()) {
            $bank = $this->paginate(json_decode(file_get_contents("upload/json/bankdetails_file.json"), true));
            // dd($bank);
            return view('bank_ajax', compact('bank'))->render();
        }
    }

    public function index_ajax_college(Request $request)
    {
        if ($request->ajax()) {
            $college = $this->paginate(json_decode(file_get_contents("upload/json/collegedetails_file.json"), true));
            return view('college_ajax', compact('college'))->render();
        }
    }

    public function paginate($items, $perPage = 5, $page = '', $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function test($ward = null)
    {

        $religion = json_decode(file_get_contents("upload/json/religion_file.json"), true, 512, JSON_UNESCAPED_UNICODE);



        return view('test', compact('religion'));
    }

    public function dashboard($ward = null)
    {
        $ward = is_null($ward) ? 0 : $ward;
        $population = Person::getPopulation($ward);
        // return $population;
        $area = Ward::getArea($ward);
        $population['density'] = number_format($population['total'] / $area->area);
        $population['male_percent'] = number_format($population['male'] / $population['total'] * 100, 2);
        $population['female_percent'] = number_format($population['female'] / $population['total'] * 100, 2);
        $literacy = Ward::getLiteracy($ward);
        $household = Cover::getHousehold($ward);
        $familySize = number_format($population['total'] / $household->number, 2);
        $absentees = Absentee::getAbsentees($ward);
        // return $absentees;
        $playgrounds = Playground::getPlayground($ward);
        $tlos = ToleLevelOrganization::getTLO($ward);
        $cooperatives = Cooperative::getCooperative($ward);
        $public_toilets = PublicToilet::getPublicToilet($ward);
        $comunitybuildings = CommunityBuilding::getCommunityBuilding($ward);
        $agroFarms = AgroFarm::getAgroFarm($ward);
        $religious = Religious::getReligious($ward);
        $tourism = Tourism::getTourism($ward);
        $bank = Bank::getBank($ward);
        $healthInstitution = HealthInstitution::getHealthInstitution($ward);
        $college = College::getCollege($ward);
        $childrenClub = ChildrenClub::getChildrenClub($ward);
        $socialHome = SocialHome::getSocialHome($ward);
        $womenGroup = WomenGroup::getWomenGroup($ward);
        $crematoriums = Crematorium::getcrematorium($ward);
        $drinkwaters = DrinkWaterProject::getDrinkwaterproject($ward);
        // $bridges = Bridge::getBridge($ward);
        // $roadnetworks = RoadNetwork::getRoadNetwork($ward);
        $communityforests = CommunityForest::getCommunityForest($ward);
        $irrigatinprojects = IrrigationProject::getIrrigationProject($ward);
        $agrovetpockets = AgroVetPocket::getAgroVetPocket($ward);
        $pondslakeswetlands = PondsLakesWetland::getPondsLakesWetland($ward);
        $haatbazars = HaatBazar::getHaatBazar($ward);
        $agroVet = AgroVetGroup::getAgroVet($ward);
        $publicPlace = PublicPlace::getPublicPlace($ward);
        $youthClub = Club::getYouthClub($ward);
        $data = array(
            'population' => $population,
            'area' => $area->area,
            'literacy' => $literacy->literacy,
            'household' => $household->number,
            'familySize' => $familySize,
            'absentees' => $absentees,
            'playgrounds' => $playgrounds,
            'tlos' => $tlos,
            'cooperatives' => $cooperatives,
            'publicToilets' => $public_toilets,
            'comunityBuildings' => $comunitybuildings,
            'agroFarms' => $agroFarms,
            'religious' => $religious,
            'tourism' => $tourism,
            'bank' => $bank,
            'healthInstitution' => $healthInstitution,
            'college' => $college,
            'childrenClub' => $childrenClub,
            'socialHome' => $socialHome,
            'crematoriums' => $crematoriums,
            'drinkwaters' => $drinkwaters,
            // 'bridges'=>$bridges,
            // 'roadnetworks'=>$roadnetworks,
            'communityforests' => $communityforests,
            'irrigationprojects' => $irrigatinprojects,
            'pondslakeswetlands' => $pondslakeswetlands,
            'agrovetpockets' => $agrovetpockets,
            'haatbazars' => $haatbazars,
            'womenGroup' => $womenGroup,
            'agroVet' => $agroVet,
            'publicPlace' => $publicPlace,
            'youthClub' => $youthClub
        );
        return $data;
    }

    public function updateWard()
    {
        // return updateWardData();
        return getByAgeGroup(6, 5, true);
    }


    public function homeTest()
    {
        dd(getTabularReport('household', 'A03', null, false));
    }
}

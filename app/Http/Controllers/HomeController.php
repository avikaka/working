<?php

namespace App\Http\Controllers;

use App\Cover;


use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $gender = json_decode(file_get_contents("upload/json/sex_file.json"), true);
        $house = json_decode(file_get_contents("upload/json/householdowner_file.json"), true);
        $absentees = json_decode(file_get_contents("upload/json/asex_file.json"), true);
        $electricity = json_decode(file_get_contents("upload/json/lightsource_file.json"), true);
        $cooking = json_decode(file_get_contents("upload/json/cookingfuel_file.json"), true);
        $housetype = json_decode(file_get_contents("upload/json/housetype_file.json"), true);
        $lifelong = json_decode(file_get_contents("upload/json/lifelong_file.json"), true);
        $vaccination = json_decode(file_get_contents("upload/json/vaccination_file.json"), true);
        $data = Cover::paginate(10);

        // dd($absentees);

        return view('admin.home.home', compact('gender', 'house', 'absentees', 'electricity', 'cooking', 'housetype', 'lifelong', 'vaccination', 'data'));
    }

    public function search(Request $request)
    {

        $search = $request->get('searchReq');
        $tole = $request->get('toleReq');
        $road = $request->get('roadReq');


        if (!empty($tole) and !empty($search) and !empty($road)) {
            $data = Cover::where('RESPONDENT', 'like', '%' . $search . '%')
                ->Where('tole', 'like', '%' . $tole . '%')
                ->Where('road', 'like', '%' . $road . '%')


                ->get();


            return json_encode($data);
        } elseif (empty($tole) and !empty($search) and !empty($road)) {
            $data = Cover::where('RESPONDENT', 'like', '%' . $search . '%')
                ->Where('road', 'like', '%' . $road . '%')



                ->get();


            return json_encode($data);
        } elseif (!empty($tole) and !empty($search) and empty($road)) {
            $data = Cover::where('RESPONDENT', 'like', '%' . $search . '%')
                ->Where('tole', 'like', '%' . $tole . '%')



                ->get();


            return json_encode($data);
        } elseif (!empty($tole) and empty($search) and !empty($road)) {
            $data = Cover::where('tole', 'like', '%' . $tole . '%')
                ->Where('road', 'like', '%' . $road . '%')



                ->get();


            return json_encode($data);
        } elseif (empty($tole) and empty($search) and !empty($road)) {
            $data = Cover::Where('road', 'like', '%' . $road . '%')
                ->get();


            return json_encode($data);
        } elseif (empty($tole) and !empty($search) and empty($road)) {
            $data = Cover::where('RESPONDENT', 'like', '%' . $search . '%')
                ->get();

            // $data = $data->paginate(5);
            // dd($data);

            return json_encode($data);
        } elseif (!empty($tole) and empty($search) and empty($road)) {
            $data = Cover::where('tole', 'like', '%' . $tole . '%')->get();


            return json_encode($data);
        }
    }
}

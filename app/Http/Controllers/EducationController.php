<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EducationController extends Controller
{
    public function educationLevel()
    {
        $shipments = json_decode(file_get_contents("upload/json/educationlevel_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];

        return view('education.edu-lvl',  compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function reason()
    {
        $shipments = json_decode(file_get_contents("upload/json/reasoneducation_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        return view('education.not-attend-reason',  compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function attend()
    {

        $attend = DB::select("SELECT WARD,
        (SELECT COUNT(*)FROM persons AS p WHERE ATTENDING_SCHOOL=1 AND persons.WARD=p.WARD) AS cha,
        (SELECT COUNT(*)FROM persons AS p WHERE ATTENDING_SCHOOL=2 AND persons.WARD=p.WARD) AS chaina,
        (SELECT COUNT(*)FROM persons AS p WHERE ATTENDING_SCHOOL IS NULL AND persons.WARD=p.WARD) AS nul,
        COUNT(*) AS Total FROM persons GROUP BY WARD");

        return view('education.attending-school', ['attend' => $attend]);
    }

    public function skill()
    {

        $shipments = json_decode(file_get_contents("upload/json/skill_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        return view('education.skill',  compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function training()
    {
        $shipments = json_decode(file_get_contents("upload/json/training_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];

        return view('education.training',  compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }
}

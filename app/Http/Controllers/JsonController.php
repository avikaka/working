<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use File;

class JsonController extends Controller
{
    public function makejson()
    {
        $householdowner = getTabularReport('household', 'A01', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($householdowner);
        $file = 'householdowner' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $landowner = getTabularReport('household', 'A02', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($landowner);
        $file = 'landowner' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $housetype = getTabularReport('household', 'A03', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($housetype);
        $file = 'housetype' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $watersource = getTabularReport('household', 'A12', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($watersource);
        $file = 'watersource' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $cookingfuel = getTabularReport('household', 'A17', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($cookingfuel);
        $file = 'cookingfuel' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $lightsource = getTabularReport('household', 'A19', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($lightsource);
        $file = 'lightsource' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $toiletstatus = getTabularReport('household', 'A21', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($toiletstatus);
        $file = 'toiletstatus' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $garbage = getTabularReport('household', 'A22', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($garbage);
        $file = 'garbage' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $accessroad = getTabularReport('household', 'A24', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($accessroad);
        $file = 'accessroad' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $femaleownership = getTabularReport('household', 'A39', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($femaleownership);
        $file = 'femaleownership' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $bankaccount = getTabularReport('household', 'A38', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($bankaccount);
        $file = 'bankaccount' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);


        //personal json

        $sex = getTabularReport('persons', 'SEX', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($sex);
        $file = 'sex' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $religion = getTabularReport('persons', 'religion', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($religion);
        $file = 'religion' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);



        $age_group = DB::select("SELECT WARD,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>=0 AND AGE<=4 AND h.WARD=household.WARD) AS a,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>4 AND AGE<=9 AND h.WARD=household.WARD) AS b,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>9 AND AGE<=14 AND h.WARD=household.WARD) AS c,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>14 AND AGE<=19 AND h.WARD=household.WARD) AS d,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>19 AND AGE<=24 AND h.WARD=household.WARD) AS e,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>24 AND AGE<=29 AND h.WARD=household.WARD) AS f,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>29 AND AGE<=34 AND h.WARD=household.WARD) AS g,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>34 AND AGE<=39 AND h.WARD=household.WARD) AS h,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>39 AND AGE<=44 AND h.WARD=household.WARD) AS i,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>44 AND AGE<=49 AND h.WARD=household.WARD) AS j,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>49 AND AGE<=54 AND h.WARD=household.WARD) AS k,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>54 AND AGE<=59 AND h.WARD=household.WARD) AS l,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>59 AND AGE<=64 AND h.WARD=household.WARD) AS m,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>64 AND AGE<=69 AND h.WARD=household.WARD) AS n,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>69 AND AGE<=74 AND h.WARD=household.WARD) AS o,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE>75 AND h.WARD=household.WARD) AS p
        -- (SELECT COUNT(*) FROM household AS h WHERE h.WARD=household.WARD) AS Total
        FROM household WHERE WARD IS NOT NULL GROUP BY WARD;");

        $data = json_encode($age_group);
        $file = 'age_group' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);



        $birthplace = getTabularReport('persons', 'G01', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($birthplace);
        $file = 'birthplace' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $literacy = getTabularReport('persons', 'LITERATE', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($literacy);
        $file = 'literacy' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $martitalstatus = getTabularReport('persons', 'MARITAL_STATUS', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($martitalstatus);
        $file = 'martitalstatus' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $disability = getTabularReport('persons', 'G07', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($disability);
        $file = 'disability' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $mothertounge = getTabularReport('persons', 'MOTHER_TONGUE', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($mothertounge);
        $file = 'mothertounge' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $caste = getTabularReport('persons', 'CASTE', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($caste);
        $file = 'caste' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);




        // //Education

        $educationlevel = getTabularReport('persons', 'EDU_LEVEL', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($educationlevel);
        $file = 'educationlevel' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);


        $reasoneducation = getTabularReport('persons', 'NOT_ATTENDING_REASON', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($reasoneducation);
        $file = 'reasoneducation' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);


        $skill = getTabularReport('persons', 'SPECIAL_SKILL', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($skill);
        $file = 'skill' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);


        $training = getTabularReport('persons', 'HAS_TRAINED', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($training);
        $file = 'training' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);


        // Health
        $lifelong = getTabularReport('persons', 'H02', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($lifelong);
        $file = 'lifelong' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $emergency = getTabularReport('persons', 'H05', $condition = null, $with_total = true, $ward_no = null, $without_notstated = false,  $caption_lang = 'NE');
        $data = json_encode($emergency);
        $file = 'emergency' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $vaccination = getTabularReport('persons', 'VACINATION', $condition = null, $with_total = true, $ward_no = null, $without_notstated = true,  $caption_lang = 'NE');
        $data = json_encode($vaccination);
        $file = 'vaccination' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        $birth = getTabularReport('persons', 'H01', $condition = null, $with_total = true, $ward_no = null, $without_notstated = true,  $caption_lang = 'NE');
        $data = json_encode($birth);
        $file = 'birth' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);


        $birthWeight = getTabularReport('persons', 'I07', $condition = null, $with_total = true, $ward_no = null,  $caption_lang = 'NE');
        $data = json_encode($birthWeight);
        $file = 'birthWeight' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);





        //Absentees

        $aeducationlevel = getTabularReport('absentees', 'D06', $condition = null, $with_total = true, $ward_no = null,  $caption_lang = 'NE');
        $data = json_encode($aeducationlevel);
        $file = 'aeducationlevel' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);



        $absCurrent = getTabularReport('absentees', 'D09_1', $condition = null, $with_total = true, $ward_no = null,  $caption_lang = 'NE');
        $data = json_encode($absCurrent);
        $file = 'absCurrent' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        // dd('check');


        // To get data for agewise gender chart
        $newArray = [];
        $final = [];
        //TO get value set name
        $agewiseTitle = getFieldValues('persons', 'age_group');

        foreach ($agewiseTitle['title'] as $key => $value) {
            array_push($newArray, $value);
        }

        $final = array_slice($newArray, 0, 18);
        //To get all the genderwise population according to age_group
        $agewise = DB::select("SELECT age_group,
        (SELECT COUNT(age_group) FROM persons AS a WHERE a.age_group=persons.age_group AND SEX=1)  AS male,
        (SELECT COUNT(age_group) FROM persons AS a WHERE a.age_group=persons.age_group AND SEX=2)  AS female
        FROM persons WHERE SEX IS NOT NULL AND age_group !=0 AND SEX !=3 GROUP BY age_group ORDER BY age_group, SEX");

        //compare keys of two arrays and replace age group with proper titles
        for ($i = 0; $i < count($agewise); $i++) {
            if ($i == 18) {
                break;
            }
            $agewise[$i]->age_group = $final[$i];
        }
        // dd($agewise);

        $data = json_encode($agewise);
        $file = 'agewise' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);




        //Household Equipments

        $amenities = DB::select("SELECT WARD,
        (SELECT COUNT(*) FROM household AS h WHERE A27_01=1 AND h.WARD=household.WARD) AS Radio,
        (SELECT COUNT(*) FROM household AS h WHERE A27_02=1 AND h.WARD=household.WARD) AS Tv,
        (SELECT COUNT(*) FROM household AS h WHERE A27_03=1 AND h.WARD=household.WARD) AS Cable,
        (SELECT COUNT(*) FROM household AS h WHERE A27_04=1 AND h.WARD=household.WARD) AS Computer,
        (SELECT COUNT(*) FROM household AS h WHERE A27_05=1 AND h.WARD=household.WARD) AS Net,
        (SELECT COUNT(*) FROM household AS h WHERE A27_06=1 AND h.WARD=household.WARD) AS Phone,
        (SELECT COUNT(*) FROM household AS h WHERE A27_07=1 AND h.WARD=household.WARD) AS Mobile,
        (SELECT COUNT(*) FROM household AS h WHERE A27_08=1 AND h.WARD=household.WARD) AS Bike,
        (SELECT COUNT(*) FROM household AS h WHERE A27_09=1 AND h.WARD=household.WARD) AS Car,
        (SELECT COUNT(*) FROM household AS h WHERE A27_10=1 AND h.WARD=household.WARD) AS Bus,
        (SELECT COUNT(*) FROM household AS h WHERE A27_11=1 AND h.WARD=household.WARD) AS Trackter,
        (SELECT COUNT(*) FROM household AS h WHERE A27_12=1 AND h.WARD=household.WARD) AS Rikshaw,
        (SELECT COUNT(*) FROM household AS h WHERE A27_13=1 AND h.WARD=household.WARD) AS Cycle,
        (SELECT COUNT(*) FROM household AS h WHERE A27_14=1 AND h.WARD=household.WARD) AS Pumpset,
        (SELECT COUNT(*) FROM household AS h WHERE A27_15=1 AND h.WARD=household.WARD) AS Fridge,
        (SELECT COUNT(*) FROM household AS h WHERE A27_16=1 AND h.WARD=household.WARD) AS AC,
        (SELECT COUNT(*) FROM household AS h WHERE A27_17=1 AND h.WARD=household.WARD) AS WashingM,
        (SELECT COUNT(*) FROM household AS h WHERE A27_18=1 AND h.WARD=household.WARD) AS Solar,
        (SELECT COUNT(*) FROM household AS h WHERE A27_19=1 AND h.WARD=household.WARD) AS Heater
        -- (SELECT COUNT(*) FROM household AS h WHERE A25_17=1 AND h.WARD=household.WARD) AS SolarHeater
        -- (SELECT COUNT(*) FROM household AS h WHERE h.WARD=household.WARD) AS Total
        FROM household WHERE WARD IS NOT NULL GROUP BY WARD;");


        $data = json_encode($amenities);
        $file = 'amenities' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);




        $delivery = getTabularReport('persons', 'I08', $condition = null, $with_total = true, $ward_no = null,  $caption_lang = 'NE');
        $data = json_encode($delivery);
        $file = 'delivery' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);


        $asex = getTabularReport('absentees', 'D04', $condition = null, $with_total = true, $ward_no = null,  $caption_lang = 'NE');
        $data = json_encode($asex);
        $file = 'asex' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);


        $absReason = getTabularReport('absentees', 'D08', $condition = null, $with_total = true, $ward_no = null,  $caption_lang = 'NE');
        $data = json_encode($absReason);
        $file = 'absReason' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $data);

        return back();
    }
}

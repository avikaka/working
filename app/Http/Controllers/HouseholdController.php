<?php

namespace App\Http\Controllers;

use App\Household;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use File;

class HouseholdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Household::findOrFail('23101000001');
        return Household::select('MUN', 'WARD', 'EA', 'HNO', 'HHNO', 'A01', 'A02')
            ->where([['WARD', '=', '1'], ['EA', '=', 1]])->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Household  $household
     * @return \Illuminate\Http\Response
     */
    public function show(Household $household)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Household  $household
     * @return \Illuminate\Http\Response
     */
    public function edit(Household $household)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Household  $household
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Household $household)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Household  $household
     * @return \Illuminate\Http\Response
     */
    public function destroy(Household $household)
    {
        //
    }
    public function getData()
    {
        $households = Household::select('id', 'WARD', 'HNO', 'HHNO', 'A01', 'A02')->limit(15)->get();
        foreach ($households as $household) :
            $data[] = array('household' => $household, 'person' => $household->persons('id', 'WARD', 'NAME')->get());
        endforeach;
        $hh = $households[0];
        // return $hh;
        $p = $households[0]->persons()->select('NAME', 'SEX', 'AGE', 'age_group')->get();
        // return $p;
        return $data[0];
    }

    public function householdType()
    {

        $shipments = json_decode(file_get_contents("upload/json/housetype_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        $pie = $shipments['pie'];
        // dd($title);
        // dd(json_encode($shipments['final_data']));
        return view('household.household-type', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage', 'pie'));
    }

    public function waterSource()
    {
        $shipments = json_decode(file_get_contents("upload/json/watersource_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        // dd($shipments);
        return view('household.water-source', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function cookingFuelSource()
    {
        $shipments = json_decode(file_get_contents("upload/json/cookingfuel_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        return view('household.fuel-type', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function lightFuelSource()
    {
        $shipments = json_decode(file_get_contents("upload/json/lightsource_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];

        return view('household.light-source', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function accessRoad()
    {
        $shipments = json_decode(file_get_contents("upload/json/accessroad_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];

        return view('household.road-condition', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function womenOwnership()
    {
        $shipments = json_decode(file_get_contents("upload/json/femaleownership_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        return view('household.women-ownership', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function bankaccount()
    {
        $shipments = json_decode(file_get_contents("upload/json/bankaccount_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];

        return view('household.bank-account', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function houseequipment()
    {

        $amenities = json_decode(file_get_contents("upload/json/amenities_file.json"), true);

        // dd($amenities);

        //To get the correct number of total equipments according to each ward.
        $forTotal = [];
        $sum1 = array();
        foreach ($amenities as $t) {
            // $t = array_slice($t, 1);
            $total1 = array_sum($t) - $t['WARD'];
            $sum1["Total"] = $total1;
            $test = array_merge($t, $sum1);
            array_push($forTotal, $test);
        }
        // dd($forTotal);

        //To get the altogether total sum each equipments.
        $sum = array();
        foreach ($forTotal as $key => $sub_array) {
            foreach ($sub_array as $sub_key => $value) {

                //If array key doesn't exists then create and initize first before we add a value.
                //Without this we will have an Undefined index error.
                if (!array_key_exists($sub_key, $sum)) $sum[$sub_key] = 0;

                //Add Value
                $sum[$sub_key] += $value;
            }
        }

        //To get percentage of each equipments.
        $total = array_slice($sum, 1);
        $per = array();
        foreach ($total as $t) {
            // 16574 must be total value
            $percentage = round($t / $total['Total'] * 100, 1);
            array_push($per, $percentage);
        }

        return view('household.equipment', compact('amenities','forTotal', 'per', 'total'));
    }

    public function toilet()
    {
        $shipments = json_decode(file_get_contents("upload/json/toiletstatus_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        return view('household.toilet', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function garbage()
    {
        $shipments = json_decode(file_get_contents("upload/json/garbage_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];


        return view('household.garbage', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function houseOwnership()
    {
        $shipments = json_decode(file_get_contents("upload/json/householdowner_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        // dd(json_encode($shipments['final_data']));

        return view('household.house-ownership', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function landOwnership()
    {
        $shipments = json_decode(file_get_contents("upload/json/landowner_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];

        // dd(json_encode($shipments['final_data']));
        return view('household.land-ownership', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }
}

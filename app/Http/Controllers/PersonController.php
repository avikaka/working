<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Person;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    public function index()
    {
        $file_name = 'dashboard.json';
        $file_path = base_path('public/json/' . $file_name);
        // dd($person);
        if (!file_exists($file_path) /* || (time() - filemtime($file_path) >= 60) */) {
            $person = Person::where('ID', 23101000010)->get();
            //save php array by encoding in json object with the given file_path
            file_put_contents($file_path, json_encode($person));
        }
        $jsonFile = file_get_contents($file_path);
        $persons = json_decode($jsonFile, true);
        // echo time() - filemtime($file_path);
        // dd($data);
        // $data[] = ['name'=>'Bishnu', 'subject'=>['Nepali','Math','English'=>['writing','speaking','listening','teaching']]];
        // $data[] = ['name'=>'Dipa', 'subject'=>['Science','Social','English']];
        // $data[] = ['name'=>'Aditi', 'subject'=>['English','Math','Health']];
        // $data[] = ['name'=>'Arpit', 'subject'=>['Geography','Biology','Chemistry']];

        // return json_encode($data);
        // return serialize($data);
        return view('person', ['persons' => $persons]);
        //return $person;
    }

    public function wardWiseSex($caller = 'Route') //$caller = Route | Core
    {
        $persons = DB::select('SELECT p.WARD AS `Ward`
                , (SELECT COUNT(p1.SEX) FROM persons AS p1 WHERE p1.SEX=1 AND p.`WARD`=p1.WARD) AS `male`
                , (SELECT COUNT(p1.SEX) FROM persons AS p1 WHERE p1.SEX=2 AND p.`WARD`=p1.WARD) AS `female`
                , (SELECT COUNT(p1.SEX) FROM persons AS p1 WHERE p1.SEX=3 AND p.`WARD`=p1.WARD) AS `other`
            FROM persons AS p
            GROUP BY p.`WARD` ORDER BY p.`WARD` ASC;');
        // dd($persons);
        // if ($caller <> 'Core'):
        //     return view('reports.index',compact('persons'));
        // else:
        //     return $persons;
        // endif;
        return $persons;
    }
    function ageGroup($caller = 'Route')
    {
        return DB::select('SELECT p.`WARD`, p.`age_group`, COUNT(*) `total` FROM `persons` p GROUP BY p.`WARD`, p.`age_group` ORDER BY p.`WARD`, p.`age_group`;');
    }
    function personsReport()
    {
        $wardWiseSex = $this->wardWiseSex('Core');
        // $ageGroup = $this->ageGroup('Core');
        dd($wardWiseSex);
        return view('reports.index', compact('wardWiseSex'));
    }
    //Testing for Json Format
    function json()
    {
        $persons = DB::select('SELECT p.WARD AS `Ward`
                , (SELECT COUNT(p1.SEX) FROM persons AS p1 WHERE p1.SEX=1 AND p.`WARD`=p1.WARD) AS `Male`
                , (SELECT COUNT(p1.SEX) FROM persons AS p1 WHERE p1.SEX=2 AND p.`WARD`=p1.WARD) AS `Female`
                , (SELECT COUNT(p1.SEX) FROM persons AS p1 WHERE p1.SEX=3 AND p.`WARD`=p1.WARD) AS `Other`
            FROM persons AS p
            GROUP BY p.`WARD` ORDER BY p.`WARD` ASC;');
        return $persons;
    }
    function jsonAgeGroup()
    {
        return $this->wardWiseSex('Core');
        // dd($ageGroup);
        // return $this->wardWiseSex();
    }

    function getByCaste($byWard = false)
    {
        if ($byWard)
            return DB::select('
                ');

        // return DB::select('SELECT `CASTE`, count(*) AS `Number` FROM `persons` GROUP BY `CASTE` ORDER BY `Number` DESC;');
        return DB::table('persons')->select('CASTE', DB::raw('COUNT(*) AS number'))->groupBy('CASTE')->orderBy('number', 'desc')->get();
    }

    function wardWise()
    {
        return $this->wardWiseSex();
    }

    public function genderwise()
    {
        $shipments = json_decode(file_get_contents("upload/json/sex_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $pie = $shipments['pie'];
        $percentage = $shipments['percentage'];



        return view('persons.genderwise', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage', 'pie'));
    }

    public function agewise()
    {
        $age = DB::select("SELECT * FROM age_wise");
        // dd($age);
        return view('persons.age-wise', ['ages' => $age]);
    }

    /*
    * get population by five-year age-group and sex for population pyramid chart
    */
    public function getAgeGroupSex()
    {
    }

    // returns ward-wise five-years age-group population
    public function ageGroupSex()
    {
        $sql = "SELECT p.`WARD`, p.`age_group`, COUNT(*) `total`
        FROM `persons` p GROUP BY p.`WARD`, p.`age_group` ORDER BY p.`WARD`, p.`age_group`;";
        $sql1 = "SELECT label_EN FROM `values` AS v WHERE v.Value = p.`age_group` AND v.value_set_id=87";
        $ageGroup = DB::select($sql);
        $age_group_values = \App\Field::where('table_name', '=', 'persons')
            ->where('field_name', '=', 'age_group')->first()
            ->valueSet->values()->select('Value', 'label_EN')->get();
        $age_group_distinct = \App\Person::select('age_group')->distinct()->orderBy('age_group')->get();
        // dd($age_group_values);
        //  return $age_group_distinct;
        // $data = array();
        for ($i = 1; $i <= 12; $i++) {
            foreach ($ageGroup as $ag) {
                // $data[$ag->WARD] = Array('age_group'=>$ag->age_group);
                // $ags["Ward ".$ag->WARD][$ag->age_group][$ag->SEX] = $ag->total;
                if ($ag->WARD == $i) {
                    $ags['age_group_' . $ag->age_group] = $ag->total;
                }
            }
            $data["ward_" . $i] = $ags;
        }
        return $data;
    }

    public function religion()
    {
        $shipments = json_decode(file_get_contents("upload/json/religion_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        dd($table);
        return view('persons.religion', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function caste()
    {
        $shipments = json_decode(file_get_contents("upload/json/caste_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        return view('persons.caste-wise', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function motherTongue()
    {
        $shipments = json_decode(file_get_contents("upload/json/mothertounge_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        return view('persons.mother-tongue', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function disability()
    {
        $shipments = json_decode(file_get_contents("upload/json/disability_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        return view('persons.disability', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function maritalStatus()
    {
        $shipments = json_decode(file_get_contents("upload/json/martitalstatus_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        return view('persons.marital_status', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function ageAtFirstMarriage()
    {
        $marriage = DB::select("SELECT WARD,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE_AT_FIRST_MARRIAGE>=0 AND AGE_AT_FIRST_MARRIAGE<=10 AND h.WARD=persons.WARD) AS A,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE_AT_FIRST_MARRIAGE>=11 AND AGE_AT_FIRST_MARRIAGE<=20 AND h.WARD=persons.WARD) AS B,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE_AT_FIRST_MARRIAGE>=21 AND AGE_AT_FIRST_MARRIAGE<=30 AND h.WARD=persons.WARD) AS C,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE_AT_FIRST_MARRIAGE>=31 AND AGE_AT_FIRST_MARRIAGE<=40 AND h.WARD=persons.WARD) AS D,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE_AT_FIRST_MARRIAGE>=41 AND AGE_AT_FIRST_MARRIAGE<=50 AND h.WARD=persons.WARD) AS E,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE_AT_FIRST_MARRIAGE>=51 AND AGE_AT_FIRST_MARRIAGE<=60 AND h.WARD=persons.WARD) AS F,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE_AT_FIRST_MARRIAGE>=61 AND AGE_AT_FIRST_MARRIAGE<=70 AND h.WARD=persons.WARD) AS G,
        (SELECT COUNT(*) FROM persons AS h WHERE AGE_AT_FIRST_MARRIAGE>=71 AND h.WARD=persons.WARD) AS H,
        COUNT(AGE_AT_FIRST_MARRIAGE) AS Total
        FROM persons GROUP BY WARD;");

        return view("persons.first_marriage", ['data' => $marriage]);
    }


    public function literacy()
    {
        $shipments = json_decode(file_get_contents("upload/json/literacy_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];

        return view('persons.literacy', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function birthplace()
    {
        $shipments = json_decode(file_get_contents("upload/json/birthplace_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];

        return view('persons.birthplace', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Darta;
use Illuminate\Http\Request;

class DartaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Darta  $darta
     * @return \Illuminate\Http\Response
     */
    public function show(Darta $darta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Darta  $darta
     * @return \Illuminate\Http\Response
     */
    public function edit(Darta $darta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Darta  $darta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Darta $darta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Darta  $darta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Darta $darta)
    {
        //
    }
}

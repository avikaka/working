<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AbsenteesController extends Controller
{
    public function gender()
    {


        $shipments = json_decode(file_get_contents("upload/json/asex_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        return view('absentees.gender', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function agewise()
    {


        $age = DB::select("SELECT * FROM age_wise_absentees;");

        return view("absentees.age-wise", ['ages' => $age]);
    }

    public function education()
    {
        $shipments = json_decode(file_get_contents("upload/json/aeducationlevel_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];

        return view("absentees.education-wise", compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function reason()
    {
        // $absReason = getTabularReport('absentees', 'C09', $condition = null, $with_total = true, $ward_no = null,  $caption_lang = 'NE');
        // dd($absReason);
        $shipments = json_decode(file_get_contents("upload/json/absReason_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];

        return view("absentees.reason", compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }

    public function current()
    {
        $shipments = json_decode(file_get_contents("upload/json/absCurrent_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];

        return view("absentees.current-place", compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage'));
    }
}

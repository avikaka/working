<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HealthController extends Controller
{
    public function lifelong()
    {
        // $household=getTabularReport('persons', 'G08', $condition=null, $with_total = true, $ward_no = null,  $caption_lang = 'NE');
        // $table = $household['final_data'];
        // $title = $household['title'];
        // $keys = array_keys($title);

        // // dd($household);
        // return view('health.life-long-disease', compact('table','title', 'keys'));


        $shipments = json_decode(file_get_contents("upload/json/lifelong_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        $pie = $shipments['pie'];
        // dd($shipments);

        return view('health.life-long-disease', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage', 'pie'));
    }

    public function vaccination()
    {
        $shipments = json_decode(file_get_contents("upload/json/vaccination_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        $pie = $shipments['pie'];
        // dd($shipments);

        return view('health.vaccination', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage', 'pie'));
    }

    public function safe()
    {

        $shipments = json_decode(file_get_contents("upload/json/birth_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        $pie = $shipments['pie'];
        // dd($shipments);

        return view('health.safe-delivery', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage', 'pie'));
    }

    public function birth()
    {
        $data = DB::select("SELECT WARD,
        (SELECT COUNT(*) FROM persons AS p WHERE H01=1 AND p.WARD=persons.WARD) AS cha,
        (SELECT COUNT(*) FROM persons AS p WHERE H01=2 AND p.WARD=persons.WARD) AS chaina,
        (SELECT COUNT(*) FROM persons AS p WHERE H01 IS NULL AND p.WARD=persons.WARD) AS nul,
        COUNT(H01) AS total
        FROM persons GROUP BY WARD;");

        return view('reproductive-health.birth', ['data' => $data]);
    }

    public function totalBirth()
    {
        $shipments = json_decode(file_get_contents("upload/json/vaccination_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        $pie = $shipments['pie'];
        // dd($shipments);

        return view('reproductive-health.birth', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage', 'pie'));
    }

    public function childWeight()
    {
        $shipments = json_decode(file_get_contents("upload/json/birthWeight_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        $pie = $shipments['pie'];
        // dd($shipments);

        return view('reproductive-health.child-weight', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage', 'pie'));
    }

    public function delivery()
    {
        $shipments = json_decode(file_get_contents("upload/json/delivery_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        $pie = $shipments['pie'];
        // dd($shipments);

        return view('reproductive-health.delivery-place', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage', 'pie'));
    }

    public function motherAge()
    {
        $data = DB::select("SELECT WARD,
        (SELECT COUNT(*) FROM persons AS p WHERE H09<=14 AND p.WARD=persons.WARD) AS a,
        (SELECT COUNT(*) FROM persons AS p WHERE H09>=15 AND H09<=19 AND p.WARD=persons.WARD) AS b,
        (SELECT COUNT(*) FROM persons AS p WHERE H09>=20 AND H09<=24 AND p.WARD=persons.WARD) AS c,
        (SELECT COUNT(*) FROM persons AS p WHERE H09>=25 AND H09<=29 AND p.WARD=persons.WARD) AS d,
        (SELECT COUNT(*) FROM persons AS p WHERE H09>=30 AND H09<=34 AND p.WARD=persons.WARD) AS e,
        (SELECT COUNT(*) FROM persons AS p WHERE H09>=35 AND p.WARD=persons.WARD) AS f,
        COUNT(H09) AS total
        FROM persons GROUP BY WARD;");

        return view('reproductive-health.mother-age', ['data' => $data]);
    }

    public function emergency()
    {
        $shipments = json_decode(file_get_contents("upload/json/emergency_file.json"), true);
// dd($shipments);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_keys($title);
        $count = count($title);
        $total = $shipments['total'];
        $percentage = $shipments['percentage'];
        $pie = $shipments['pie'];
        // dd($shipments);

        return view('health.emergency-disease', compact('shipments','table', 'title', 'keys', 'count', 'total', 'percentage', 'pie'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Cooperative;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
// use DB;
// use Illuminate\Support\Facades\DB as FacadesDB;
use File;


class CooperativeController extends Controller
{
    public function create()
    {
        return view('admin.Cooperatives.cooperativeAdd', ['data' => new Cooperative()]);
    }

    public function store(Request $request)
    {
        self::validatedData($request);
        $data = new Cooperative();
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/cooperative';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $this->setDataValue($request, $data);
        $data->save();
        self::Coopjson();
        session()->flash('status', 'insert');
        $request->session()->flash('msg', 'Data saved successfully !');
        return redirect('admin/cooperative');
    }

    public function index()
    {
        $data = Cooperative::paginate(10);
        return view('admin.Cooperatives.cooperative_show')->with('cooperative', $data);
    }

    public function edit($id)
    {
        $data = Cooperative::findOrFail($id);
        return view('admin.Cooperatives.cooperativeAdd')->with('data', $data);
    }

    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = Cooperative::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = Cooperative::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/cooperative/{$data_image->photo}")) {
                unlink("uploads/cooperative/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/cooperative';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::Coopjson();
        session()->flash('status', 'success');
        return redirect('admin/cooperative');
    }

    public function destroy($id)
    {
        $del = Cooperative::findOrFail($id);
        if ($del->photo && file_exists("uploads/cooperative/{$del->photo}")) {
            unlink("uploads/cooperative/{$del->photo}");
        }
        $data = Cooperative::findOrFail($id);
        $data->delete();
        self::Coopjson();
        session()->flash('status', 'delet');

        return redirect('admin/cooperative');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->address = $request->input('address'),
            $data->type = $request->input('type'),
            $data->saving = $request->input('saving'),
            $data->loans = $request->input('loans'),
            $data->loan_area = $request->input('loan_area'),
            $data->male = $request->input('male'),
            $data->female = $request->input('female'),
            $data->contact_person = $request->input('contact_person'),
            $data->contact_no = $request->input('contact_no'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude'),
            $data->remarks = $request->input('remarks'),

        );
    }


    protected function validatedData(Request $request)
    {



        $rules = [
            'name' => 'required',
            'ward' => 'nullable',
            'address' => 'nullable',
            'saving' => 'nullable',
            'loans' => 'nullable',
            'loan_area' => 'nullable',
            'type' => 'nullable',
            'male' => 'nullable',
            'female' => 'nullable',
            'contact_person' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'remarks' => 'nullable',
            'contact_no' => 'nullable',
        ];

        $customMessages = [
            'name.required' => 'सहकारीको नाम क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'saving.required' => 'बचत क्षेत्र आवश्यक छ।',
            'type.required' => 'प्रकार क्षेत्र आवश्यक छ।',
            'loans.required' => 'ऋण लगानी क्षेत्र आवश्यक छ।',
            'loan_area.required' => 'लगानीका मुख्य क्षेत्र आवश्यक छ।',
            'male.required' => 'पुरूष क्षेत्र आवश्यक छ।',
            'female.required' => 'महिला क्षेत्र आवश्यक छ।',
            'contact_person.required' => 'सम्पर्क वयक्ति क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            'contact_no.required' => 'सम्पर्क नं क्षेत्र आवश्यक छ।',
        ];

        $this->validate($request, $rules, $customMessages);
    }

    public function Coopjson()
    {
        $coop = Cooperative::all();
        $cooperativetdata = json_encode($coop,JSON_UNESCAPED_UNICODE);
        $file = 'cooperativedetails' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cooperativetdata);
    }


    // public function cooperartivetype($type)
    // {
    //     $cooperative = Cooperative::where('type', '=', $type)->get();
    //     $cdata = Cooperative::select('type', DB::raw('count(*) as count'))
    //         ->groupBy('type')->where('type', '!=', $type)
    //         ->get();

    //     return view('bank.cooperativedetails', compact('cooperative', 'cdata'));
    // }
}

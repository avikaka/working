<?php
namespace App\Http\Controllers\Admin;

use App\PublicToilet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
// use DB;
// use Illuminate\Support\Facades\DB as FacadesDB;
use File;

class PublicToiletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (file_exists("upload/json/toiletdetails_file.json")) {
            $data  = PublicToilet::paginate(10);
        return view('Toilet.toilet_detail')->with('publictoilet', $data);
            // return view('Toilet.toilet_detail')->with('data', $datas);
        // } else {
        // }
        //     $data = PublicToilet::all();
        // return view('toilet.toiletdetails')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Toilet.toilet_add', ['data' => new PublicToilet()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        self::validatedData($request);

        $data = new PublicToilet();
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/toilet';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $data->save();
        self::Toiletjson();

        return redirect('admin/public-toilet');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $data = PublicToilet::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/public-toilet');
        }

        return view('Toilet.toilet_add', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = PublicToilet::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $data_image = PublicToilet::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/toilet/{$data_image->photo}")) {
                unlink("uploads/toilet/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/toilet';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::Toiletjson();
        session()->flash('status', 'success');
        return redirect('admin/public-toilet');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {

        $del = PublicToilet::findOrFail($id);
        if ($del->photo && file_exists("uploads/toilet/{$del->photo}")) {
            unlink("uploads/toilet/{$del->photo}");
        }
        $data = PublicToilet::findOrFail($id);
        $data->delete();
        self::Toiletjson();
        session()->flash('status', 'delet');

        return redirect('admin/public-toilet');
    }

    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->address = $request->input('address'),
            $data->numbers = $request->input('numbers'),
            $data->status = $request->input('status'),
            $data->for_women = $request->input('for_women'),
            $data->remarks = $request->input('remarks'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude')
        );
    }

    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'ward' => 'required',
            'address' => 'nullable',
            'numbers' => 'nullable',
            'status' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'remarks' => 'nullable',
            'for_women' => 'nullable',
        ];

        $customMessages = [
            'name.required' => 'भवनको नाम क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'status.required' => 'अवस्था क्षेत्र आवश्यक छ।',
            'numbers.required' => 'संख्या क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            'for_women.required' => 'महिलाको लागि छुट्टै भएको क्षेत्र आवश्यक छ।',

        ];
        $this->validate($request, $rules, $customMessages);
    }

    public function Toiletjson()
    {
        $toilet = PublicToilet::all();
        $toiletdata = json_encode($toilet,JSON_UNESCAPED_UNICODE);
        $file = 'toiletdetails' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $toiletdata);
    }
}

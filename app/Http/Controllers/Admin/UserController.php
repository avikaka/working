<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Redirect;
use Session;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\support\facades\Input;
use Illuminate\Contracts\Auth\Guard;
use \Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\User;
use App\Mail\Validation;
use App\Mail\Changepassword;
use Hash;
use Mail;
use Auth;

class UserController extends Controller
{
    public function showChangePasswordForm(Request $request,$id)
    {
        $data = User::findOrFail($id);
        // $data->password = bcrypt($request->get('new-password'));
        // dd($data);

        return view('admin.user.changepassword')->with('data',$data);
    }



    public function changePassworduser(Request $request, $id)
    {
        // if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
        //     // The passwords matches
        //     return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
        // }
        // if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
        //     //Current password and new password are same
        //     return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        // }
        // $validatedData = $request->validate([
        //     'current-password' => 'required',
        //     'new-password' => 'required|string|min:6|confirmed',
        // ]);
        //Change Password
        // $user = Auth::user();
        // $user->password = bcrypt($request->get('new-password'));
        // $user->save();
        // return redirect('/admin/home');

        $user = User::findOrFail($id);
        $user->password = bcrypt($request->get('new-password'));
        if($request->get('new-password')==$request->get('new-password_confirmation')){
            $user->update();
            return redirect('/admin/viewuser');
        }else
        return redirect('admin/changePassword/'.$user->id)->with('message','Confirmation password does not match with your new password.');
    }

    public function forget()
    {

        return view('auth.forgetpassword');
    }


    public function changepassword(Request $request)
    {
        $validatedData = $request->validate([

            'email' => 'required|email',
        ]);


        if (User::where('email', '=', $request->input('email'))->exists()) {

            $user = User::where('email', $request->email)->get()->first();
            $name = $user->name;
            $id = $user->id;
            $email = $user->email;




            $username = $name;
            $useremail = $email;
            $userslug = $id;
            $data = array('username' => $username, 'useremail' => $useremail, 'userslug' => $userslug);
            Mail::to($useremail)->send(new Changepassword($data));
            $user->save();
            session()->flash('status', 'insert');
            return redirect('/d-admin');
        } else {
            session()->flash('status', 'fail');
            return redirect('/d-admin');
        }
    }

    public function linkpassword($id)
    {

        $user = User::where('id', $id)->get()->first();
        return view('admin.user.linkpassword', ['user' => $user]);
    }



    public function newpasswordforget(Request $request, $id)
    {

        $validatedData = $request->validate([
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        $user = User::findorfail($id);
        $user->password = bcrypt($request->get('new-password'));
        $user->update();
        return redirect('/d-admin');
    }

    public function createuser(Request $request)
    {

        return view('admin.user.userinsert');
    }

    public function storeuser(Request $request)
    {


        $validatedData = $request->validate([
            'name' => 'required|min:3|max:255',
            'email' => 'required|email|max:255|unique:users,email',
            'new-password' => 'required|string|min:6|confirmed',

        ]);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $test = uniqid() . '_' . $user->id;
        $user->slug = $test;
        $user->status = $request->status;
        // $pass = uniqid();
        // $user->password = Hash::make($pass);
        $user->password = bcrypt($request->get('new-password'));
        if($request->get('new-password')==$request->get('new-password_confirmation')){
            $user->save();
            session()->flash('status', 'insert');
            return redirect('/admin/viewuser');
        }else
        return redirect('admin/insertuser/')->with('message','Confirmation password does not match with your new password.');

        // $username = $user->name;
        // $useremail = $user->email;
        // $userslug = $user->slug;
        // $userpassword = $pass;
        // $data = array('userpassword' => $pass, 'username' => $request->name, 'useremail' => $request->email, 'userslug' => $test);
        // Mail::to($useremail)->send(new Validation($data));
        // $user->save();


        // session()->flash('status', 'insert');
        // dd('check mail');
        // return redirect('/admin/viewuser');
    }

    public function validation($slug)
    {

        $user = User::where('slug', $slug)->get()->first();
        return view('admin.user.userpassword', ['user' => $user]);
    }


    public function newpassword(Request $request, $id)
    {
        $getuser = User::findorfail($id);
        if (!(Hash::check($request->get('current-password'), $getuser->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
        }
        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        $user = User::findorfail($id);
        $user->password = bcrypt($request->get('new-password'));
        $user->cp_status = 1;
        $user->update();
        return redirect('/d-admin');
    }

    public function viewuser(Request $request)
    {
        if (Auth::user()->status == 'super-admin' || 'admin') {
            $user = User::where('status', '!=', 'super-admin')->where('id', '!=', Auth::user()->id)->get()->all();
            return view('admin.user.userview', ['user' => $user]);
        } else {
            return redirect('/admin/home');
        }
    }


    public function deleteuser($id)
    {

        if (Auth::user()->status == 'super-admin' || 'admin') {
            $data = User::destroy($id);
            Session::flash('status', 'Deleted!!!');
            return redirect('/admin/viewuser');
        } else {
            return redirect('/admin/home');
        }
    }
}

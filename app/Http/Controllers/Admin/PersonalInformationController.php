<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PersonalInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select("SELECT * FROM  fields Where Table_Name = 'persons' AND question !='';");


        foreach ($data as $row) {
            $id = $row->value_set_id;

            $values = array();
            $value = DB::select("SELECT * FROM `values` WHERE value_set_id= '$id';");
            foreach ($value as $val) {
                $values[] = $val;
            }

            $compile[] = array("row" => $row, "values" => $values);
        }
        
        dd(getQuestions());

        return view('family.personal_information', ['datas' => $compile]);
        
    }

    public function update(Request $request)
    {


        $householdId = $request->get('householdId');
        $quename = $request->get('que_ans');
        dd($quename);
        $update_string = "";
        
        DB::table('household')->where('id',$householdId)->update($quename);

        

        return redirect('/admin/PersonalInof')->with('message', 'Data successfully added');

        
    }
    public function search(Request $request)
    {
        // $householdid = $request->get('householdid');
        // dd($householdid); 
        
        $houseid=$request->householdid;
        return view('family.personal_information', ['datas' => $houseid]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\admin\PersonalInformation  $personalInformation
     * @return \Illuminate\Http\Response
     */
    public function show(PersonalInformation $personalInformation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\admin\PersonalInformation  $personalInformation
     * @return \Illuminate\Http\Response
     */
    public function edit(PersonalInformation $personalInformation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\admin\PersonalInformation  $personalInformation
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\admin\PersonalInformation  $personalInformation
     * @return \Illuminate\Http\Response
     */
    public function destroy(PersonalInformation $personalInformation)
    {
        //
    }
}
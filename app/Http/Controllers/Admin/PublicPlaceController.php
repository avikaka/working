<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PublicPlace;
use Illuminate\Http\Request;
use File;

class PublicPlaceController extends Controller
{
    public function create()
    {
        return view('admin.PublicPlace.public_placeAdd', ['data' => new PublicPlace()]);
    }

    public function store(Request $request)
    {
        self::validatedData($request);
        $data = new PublicPlace();
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/public-place';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $data->save();
        self::json();
        session()->flash('status', 'insert');
        return redirect('admin/public-place');
    }

    public function index()
    {
        $data = PublicPlace::paginate(10);
        return view('admin.PublicPlace.public_place_show')->with('publicplace', $data);
    }

    public function edit($id)
    {
        $data = PublicPlace::findOrFail($id);
        return view('admin.PublicPlace.public_placeAdd')->with('data', $data);
    }

    public function update(Request $request, $id)
    {
        self::validatedData($request);

        $data = PublicPlace::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $data_image = PublicPlace::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/public-place/{$data_image->photo}")) {
                unlink("uploads/public-place/{$data_image->image}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/public-place';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $data->update();
        self::json();
        session()->flash('status', 'success');
        $request->session()->flash('msg', 'Data saved successfully !');
        return redirect('admin/public-place');
    }

    public function destroy($id)
    {
        $del = PublicPlace::findOrFail($id);
        if ($del->photo && file_exists("uploads/public-place/{$del->photo}")) {
            unlink("uploads/public-place/{$del->photo}");
        }
        $data = PublicPlace::findOrFail($id);
        $data->delete();
        self::json();
        session()->flash('status', 'delet');

        return redirect('admin/public-place');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->type = $request->input('type'),
            $data->address = $request->input('address'),
            $data->uses = $request->input('uses'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude'),
            $data->remarks = $request->input('remarks'),
            $data->photo = $request->input('photo')
        );
    }

    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'ward' => 'required',
            'type' => 'required',
            'address' => 'required',
            'uses' => 'required',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'remarks' => 'nullable',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
        $customMessages = [
            'name.required' => 'सार्वजनिक स्थलको नाम आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'type.required' => 'प्रकार आवश्यक छ।',
            'address.required' => 'ठेगाना आवश्यक छ।',
            'uses.required' => 'प्रयोग आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',

        ];

        $this->validate($request, $rules, $customMessages);
    }

    public function json()
    {
        $data = PublicPlace::all();
        $cdata = json_encode($data,JSON_UNESCAPED_UNICODE);
        $file = 'public-place' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cdata);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Bridge;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Input;
use DB;
use File;

class BridgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (file_exists("upload/json/bridgedetails_file.json")) {
        //     $data = json_decode(file_get_contents("upload/json/bridgedetails_file.json"));
        //     return view('bridge.bridgedetails')->with('data', $data);
        // } else
            $bridge = Bridge::paginate(10);
        return view('bridge.bridgedetails')->with('bridge', $bridge);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('bridge.bridge', ['data' => new Bridge()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        self::validatedData($request);

        $data = new Bridge();
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/bridge';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->save();
        self::Bridgejson();
        geoJson(Bridge::class,'bridge');
        session()->flash('status', 'insert');


        return redirect('admin/bridge');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bridge  $bridge
     * @return \Illuminate\Http\Response
     */
    public function show(Bridge $bridge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bridge  $bridge
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $data = Bridge::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/bridge');
        }
        return view('bridge.bridge', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bridge  $bridge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        self::validatedData($request);

        $data = Bridge::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = Bridge::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/bridge/{$data_image->photo}")) {
                unlink("uploads/bridge/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/bridge';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::Bridgejson();
        geoJson(Bridge::class,'bridge');
        session()->flash('status', 'success');

        return redirect('admin/bridge');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bridge  $bridge
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = Bridge::findOrFail($id);
        if ($del->photo && file_exists("uploads/bridge/{$del->photo}")) {
            unlink("uploads/bridge/{$del->photo}");
        }
        $data = Bridge::findOrFail($id);
        $data->delete();
        self::Bridgejson();
        geoJson(Bridge::class,'bridge');
        session()->flash('status', 'delet');

        return redirect('admin/bridge');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->river_name = $request->input('river_name'),
            $data->connecting_settlements = $request->input('connecting_settlements'),
            $data->status = $request->input('status'),
            $data->remarks = $request->input('remarks'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude')
        );
    }


    protected function validatedData(Request $request)
    {



        $rules = [
            'name' => 'required',
            'river_name' => 'required',
            'connecting_settlements' => 'nullable',
            'status' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'remarks' => 'nullable',
            'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];

        $customMessages = [
            'name.required' => 'पुलको  नाम क्षेत्र आवश्यक छ।',
            'river_name.required' => 'नदी/खोला/खोल्सीको नाम  आवश्यक छ।',
            'connecting_settlements.required' => 'पुलले जोडने वस्तीहरु क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'status.required' => 'अवस्था क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
        ];

        $this->validate($request, $rules, $customMessages);
    }

    public function Bridgejson()
    {
        $bridge = Bridge::all();
        $bridgedata = json_encode($bridge,JSON_UNESCAPED_UNICODE);
        $file = 'bridgedetails' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $bridgedata);
    }
}

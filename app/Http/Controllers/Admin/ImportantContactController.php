<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use App\ImportantContact;
use File;

class ImportantContactController extends Controller
{
    public function createcontact(Request $request)
    {

        return view('admin.contact.contactinsert', ['data' => new ImportantContact()]);
    }


    public function storecontact(Request $request)
    {
        self::contactvalidation($request);
        $fdes = new ImportantContact;
        $fdes->name = $request->name;;
        $fdes->post = $request->post;
        $fdes->contact = $request->contact;
        $fdes->website = $request->website;
        $fdes->email = $request->email;
        $fdes->address = $request->address;
        $fdes->status = $request->status;
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/contact';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $fdes->photo = $uploadedImageName;
        }
        // dd($fdes);
        $fdes->save();
        self::ImportantContactjson();
        //   dd('sucess');
        session()->flash('status', 'insert');
        return redirect('/admin/viewcontact');
    }


    public function viewcontact(Request $request)
    {

        $contact = ImportantContact::paginate(5);
        return view('admin.contact.contactview', ['contact' => $contact]);
    }


    public function changeStatus($id)
    {
        try {
            $id = (int)$id;
            $contact = ImportantContact::find($id);
            if ($contact->status == 0) {
                $contact->status = 1;
                $contact->save();
                session()->flash('status', 'change');
                self::ImportantContactjson();

                return back();
            } else {
                $contact->status = 0;
                $contact->save();
                session()->flash('status', 'change');
                self::ImportantContactjson();

                return back();
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            session()->flash('error', $message);
            return back();
        }
    }


    public function  editcontact(Request $request, $id)
    {
        // dd('im here');
        $data = ImportantContact::where('id', $id)->get()->first();
        return view('admin.contact.contactinsert', ['data' => $data]);
    }




    public function updatecontact(Request $request, $id)


    {


        self::contactvalidation($request);
        $getedits = ImportantContact::findorfail($id);
        $getedits->name = $request->name;;
        $getedits->post = $request->post;
        $getedits->contact = $request->contact;
        $getedits->website = $request->website;
        $getedits->email = $request->email;
        $getedits->address = $request->address;
        if ($request->hasFile('photo')) {

            $data_image = ImportantContact::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/contact/{$data_image->photo}")) {
                unlink("uploads/contact/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/contact';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $getedits->photo = $uploadedImageName;
        }
        $getedits->status = $request->status;
        $getedits->update();
        self::ImportantContactjson();


        session()->flash('status', 'success');
        return redirect('/admin/viewcontact');
    }



    public function deletecontact($id)
    {
        $contact = ImportantContact::findOrFail($id);
        if ($contact->photo && file_exists("uploads/contact/{$contact->photo}")) {
            unlink("uploads/contact/{$contact->photo}");
        }
        $contact = ImportantContact::destroy($id);
        Session::flash('status', 'delet');
        self::ImportantContactjson();

        return redirect('/admin/viewcontact');
    }


    public function contactvalidation(Request $request)
    {

        $rules = [
            'name'  => 'required',
            'post'  => 'required',
            'status'  => 'nullable',
            'website' => 'nullable',
            'email' => 'nullable',
            'contact'  => 'required',
            'address'  => 'nullable',
            'photo.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];

        $customMessages = [
            'name.required' => 'नाम क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'post.required' => 'पद क्षेत्र आवश्यक छ।',
            'website.required' => 'वेब साइट क्षेत्र आवश्यक छ।',
            'email.required' => 'ईमेल क्षेत्र आवश्यक छ।',
            'status.required' => 'अवस्था क्षेत्र आवश्यक छ।',
            'contact.required' => 'सम्पर्क नं क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
        ];

        $this->validate($request, $rules, $customMessages);
    }



    public function ImportantContactjson()
    {
        $data = ImportantContact::where('status', 1)->get();
        $cdata = json_encode($data,JSON_UNESCAPED_UNICODE);
        $file = 'importantcontact' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cdata);
    }
}

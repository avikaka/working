<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use App\Description;
use File;

class DescriptionController extends Controller
{
    public function createdescription(Request $request)
    {

        return view('admin.description.descriptioninsert', ['data' => new Description()]);
    }


    public function storedescription(Request $request)
    {
        self::descriptionvalidation($request);
        $fdes = new Description();
        $fdes->description = $request->description;
        $fdes->status = $request->status;
        $fdes->save();
        self::descriptionjson();
        session()->flash('status', 'insert');
        return redirect('/admin/viewdescription');
    }


    public function viewdescription(Request $request)
    {

        $description = Description::all();
        return view('admin.description.descriptionview', ['description' => $description]);
    }


    public function changeStatus($id)
    {
        try {
            $id = (int)$id;
            $description = Description::find($id);
            if ($description->status == 0) {
                $description->status = 1;
                $description->save();
                session()->flash('status', 'change');
                self::descriptionjson();

                return back();
            } else {
                $description->status = 0;
                $description->save();
                session()->flash('status', 'change');
                self::descriptionjson();

                return back();
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            session()->flash('error', $message);
            return back();
        }
    }

    public function  editdescription(Request $request, $id)
    {
        // dd('im here');
        $data = description::where('id', $id)->get()->first();
        return view('admin.description.descriptioninsert', ['data' => $data]);
    }

    public function updatedescription(Request $request, $id)
    {

        // dd('im here');
        self::descriptionvalidation($request);
        $getedits = Description::findorfail($id);
        $getedits->description = $request->description;
        $getedits->status = $request->status;
        $getedits->update();
        self::descriptionjson();


        session()->flash('status', 'success');
        return redirect('/admin/viewdescription');
    }



    public function deletedescription($id)
    {
        $check = Description::all();
        $test = count($check);
        if ($test == 1) {
            return back()->with('status', 'Sorry you cant delete');
        } else {
            $description = Description::destroy($id);
            Session::flash('status', 'delet');
            self::descriptionjson();
        }


        return redirect('/admin/viewdescription');
    }


    public function descriptionvalidation(Request $request)
    {

        $validatedData = $request->validate([
            'description' => 'required',
            'status' => 'required'
        ]);
    }



    public function descriptionjson()
    {
        $data = description::where('status', 1)->orderBy('id', 'DESC')->take(1)->get();
        $cdata = json_encode($data,JSON_UNESCAPED_UNICODE);
        $file = 'description' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cdata);
    }
}

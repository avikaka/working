<?php

namespace App\Http\Controllers\Admin;

use App\ChildrenClub;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use File;

class ChildrenClubController extends Controller
{
    public function create()
    {
        return view('admin.ChildrenClub.children_clubAdd', ['data' => new ChildrenClub()]);
    }

    public function store(Request $request)
    {
        self::validatedData($request);

        $data = new ChildrenClub();
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/childclub';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $this->setDataValue($request, $data);

        $data->save();
        self::Childjson();
        session()->flash('status', 'insert');
        return redirect('admin/children-club');
    }

    public function index()
    {
        $data = ChildrenClub::paginate(10);
        return view('admin.ChildrenClub.children_club_show')->with('childrenclub', $data);
    }

    public function edit($id)
    {
        $data = ChildrenClub::findOrFail($id);
        return view('admin.ChildrenClub.children_clubAdd')->with('data', $data);
    }

    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = ChildrenClub::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = ChildrenClub::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/childclub/{$data_image->photo}")) {
                unlink("uploads/childclub/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/childclub';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::Childjson();
        session()->flash('status', 'success');
        return redirect('admin/children-club');
    }

    public function destroy($id)
    {
        $del = ChildrenClub::findOrFail($id);
        if ($del->photo && file_exists("uploads/childclub/{$del->photo}")) {
            unlink("uploads/childclub/{$del->photo}");
        }
        $data = ChildrenClub::findOrFail($id);
        $data->delete();
        self::Childjson();
        session()->flash('status', 'delet');

        return redirect('admin/children-club');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->type = $request->input('type'),
            $data->address = $request->input('address'),
            $data->girls = $request->input('girls'),
            $data->boys = $request->input('boys'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude')
        );
    }
    protected function validatedData(Request $request)
    {



        $rules = [
            'name' => 'required',
            'ward' => 'nullable',
            'address' => 'nullable',
            'type' => 'nullable',
            'girls' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'remarks' => 'nullable',
            'boys' => 'nullable',
        ];

        $customMessages = [
            'name.required' => 'भवनको नाम क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'type.required' => 'अवस्था क्षेत्र आवश्यक छ।',
            'girls.required' => 'संख्या क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            'boys.required' => 'महिलाको लागि छुट्टै भएको क्षेत्र आवश्यक छ।',
        ];

        $this->validate($request, $rules, $customMessages);
    }
    public function Childjson()
    {
        $childclub = ChildrenClub::all();
        $childclubdata = json_encode($childclub,JSON_UNESCAPED_UNICODE);
        $file = 'childclubdetails' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $childclubdata);
    }
}

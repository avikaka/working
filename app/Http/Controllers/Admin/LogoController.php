<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use App\Logo;
use File;
use Illuminate\Support\Facades\Redirect as FacadesRedirect;

class LogoController extends Controller
{
    public function createlogo(Request $request)
    {
        return view('admin.logo.logoinsert', ['data' => new Logo()]);
    }

    public function storelogo(Request $request)
    {
        $check = Logo::all();
        if (count($check) == 1) {
            return redirect('admin/viewlogo')->withErrors(['msg' => 'Cannot add another logo, please update if needed.']);
        } else {
            self::logovalidation($request);
            $fdes = new Logo();
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $destinationpath = 'uploads/logo';
                $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
                $file->move($destinationpath, $uploadedImageName);
                $fdes->image = $uploadedImageName;
            }
            $fdes->name = $request->name;
            $fdes->status = $request->status;
            $fdes->save();
            self::logojson();
            session()->flash('status', 'insert');
            return redirect('admin/viewlogo');
        }
    }

    public function viewlogo(Request $request)
    {
        $logo = Logo::all();
        return view('admin.logo.logoview', ['logo' => $logo]);
    }

    public function changeStatus($id)
    {
        $check = Logo::all();
        // dd(count($check));
        if (count($check) == 1) {
            return FacadesRedirect::back()->withErrors(['msg' => 'At least one item needs to be active']);
        } else {
            try {
                $id = (int)$id;
                $logo = Logo::find($id);
                if ($logo->status == 0) {
                    $logo->status = 1;
                    $logo->save();
                    session()->flash('status', 'change');
                    self::logojson();

                    return back();
                } else {
                    $logo->status = 0;
                    $logo->save();
                    session()->flash('status', 'change');
                    self::logojson();

                    return back();
                }
            } catch (\Exception $e) {
                $message = $e->getMessage();
                session()->flash('error', $message);
                return back();
            }
        }
    }

    public function  editlogo(Request $request, $id)
    {
        // dd('im here');
        $data = Logo::where('id', $id)->get()->first();
        return view('admin.logo.logoinsert', ['data' => $data]);
    }

    public function updatelogo(Request $request, $id)
    {
        // dd('im here');
        $count = Logo::all();

        self::logovalidation($request);
        $getedits = Logo::findorfail($id);
        if ($request->hasFile('image')) {

            $data_image = Logo::findOrFail($id);
            if ($data_image->image && file_exists("uploads/logo/{$data_image->image}")) {
                unlink("uploads/logo/{$data_image->image}");
            }
            $file =  $request->file('image');
            $destinationpath = 'uploads/logo';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $getedits->image = $uploadedImageName;
        }
        $getedits->name = $request->name;
        if (count($count) == 1) {
            $getedits->status = 1;
        } else
            $getedits->status = $request->status;

        $getedits->update();
        self::logojson();

        session()->flash('status', 'success');
        return redirect('/admin/viewlogo');
    }

    public function deletelogo($id)
    {
        $check = Logo::all();
        $test = count($check);
        if ($test == 1) {
            return FacadesRedirect::back()->withErrors(['msg' => 'Atleast one logo needs to be here.']);
        } else {
            $logo = Logo::findOrFail($id);
            if ($logo->image && file_exists("uploads/logo/{$logo->image}")) {
                unlink("uploads/logo/{$logo->image}");
            }
            $logo = Logo::destroy($id);
            Session::flash('status', 'delet');
            self::logojson();
        }



        return redirect('/admin/viewlogo');
    }


    public function logovalidation(Request $request)
    {

        $validatedData = $request->validate([
            'image.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required',
            'status' => 'required'

        ]);
    }



    public function logojson()
    {
        $data = Logo::where('status', 1)->orderBy('id', 'DESC')->take(1)->get();
        $cdata = json_encode($data,JSON_UNESCAPED_UNICODE);
        $file = 'logo' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cdata);
    }
}

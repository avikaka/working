<?php

namespace App\Http\Controllers\Admin;

use App\RoadNetwork;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use File;


class RoadNetworkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (file_exists("upload/json/roadnetworkdetails_file.json")) {
        //     $data = json_decode(file_get_contents("upload/json/roadnetworkdetails_file.json"));
        //     return view('Road_Network.road_networkdetails')->with('data', $data);
        // } else
            $data = RoadNetwork::paginate(10);
        return view('Road_Network.road_networkdetails')->with('road', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('road_network.road_network', ['data' => new RoadNetwork()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        self::validatedData($request);
        $data = new RoadNetwork();
        $this->setDataValue($request, $data);
        $data->save();
        self::Roadjson();
        session()->flash('status', 'insert');
        return redirect('admin/road-network');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bridge  $bridge
     * @return \Illuminate\Http\Response
     */
    public function show(Bridge $bridge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bridge  $bridge
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = RoadNetwork::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/road-network');
        }
        return view('road_network.road_network', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bridge  $bridge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = RoadNetwork::findOrFail($id);
        $this->setDataValue($request, $data);
        $data->update();
        self::Roadjson();
        session()->flash('status', 'success');
        return redirect('admin/road-network');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bridge  $bridge
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $data = RoadNetwork::findOrFail($id);
        $data->delete();
        self::Roadjson();
        session()->flash('status', 'delet');
        return redirect('admin/road-network');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->category = $request->input('category'),
            $data->type = $request->input('type'),
            $data->surface = $request->input('surface'),
            $data->length = $request->input('length'),
            $data->start = $request->input('start'),
            $data->end = $request->input('end'),
            $data->settlement = $request->input('settlement'),
            $data->public_transport = $request->input('public_transport'),
            $data->remarks = $request->input('remarks'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude')
        );
    }
    protected function validatedData(Request $request)
    {



        $rules = [
            'name' => 'required',
            'category' => 'nullable',
            'type' => 'nullable',
            'surface' => 'nullable',
            'length' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'remarks' => 'nullable',
            'start' => 'nullable',
            'end' => 'nullable',
            'settlement' => 'nullable',
            'public_transport' => 'nullable',

        ];

        $customMessages = [
            'name.required' => 'सडकको  नाम क्षेत्र आवश्यक छ।',
            'category.required' => 'सडकको वर्गीकरण क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'type.required' => 'प्रकार क्षेत्र आवश्यक छ।',
            'surface.required' => 'सहतको प्रकार क्षेत्र आवश्यक छ।',
            'length.required' => 'अनुमानित लम्बाई क्षेत्र आवश्यक छ।',
            'start.required' => 'सुरु बिन्दु क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            'end.required' => 'अन्तिम बिन्दु क्षेत्र आवश्यक छ।',
            'settlement.required' => 'सडकले छुने बस्तिहरु क्षेत्र आवश्यक छ।',
            'public_transport.required' => 'सार्वजनिक यातायात सञ्चालनको अवस्था क्षेत्र आवश्यक छ।',
        ];

        $this->validate($request, $rules, $customMessages);
    }

    public function Roadjson()
    {
        $road = RoadNetwork::all();
        $roaddata = json_encode($road,JSON_UNESCAPED_UNICODE);
        $file = 'roadnetworkdetails' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $roaddata);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\AgroVetPocket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use File;

class AgroVetPocketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (file_exists("upload/json/agropocketdetails_file.json")) {
        //     $datas = json_decode(file_get_contents("upload/json/agropocketdetails_file.json"));
        //     return view('agro.agro_details')->with('datas', $datas);
        // } else
            $agropocket = AgroVetPocket::paginate(10);
        return view('agro.agro_details')->with('agropocket', $agropocket);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agro.agro', ['data' => new AgroVetPocket()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        self::validatedData($request);
        $data = new AgroVetPocket();
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/agropocket';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $data->save();
        self::Agropocketjson();
        geoJson(AgroVetPocket::class,'agroVetPocket');
        session()->flash('status', 'insert');
        return redirect('admin/agro-vet-pocket');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = AgroVetPocket::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/agro-vet-pocket');
        }
        return view('agro.agro', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bridge  $bridge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = AgroVetPocket::findOrFail($id);
        if ($request->hasFile('photo')) {

            $data_image = AgroVetPocket::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/agropocket/{$data_image->photo}")) {
                unlink("uploads/agropocket/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/agropocket';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $this->setDataValue($request, $data);
        $data->update();
        self::Agropocketjson();
        geoJson(AgroVetPocket::class,'agroVetPocket');
        session()->flash('status', 'success');
        return redirect('admin/agro-vet-pocket');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $del = AgroVetPocket::findOrFail($id);
        if ($del->photo && file_exists("uploads/agropocket/{$del->photo}")) {
            unlink("uploads/agropocket/{$del->photo}");
        }
        $data = AgroVetPocket::findOrFail($id);
        $data->delete();
        self::Agropocketjson();
        geoJson(AgroVetPocket::class,'agroVetPocket');
        session()->flash('status', 'delet');
        return redirect('admin/agro-vet-pocket');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->address = $request->input('address'),
            $data->sector = $request->input('sector'),
            $data->type = $request->input('type'),
            $data->project_level = $request->input('project_level'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude')
        );
    }
    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'ward' => 'nullable',
            'address' => 'nullable',
            'type' => 'nullable',
            'sector' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'remarks' => 'nullable',
            'project_level' => 'nullable',


        ];

        $customMessages = [
            'name.required' => 'पंक्षी पकेट क्षेत्रको नाम आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'sector.required' => ' क्षेत्र आवश्यक छ।',
            'type.required' => 'मुख्य बाली वा पशु पंक्षीको जात क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            'project_level.required' => 'कार्यन्वयन बजेटको श्रोत क्षेत्र आवश्यक छ।',




        ];

        $this->validate($request, $rules, $customMessages);
    }

    public function Agropocketjson()
    {
        $agro = AgroVetPocket::all();
        $agrodata = json_encode($agro,JSON_UNESCAPED_UNICODE);
        $file = 'agropocketdetails' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $agrodata);
    }
}

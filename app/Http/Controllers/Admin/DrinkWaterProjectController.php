<?php


namespace App\Http\Controllers\Admin;

use App\DrinkWaterProject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use File;

class DrinkWaterProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        // if (file_exists("upload/json/drinkwater_file.json")) {
        //     $data = json_decode(file_get_contents("upload/json/drinkwater_file.json"));
        //     return view('DrinkWater.drink_water_details')->with('data', $data);
        // } else
            $data = DrinkWaterProject::paginate(10);
        return view('DrinkWater.drink_water_details')->with('drinkwater', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('DrinkWater.drink_water', ['data' => new DrinkWaterProject()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        self::validatedData($request);
        $data = new DrinkWaterProject();
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/drinkwater';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->save();
        self::Drinkwaterjson();
        session()->flash('status', 'insert');
        return redirect('admin/drinking-water');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DDrinkWaterProject  $dDrinkWaterProject
     * @return \Illuminate\Http\Response
     */
    public function show(DDrinkWaterProject $dDrinkWaterProject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DDrinkWaterProject  $dDrinkWaterProject
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $data = DrinkWaterProject::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/drinking-water');
        }
        return view('DrinkWater.drink_water', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bridge  $bridge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        self::validatedData($request);

        $data = DrinkWaterProject::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = DrinkWaterProject::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/drinkwater/{$data_image->photo}")) {
                unlink("uploads/drinkwater/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/drinkwater';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::Drinkwaterjson();
        session()->flash('status', 'success');

        return redirect('admin/drinking-water');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DDrinkWaterProject  $dDrinkWaterProject
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $del = DrinkWaterProject::findOrFail($id);
        if ($del->photo && file_exists("uploads/drinkwater/{$del->photo}")) {
            unlink("uploads/drinkwater/{$del->photo}");
        }
        $data = DrinkWaterProject::findOrFail($id);
        $data->delete();
        self::Drinkwaterjson();
        session()->flash('status', 'delet');
        return redirect('admin/drinking-water');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->source = $request->input('source'),
            $data->type = $request->input('type'),
            $data->beneficiary = $request->input('beneficiary'),
            $data->beneficiary_settlement = $request->input('beneficiary_settlement'),
            $data->contact_person = $request->input('contact_person'),
            $data->contact_no = $request->input('contact_no'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude'),
            $data->remarks = $request->input('remarks')
        );
    }

    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'source' => 'required',
            'ward' => 'nullable',
            'type' => 'nullable',
            'contact_person' => 'nullable',
            'contact_no' => 'nullable',
            'beneficiary' => 'nullable',
            'beneficiary_settlement' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'remarks' => 'nullable',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
        $customMessages = [
            'name.required' => 'खानेपानी आयोजनाको नाम क्षेत्र आवश्यक छ।',
            'source.required' => 'मुहान क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'beneficiary.required' => 'लाभान्वित घरधुरी क्षेत्र आवश्यक छ।',
            'beneficiary_settlement.required' => 'सेवा प्राप्त गर्ने क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'type.required' => 'प्रकार क्षेत्र आवश्यक छ।',
            'contact_person.required' => 'सम्पर्क व्यक्ति क्षेत्र आवश्यक छ।',
            'contact_no.required' => 'सम्पर्क नं क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
        ];
        $this->validate($request, $rules, $customMessages);
    }

    public function Drinkwaterjson()
    {
        $data = DrinkWaterProject::all();
        $cdata = json_encode($data,JSON_UNESCAPED_UNICODE);
        $file = 'drinkwater' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cdata);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Club;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClubController extends Controller
{
    public function index()
    {
        $data = Club::paginate(10);
        return view('admin.Club.club_show')->with('club', $data);
    }
    public function create()
    {
        return view('admin.Club.clubAdd');
    }

    public function store(Request $request)
    {
        $data = new Club();
        $this->setDataValue($request, $data);
        $data->save();
        $request->session()->flash('msg', 'Data saved successfully !');
        return redirect('admin/club');
    }



    public function edit($id)
    {
        $data = Club::findOrFail($id);
        return view('admin.Club.clubAdd')->with('data', $data);
    }

    public function update(Request $request, $id)
    {
        $data = Club::findOrFail($id);
        $this->setDataValue($request, $data);
        $data->update();
        $request->session()->flash('msg', 'Data saved successfully !');
        return redirect('admin/club');
    }

    public function destroy($id)
    {
        $data = Club::findOrFail($id);
        $data->delete();

        return redirect('admin/club');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->type = $request->input('type'),
            $data->address = $request->input('address'),
            $data->members = $request->input('members'),
            $data->contact_person = $request->input('contact_person'),
            $data->contact_no = $request->input('contact_no'),
            $data->remarks = $request->input('remarks'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude')
        );
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Bank;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Pagination\Paginator;

class BankController extends Controller
{
    public function create()
    {
        return view('admin.Bank.bankAdd', ['data' => new Bank()]);
    }

    public function store(Request $request)
    {
        self::validatedData($request);
        $data = new Bank();
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/bank';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->save();
        self::Bankjson();
        geoJson(Bank::class,'bank');
        session()->flash('status', 'insert');

        return redirect('admin/bank');
    }

    public function index()
    {
        // if (file_exists("upload/json/bankdetails_file.json")) {
        //     $data = $this->paginate(json_decode(file_get_contents("upload/json/bankdetails_file.json")));
        //     return view('admin.Bank.bank_show')->with('data', $data);
        // } else
            $bank = Bank::paginate(10);
        return view('admin.Bank.bank_show')->with('bank', $bank);
    }

    // public function paginate($items, $perPage = 5, $page = '', $options = [])
    // {

    //     $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

    //     $items = $items instanceof Collection ? $items : Collection::make($items);

    //     return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    // }

    public function edit($id)
    {
        $data = Bank::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/bank');
        }
        return view('admin.Bank.bankAdd')->with('data', $data);
    }

    public function update(Request $request, $id)
    {
        self::validatedData($request);

        $data = Bank::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = Bank::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/bank/{$data_image->photo}")) {
                unlink("uploads/bank/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/bank';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::Bankjson();
        geoJson(Bank::class,'bank');
        session()->flash('status', 'success');
        return redirect('admin/bank');
    }

    public function search(Request $request)
    {
        $search = $request->get('BankReq');
        $t = json_decode(file_get_contents("upload/json/bankdetails_file.json"));
        foreach ($t as $k) {
            $data = $k::where($k->name, 'like', '%' . $search . '%')
                ->get();
        }
        return json_encode($data);


        // $item = [];
        // for ($i = 0; $i < count($data); $i++) {
        //     if ($data[$i]->name == $search) {
        //         array_push($item, $data[$i]);
        //         // dd($item);
        //     }
        // }
        // if (!empty($item)) {
        //     return view('admin.Bank.bank_show')->with('data', $item);
        // } else {
        //     $request->session()->flash('msg', 'Data not found !');
        //     return redirect('admin/bank');
        // }
        // foreach ($data as $data) {
        //     // dd($data->name);
        //     if ($data->name == $search) {

        //         return view('admin.Bank.bank_show')->with('data', $data);
        //     } else {
        //         $request->session()->flash('msg', 'Data not found !');
        //         return redirect('admin/bank');
        //     }
        // }
        // $data = DB::('banks')->where('name', 'like', '%' . $search . '%')->paginate(5);
    }

    public function destroy($id)
    {
        $del = Bank::findOrFail($id);
        if ($del->photo && file_exists("uploads/bank/{$del->photo}")) {
            unlink("uploads/bank/{$del->photo}");
        }
        $data = Bank::findOrFail($id);
        $data->delete();
        self::Bankjson();
        geoJson(Bank::class,'bank');
        session()->flash('status', 'delet');
        return redirect('admin/bank');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->type = $request->input('type'),
            $data->address = $request->input('address'),
            $data->branch_type = $request->input('branch_type'),
            $data->contact_person = $request->input('contact_person'),
            $data->contact_number = $request->input('contact_number'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude'),
            $data->remarks = $request->input('remarks'),

        );
    }

    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'ward' => 'required',
            'address' => 'nullable',
            'type' => 'nullable',
            'branch_type' => 'nullable',
            'contact_number' => 'nullable',
            'contact_person' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'image.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'


        ];
        $customMessages = [
            'name.required' => 'बैँक तथा बित्तीयसंस्थाको नाम क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'type.required' => 'प्रकार क्षेत्र आवश्यक छ।',
            'branch_type.required' => 'साखाको प्रकार क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            'contact_number.required' => 'सम्पर्क नं. क्षेत्र आवश्यक छ।',
            'contact_person.required' => 'सम्पर्क वयक्ति. क्षेत्र आवश्यक छ।',

        ];

        $this->validate($request, $rules, $customMessages);
    }

    public function Bankjson()
    {
        $bank = Bank::all();
        $bankdata = json_encode($bank,JSON_UNESCAPED_UNICODE);
        $file = 'bankdetails' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $bankdata);
    }



    // public function banktype($type)
    // {
    //     $bank = Bank::where('type', '=', $type)->get();
    //     $bdata = Bank::select('type', DB::raw('count(*) as count'))
    //         ->groupBy('type')->where('type', '!=', $type)
    //         ->get();

    //     return view('bank.bankdetails', compact('bank', 'bdata'));
    // }
}

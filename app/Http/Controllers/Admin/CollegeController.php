<?php

namespace App\Http\Controllers\Admin;

use App\College;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;


class CollegeController extends Controller
{
    public function create()
    {
        return view('admin.college.college_add', ['data' => new College()]);
    }

    public function store(Request $request)
    {
        self::validatedData($request);

        $data = new College();
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/college';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $this->setDataValue($request, $data);

        $data->save();
        self::Collegejson();
        geoJson(College::class,'college');
        session()->flash('status', 'insert');
        return redirect('admin/college');
    }

    public function index()
    {
        $data  = College::paginate(10);
        return view('admin.college.college_show')->with('college', $data);
    }

    public function edit($id)
    {
        $data  = College::findOrFail($id);
        return view('admin.college.college_add')->with('data', $data);
    }

    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = College::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = College::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/college/{$data_image->photo}")) {
                unlink("uploads/college/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/college';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::Collegejson();
        geoJson(College::class,'college');
        session()->flash('status', 'success');
        return redirect('admin/college');
    }

    public function destroy($id)
    {
        $del = College::findOrFail($id);
        if ($del->photo && file_exists("uploads/college/{$del->photo}")) {
            unlink("uploads/college/{$del->photo}");
        }
        $data = College::findOrFail($id);
        $data->delete();
        self::Collegejson();
        geoJson(College::class,'college');
        session()->flash('status', 'delet');
        return redirect('admin/college');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->programmes = $request->input('programmes'),
            $data->address = $request->input('address'),
            $data->type = $request->input('type'),
            $data->boys = $request->input('boys'),
            $data->girls = $request->input('girls'),
            $data->contact_person = $request->input('contact_person'),
            $data->contact_no = $request->input('contact_no'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude')
        );
    }

    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'ward' => 'nullable',
            'address' => 'nullable',
            'type' => 'nullable',
            'programmes' => 'nullable',
            'boys' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'remarks' => 'nullable',
            'girls' => 'nullable',
            'contact_person' => 'nullable',
            'contact_no' => 'nullable',
        ];
        $customMessages = [
            'name.required' => 'शैक्षिक संस्थाको नाम क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'type.required' => 'प्रकार क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'programmes.required' => 'सञ्चालित कार्यक्रम तथा तह क्षेत्र आवश्यक छ।',
            'boys.required' => 'boys क्षेत्र आवश्यक छ।',
            'girls.required' => 'छात्रा क्षेत्र आवश्यक छ।',
            'contact_person.required' => 'सम्पर्क व्यक्ति आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार आवश्यक छ।',
            'contact_no.required' => 'सम्पर्क नं आवश्यक छ।',
        ];
        $this->validate($request, $rules, $customMessages);
    }

    public function Collegejson()
    {
        $college = College::all();
        $collegedata = json_encode($college,JSON_UNESCAPED_UNICODE);
        $file = 'collegedetails' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $collegedata);
    }

    // public function geoJson(){

    //     $college = College::all();

    //     $emptyArray = '';

    //     foreach($college as $row){
    //         $features = "{
    //                 'type': 'Feature',
    //                 'properties': {
    //                     'name' : '". $row->name ."',
    //                     'address' : '". $row->type ."'
    //                 },
    //                 'geometry': {
    //                 'type': 'Point',
    //                 'coordinates': [" . $row->longitude . ", " . $row->latitude ."]
    //                 }
    //             },";

    //             $emptyArray .= $features;
    //             }

    //             $alternative =
    //             "{
    //                 'type': 'FeatureCollection',
    //                 'features': [
    //                 ${emptyArray}
    //                 ]
    //             }";

    //         $file = 'geo_college' . '_file.json';
    //         $destinationPath = public_path() . "/upload/geo/";
    //         if (!is_dir($destinationPath)) {
    //             mkdir($destinationPath, 0777, true);
    //         }
    //         File::put($destinationPath . $file, $alternative);

    //     }
}

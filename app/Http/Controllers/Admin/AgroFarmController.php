<?php

namespace App\Http\Controllers\Admin;

use App\AgroFarm;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use File;

class AgroFarmController extends Controller
{
    public function create()
    {
        return view('admin.AgroFarm.agro_farm_add', ['data' => new AgroFarm()]);
    }

    public function store(Request $request)
    {
        self::validatedData($request);

        $data = new AgroFarm();
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/agrofarm';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $this->setDataValue($request, $data);
        $data->save();
        self::Agrojson();
        geoJson(AgroFarm::class,'agroFarm');
        session()->flash('status', 'insert');
        return redirect('admin/agro-farm');
    }

    public function index()
    {
        // if (file_exists("upload/json/agrofarmdetails_file.json")) {
        //     $data = json_decode(file_get_contents("upload/json/agrofarmdetails_file.json"));
        //     return view('admin.AgroFarm.agro_farm_show')->with('data', $data);
        // } else
            $agrofarm = AgroFarm::paginate(10);
        return view('admin.AgroFarm.agro_farm_show')->with('agrofarm', $agrofarm);
    }

    public function edit($id)
    {
        $data = AgroFarm::findOrFail($id);
        return view('admin.AgroFarm.agro_farm_add')->with('data', $data);
    }

    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = AgroFarm::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = AgroFarm::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/agrofarm/{$data_image->photo}")) {
                unlink("uploads/agrofarm/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/agrofarm';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::Agrojson();
        geoJson(AgroFarm::class,'agroFarm');
        session()->flash('status', 'success');
        return redirect('admin/agro-farm');
    }

    public function destroy($id)
    {
        $del = AgroFarm::findOrFail($id);
        if ($del->photo && file_exists("uploads/agrofarm/{$del->photo}")) {
            unlink("uploads/toilet/{$del->photo}");
        }
        $data = AgroFarm::findOrFail($id);
        $data->delete();
        self::Agrojson();
        geoJson(AgroFarm::class,'agroFarm');
        session()->flash('status', 'delet');
        return redirect('admin/agro-farm');
    }

    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->type = $request->input('type'),
            $data->address = $request->input('address'),
            $data->contact_person = $request->input('contact_person'),
            $data->contact_no = $request->input('contact_no'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude')
        );
    }

    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'ward' => 'required',
            'address' => 'nullable',
            'type' => 'nullable',
            'contact_person' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'remarks' => 'nullable',
            'contact_no' => 'nullable',
        ];

        $customMessages = [
            'name.required' => 'कृषि फर्मको नाम क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'type.required' => 'फर्मको किसिम क्षेत्र आवश्यक छ।',
            'contact_no.required' => 'सम्पर्क नं क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            'contact_person.required' => 'सम्पर्क वयक्ति क्षेत्र आवश्यक छ।',
        ];

        $this->validate($request, $rules, $customMessages);
    }

    public function Agrojson()
    {
        $agro = AgroFarm::all();
        $agrodata = json_encode($agro,JSON_UNESCAPED_UNICODE);
        $file = 'agrofarmdetails' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $agrodata);
    }
}

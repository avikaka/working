<?php

namespace App\Http\Controllers\Admin;

use App\CommunityForest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use File;
use DB;

class CommunityForestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = CommunityForest::paginate(10);
        return view('community.community_forest_details', ['communityforest' => $datas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('community.community_forest', ['data' => new CommunityForest()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        self::validatedData($request);
        // if ()) {
        $data = new CommunityForest();
        $this->setDataValue($request, $data);
        // dd($data);
        $data->save();
        self::json();
        session()->flash('status', 'insert');
        return redirect('admin/community-forest');
        // } else {
        //     return redirect('admin.irrigation.create')->withErrors($validator)->withInput();
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = CommunityForest::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/community-forest');
        }
        return view('community.community_forest', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = CommunityForest::findOrFail($id);
        $this->setDataValue($request, $data);
        $data->update();
        self::json();
        session()->flash('status', 'success');
        return redirect('admin/community-forest');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {

        $data = CommunityForest::findOrFail($id);
        $data->delete();
        self::json();
        session()->flash('status', 'delet');
        return redirect('admin/community-forest');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->forest_area = $request->input('forest_area'),
            $data->occupied_area = $request->input('occupied_area'),
            $data->member_household = $request->input('member_household'),
            $data->latitude = $request->input('latitude')
            // $data->longitude = $request->input('longitude')
        );
    }
    protected function validatedData(Request $request)
    {
        $rules =  [
            'name' => 'required',
            'ward' => 'nullable',
            'forest_area' => 'nullable',
            'occupied_area' => 'nullable',
            'member_household' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'remarks' => 'nullable',
            // 'photo' => 'nullable||image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];

        $customMessages = [
            'name.required' => 'सामुदायिक वनको नाम क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'forest_area.required' => 'वनको क्षेत्रफल आवश्यक छ।',
            'occupied_area.required' => 'ओगटेको क्षेत्रफल आवश्यक छ।',
            'member_household.required' => 'सामुदायिक वनको सदस्य घरधुरी आवश्यक छ।',
            'la.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            // 'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
        ];
        $this->validate($request, $rules, $customMessages);
        // dd($customMessages);
    }

    public function json()
    {
        $data = CommunityForest::all();
        $cdata = json_encode($data,JSON_UNESCAPED_UNICODE);
        $file = 'communityforest' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cdata);
    }
}

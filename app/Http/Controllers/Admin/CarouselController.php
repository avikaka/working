<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use App\Carousel;
use File;


class CarouselController extends Controller
{
    public function createcarousel(Request $request)
    {

        return view('admin.carousel.carouselinsert', ['data' => new Carousel()]);
    }

    public function storecarousel(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $fdes = new Carousel();
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $destinationpath = 'uploads/carousel';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $fdes->heading = $request->heading;
            $fdes->caption = $request->caption;
            $fdes->image = $uploadedImageName;
        }
        $fdes->status = $request->status;
        $fdes->save();
        self::carouseljson();
        session()->flash('status', 'insert');
        return redirect('/admin/viewcarousel')->with('success', 'Image uploaded successfully');
    }


    public function viewcarousel(Request $request)
    {
        // if (file_exists("upload/json/carousel_file.json")) {
        //     $data = json_decode(file_get_contents("upload/json/carousel_file.json"));
        //     return view('admin.carousel.carouselview')->with('carousel', $data);
        // } else
            $data = Carousel::paginate(10);
        return view('admin.carousel.carouselview')->with('carousel', $data);
    }


    public function changeStatus($id)
    {
        try {
            $id = (int)$id;
            $carousel = Carousel::find($id);
            if ($carousel->status == 0) {
                $carousel->status = 1;
                $carousel->save();
                session()->flash('status', 'change');
                self::carouseljson();

                return back();
            } else {
                $carousel->status = 0;
                $carousel->save();
                session()->flash('status', 'change');
                self::carouseljson();

                return back();
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            session()->flash('error', $message);
            return back();
        }
    }


    public function  editcarousel(Request $request, $id)
    {
        // dd('im here');
        $data = Carousel::where('id', $id)->get()->first();
        return view('admin.carousel.carouselinsert', ['data' => $data]);
    }




    public function updatecarousel(Request $request, $id)
    {

        // dd('im here');
        self::carouselvalidation($request);
        $getedits = Carousel::findorfail($id);
        if ($request->hasFile('image')) {

            $data_image = Carousel::findOrFail($id);
            if ($data_image->image && file_exists("uploads/carousel/{$data_image->image}")) {
                unlink("uploads/carousel/{$data_image->image}");
            }
            $file =  $request->file('image');
            $destinationpath = 'uploads/carousel';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $getedits->heading = $request->heading;
            $getedits->caption = $request->caption;
            $getedits->image = $uploadedImageName;
        }

        $getedits->status = $request->status;
        $getedits->update();
        self::carouseljson();


        session()->flash('status', 'success');
        return redirect('/admin/viewcarousel');
    }



    public function deletecarousel($id)
    {
        $carousel = Carousel::findOrFail($id);
        if ($carousel->image && file_exists("uploads/carousel/{$carousel->image}")) {
            unlink("uploads/carousel/{$carousel->image}");
        }
        $carousel = Carousel::destroy($id);
        Session::flash('status', 'delet');
        self::carouseljson();

        return redirect('/admin/viewcarousel');
    }


    public function carouselvalidation(Request $request)
    {

        $validatedData = $request->validate([
            'image.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ]);
    }



    public function Carouseljson()
    {
        $data = Carousel::where('status', 1)->orderBy('id', 'DESC')->take(3)->get();
        $cdata = json_encode($data,JSON_UNESCAPED_UNICODE);
        $file = 'carousel' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cdata);
    }
}

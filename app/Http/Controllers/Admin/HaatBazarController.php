<?php

namespace App\Http\Controllers\Admin;

use App\HaatBazar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use File;


class HaatBazarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (file_exists("upload/json/haatbazar_file.json")) {
        //     $data = json_decode(file_get_contents("upload/json/haatbazar_file.json"));
        //     return view('haat_bazar.haat_bazar_details')->with('data', $data);
        // } else
            $data = HaatBazar::paginate(10);
        return view('haat_bazar.haat_bazar_details')->with('haatbazar', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('haat_bazar.haat_bazar', ['data' => new HaatBazar()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        self::validatedData($request);
        $data = new HaatBazar();
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/haatbazar';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $this->setDataValue($request, $data);
        $data->save();
        self::Haatbazarjson();
        session()->flash('status', 'insert');
        return redirect('admin/haat-bazar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $data = HaatBazar::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/haat-bazar');
        }
        return view('haat_bazar.haat_bazar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        self::validatedData($request);

        $data = HaatBazar::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = HaatBazar::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/haatbazar/{$data_image->photo}")) {
                unlink("uploads/haatbazar/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/haatbazar';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }

        self::Haatbazarjson();
        $data->update();
        session()->flash('status', 'success');
        return redirect('admin/haat-bazar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $del = HaatBazar::findOrFail($id);
        if ($del->photo && file_exists("uploads/haatbazar/{$del->photo}")) {
            unlink("uploads/haatbazar/{$del->photo}");
        }
        $data = HaatBazar::findOrFail($id);
        $data->delete();
        self::Haatbazarjson();
        session()->flash('status', 'delet');
        return redirect('admin/haat-bazar');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->address = $request->input('address'),
            $data->type = $request->input('type'),
            $data->day = $request->input('day'),
            $data->interval = $request->input('interval'),
            $data->infrastructure = $request->input('infrastructure'),
            $data->remarks = $request->input('remarks'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude')


        );
    }
    protected function validatedData(Request $request)


    {
        $rules = [
            'name' => 'required',
            'ward' => 'required',
            'address' => 'nullable',
            'infrastructure' => 'nullable',
            'day' => 'nullable',
            'type' => 'nullable',
            'interval' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'remarks' => 'nullable',
        ];
        $customMessages = [
            'name.required' => 'हाट बजारस्थलको क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'infrastructure.required' => 'उपलब्ध प्रमुख पूर्वाधार क्षेत्र आवश्यक छ।',
            'type.required' => 'प्रकार क्षेत्र आवश्यक छ।',
            'day.required' => 'हाट बजार लाग्ने दिन क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            'interval.required' => 'हाट लाग्ने अन्तराल क्षेत्र आवश्यक छ।',
        ];
        $this->validate($request, $rules, $customMessages);
    }

    public function Haatbazarjson()
    {
        $haat = HaatBazar::all();
        $haatdata = json_encode($haat,JSON_UNESCAPED_UNICODE);
        $file = 'haatbazar' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $haatdata);
    }
}

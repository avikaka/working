<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Person;
use App\Value;
use App\ValueSet;
use App\Cover;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use File;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;


use Illuminate\Pagination\LengthAwarePaginator;




class BirthcertificationController extends Controller
{

    //For birth
    public function index()
    {
        $data = Cover::paginate(10);
        return view('admin.birth-death.ajax', compact('data'));
    }

    public function cover_ajax(Request $request)
    {
        // dd($request->ajax());
        if ($request->ajax()) {
            $data = Cover::paginate(10);
            // dd($data);
            return view('ajax_pag', compact('data'));
        }
    }

    public function search(Request $request)
    {
        $search = $request->get('searchReq');
        $tole = $request->get('toleReq');
        $road = $request->get('roadReq');

        if (!empty($tole) and !empty($search) and !empty($road)) {
            $data = Cover::where('RESPONDENT', 'like', '%' . $search . '%')
                ->Where('tole', 'like', '%' . $tole . '%')
                ->Where('road', 'like', '%' . $road . '%')
                ->get();

            return json_encode($data);
        } elseif (empty($tole) and !empty($search) and !empty($road)) {
            $data = Cover::where('RESPONDENT', 'like', '%' . $search . '%')
                ->Where('road', 'like', '%' . $road . '%')
                ->get();

            return json_encode($data);
        } elseif (!empty($tole) and !empty($search) and empty($road)) {
            $data = Cover::where('RESPONDENT', 'like', '%' . $search . '%')
                ->Where('tole', 'like', '%' . $tole . '%')
                ->get();

            return json_encode($data);
        } elseif (!empty($tole) and empty($search) and !empty($road)) {
            $data = Cover::where('tole', 'like', '%' . $tole . '%')
                ->Where('road', 'like', '%' . $road . '%')
                ->get();

            return json_encode($data);
        } elseif (empty($tole) and empty($search) and !empty($road)) {
            $data = Cover::Where('road', 'like', '%' . $road . '%')
                ->get();

            return json_encode($data);
        } elseif (empty($tole) and !empty($search) and empty($road)) {
            $data = Cover::where('RESPONDENT', 'like', '%' . $search . '%')
                ->get();

            // $data = $data->paginate(5);
            // dd($data);

            return json_encode($data);
        } elseif (!empty($tole) and empty($search) and empty($road)) {
            $data = Cover::where('tole', 'like', '%' . $tole . '%')->get();


            return json_encode($data);
        }
    }

    public function filterSearch(Request $request){

        $ward  = $request->get('ward');
        $caste = $request->get('caste');
        $religion = $request->get('religion');
        $education = $request->get('education');

        if (!empty($ward) and !empty($caste) and !empty($religion)) {
            $data = Person::where('WARD', 'like', '%' . $ward . '%')
                ->Where('CASTE', 'like', '%' . $caste . '%')
                ->Where('RELIGION', 'like', '%' . $religion . '%')->paginate(10);

            return (json_encode($data));
        }
        elseif (!empty($ward) and !empty($caste) and empty($religion)) {
            $data = Person::where('WARD', 'like', '%' . $ward . '%')
                ->Where('CASTE', 'like', '%' . $caste . '%')
                ->paginate(10);

            return (json_encode($data));
        }
        elseif (!empty($ward) and empty($caste) and empty($religion)) {
            $data = Person::where('WARD', 'like', '%' . $ward . '%')
                ->paginate(10);

            return (json_encode($data));
        }
        elseif (!empty($ward) and empty($caste) and !empty($religion)) {
            $data = Person::where('WARD', 'like', '%' . $ward . '%')
                    ->Where('RELIGION', 'like', '%' . $religion . '%')->paginate(10);

            return (json_encode($data));
        }
        elseif (empty($ward) and !empty($caste) and !empty($religion)) {
            $data = Person::where('CASTE', 'like', '%' . $caste . '%')
                    ->Where('RELIGION', 'like', '%' . $religion . '%')->paginate(10);

            return (json_encode($data));
        }
        elseif (empty($ward) and !empty($caste) and !empty($religion)) {
            $data = Person::where('CASTE', 'like', '%' . $caste . '%')
                    ->paginate(10);

            return (json_encode($data));
        }
        elseif (empty($ward) and empty($caste) and !empty($religion)) {
            $data = Person::where('RELIGION', 'like', '%' . $religion . '%')
                    ->paginate(10);

            return (json_encode($data));
        }
        else
        return response()->json(['foo'=>'bar']);
    }

    public function createbirth(Request $request, $household_id)
    {
        $data = $household_id;
        return view('admin.birth-death.birthcertification', ['data' => $data]);
    }

    public function person_ajax(Request $request)
    {
        if ($request->ajax()) {
            $data = Person::paginate(10);
            return view('admin.birth-death.person_pag', compact('data'));
        }
    }

    public function storebirth(Request $request)
    {
        $fdes = new Person;
        $fdes->NAME = $request->name;
        $fdes->HOUSEHOLD_ID = $request->household_id;
        $fdes->SEX = $request->sex;

        $birthday = $request->date;

        $user_age = Carbon::parse($birthday)->diff(Carbon::now())->format('%y');
        $fdes->AGE = $user_age;
        $data = Person::where('HOUSEHOLD_ID', $request->household_id)->get()->last();
        $fdes->PROV = $data->PROV;
        $fdes->DIST = $data->DIST;
        $fdes->WARD = $data->WARD;
        $fdes->MUN = $data->MUN;
        $fdes->NATIONALITY = $data->NATIONALITY;
        $fdes->CASTE = $data->CASTE;
        $fdes->MOTHER_TONGUE = $data->MOTHER_TONGUE;
        $fdes->LINE = 1 + ($data->LINE);
        $fdes->save();
        // $cover_id = Cover::where('HOUSEHOLD_ID', $request->household_id)->get('id')->first();
        $Cover = Cover::where('HOUSEHOLD_ID', $request->household_id)->update(['MEMBERS' => 1 + ($data->LINE)]);

        session()->flash('status', 'insert');
        return redirect('/admin/householdsearch');
    }

    //For death


    public function view()
    {
        $data = Person::paginate(10);
        return view('admin.birth-death.personsearch', compact('data'));
    }


    public function person(Request $request)
    {
        $name = $request->get('nameReq');
        $house = $request->get('houseReq');

        if (!empty($name) and  !empty($house)) {
            $data = Person::where('NAME', 'like', '%' . $name . '%')

                ->Where('HOUSEHOLD_ID', 'like', '%' . $house . '%')
                ->get();

            return json_encode($data);
        } elseif (empty($name)  and !empty($house)) {
            $data = Person::Where('HOUSEHOLD_ID', 'like', '%' . $house . '%')

                ->get();

            return json_encode($data);
        } elseif (!empty($name)  and empty($house)) {
            $data = Person::where('NAME', 'like', '%' . $name . '%')

                ->get();

            return json_encode($data);
        }
    }

    public function createdeath(Request $request, $id,$hId)
    {
        $data = Person::where('NAME',$id)->where('HOUSEHOLD_ID',$hId)->get()->first();
        // dd($data);
        return view('admin.birth-death.deathinsert', ['data' => $data]);
    }

    public function storedeath(Request $request, $id, $hId)
    {
        $getedits = Person::where('NAME',$id)->where('HOUSEHOLD_ID',$hId)->get()->first();
        if ($getedits->death == $request->death) {
            return redirect('/admin/personsearch');
        } else {
            if ($request->death == 0) {
                $countLine = DB::select("SELECT COUNT(LINE) AS TOTAL FROM persons WHERE HOUSEHOLD_ID=$hId AND death=0;");
                    foreach($countLine as $d){
                        $t = Person::where('NAME', $id)->where('HOUSEHOLD_ID',$hId)->update(['LINE' => $d->TOTAL,'death' => 0]);
                        $cov = Cover::where('HOUSEHOLD_ID',$hId)->update(['MEMBERS'=>$d->TOTAL+1]);
                    }
                    $data = Person::where('NAME', '=', $id)->where('HOUSEHOLD_ID', '=', $getedits->HOUSEHOLD_ID)->get();
            } else {
                $t = Person::where('NAME',$id)->where('HOUSEHOLD_ID',$hId)->update(['LINE' => 0, 'death' => 1]);
                $data = Person::where('NAME', '=', $id)->where('HOUSEHOLD_ID', '=', $getedits->HOUSEHOLD_ID)->get();
                $maxMem = DB::select("SELECT MAX(MEMBERS) AS TOTAL FROM cover WHERE HOUSEHOLD_ID=$hId");
                foreach ($maxMem as $k) {
                    $cov = Cover::where('HOUSEHOLD_ID',$hId)->update(['MEMBERS'=>$k->TOTAL-1]);
                }
            }
        }
        return redirect('/admin/personsearch');
    }

    public function personsdetail(Request $request, $id)
    {
        $household = Cover::where('HOUSEHOLD_ID', $id)->get()->first();

        $data = Person::where('HOUSEHOLD_ID', $id)->where('death', 0)->get()->all();

        $shipments = json_decode(file_get_contents("upload/json/mothertounge_file.json"), true);
        $table = $shipments['final_data'];
        $title = $shipments['title'];
        $keys = array_values($title);
        $count = count($title);
        // dd($title);
        // foreach ($data as $key => $value) {
        //     dd($value);
        // }


        // Value::joinRelation('valueSet.Value.persons', function ($join) {
        //     $join->where('Value.value_set_id', '=', 6)
        // });
        // dd($data);
        return view('admin.birth-death.memberslistshow', ['household' => $household, 'data' => $data, 'keys'=> $keys]);
    }

    public function getDetail(Request $request, $id)
    {
        $getedits = Person::where('id',$id)->get();

        return json_encode($getedits);
    }

    public function paginate($items, $perPage = 5, $page = '', $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}

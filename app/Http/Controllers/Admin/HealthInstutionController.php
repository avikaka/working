<?php

namespace App\Http\Controllers\Admin;

use App\HealthInstitution;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use File;
use Illuminate\Support\Facades\DB;

class HealthInstutionController extends Controller
{
    public function create()
    {
        return view('admin.HealthInstution.health-institutionAdd', ['data' => new HealthInstitution()]);
    }

    public function store(Request $request)
    {
        self::validatedData($request);
        $data = new HealthInstitution();
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/healthinst';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $this->setDataValue($request, $data);

        $data->save();
        self::Healthjson();
        session()->flash('status', 'insert');
        return redirect('admin/health-institution');
    }

    public function index()
    {
        $data = HealthInstitution::paginate(10);
        return view('admin.HealthInstution.health_instution_show')->with('healthinstitution', $data);
    }

    public function edit($id)
    {
        $data = HealthInstitution::findOrFail($id);
        return view('admin.HealthInstution.health-institutionAdd')->with('data', $data);
    }

    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = HealthInstitution::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = HealthInstitution::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/healthinst/{$data_image->photo}")) {
                unlink("uploads/healthinst/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/healthinst';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::Healthjson();
        session()->flash('status', 'success');
        return redirect('admin/health-institution');
    }

    public function destroy($id)
    {
        $del = HealthInstitution::findOrFail($id);
        if ($del->photo && file_exists("uploads/healthinst/{$del->photo}")) {
            unlink("uploads/healthinst/{$del->photo}");
        }
        $data = HealthInstitution::findOrFail($id);
        $data->delete();
        self::Healthjson();
        session()->flash('status', 'delet');
        return redirect('admin/health-institution');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->address = $request->input('address'),
            $data->services = $request->input('services'),
            $data->type = $request->input('type'),
            $data->contact_person = $request->input('contact_person'),
            $data->contact_no = $request->input('contact_no'),
            $data->remarks = $request->input('remarks'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude')

        );
    }

    protected function validatedData(Request $request)
    {



        $rules = [
            'name' => 'required',
            'ward' => 'required',
            'address' => 'nullable',
            'services' => 'nullable',
            'type' => 'nullable',
            'contact_person' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'remarks' => 'nullable',
            'contact_no' => 'nullable',
        ];

        $customMessages = [
            'name.required' => 'स्वास्थ्य संथाको नाम क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'services.required' => 'स्वास्थ्य संस्थामा उपलब्ध
            सेवाहरु क्षेत्र आवश्यक छ।',
            'type.required' => 'प्रकार क्षेत्र आवश्यक छ।',
            'contact_person.required' => 'सम्पर्क वयक्ति क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            'contact_no.required' => 'सम्पर्क नं क्षेत्र आवश्यक छ।',
        ];

        $this->validate($request, $rules, $customMessages);
    }
    public function Healthjson()
    {
        $health = HealthInstitution::all();
        $healthdata = json_encode($health,JSON_UNESCAPED_UNICODE);
        $file = 'healthinstutiondetails' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $healthdata);
    }

    // public function healthtype($type)
    // {
    //     $health = HealthInstitution::where('type', '=', $type)->get();
    //     $hdata = HealthInstitution::select('type', DB::raw('count(*) as count'))
    //         ->groupBy('type')->where('type', '!=', $type)
    //         ->get();

    //     return view('bank.healthdetails', compact('health', 'hdata'));
    // }
}

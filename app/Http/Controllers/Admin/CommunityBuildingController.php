<?php

namespace App\Http\Controllers\Admin;

use App\CommunityBuilding;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use App\Http\Controllers\Controller;
use Validator;
use DB;
use File;


class CommunityBuildingController extends Controller
{

    public function index()
    {
        $data = CommunityBuilding::paginate(10);
        return view('admin.CommunityBuildings.community_building_show')->with('communitybuilding', $data);
    }
    public function create()
    {
        return view('admin.CommunityBuildings.community_building_add', ['data' => new CommunityBuilding()]);
    }

    public function store(Request $request)
    {

        self::validatedData($request);
        $data = new CommunityBuilding();
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/communitybuilding';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->save();
        self::json();
        geoJson(CommunityBuilding::class,'communityBuilding');
        session()->flash('status', 'insert');
        return redirect('admin/community-building');
    }

    public function edit($id)
    {
        $data = CommunityBuilding::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/community-building');
        }
        return view('admin.CommunityBuildings.community_building_add', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bridge  $bridge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        self::validatedData($request);

        $data = CommunityBuilding::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = CommunityBuilding::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/communitybuilding/{$data_image->photo}")) {
                unlink("uploads/communitybuilding/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/communitybuilding';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::json();
        session()->flash('status', 'success');
        return redirect('admin/community-building');
    }


    public function destroy($id)
    {
        $del = CommunityBuilding::findOrFail($id);
        if ($del->photo && file_exists("uploads/communitybuilding/{$del->photo}")) {
            unlink("uploads/communitybuilding/{$del->photo}");
        }
        $data = CommunityBuilding::findOrFail($id);
        $data->delete();
        self::json();
        session()->flash('status', 'delet');
        return redirect('admin/community-building');
    }

    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->type = $request->input('type'),
            $data->address = $request->input('address'),
            $data->status = $request->input('status'),
            $data->remarks = $request->input('remarks'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude'),
            $data->photo = $request->input('photo')

        );
    }

    protected function validatedData(Request $request)
    {


        $rules = [
            'name' => 'required',
            'type' => 'nullable',
            'address' => 'nullable',
            'ward' => 'nullable',
            'status' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'remarks' => 'nullable'

        ];

        $customMessages = [
            'name.required' => 'भवनको नाम क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'forest_area.required' => 'जंगलको क्षेत्रफल आवश्यक छ।',
            'member_household.required' => 'सदस्य घरधुरी आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
        ];

        $this->validate($request, $rules, $customMessages);
    }

    public function json()
    {
        $data = CommunityBuilding::all();
        $cdata = json_encode($data,JSON_UNESCAPED_UNICODE);
        $file = 'communitybuilding' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cdata);
    }

}

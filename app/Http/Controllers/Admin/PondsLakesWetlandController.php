<?php

namespace App\Http\Controllers\Admin;

use App\PondsLakesWetland;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use File;

class PondsLakesWetlandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (file_exists("upload/json/ponds_file.json")) {
            $data = PondsLakesWetland::paginate(10);
            // dd($datas);
            return view('ponds.ponds_lakes_wetland_details')->with('ponds', $data);
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ponds.ponds_lakes_wetland', ['data' => new PondsLakesWetland()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        self::validatedData($request);

        $data = new PondsLakesWetland();
        $this->setDataValue($request, $data);
        // dd($request->hasFile('photo'));
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/ponds_lakes_wetland';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $data->save();
        self::json();
        session()->flash('status', 'insert');

        return redirect('admin/pond-lake-wetland');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id, Request $request)
    {
        $data = PondsLakesWetland::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/pond-lake-wetland');
        }
        return view('ponds.ponds_lakes_wetland', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        self::validatedData($request);
        // if ($validator->passes()) {
        $data = PondsLakesWetland::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/ponds_lakes_wetland';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $data->update();
        self::json();
        session()->flash('status', 'success');
        return redirect('admin/pond-lake-wetland');
        // } else {
        //     return redirect('admin/pond-lake-wetland/' . $id . '/edit')->withErrors($validator)->withInput();
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $del = PondsLakesWetland::findOrFail($id);
        if ($del->photo && file_exists("uploads/ponds_lakes_wetland/{$del->photo}")) {
            unlink("uploads/ponds_lakes_wetland/{$del->photo}");
        }
        $data = PondsLakesWetland::findOrFail($id);
        $data->delete();
        self::json();
        session()->flash('status', 'delete');

        return redirect('admin/pond-lake-wetland');
    }

    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->address = $request->input('address'),
            // $data->type = $request->input('type'),
            $data->ownership = $request->input('ownership'),
            $data->area = $request->input('area'),
            $data->drying = $request->input('drying'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude'),
            $data->remarks = $request->input('remarks'),
            $data->photo = $request->input('photo')
        );
    }

    protected function validatedData(Request $request)
    {
        $rules = [

            'name' => 'required',
            'ward' => 'required',
            'address' => 'nullable',
            //    'type'  => 'nullable',
            'ownership' => 'nullable',
            'area' => 'nullable',
            'drying' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'remarks' => 'nullable'
        ];

        $customMessages = [
            'name.required' => 'सार्वजनिक निजी पोखरी, ताल, सिमसारको नाम आवश्यक छ।',
            'address.required' => 'ठेगाना आवश्यक छ।',
            'ward.required' => 'वडा आवश्यक छ।',
            // 'type.required' => 'प्रकार आवश्यक छ।',
            'ownership.required' => 'स्वमित्व आवश्यक छ।',
            'area.required' => 'क्षेत्रफल आवश्यक छ।',
            'drying.required' => 'सुख्खाको खतरा छान्नुहोस।',
            // 'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            // 'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            // 'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            // 'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            // 'for_women.required' => 'महिलाको लागि छुट्टै भएको क्षेत्र आवश्यक छ।',

        ];

        $this->validate($request, $rules, $customMessages);
    }


    public function json()
    {
        $data = PondsLakesWetland::all();
        $cdata = json_encode($data,JSON_UNESCAPED_UNICODE);
        $file = 'ponds' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cdata);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Crematorium;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Validator;
use DB;
use File;

class CrematoriumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Crematorium::paginate(10);
        return view('Crematorium.crematoriumdetails', ['crematorium' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Crematorium.crematorium', ['data' => new Crematorium()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        self::validatedData($request);
        // if ($validator->passes()) {
            $data = new Crematorium();
            $this->setDataValue($request, $data);
            if ($request->hasFile('photo')) {
                $file = $request->file('photo');
                $destinationpath = 'uploads/crematourim';
                $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
                $file->move($destinationpath, $uploadedImageName);
                $data->photo = $uploadedImageName;
            }
            // dd($data);
            $data->save();
            self::json();;
            session()->flash('status', 'insert');
            return redirect('admin/crematorium');
        // }
        //  else {
        //     return redirect('admin/crematorium/create')->withErrors($validator)->withInput();
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\crematorium  $crematorium
     * @return \Illuminate\Http\Response
     */
    public function show(crematorium $crematorium)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\crematorium  $crematorium
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Crematorium::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/crematorium');
        }
        return view('crematorium.crematorium', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bridge  $bridge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        self::validatedData($request);
        // if ($validator->passes()) {
            $data = Crematorium::findOrFail($id);
            $this->setDataValue($request, $data);
            if ($request->hasFile('photo')) {

                $data_image = Crematorium::findOrFail($id);
                if ($data_image->photo && file_exists("uploads/crematorium/{$data_image->photo}")) {
                    unlink("uploads/crematorium/{$data_image->image}");
                }
                $file =  $request->file('photo');
                $destinationpath = 'uploads/crematorium';
                $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
                $file->move($destinationpath, $uploadedImageName);
                $data->photo = $uploadedImageName;
            }
            // dd($data);
            $data->update();
            self::json();
            session()->flash('status', 'success');
            return redirect('admin/crematorium');
        // } else {
        //     return redirect('admin/crematorium/' . $id . '/edit')->withErrors($validator)->withInput();
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\crematorium  $crematorium
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = Crematorium::findOrFail($id);
        if ($del->photo && file_exists("uploads/crematorium/{$del->photo}")) {
            unlink("uploads/crematorium/{$del->photo}");
        }
        $data = Crematorium::findOrFail($id);
        $data->delete();
        session()->flash('status', 'delet');
        self::json();
        return redirect('admin/crematorium');
    }

    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->address = $request->input('address'),
            $data->community = $request->input('community'),
            $data->infrastructure = $request->input('infrastructure'),
            $data->remarks = $request->input('remarks'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude')
        );
    }
    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'ward' => 'nullable',
            'address' => 'nullable',
            'community' => 'nullable',
            'infrastructure' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'remarks' => 'nullable',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];

        $customMessages = [
            'name.required' => 'शिदाहस्थल/चहानस्थल/कब्रस्थानको नाम आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना आवश्यक छ।',
            'community.required' => 'जातजार्त/समुदाय आवश्यक छ।',
            'infrastructure.required' => 'भौतिक पूर्वाधार आवश्यक छ।'
            // 'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            // 'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।'
        ];

        $this->validate($request, $rules, $customMessages);
    }

    public function json()
    {
        $data = Crematorium::all();
        $cdata = json_encode($data,JSON_UNESCAPED_UNICODE);
        $file = 'crematourim' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cdata);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Religious;
use Illuminate\Http\Request;
use Validator;
use File;

class ReligiousController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('admin.Religious.religiousAdd', ['data' => new Religious()]);
    }

    public function store(Request $request)
    {
        self::validatedData($request);
        $data = new Religious();
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/religious';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->save();
        self::Religiousjson();

        return redirect('admin/religious');
    }

    public function index()
    {
        $data = Religious::paginate(10);
        return view('admin.Religious.religious_show')->with('religious', $data);
    }

    public function edit($id, Request $request)
    {
        $data = Religious::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/religious');
        }
        return view('admin.Religious.religiousAdd', compact('data'));
    }

    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = Religious::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = Religious::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/religious/{$data_image->photo}")) {
                unlink("uploads/religious/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/religious';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::Religiousjson();
        $request->session()->flash('msg', 'Data updated successfully !');
        return redirect('admin/religious');
    }

    public function destroy($id)
    {
        $del = Religious::findOrFail($id);
        if ($del->photo && file_exists("uploads/religious/{$del->photo}")) {
            unlink("uploads/religious/{$del->photo}");
        }
        $data = Religious::findOrFail($id);
        $data->delete();
        self::Religiousjson();
        return redirect('admin/religious');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->address = $request->input('address'),
            $data->access_road = $request->input('access_road'),
            $data->community = $request->input('community'),
            $data->features = $request->input('features'),
            $data->festive = $request->input('festive'),
            $data->occasion = $request->input('occasion'),
            $data->remarks = $request->input('remarks'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude')
        );
    }

    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'ward' => 'nullable',
            'address' => 'nullable',
            'features' => 'nullable',
            'community' => 'nullable',
            'access_road' => 'nullable',
            'festive' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'remarks' => 'nullable',
            'occasion' => 'nullable',

        ];

        $customMessages = [
            'name.required' => 'धार्मिक स्थलको क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'features.required' => 'विशेषता क्षेत्र आवश्यक छ।',
            'community.required' => 'धर्म/समुदाय क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'festive.required' => 'मेला लाग्छ क्षेत्र आवश्यक छ।',
            'occasion.required' => 'मेला लाग्ने समय/अवसर क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            'access_road.required' => 'सडकको पहुँच क्षेत्र आवश्यक छ।',
        ];

        $this->validate($request, $rules, $customMessages);
    }
    public function Religiousjson()
    {
        $data = Religious::all();
        $cdata = json_encode($data,JSON_UNESCAPED_UNICODE);
        $file = 'religious' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cdata);
    }
}

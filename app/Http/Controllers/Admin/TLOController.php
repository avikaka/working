<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ToleLevelOrganization;
use Illuminate\Http\Request;
use Validator;
use File;

class TLOController extends Controller
{
    public function create()
    {
        return view('admin.TLO.TLOAdd', ['data' => new ToleLevelOrganization()]);
    }

    public function store(Request $request)
    {
        self::validatedData($request);
        $data = new ToleLevelOrganization();
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/tlo';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $this->setDataValue($request, $data);

        $data->save();
        self::Tlojson();
        session()->flash('status', 'insert');

        return redirect('admin/tlo');
    }

    public function index()
    {
        // if (file_exists("upload/json/tlo_file.json")) {
        //     $data = json_decode(file_get_contents("upload/json/tlo_file.json"));
        //     return view('admin.TLO.TLO_show')->with('data', $data);
        // } else
            $data = ToleLevelOrganization::paginate(10);
        return view('admin.TLO.TLO_show')->with('tlo', $data);
    }

    public function edit($id)
    {
        $data = ToleLevelOrganization::findOrFail($id);
        return view('admin.TLO.TLOAdd')->with('data', $data);
    }

    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = ToleLevelOrganization::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = ToleLevelOrganization::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/tlo/{$data_image->photo}")) {
                unlink("uploads/tlo/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/tlo';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::Tlojson();
        session()->flash('status', 'success');
        return redirect('admin/tlo');
    }

    public function destroy($id)
    {
        $data = ToleLevelOrganization::findOrFail($id);
        $data->delete();

        self::Tlojson();
        session()->flash('status', 'delet');

        return redirect('admin/tlo');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->families = $request->input('families'),
            $data->villages = $request->input('villages'),
            $data->contact_person = $request->input('contact_person'),
            $data->contact_no = $request->input('contact_number'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude'),
            $data->remarks = $request->input('remarks'),

        );
    }

    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'ward' => 'nullable',
            'families' => 'nullable',
            'villages' => 'nullable',
            'contact_person' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'remarks' => 'nullable',
            'contact_number' => 'nullable',
        ];

        $customMessages = [
            'name.required' => 'टोल विकास संस्थाको नाम क्षेत्र आवश्यक छ।',
            'families.required' => 'आवद्ध परिवार संख्या क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'contact_person.required' => 'सम्पर्क वयक्ति क्षेत्र आवश्यक छ।',
            'villages.required' => 'समेटेको वस्तीहरू क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            'contact_number.required' => 'सम्पर्क नं.क्षेत्र आवश्यक छ।',
        ];

        $this->validate($request, $rules, $customMessages);
    }
    public function Tlojson()
    {
        $tlo = ToleLevelOrganization::all();
        $tlodata = json_encode($tlo,JSON_UNESCAPED_UNICODE);
        $file = 'tlodetails' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $tlodata);
    }
}

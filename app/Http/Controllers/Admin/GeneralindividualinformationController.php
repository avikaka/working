<?php

namespace App\Http\Controllers\Admin;

use App\generalindividualinformation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\IndividualInformation;
use Illuminate\Support\Facades\DB;

class GeneralindividualinformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = DB::table('cover')->paginate(25);
        return view('family.general_individual_details', ['datas' => $datas]);
    }
    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $output = '';
            $name = trim($request->get('name'));
            $houseno = trim($request->get('houseNo'));
            //   return $houseno;
            if (!empty($name) and !empty($houseno)) {
                //   return "both data supplied";
                $data = DB::table('cover')->where('ROAD', 'like', '%' . $name . '%')->andWhere('household_id', '=', $houseno)->paginate(15);;
            } elseif (!empty($name) and empty($houseno)) {
                //   return "Name only";
                $data = DB::table('cover')->where('ROAD', 'like', '%' . $name . '%')->paginate(15);;
            } elseif (empty($name) and !empty($houseno)) {
                //   return "Household is supplied";
                $data = DB::table('cover')->where('household_id', '=', $houseno)->paginate(15);;
            } else {
                $datas = DB::table('cover')->paginate(15);
            }
        }

        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $row) {
                $output .= '
        <tr>

            <td class="font-weight-bold">' . $row->household_id . '</td>
            <td>' . $row->TOLE . '</td>
            <td>' . $row->ROAD . '</td>
            <td>' . $row->RESPONDENT . '</td>
            <td>' . $row->RES_PHONE . '</td>
            <td>' . $row->MEMBERS . '</td>


        </tr>
        ';
            }
        }
        $data = array(
            'table_data'  => $output,

        );
        return $data;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('family.general_individual_information');
        // $data = new IndividualInformation();

        // $houseId = DB::select('SELECT MAX(id) + 1 AS Result FROM household;');
        // $coverId = DB::select('SELECT MAX(id) + 1 AS Result FROM cover;');

        // $house =  ($houseId[0]->Result);
        // $cover = ($coverId[0]->Result);

        // $data->id = $cover;
        // $data->PROV = $request->input('province');
        // $data->DIST = $request->input('district');
        // $data->MUN = $request->input('municipality');
        // // dd($data);
        // $data->household_id = $house;
        // $data->MEMBERS = $request->input('members');
        // $data->TOLE = $request->input('tole');
        // // $data->tlo = $request->input('tlo');
        // $data->ROAD = $request->input('road');
        // $data->LATITUDE = $request->input('latitude');
        // $data->LONGITUDE = $request->input('longitude');
        // $data->GPS_ACCURACY = $request->input('accuracy');
        // $data->SATELLITES = $request->input('satellites');
        // $data->RESPONDENT = $request->input('respondent');
        // $data->RES_SEX = $request->input('res_sex');
        // $data->RES_PHONE = $request->input('res_phone');
        // $data->USE_OF_HOUSEHOLD = $request->input('use_of_household');

        // $data->save();

        // $values = array('id' => $data->household_id);
        // DB::table('household')->insert($values);

        // return redirect('admin/cover')->with('message', 'Data successfully added');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request)
    // {
    //     //
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\generalindividualinformation  $generalindividualinformation
     * @return \Illuminate\Http\Response
     */
    public function show(generalindividualinformation $generalindividualinformation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\generalindividualinformation  $generalindividualinformation
     * @return \Illuminate\Http\Response
     */
    public function edit(generalindividualinformation $generalindividualinformation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\generalindividualinformation  $generalindividualinformation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, generalindividualinformation $generalindividualinformation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\generalindividualinformation  $generalindividualinformation
     * @return \Illuminate\Http\Response
     */
    public function destroy(generalindividualinformation $generalindividualinformation)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class viewdetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $data = DB::table('cover')->get();
        
        $household = DB::table('cover')->where('cover.household_id','=',$id)
                ->join('household', 'cover.household_id', '=', 'household.id')->get(); 
        $persons = DB::table('persons')->where('household_id','=',$id)->get();
        
        $absentees = DB::table('absentees')->where('household_id','=',$id)->get();
        return view('family.viewdetails',['household' => $household[0], 'persons' => $persons, 'absentees' => $absentees]);
    }
    public function getDetails(Request $request)
    {
        $householdid = $request->get('householdid');
        $data = DB::table('cover')->where('household_id','=',$householdid)
                ->join('household', 'cover.household_id', '=', 'household.id')
                ->get(); 
        
        return view('family.viewdetails',['datas' => $data]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;
use App\Bank;
use App\Cooperative;
use App\Health_institution;
use App\Religious;
use App\Tole_level_organization;
use App\Tourism;


class FormController extends Controller
{
    public function showR()
    {
        return view('input-fields.religious');
    }

    public function storeR(Request $request)
    {
        $data = new Religious();
        $data->name = $request->input('name');
        $data->ward = $request->input('ward');
        $data->address = $request->input('address');
        $data->access_road = $request->input('access_road');
        $data->community = $request->input('community');
        $data->features = $request->input('features');
        $data->festive = $request->input('festive');
        $data->occasion = $request->input('occasion');
        $data->remarks = $request->input('remarks');

        $data->save();
        return redirect('religious-entry');
    }

    public function showT()
    {
        return view('input-fields.tourism');
    }

    public function storeT(Request $request)
    {
        $data = new Tourism();
        $data->name = $request->input('name');
        $data->ward = $request->input('ward');
        $data->address = $request->input('address');
        $data->access_road = $request->input('access_road');
        $data->ownership = $request->input('ownership');
        $data->features = $request->input('features');
        $data->yearly_tourist_number = $request->input("yearly_toursit_number");

        // dd($data);

        $data->save();
        return redirect('tourism-entry');
    }

    public function showTlo()
    {
        return view('input-fields.TLO');
    }

    public function storeTlo(Request $request)
    {
        $data = new Tole_level_organization();
        $data->name = $request->input('name');
        $data->ward = $request->input('ward');
        $data->families = $request->input('families');
        $data->villages = $request->input('villages');
        $data->contact_person = $request->input('contact_person');
        $data->contact_no = $request->input('contact_number');

        // dd($data);

        $data->save();
        return redirect('tlo-entry');
    }

    public function showCoo()
    {
        return view('input-fields.cooperative');
    }

    public function storeCoo(Request $request)
    {
        $data = new Cooperative();
        $data->name = $request->input('name');
        $data->ward = $request->input('ward');
        $data->type = $request->input('type');
        $data->saving = $request->input('savings');
        $data->loans = $request->input('loans');
        $data->loan_area = $request->input('loan_area');
        $data->male = $request->input('male');
        $data->female = $request->input('female');
        $data->contact_person = $request->input('contact_person');
        $data->contact_no = $request->input('contact_number');


        // dd($data);

        $data->save();
        return redirect('cooperatives-entry');
    }

    public function showBanks()
    {
        return view('input-fields.bank');
    }

    public function storeBanks(Request $request)
    {
        $data = new Bank();
        $data->name = $request->input('name');
        $data->ward = $request->input('ward');
        $data->type = $request->input('type');
        $data->address = $request->input('address');
        $data->branch_type = $request->input('branch_type');
        $data->contact_person = $request->input('contact_person');
        $data->contact_number = $request->input('contact_number');


        // dd($data);

        $data->save();
        return redirect('bank-entry');
    }

    public function showHI()
    {
        return view('input-fields.health-institutions');
    }

    public function storeHI(Request $request)
    {
        $data = new Health_institution();
        $services = $request->input('services');
        $data['services'] = implode(',', $services);
        $data->name = $request->input('name');
        $data->ward = $request->input('ward');
        $data->type = $request->input('type');
        $data->address = $request->input('address');
        // $data->services = implode(',', Input::get(''));
        $data->contact_person = $request->input('contact_person');
        $data->contact_no = $request->input('contact_number');
        $imagePath = request('photo')->store('uploads', 'public');
        $data->photo = $imagePath;


        // dd($data);

        $data->save();
        return redirect('health-institution-entry');
    }
    
}
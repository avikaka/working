<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Playground;
use Illuminate\Http\Request;
use File;

class PlaygroundController extends Controller
{
    public function create()
    {
        return view('admin.Playgrounds.playgroundAdd', ['data' => new Playground()]);
    }

    public function store(Request $request)
    {
        self::validatedData($request);
        $data = new Playground();
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/playground';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->save();
        self::Playgroundjson();
        session()->flash('status', 'insert');

        return redirect('admin/playground');
    }

    public function index()
    {
        $data = Playground::paginate(10);
        return view('admin.Playgrounds.playground_show')->with('playground', $data);
    }

    public function edit($id, Request $request)
    {
        $data = Playground::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/playground');
        }
        return view('admin.Playgrounds.playgroundAdd')->with('data', $data);
    }

    public function update(Request $request, $id)
    {
        self::validatedData($request);

        $data = Playground::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = Playground::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/playground/{$data_image->photo}")) {
                unlink("uploads/playground/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/playground';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::Playgroundjson();
        session()->flash('status', 'success');
        return redirect('admin/playground');
    }

    public function destroy($id)
    {
        $del = Playground::findOrFail($id);
        if ($del->photo && file_exists("uploads/playground/{$del->photo}")) {
            unlink("uploads/playground/{$del->photo}");
        }
        $data = Playground::findOrFail($id);
        $data->delete();
        self::Playgroundjson();
        session()->flash('status', 'delet');
        return redirect('admin/playground');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->type = $request->input('type'),
            $data->address = $request->input('address'),
            $data->area = $request->input('area'),
            $data->remarks = $request->input('remarks'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude')
        );
    }
    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'ward' => 'required',
            'type' => 'nullable',
            'address' => 'nullable',
            'area' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
        $customMessages = [
            'name.required' => 'खेलसथ्लको नाम क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'area.required' => 'क्षेत्रफल क्षेत्र आवश्यक छ।',
            'type.required' => 'प्रकार क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
        ];

        $this->validate($request, $rules, $customMessages);
    }


    public function Playgroundjson()
    {
        $data = Playground::all();
        $cdata = json_encode($data,JSON_UNESCAPED_UNICODE);
        $file = 'playground' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cdata);
    }
}

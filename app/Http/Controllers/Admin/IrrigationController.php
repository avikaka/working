<?php

namespace App\Http\Controllers\Admin;

use App\IrrigationProject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use File;

class IrrigationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = IrrigationProject::paginate(10);
        return view('irrigation.irrigation_details')->with('irrigation', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('irrigation.irrigation', ['data' => new IrrigationProject()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = new IrrigationProject();
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/irrigation';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->save();
        self::Irrigationjson();

        return redirect('admin/irrigation');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = IrrigationProject::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/irrigation');
        }
        return view('irrigation.irrigation', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bridge  $bridge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->validatedData());
        if ($validator->passes()) {
            $data = IrrigationProject::findOrFail($id);
            $this->setDataValue($request, $data);
            if ($request->hasFile('photo')) {

                $data_image = IrrigationProject::findOrFail($id);
                if ($data_image->photo && file_exists("uploads/irrigation/{$data_image->photo}")) {
                    unlink("uploads/irrigation/{$data_image->photo}");
                }
                $file =  $request->file('photo');
                $destinationpath = 'uploads/irrigation';
                $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
                $file->move($destinationpath, $uploadedImageName);
                $data->photo = $uploadedImageName;
            }
            // dd($data);
            $data->update();
            self::Irrigationjson();
            $request->session()->flash('msg', 'Data updated successfully !');
            return redirect('admin/irrigation');
        } else {
            return redirect('admin/irrigation/' . $id . '/edit')->withErrors($validator)->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function destroy($id, Request $request)
    {
        $del = IrrigationProject::findOrFail($id);
        if ($del->photo && file_exists("uploads/irrigation/{$del->photo}")) {
            unlink("uploads/irrigation/{$del->photo}");
        }
        $data = IrrigationProject::findOrFail($id);
        $data->delete();
        self::Irrigationjson();
        return redirect('admin/irrigation');
    }

    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->address = $request->input('address'),
            $data->medium = $request->input('medium'),
            $data->area = $request->input('area'),
            $data->type = $request->input('type'),
            $data->beneficiary = $request->input('beneficiary'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude'),
            $data->remark = $request->input('remark')
        );
    }

    protected function validatedData()
    {
        return array(
            'name' => 'required',
            'ward' => 'nullable',
            'address' => 'nullable',
            'medium' => 'nullable',
            'area' => 'nullable',
            'type' => 'nullable',
            'beneficiary' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
        );
    }

    public function Irrigationjson()
    {
        $irrigation = IrrigationProject::all();
        $irrigationdata = json_encode($irrigation,JSON_UNESCAPED_UNICODE);
        $file = 'irrigationdetails' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $irrigationdata);
    }
}

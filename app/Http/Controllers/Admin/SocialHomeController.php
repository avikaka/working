<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SocialHome;
use Illuminate\Http\Request;
use Validator;
use File;

class SocialHomeController extends Controller
{
    public function create()
    {
        return view('admin.SocialHome.social_homeAdd', ['data' => new SocialHome()]);
    }

    public function store(Request $request)
    {
        self::validatedData($request);
        $data = new SocialHome();
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/socialhome';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $this->setDataValue($request, $data);

        $data->save();
        self::Socialjson();
        session()->flash('status', 'insert');
        return redirect('admin/social-home');
    }

    public function index()
    {
        $data = SocialHome::paginate(10);
        return view('admin.SocialHome.social_home_show')->with('socialhome', $data);
    }

    public function edit($id)
    {
        $data = SocialHome::findOrFail($id);
        return view('admin.SocialHome.social_homeAdd')->with('data', $data);
    }

    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = SocialHome::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = SocialHome::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/socialhome/{$data_image->photo}")) {
                unlink("uploads/socialhome/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/socialhome';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::Socialjson();
        session()->flash('status', 'success');
        return redirect('admin/social-home');
    }

    public function destroy($id)
    {

        $del = SocialHome::findOrFail($id);
        if ($del->photo && file_exists("uploads/socialhome/{$del->photo}")) {
            unlink("uploads/socialhome/{$del->photo}");
        }
        $data = SocialHome::findOrFail($id);
        $data->delete();
        self::Socialjson();
        session()->flash('status', 'delet');

        return redirect('admin/social-home');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->type = $request->input('type'),
            $data->address = $request->input('address'),
            $data->contact_person = $request->input('contact_person'),
            $data->contact_number = $request->input('contact_no'),
            $data->latitude = $request->input('latitude'),
            $data->remarks = $request->input('remarks'),
            $data->longitude = $request->input('longitude')
        );
    }

    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'ward' => 'nullable',
            'address' => 'nullable',
            'type' => 'nullable',
            'contact_person' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'remarks' => 'nullable',
            'contact_no' => 'nullable',
        ];

        $customMessages = [
            'name.required' => 'गृह / केन्द्रको नाम क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'type.required' => 'प्रकार क्षेत्र आवश्यक छ।',
            'contact_person.required' => 'सम्पर्क वयक्ति क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
            'contact_no.required' => 'सम्पर्क नं क्षेत्र आवश्यक छ।',
        ];

        $this->validate($request, $rules, $customMessages);
    }
    public function Socialjson()
    {
        $social = SocialHome::all();
        $socialdata = json_encode($social,JSON_UNESCAPED_UNICODE);
        $file = 'socialhomedetails' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $socialdata);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Tourism;
use Illuminate\Http\Request;
use File;

class TourismController extends Controller
{
    public function create()
    {
        return view('admin.Tourism.tourism_add', ['data' => new Tourism()]);
    }

    public function store(Request $request)
    {
        self::validatedData($request);
        $data = new Tourism();
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/tourism';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->save();
        self::json();

        return redirect('admin/tourism');
    }

    public function index()
    {
        $data = Tourism::paginate(10);
        return view('admin.Tourism.tourism_show')->with('tourism', $data);
    }

    public function edit($id, Request $request)
    {
        $data = Tourism::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/tourism');
        }
        // dd($data->name);
        return view('admin.Tourism.tourism_add', compact('data'));
    }

    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = Tourism::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = Tourism::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/tourism/{$data_image->photo}")) {
                unlink("uploads/tourism/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/tourism';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::json();
        session()->flash('status', 'success');
        return redirect('admin/tourism');
    }

    public function destroy($id)
    {
        $del = Tourism::findOrFail($id);
        if ($del->photo && file_exists("uploads/tourism/{$del->photo}")) {
            unlink("uploads/tourism/{$del->photo}");
        }
        $data = Tourism::findOrFail($id);
        $data->delete();
        self::json();
        session()->flash('status', 'delet');
        return redirect('admin/tourism');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->address = $request->input('address'),
            $data->access_road = $request->input('access'),
            $data->ownership = $request->input('ownership'),
            $data->features = $request->input('features'),
            $data->yearly_tourist_number = $request->input('yearly_tourist_number'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude'),
            $data->remarks = $request->input('remarks'),
        );
    }

    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'ward' => 'required',
            'address' => 'nullable',
            'access' => 'nullable',
            'ownership' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'features' => 'nullable',
            'remarks' => 'nullable',
            'yearly_tourist_number' => 'nullable',
            'photo.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];


        $customMessages = [
            'name.required' => 'भवनको नाम क्षेत्र आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'ownership.required' => 'भवनको प्रकार आवश्यक छ।',
            'access.required' => 'सडकको पहुँच आवश्यक छ।',
            'yearly_tourist_number.required' => 'अनुमानित बार्षिक आगन्तुक संख्या आवश्यक छ।',
            'features.required' => 'विशेषता क्षेत्र आवश्यक छ।',
            'photo.required' => 'फोटो क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत प्रकार क्षेत्र आवश्यक छ।',
        ];

        $this->validate($request, $rules, $customMessages);
    }

    public function json()
    {
        $data = Tourism::all();
        $cdata = json_encode($data,JSON_UNESCAPED_UNICODE);
        $file = 'tourism' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $cdata);
    }
}

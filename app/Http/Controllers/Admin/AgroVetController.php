<?php

namespace App\Http\Controllers\Admin;

use App\Agro_vet_group;
use App\AgroVetGroup;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Validator;

class AgroVetController extends Controller
{
    public function index()
    {
        $agrovet = Agro_vet_group::paginate(10);
        return view('admin.AgroVet.agro_vet_show')->with('agrovet', $agrovet);
    }

    public function create()
    {
        return view('admin.AgroVet.agro_vetAdd', ['data' => new Agro_vet_group()]);
    }

    public function store(Request $request)
    {

        $data = new Agro_vet_group();
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationpath = 'uploads/agro_group';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        $data->save();
        self::AgroGroupjson();
        geoJson(AgroVetGroup::class,'agroVetGroup');
        return redirect('admin/agro-vet');
    }



    public function edit($id)
    {
        $data = Agro_vet_group::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/irrigation');
        }
        return view('admin.AgroVet.agro_vetAdd', compact('data'));
    }

    public function update(Request $request, $id)
    {
        // if (self::validatedData($request)) {
        $data = Agro_vet_group::findOrFail($id);
        $this->setDataValue($request, $data);
        if ($request->hasFile('photo')) {

            $data_image = Agro_vet_group::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/agro_group/{$data_image->photo}")) {
                unlink("uploads/agro_group/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/agro_group';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::AgroGroupjson();
        geoJson(AgroVetGroup::class,'agroVetGroup');
        $request->session()->flash('msg', 'Data updated successfully !');
        return redirect('admin/agro-vet');
        // } else {
        //     return back();
        // }
    }


    public function destroy($id)
    {
        $del = Agro_vet_group::findOrFail($id);
        if ($del->photo && file_exists("uploads/agro_group/{$del->photo}")) {
            unlink("uploads/agro_group/{$del->photo}");
        }
        $data = Agro_vet_group::findOrFail($id);
        $data->delete();
        self::AgroGroupjson();
        geoJson(AgroVetGroup::class,'agroVetGroup');
        return redirect('admin/agro-vet');
    }

    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->type = $request->input('type'),
            $data->address = $request->input('address'),
            $data->contact_person = $request->input('contact_person'),
            $data->contact_no = $request->input('contact_no'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude'),
            $data->remarks = $request->input('remarks'),
            $data->photo = $request->input('photo')
        );
    }
    protected function validatedData()
    {
        return array(
            'name' => 'required',
            'ward' => 'nullable',
            'type' => 'nullable',
            'address' => 'nullable',
            'contact_person' => 'nullable',
            'contact_no' => 'nullable|numeric',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable',
            'remarks' => 'nullabe',
            'photo' => 'nullabe',
        );
    }

    public function AgroGroupjson()
    {
        $agro_group = Agro_vet_group::all();
        $agro_groupdata = json_encode($agro_group,JSON_UNESCAPED_UNICODE);
        $file = 'agro_group' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $agro_groupdata);
    }
}

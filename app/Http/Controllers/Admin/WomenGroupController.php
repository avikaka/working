<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\WomenGroup;
use Illuminate\Http\Request;
use File;

class WomenGroupController extends Controller
{
    public function index()
    {
        // if (file_exists("upload/json/womenGroup_file.json")) {
        //     $data = json_decode(file_get_contents("upload/json/womenGroup_file.json"));
        //     return view('admin.WomenGroup.women_group_show')->with('data', $data);
        // } else
            $data = WomenGroup::paginate(10);
        return view('admin.WomenGroup.women_group_show')->with('womengroup', $data);
    }

    public function create()
    {
        return view('admin.WomenGroup.women_groupAdd', ['data' => new WomenGroup()]);
    }

    public function store(Request $request)
    {
        self::validatedData($request);


        $data = new WomenGroup();

        $this->setDataValue($request, $data);
        // dd($data);
        $data->save();
        self::WomenGroupjson();

        return redirect('admin/women-group');
    }


    public function edit($id , Request $request)
    {
        $data = WomenGroup::find($id);
        if (!$data) {
            $request->session()->flash('msg', 'Record not found !');
            return redirect('admin/women-group');
        }

        return view('admin.WomenGroup.women_groupAdd', compact('data'));
    }

    public function update(Request $request, $id)
    {
        self::validatedData($request);
        $data = WomenGroup::findOrFail($id);
        $this->setDataValue($request, $data);
        // dd($data);
        if ($request->hasFile('photo')) {

            $data_image = WomenGroup::findOrFail($id);
            if ($data_image->photo && file_exists("uploads/womenGroup/{$data_image->photo}")) {
                unlink("uploads/womenGroup/{$data_image->photo}");
            }
            $file =  $request->file('photo');
            $destinationpath = 'uploads/womenGroup';
            $uploadedImageName = uniqid() . '_' . $file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath, $uploadedImageName);
            $data->photo = $uploadedImageName;
        }
        // dd($data);
        $data->update();
        self::WomenGroupjson();
        session()->flash('status', 'success');
        return redirect('admin/women-group');
    }

    public function destroy($id)
    {
        $del = WomenGroup::findOrFail($id);
        if ($del->photo && file_exists("uploads/womenGroup/{$del->photo}")) {
            unlink("uploads/womenGroup/{$del->photo}");
        }
        $data = WomenGroup::findOrFail($id);
        $data->delete();
        self::womenGroupjson();
        session()->flash('status', 'delete');
        return redirect('admin/women-group');
    }
    protected function setDataValue($request, $data)
    {
        return array(
            $data->name = $request->input('name'),
            $data->ward = $request->input('ward'),
            $data->savings = $request->input('savings'),
            $data->address = $request->input('address'),
            $data->loans = $request->input('loans'),
            $data->loan_area = $request->input('loan_area'),
            $data->members = $request->input('members'),
            $data->contact_person = $request->input('contact_person'),
            $data->contact_no = $request->input('contact_no'),
            $data->latitude = $request->input('latitude'),
            $data->longitude = $request->input('longitude'),
            $data->remarks = $request->input('remarks')
        );
    }

    protected function validatedData(Request $request)
    {
        $rules = [
            'name' => 'required',
            'ward' => 'nullable',
            'savings' => 'nullable',
            'address' => 'nullable',
            'loans' => 'nullable',
            'loan_area' => 'nullable',
            'members' => 'nullable',
            'contact_person' => 'nullable',
            'contact_no' => 'nullable',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'remarks' => 'nullable'
        ];
        $customMessages = [
            'name.required' => 'भवनको नाम क्षेत्र आवश्यक छ।',
            'ward.required' => 'वडा क्षेत्र आवश्यक छ।',
            'savings.required' => 'बचत रकम आवश्यक छ।',
            'address.required' => 'ठेगाना क्षेत्र आवश्यक छ।',
            'loans.required' => 'लगानी रकम आवश्यक छ।',
            'loan_area.required' => 'लगानीको मुख्य क्षेत्र आवश्यक छ।',
            'members.required' => 'सदस्य संख्या आवश्यक छ।',
            'contact_person.required' => 'सम्पर्क वयक्ति आवश्यक छ।',
            'contact_no.required' => 'सम्पर्क नं. आवश्यक छ।',
            'latitude.required' => 'अक्षांश क्षेत्र आवश्यक छ।',
            'longitude.required' => 'देशान्तर क्षेत्र आवश्यक छ।',
            'remarks.required' => 'कैफियत आवश्यक छ।'
        ];

        $this->validate($request, $rules, $customMessages);
    }

    public function womenGroupjson()
    {
        $womenGroup = WomenGroup::all();
        $womenGroupdata = json_encode($womenGroup,JSON_UNESCAPED_UNICODE);
        $file = 'womenGroup' . '_file.json';
        $destinationPath = public_path() . "/upload/json/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        File::put($destinationPath . $file, $womenGroupdata);
    }

}

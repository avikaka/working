<?php

namespace App\Http\Controllers;

use App\Value;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $values = Value::where('id', 4)->first();
        // $vs = $values->valueSet;
        // dd($values);
        // return getFieldName('persons','RELIGION');
        return getTabularReport('persons','G08','G07=1',true,null);
        // return DB::table('persons')->select(DB::raw("WARD, 'G08', COUNT(*) AS `number`"))->whereRaw('WARD=8')->groupBy('WARD', 'G08')->orderBy('WARD')->orderBy('G08')->toSql();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Value  $value
     * @return \Illuminate\Http\Response
     */
    public function show(Value $value)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Value  $value
     * @return \Illuminate\Http\Response
     */
    public function edit(Value $value)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Value  $value
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Value $value)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Value  $value
     * @return \Illuminate\Http\Response
     */
    public function destroy(Value $value)
    {
        //
    }
}

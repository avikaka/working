<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Null_;

class InstitutionalController extends Controller
{
    public function communityBuilding(){

        $data =  getItems('communitybuilding_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);
        // dd($keyss);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function communityForest(){

        $data =  getItems('communityforest_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function publicToilets(){

        $data =  getItems('toiletdetails_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function waterLands(){

        $data =  getItems('ponds_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function irrigation(){

        $data =  getItems('irrigationdetails_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function agroFarms(){

        $data =  getItems('agrofarmdetails_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function agroGroups(){

        $data =  getItems('agro_group_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function tourisms(){

        $data =  getItems('tourism_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function religious(){

        $data =  getItems('religious_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function agroPocket(){

        $data =  getItems('agropocketdetails_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function roads(){

        $data =  getItems('roadnetworkdetails_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function bridge(){

        $data =  getItems('bridgedetails_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function drinkingWater(){

        $data =  getItems('drinkwater_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function haatBazar(){

        $data =  getItems('haatbazar_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function tlo(){

        $data =  getItems('tlodetails_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function cooperatives(){

        $data =  getItems('cooperativedetails_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function banks(){

        $data =  getItems('bankdetails_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function healthInstitutions(){

        $data =  getItems('healthinstutiondetails_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function playground(){

        $data =  getItems('playground_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }


    public function childrenClubs(){

        $data =  getItems('childclubdetails_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function publicPlaces(){

        $data =  getItems('public-place_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function crematorioum(){

        $data =  getItems('crematourim_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function womensGroup(){

        $data =  getItems('womenGroup_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function socialHome(){

        $data =  getItems('socialhomedetails_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }

    public function colleges(){

        $data =  getItems('collegedetails_file');

        if($data=='-'){
            return view('institutionalDetails.mainpage', [
                'errorMessageDuration' => 'Data Not Found.'
           ]);
        }else
        $keys = array();
        foreach($data as $datas) {
            $get = (array_keys($datas));
            array_push($keys,$get);
        }
        $keyss = ($keys[0]);

        return view('institutionalDetails.mainpage', compact('data','keyss'));

    }


}

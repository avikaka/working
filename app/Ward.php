<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    protected $primaryKey = 'ward';

    public function households(){
        return $this->hasMany(Household::class);
    }

    public function schools(){
        return $this->hasMany(School::class);
    }
    public static function getArea($ward=null){
        return self::select('area')->where('ward','=',is_null($ward)?0:$ward)->first();
    }
    public static function getLiteracy($ward=null){
        return self::select('literacy')->where('ward','=',is_null($ward)?0:$ward)->first();
    }
    public function haatbazars(){
        return $this->hasMany(HaatBazar::class);
    }
}

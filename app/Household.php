<?php

namespace App;

use App\Person;
use App\Ward;
use Illuminate\Database\Eloquent\Model;

class Household extends Model
{
    //Overriding the default table name
    protected $table = 'household';

    public function persons(){
        return $this->hasMany(Person::class);
    }

    public function cover(){
        return $this->hasOne(Cover::class);
    }

    public function wards(){
        return $this->belongsTo(Ward::class,'ward');
    }
    
    public function absentees(){
        return $this->hasMany(Absentee::class);
    }
}

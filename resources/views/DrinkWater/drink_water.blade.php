@extends('template.admin')
@section('title', 'खानेपानी आयोजनाहरू थप ')
@section('page-title')
    खानेपानी आयोजनाहरू थप
@endsection
@section('content')

    <div class="tab-pane" id="tab_2">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>खानेपानी आयोजनाहरू थप गर्नुहोस
                </div>

            </div>
            <div class="portlet-body form">
                <form class="form-sample" method="POST"
                    action="{{ isset($data->id) ? Route('admin.drinking-water.update', $data->id) : Route('admin.drinking-water.store') }}">
                    @isset($data->id)
                        {{ method_field('PUT') }}
                    @endisset
                    @csrf


                    <div class="form-body">

                        <div class="row">


                            <div class="col-md-6">
                                {!! inputField('name', ' नाम', ' नाम', isset($data) ? $data->name : old('name')) !!}
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">वडा नं.</label>
                                    {!! getWardCombo(0) !!}
                                    @error('ward')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">


                            <div class="col-md-6">
                                {!! inputField('source', 'मुहान', 'मुहान', isset($data) ? $data->source : old('source')) !!}
                                @error('source')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>


                            <div class="col-md-6">
                                {!! selectField('type', 'प्रकार', [0 => 'गुरुत्वाकर्षण', 'लिफ्ट'], '', isset($data) ? $data->type : old('type')) !!}

                                @error('type')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">


                            <div class="col-md-6">
                                {!! inputField('beneficiary', 'लाभान्वित घरधुरी', 'लाभान्वित घरधुरी', isset($data) ? $data->beneficiary : old('beneficiary')) !!}
                                @error('beneficiary')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>



                            <div class="col-md-6">
                                {!! inputField('beneficiary_settlement', 'सेवा प्राप्त गर्ने', 'सेवा प्राप्त गर्ने', isset($data) ? $data->beneficiary_settlement : old('beneficiary_settlement')) !!}
                                @error('beneficiary_settlement')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                {!! inputField('contact_person', 'सम्पर्क व्यक्ति', 'सम्पर्क व्यक्ति', isset($data) ? $data->contact_person : old('contact_person')) !!}
                                @error('contact_person')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-md-6">
                                {!! latField('contact_no', 'सम्पर्क नं', 'सम्पर्क नं', isset($data) ? $data->contact_no : old('contact_no')) !!}
                                @error('contact_no')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                {!! latField('latitude', 'अक्षांश', 'अक्षांश', isset($data) ? $data->latitude : old('latitude')) !!}
                                @error('latitude')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                {!! latField('longitude', 'देशान्तर', 'देशान्तर', isset($data) ? $data->longitude : old('longitude')) !!}
                                @error('longitude')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                        </div>



                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-md-9 text-center">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                @if ($data->id == null)
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                                                        alt="" />
                                                @else
                                                    <img src={{ url('uploads/drinkwater/' . $data->photo) }} alt="" />
                                                @endif
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail"
                                                style="max-width: 200px; max-height: 150px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> फोटो
                                                        छान्नुहोस </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="image" value="{{ $data->photo }}"
                                                        accept="image/*"></span>
                                                <a href="javascript:;" class="btn red fileinput-exists"
                                                    data-dismiss="fileinput">
                                                    Remove </a>
                                                @error('photo')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!--/span-->

                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="control-label "> कैफियत
                                    </label>
                                    <div class="col-md-12">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <textarea id="editor1"
                                                name="remarks">{{ old('remarks', $data->remarks) }} </textarea>
                                            @if ($errors->has('remarks'))
                                                <span class="text-danger font-weight-danger">
                                                    {{ $errors->first('remarks') }}
                                                </span>

                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div>
                            <button type="button"
                                class=" btn
                                                                                                                                                                                                                                                                                                                                                                                                        default"
                                href="{{ url()->previous() }}">Cancel</button>
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Save</button>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        })
    </script>


@endsection

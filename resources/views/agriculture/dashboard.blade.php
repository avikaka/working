@extends('template.index')
@section('title','Agriculture Dashboard')

@section('main-content')
    <div class="row">
        @component('components.card',
          ['icon'=>'mdi-human-male-female',
          'iconColor'=>'text-danger',
          'value'=>'8172',
          'title'=>'घरधुरी'])
        @endcomponent
        @component('components.card',
          ['icon'=>'mdi-human-male-male',
          'value'=>'13,700',
          'title'=>'पुरुष'])
          (48.08%)
        @endcomponent  
        @component('components.card',
          ['icon'=>'mdi-human-female-female',
          'value'=>'14,792',
          'title'=>'महिला'])
        (51.92%)
        @endcomponent  
        @component('components.card',
          ['icon'=>'mdi-map-marker-path',
          'iconColor'=>'text-primary',
          'value'=>'105.09',
          'title'=>'क्षेत्रफल'])
        वर्ग किलोमिटर
        @endcomponent
        @component('components.card',
          ['icon'=>'mdi-pencil-box',
          'iconColor'=>'text-info',
          'value'=>'64.8%',
          'title'=>'साक्षरता'])
        १५-६० वर्ष उमेरसमूहमा
        @endcomponent
        @component('components.card',
          ['icon'=>'mdi-account-group',
          'iconColor'=>'text-success',
          'value'=>'271',
          'title'=>'जनघनत्व'])
        प्रति वर्ग किलोमिटर
        @endcomponent
    </div>
    @include('reports.inc.main')
    @include('reports.inc.sex-wise')    
    @include('reports.inc.age-group')    

@endsection
{{-- end of main-content section --}}

@section('footer-script')
<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
@endsection

@section('header-style')
<style>
  .ward-population{
    height: 450px;
  }
</style>
@endsection
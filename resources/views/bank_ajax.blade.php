{{-- {{ dd($bank) }} --}}
@foreach ($bank as $data)
    <tr>
        <td class="font-weight-bold">{{ $data['ward'] }}</td>
        <td>{{ $data['name'] }}</td>
        <td>{{ $data['address'] }}</td>
        <td>{{ $data['type'] }}</td>
        {{-- <td>{{ $data['branch_type'] }}</td>
        <td>{{ $data['contact_person'] }}</td> --}}
        <td>{{ $data['contact_number'] }}</td>
    </tr>
@endforeach
{{-- {{ dd(count($bank)) }} --}}
@if ($bank->total() > 5)
    <tr class="justify-content-center tt">
        <td class="bank" colspan="2">
            {{ $bank->links() }}
        </td>
    </tr>
@endif

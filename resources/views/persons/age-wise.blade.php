@extends('template.index')
@section('title','उमेर समुह अनुसार')
@section('main-content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h3>वडाअनुसारको उमेर समुह</h3>
        </div>
        <div class="col grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-primary table-striped">
                            <thead>
                                <tr>
                                    <th>वडा</th>
                                    <th>0 वर्ष-4 वर्ष</th>
                                    <th>5 वर्ष-9 वर्ष</th>
                                    <th>10 वर्ष-14 वर्ष</th>
                                    <th>15 वर्ष-19 वर्ष</th>
                                    <th>20 वर्ष-24 वर्ष</th>
                                    <th>25 वर्ष-29 वर्ष</th>
                                    <th>30 वर्ष-34 वर्ष</th>
                                    <th>35 वर्ष-39 वर्ष</th>
                                    <th>40 वर्ष-44 वर्ष</th>
                                    <th>45 वर्ष-49 वर्ष</th>
                                    <th>50 वर्ष-54 वर्ष</th>
                                    <th>55 वर्ष-59 वर्ष</th>
                                    <th>60 वर्ष-64 वर्ष</th>
                                    <th>65 वर्ष-69 वर्ष</th>
                                    <th>70 वर्ष-74 वर्ष</th>
                                    <th>75 वर्ष सो भन्दा बढी</th>
                                    <th>उल्लेख नगरिएको</th>
                                    <th>जम्मा</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($ages as $age)
                                <tr>
                                    <td class="font-weight-bold">{{ $age->WARD }}</td>
                                    <td>{{ $age->A }}</td>
                                    <td>{{ $age->B }}</td>
                                    <td>{{ $age->C }}</td>
                                    <td>{{ $age->D }}</td>
                                    <td>{{ $age->E }}</td>
                                    <td>{{ $age->F }}</td>
                                    <td>{{ $age->G }}</td>
                                    <td>{{ $age->H }}</td>
                                    <td>{{ $age->I }}</td>
                                    <td>{{ $age->J }}</td>
                                    <td>{{ $age->K }}</td>
                                    <td>{{ $age->L }}</td>
                                    <td>{{ $age->M }}</td>
                                    <td>{{ $age->N }}</td>
                                    <td>{{ $age->O }}</td>
                                    <td>{{ $age->P }}</td>
                                    <td>{{ $age->Q }}</td>
                                    <td>{{ $age->Total }}</td>
                                </tr>
                                @endforeach

                            </tbody>
                            {{-- <tfoot>
                          <th>Total</th>
                          <th>{{$totalMale}}</th>
                            <th>{{$totalFemale}}</th>
                            <th>{{$totalOther}}</th>
                            <th>{{$totalMale + $totalFemale + $totalOther}}</th>
                            </tfoot> --}}
                        </table>{{-- end of table --}}
                    </div> {{-- end of .table-responsive --}}
                </div>
            </div>
        </div>
    </div> {{-- row end --}}

</div>
{{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
@endsection

@section('header-style')
<style>
.ward-population {
    height: 450px;
}
</style>
@endsection
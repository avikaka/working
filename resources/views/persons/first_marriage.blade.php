@extends('template.index')
@section('title','उमेर समुह अनुसार वैवाहिक स्थिति')
@section('main-content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h3>पहिलो पटक विवाह गर्दाको उमेरसम्बन्धी विवरण</h3>
        </div>
        <div class="col-xl-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-primary table-striped">
                            <thead>
                                <tr>
                                    <th>वडा</th>
                                    <th>0 वर्ष-10 वर्ष</th>
                                    <th>11 वर्ष-20 वर्ष</th>
                                    <th>21 वर्ष-30 वर्ष</th>
                                    <th>31 वर्ष-40 वर्ष</th>
                                    <th>41 वर्ष-50 वर्ष</th>
                                    <th>51 वर्ष-60 वर्ष</th>
                                    <th>61 वर्ष-70 वर्ष</th>
                                    <th>71 वर्ष सो भन्दा बढी</th>
                                    <!-- <th>उल्लेख नगरिएको</th> -->
                                    <th>जम्मा</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($data as $row)
                                <tr>
                                    <td class="font-weight-bold">{{ $row->WARD }}</td>
                                    <td>{{ $row->A }}</td>
                                    <td>{{ $row->B }}</td>
                                    <td>{{ $row->C }}</td>
                                    <td>{{ $row->D }}</td>
                                    <td>{{ $row->E }}</td>
                                    <td>{{ $row->F }}</td>
                                    <td>{{ $row->G }}</td>
                                    <td>{{ $row->H }}</td>
                                    <td>{{ $row->Total }}</td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>{{-- end of table --}}
                    </div> {{-- end of .table-responsive --}}
                </div>
            </div>
        </div>
    </div> {{-- row end --}}

</div>
{{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
@endsection

@section('header-style')
<style>
    .ward-population {
        height: 450px;
    }

</style>
@endsection

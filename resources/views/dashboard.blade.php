@extends('template.index')
@section('title','Dashboard')

@section('main-content')
    <div class="row">
      <div class="col">
        <div id="map"></div>
      </div>
    </div>
    <div class="row">
        @component('components.card',
          ['icon'=>'mdi-human-male-female',
          'iconColor'=>'text-danger',
          'value'=>'28,492',
          'title'=>'जनसंख्या'])
          अक्सर बसोबास गर्ने
        @endcomponent
        @component('components.card',
          ['icon'=>'mdi-human-male-male',
          'value'=>'13,700',
          'title'=>'पुरुष'])
          (48.08%)
        @endcomponent
        @component('components.card',
          ['icon'=>'mdi-human-female-female',
          'value'=>'14,792',
          'title'=>'महिला'])
        (51.92%)
        @endcomponent
        @component('components.card',
          ['icon'=>'mdi-map-marker-path',
          'iconColor'=>'text-primary',
          'value'=>'105.09',
          'title'=>'क्षेत्रफल'])
        वर्ग किलोमिटर
        @endcomponent
        @component('components.card',
          ['icon'=>'mdi-pencil-box',
          'iconColor'=>'text-info',
          'value'=>'64.8%',
          'title'=>'साक्षरता'])
        १५-६० वर्ष उमेरसमूहमा
        @endcomponent
        @component('components.card',
          ['icon'=>'mdi-account-group',
          'iconColor'=>'text-success',
          'value'=>'271',
          'title'=>'जनघनत्व'])
        प्रति वर्ग किलोमिटर
        @endcomponent
    </div>
    @include('reports.inc.main')
    @include('reports.inc.sex-wise')
    @include('reports.inc.age-group')

@endsection
{{-- end of main-content section --}}

@section('header-script')
<script src="{{ asset('js/leaflet/leaflet-src-1.6.0.js') }}"></script>
<link rel="stylesheet" href="{{ asset('js/leaflet/plugins/StyledLayerControl/styledLayerControl.css') }}">
<script src="{{ asset('js/leaflet/plugins/StyledLayerControl/styledLayerControl.js') }}"></script>
<script src="{{ asset('json/indrawati.geojson') }}"></script>
@endsection

@section('footer-script')
<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
<script>
    var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
    var mapboxUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

    var grayscale = L.tileLayer(mapboxUrl, {id: 'mapbox/light-v9', tileSize: 512, zoomOffset: -1, attribution: mbAttr});


      var streets   = L.tileLayer(mapboxUrl, {id: 'mapbox/streets-v11', tileSize: 512, zoomOffset: -1, attribution: mbAttr});
    //Inatializing Map Object
    var map = L.map('map',{
        scrollWheelZoom: false,
        touchZoom: false,
        doubleClickZoom: true,
        boxZoom: true,
        zoomControl: true,
        dragging: true,
        layers: streets,
        center: [27.8090381,85.6202664],
        zoom: 12
    });

    // loading geoJSON Data
    var lyrBoundary = L.geoJSON(boundary, {
      style: function (feature){
        return {
            fillColor:'skyblue',
            weight:1,
            color:'blue',
            opacity:1,
            // dashArray:3,
            fillOpacity:0.2
        }
      },
      onEachFeature: boundaryFeture
    }).addTo(map);

    //BindingToolTip as label
    lyrBoundary.eachLayer(function(layer){
        layer.bindTooltip(layer.feature.properties.Name, {permanent: true,direction:"center",className:"wd-label"}).openTooltip()});
    var infoBox = L.control();

    infoBox.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
        this.update();
        return this._div;
    };

    // method that we will use to update the control based on feature properties passed
    infoBox.update = function (props) {
        this._div.innerHTML = '<h4>वडाको क्षेत्रफल</h4>' +  (props ?
            '<b>' + props.Name + '</b><br />' + props.Population
            : 'Hover over a ward');
    };

    function boundaryFeture(feature, layer) {
        layer.on({
            // mouseover: highlightFeature,
            // mouseout: resetHighlight,
            click: clickFeature
        });
    }
    var clickedLayer;
    function clickFeature(e) {
      lyrBoundary.resetStyle();
        map.fitBounds(e.target.getBounds());
        e.target.setStyle({
            weight: 2,
            fillColor: 'deeppink',
            color: 'deeppink',
            // fillOpacity: 0.5,
            // zIndex: 9999
        });
        clickedLayer = e.target;
        // alert(e.target.feature.properties.Name);
    }
    function highlightFeature(e) {
        var layer = e.target;
        layer.setStyle({
            weight: 2,
            fillOpacity: 0.5
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }
        infoBox.update(layer.feature.properties);
    }
    function resetHighlight(e) {
        lyrBoundary.resetStyle(e.target);
        infoBox.update();
    }
    L.control.scale({maxWidth: 150, position: 'bottomleft'}).addTo(map);

    map.addLayer()
    </script>
@endsection

@section('header-style')
<link rel="stylesheet" href="{{ asset('js/leaflet/leaflet-1.6.0.css') }}">
<style>
  .ward-population{
    height: 450px;
  }
/* for leaflet-icon */
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      #map{
        height: 350px;
    }
    /* for Ward Label */
    .wd-label{
				font-size: 15px;
				font-weight:bold;
				padding: 0px;
				background: none;
				border: none;
				color: #ec09a0;
				/* text-shadow: 0px 0px 10px black; */
				box-shadow: none;
			}
      .info {
		padding: 6px 8px;
		font: 14px/16px Arial, Helvetica, sans-serif;
		background: white;
		background: rgba(255,255,255,0.8);
		box-shadow: 0 0 15px rgba(0,0,0,0.2);
		border-radius: 5px;
	}
	.info h4 {
		margin: 0 0 5px;
		color: #777;
	}
</style>
@endsection

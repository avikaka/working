@extends('template.admin')
@section('title', 'सिंचाई आयोजनाहरुको विवरण')
@section('page-title')
    सिंचाई आयोजनाहरुको विवरण
@endsection
@section('content')

    <div class="tab-pane" id="tab_2">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>सिंचाई आयोजनाहरुको विवरण थप गर्नुहोस
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form method="POST"
                    action="{{ isset($data->id) ? Route('admin.irrigation.update', $data->id) : Route('admin.irrigation.store') }}"
                    enctype="multipart/form-data" class="horizontal-form">
                    @isset($data->id)
                        {{ method_field('PUT') }}
                    @endisset
                    @csrf
                    <div class="form-body">
                        {{-- <h3 class="form-section">सरकारी तथा सामुदायिक भवन</h3> --}}
                        <div class="row">
                            <div class="col-md-6">
                                {!! inputField('name', 'आयोजनको नाम', 'आयोजनको नाम', empty($data) ? old('name') : $data->name) !!}
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">वडा नं.</label>
                                    {!! getWardCombo(0) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                {!! inputField('address', 'ठेगाना', 'ठेगाना', isset($data) ? $data->address : old('address')) !!}
                                @error('address') <span style="color:red;  margin-left:130px">
                                    {{ $message }} </span> @Enderror
                            </div>

                            <div class="col-md-6">
                                {!! selectField('medium', 'सिंचाइको माध्यम', [0 => 'कुलो', 'नहर', 'पाइप', isset($data) ? $data->medium : old('medium')], '') !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                {!! selectField('type', 'सिंचाइको उपलब्धता', [0 => 'वर्षैभरि', 'मोसमी', isset($data) ? $data->type : old('type')], '') !!}
                            </div>
                            <div class="col-md-6">
                                {!! inputField('area', 'सिंचित क्षेत्र', 'सिंचित क्षेत्र', isset($data) ? $data->area : old('area')) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                {!! inputField('beneficiary', 'लाभान्वित घरधुरी', 'लाभान्वित घरधुरी', isset($data) ? $data->beneficiary : old('beneficiary')) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                {!! latField('latitude', 'अक्षांश', 'अक्षांश', isset($data) ? $data->latitude : old('latitude')) !!}
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                {!! latField('longitude', 'देशान्तर', 'देशान्तर', isset($data) ? $data->longitude : old('longitude')) !!}
                            </div>
                            <!--/span-->
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('remarks') ? ' has-error' : '' }} ">
                                    <label class="control-label "> कैफियत
                                    </label>
                                    <div class="col-md-12">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <textarea id="editor1"
                                                name="remarks">{{ old('remarks', $data->remarks) }} </textarea>
                                            @if ($errors->has('remarks'))
                                                <span class="text-danger font-weight-danger">
                                                    {{ $errors->first('remarks') }}
                                                </span>

                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <button type="button" class=" btn default" href="{{ url()->previous() }}">Cancel</button>
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i> Save</button>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        })
    </script>


@endsection

@foreach ($college as $data)
    <tr>
        {{-- <td class="font-weight-bold">{{ $data['ward'] }}</td> --}}
        <td>{{ $data['name'] }}</td>
        {{-- <td>{{ $data['address'] }}</td> --}}
        {{-- <td>{{ $data['programmes'] }}</td> --}}
        <td>{{ $data['girls'] }}</td>
        <td>{{ $data['boys'] }}</td>
        {{-- <td>{{ $data['contact_person'] }}</td>
        <td>{{ $data['contact_no'] }}</td> --}}
    </tr>
@endforeach

{{-- {{ dd(count($college)) }} --}}
@if ($college->total() > 5)
    <tr class="justify-content-center">
        <td class="college" colspan="2">
            {{ $college->links() }}
        </td>
    </tr>
@endif

@extends('template.index')

@section('main-content')
    <!-- Loader ends-->
    <meta charset="UTF-8">
    <!-- Page Body Start-->
    <div class="page-body-wrapper mt-5">
        <div class="page-body">
            <div class="container-fluid general-widget">
                <div class="row">
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                        <div class="cardC text-center shadow-lg p-3 mb-2 bg-white rounded">
                            <i class=" iconC" data-feather="users"></i>
                            {{-- <hr> --}}
                            <div class="bodyC">
                                <h6 class="card-text text-center mt-0">जनसंख्या</h6>
                                <h5 class="card-text text-center font-weight-bold">{{ $gendertotal }}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                        <div class="cardC text-center shadow-lg p-3 mb-2 bg-white rounded">
                            <i class="icon-bg iconC" data-feather="edit"></i>
                            {{-- <hr> --}}
                            <div class="bodyC">
                                <h6 class="card-text text-center mt-0">साक्षरता</h6>
                                <h5 class="card-text text-center font-weight-bold">
                                    {{ $literacynumb['can read and write'] }}%</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 ">
                        <div class="cardC text-center shadow-lg p-3 mb-2 bg-white rounded">
                            <i class=" iconC" data-feather="home"></i>
                            {{-- <hr> --}}
                            <div class="bodyC">
                                <h6 class="card-text text-center mt-0">घरधुरी</h6>
                                <h5 class="card-text text-center font-weight-bold">{{ $housenumber['total'] }}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 ">
                        <div class="cardC text-center shadow-lg p-3 mb-2 bg-white rounded">
                            <i class=" iconC" data-feather="users"></i>
                            {{-- <hr> --}}
                            <div class="bodyC">
                                <h6 class="card-text text-center mt-0">परिवारआकार</h6>
                                <h5 class="card-text text-center font-weight-bold">{{ $avgfamily }}%</h5>
                            </div>
                        </div>
                    </div>
                    @foreach ($maleFemale as $data)
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                            <div class="cardC text-center shadow-lg p-3 mb-2 bg-white rounded">
                                <i class=" iconC" data-feather="user-plus"></i>
                                <div>
                                    {{-- <hr> --}}
                                </div>
                                <div class="bodyC">
                                    <h6 class="card-text text-center mt-0">{{ $data['title'] }}</h6>
                                    <h5 class="card-text text-center font-weight-bold">{{ $data['total'] }}</h5>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                        <div class="cardC text-center shadow-lg p-3 mb-2 bg-white rounded">
                            <i class=" iconC" data-feather="droplet"></i>
                            {{-- <hr> --}}
                            <div class="bodyC">
                                <h6 class="card-text text-center mt-0">पानिको धारा भएको</h6>
                                <h5 class="card-text text-center font-weight-bold">
                                    {{ $drinkwater['percentage']['piped water'] }}%</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                        <div class="cardC text-center shadow-lg p-3 mb-2 bg-white rounded">
                            <i class="icon-bg iconC" data-feather="square"></i>
                            {{-- <hr> --}}
                            <div class="bodyC">
                                <h6 class="card-text text-center mt-0">काठ/दाउरा प्रयोग</h6>
                                <h5 class="card-text text-center font-weight-bold">
                                    {{ $firewood['percentage']['firewood'] }}%</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                        <div class="cardC text-center shadow-lg p-3 mb-2 bg-white rounded">
                            <i class="icon-bg iconC" data-feather="thermometer"></i>
                            {{-- <hr> --}}
                            <div class="bodyC">
                                <h6 class="card-text text-center mt-0">दीर्घरोगले पिडित</h6>
                                <h5 class="card-text text-center font-weight-bold">
                                    {{ $lifelong['total']['total'] - $lifelong['total']['not_stated'] }}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                        <div class="cardC text-center shadow-lg p-3 mb-2 bg-white rounded">
                            <i class="icon-bg iconC" data-feather="zap"></i>
                            {{-- <hr> --}}
                            <div class="bodyC">
                                <h6 class="card-text text-center mt-0">पूर्णखोप लगाएको</h6>
                                <h5 class="card-text text-center font-weight-bold">
                                    {{ $vaccination['percentage']['yes'] }}%</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                        <div class="cardC text-center shadow-lg p-3 mb-2 bg-white rounded">
                            <i class="icon-bg iconC" data-feather="feather"></i>
                            {{-- <hr> --}}
                            <div class="bodyC">
                                <h6 class="card-text text-center mt-0">स्वास्थ्य संस्थामा सुत्केरी</h6>
                                <h5 class="card-text text-center font-weight-bold">
                                    {{ $delivery['percentage']['at health institution'] }}%
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                        <div class="cardC text-center shadow-lg p-3 mb-2 bg-white rounded">
                            <i class="icon-bg iconC" data-feather="user"></i>
                            {{-- <hr> --}}
                            <div class="bodyC">
                                <h6 class="card-text text-center mt-0">अनुपस्थित जनसंख्या</h6>
                                <h5 class="card-text text-center font-weight-bold">{{ $absentees['total']['total'] }}
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Map --}}
                <div class="row">
                    <div class="col-lg-8 h-100">
                        {{-- <div class="card"> --}}
                        <div class="weather-widget">
                            <div class="card">
                                <div id="map" style="min-height: 628px;" class="text-center">लोड हुँदै...</div>
                            </div>
                        </div>
                        {{-- </div> --}}
                    </div>

                    @include('template.dashboardCount')


                </div>
                <div class="row mt-4">
                    <div class="col-xl-5">
                        <!-- Slideshow container -->
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

                            <div class="carousel-inner ">
                                @foreach ($carsouel as $key => $slider)
                                    {{-- {{ dd($slider) }} --}}
                                    <div class="image1 carousel-item {{ $key == 0 ? 'active' : '' }} ">
                                        <img class=" d-block w-100"
                                            src="{{ url('uploads/carousel/' . $slider['image']) }}" alt="First slide"
                                            width="100%" height="350px">
                                        <div class="carousel-caption">
                                            <h3>{{ $slider['heading'] }}</h3>
                                            <p>{{ $slider['caption'] }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>

                    <div class="col-xl-7 ">
                        <div class="card shadow-lg p-3 bg-white rounded h-100">
                            <div class="card-header">
                                <h3 class='red text-center bg-white' style="color: rgb(206, 70, 70)">संक्षिप्त परिचय</h3>
                            </div>
                            <div class="card-body">
                                @foreach ($description as $data)
                                    <div>

                                        {!! str_limit($data['description'], $limit = 2500, $end = '...') !!}
                                        {{-- {{ strip_tags($data['description']) }} --}}
                                        {{-- {!! nl2br !!} --}}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row  mt-3">
                    <div class="col-xl-6 grid-margin stretch-card">
                        <div class="card shadow-lg p-3 mb-1 bg-white rounded">
                            <h3 class="card-header text-center"><i class="icon caste big"></i>वार्ड अनुसार जनसंख्या
                                वितरण</h3>
                            <div class="card-body">
                                <div class="genderbar">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 ">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card shadow-lg p-3 mb-1 bg-white rounded">
                                    <h3 class="card-header text-center"><i class="icon caste big"></i>जातिगत आधारमा
                                        जनसंख्या
                                        वितरण
                                    </h3>
                                    <div class="card-body">
                                        <div id="caste"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 mt-1">
                                <div class="card shadow-lg p-3 mb-2 bg-white rounded">
                                    <h3 class="card-header text-center"><i class="icon caste big"></i>साक्षरता आधारमा
                                        जनसंख्या वितरण
                                    </h3>
                                    <div class="card-body">
                                        <div id="chartdiv"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-xl-6 grid-margin stretch-card">
                        <div class="card shadow-lg p-3 bg-white rounded">
                            <h3 class="card-header text-center"><i class="icon caste big"></i>उमेर आधारमा जनसंख्या
                                वितरण</h3>
                            <div class="card-body">
                                <div class="agewise">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6 grid-margin stretch-card">
                        <div class="card shadow-lg p-3 bg-white rounded">
                            <h3 class="card-header text-center"><i class="icon caste big"></i>स्वास्थ्य अवस्थाको सूची
                                वितरण</h3>
                            <div class="card-body">
                                <div class="disease">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-xl-12 grid-margin stretch-card">
                        <div class="card shadow-lg p-3 bg-white rounded">
                            <div class="row">
                                <div class="dataT col-xl-6 grid-margin ">
                                    <div class="card table-responsive ">
                                        <h3 class="card-header text-center "><i class="icon caste big"></i>बैँक तथा
                                            वित्तीयसंस्थाको
                                            विवरण
                                        </h3>
                                        <table id="dtBasicExample" class="table table-striped" cellspacing="0"
                                            width="100%">
                                            <thead class="flip-content Dlist">
                                                <tr>
                                                    <th>वडा</th>
                                                    <th class=" ">बैँक तथा वित्तीयसंस्थाको नाम</th>
                                                    <th class="">ठेगाना</th>
                                                    <th class="">प्रकार</th>
                                                    {{-- <th class="">शाखाको प्रकार</th>
                                                    <th class="">सम्पर्क व्यक्ति</th> --}}
                                                    <th class="">सम्पर्क नं.</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @include('bank_ajax')
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="collegeP col-xl-6">
                                    <div class="card table-responsive">
                                        <h3 class="card-header text-center"><i class="icon caste big"></i>विश्वविद्यालय
                                            तथा
                                            क्याम्पसहरुको
                                            विवरण</h3>
                                        <table id="dtBasicExample" class="table table-striped" cellspacing="0"
                                            width="100%">
                                            <thead class="flip-content Dlist">
                                                <tr>
                                                    {{-- <th class="">वडा</th> --}}
                                                    <th class="">क्याम्पसको नाम</th>
                                                    {{-- <th class="">ठेगाना</th> --}}
                                                    {{-- <th class="">सञ्चालित कार्यक्रम तथा तह</th> --}}
                                                    <th class="">छात्रा</th>
                                                    <th class="">छात्र</th>
                                                    {{-- <th class="">सम्पर्क व्यक्ति</th>
                                                    <th class="">सम्पर्क नं.</th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @include('college_ajax')
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- <div class="collegeP row mt-3">

                </div> --}}



                <div class="row mt-3">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card table-responsive shadow-lg p-3 bg-white rounded">
                            <h3 class="text-center" style="color: rgb(206, 70, 70)"><i
                                    class=" icon caste big"></i>महत्त्वपूर्ण
                                सम्पर्क जानकारी</h3>
                            <div class="row">
                                @foreach ($importantcontact as $data)
                                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 mb-2 mt-2">
                                        <div class="contact card shadow-lg">
                                            <h4 class="card-header text-center mb-2" style=" font-size: 25px;">
                                                {{ $data['post'] }}</h4>
                                            <div class="card-body">
                                                <div class=" row">
                                                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
                                                        <img src="{{ url('uploads/contact/' . $data['photo']) }}"
                                                            alt="Photo" class="avatar">
                                                    </div>
                                                    <div class="col-xl-9 col-lg-6 col-md-6 col-sm-6">
                                                        <p>
                                                            <span style="font-weight: bold">नाम: </span>
                                                            {{ $data['name'] }}
                                                        </p>
                                                        <p>
                                                            <span style="font-weight: bold">ठेगाना : </span>
                                                            {{ $data['address'] }}
                                                        </p>
                                                        <p>
                                                            <span style="font-weight: bold">सम्पर्क नं : </span>
                                                            {{ $data['contact'] }}
                                                        </p>

                                                        @if (isset($data['website']))
                                                            <p>
                                                                <span style="font-weight: bold">वेब साइट : </span>
                                                                {{ $data['website'] }}
                                                            </p>
                                                        @endif

                                                        <p>
                                                            @if (isset($data['email']))
                                                                <span style="font-weight: bold">ई-मेल : </span>
                                                                {{ $data['email'] }}
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Container-fluid Ends-->
            {{-- <div class="welcome-popup modal fade" id="loadModal" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <div class="modal-body">
                    <div class="modal-header"></div>
                    <div class="contain p-30">
                      <div class="text-center">
                        <h3>Welcome to creative admin</h3>
                        <p>start your project with developer friendly admin </p>
                        <button class="btn btn-primary btn-lg txt-white" type="button" data-dismiss="modal" aria-label="Close">Get Started</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> --}}

        </div>
    </div>
    </div>

    <script src="{{ asset('amcharts/4/core.js') }}"></script>
    <script src="{{ asset('amcharts4/core.js') }}"></script>
    <script src="{{ asset('amcharts4/charts.js') }}"></script>
    <script src="{{ asset('amcharts4/themes/material.js') }}"></script>
    <script src="{{ asset('amcharts4/themes/animated.js') }}"></script>

    <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>




    {{-- religion pie chart --}}
    <script>
        am5.ready(function() {

            // Create root element
            // https://www.amcharts.com/docs/v5/getting-started/#Root_element
            var root = am5.Root.new("caste");

            // Set themes
            // https://www.amcharts.com/docs/v5/concepts/themes/
            root.setThemes([
                am5themes_Animated.new(root)
            ]);

            // Create chart
            // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
            var chart = root.container.children.push(
                am5percent.PieChart.new(root, {
                    endAngle: 270
                })
            );

            // Create series
            // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
            var series = chart.series.push(
                am5percent.PieSeries.new(root, {
                    valueField: "total",
                    categoryField: "title",
                    endAngle: 270
                })
            );

            series.states.create("hidden", {
                endAngle: -90
            });

            let myData = {!! json_encode($religion['pie']) !!};
            console.log(myData);


            // Set data
            // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data
            series.data.setAll(myData);

            series.appear(1000, 100);

        }); // end am5.ready()
    </script>

    <script>
        am4core.ready(function() {

            // Themes begin

            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("genderbar", am4charts.XYChart);

            // Add data
            let myData = {!! json_encode($genderbar) !!};
            chart.data = myData;

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "ward";
            // categoryAxis.title.text = "Local country offices";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 20;
            categoryAxis.renderer.cellStartLocation = 0.1;
            categoryAxis.renderer.cellEndLocation = 0.9;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.min = 0;
            // valueAxis.title.text = "Expenditure (M)";

            // Create series
            function createSeries(field, name, stacked) {
                var series = chart.series.push(new am4charts.ColumnSeries());
                series.dataFields.valueY = field;
                series.dataFields.categoryX = "ward";
                series.name = name;
                series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
                series.stacked = stacked;
                series.columns.template.width = am4core.percent(95);
            }

            @foreach ($gendertitle as $key => $value)
                createSeries("{{ $key }}", "{{ $value }}", false);
            @endforeach


            // Add legend
            chart.legend = new am4charts.Legend();
            chart.responsive.enabled = true;

        }); // end am4core.ready()
    </script>


    {{-- population  bar graph according to age --}}


    <script>
        am4core.ready(function() {

            // Themes begin
            am4core.useTheme(am4themes_animated);
            chart.responsive.enabled = true;

            // Themes end

            var mainContainer = am4core.create("agewise", am4core.Container);
            mainContainer.width = am4core.percent(100);
            mainContainer.height = am4core.percent(100);
            mainContainer.layout = "horizontal";

            let myData = {!! json_encode($agewise) !!};
            var usData = myData;

            var maleChart = mainContainer.createChild(am4charts.XYChart);
            // console.log(maleChart);
            maleChart.paddingRight = 0;
            maleChart.data = JSON.parse(JSON.stringify(usData));

            // Create axes
            var maleCategoryAxis = maleChart.yAxes.push(new am4charts.CategoryAxis());
            maleCategoryAxis.dataFields.category = "age_group";
            maleCategoryAxis.renderer.grid.template.location = 0;
            //maleCategoryAxis.renderer.inversed = true;
            maleCategoryAxis.renderer.minLabelPosition = 0;
            maleCategoryAxis.renderer.maxLabelPosition = 1.95;
            maleCategoryAxis.renderer.minGridDistance = 15;

            maleCategoryAxis.renderer.grid.template.location = 0;
            maleCategoryAxis.renderer.labels.template.disabled = true;




            // var label = maleCategoryAxis.renderer.labels.template;
            // label.wrap = false;
            // label.truncate = true;
            // label.maxWidth = 120;
            // label.tooltipText = "{age_group}";

            var maleValueAxis = maleChart.xAxes.push(new am4charts.ValueAxis());
            // console.log(maleChart.xAxes);
            maleValueAxis.renderer.inversed = true;
            maleValueAxis.min = 0;
            maleValueAxis.max = 15;
            maleValueAxis.strictMinMax = true;
            maleValueAxis.renderer.grid.template.above = true;

            maleValueAxis.numberFormatter = new am4core.NumberFormatter();
            maleValueAxis.numberFormatter.numberFormat = "#.#'%'";

            // Create series
            var maleSeries = maleChart.series.push(new am4charts.ColumnSeries());
            maleSeries.dataFields.valueX = "male";
            maleSeries.dataFields.valueXShow = "percent";
            maleSeries.calculatePercent = true;
            maleSeries.dataFields.categoryY = "age_group";
            maleSeries.interpolationDuration = 1000;
            maleSeries.columns.template.tooltipText =
                "पुरूष, {categoryY}: {valueX} ({valueX.percent.formatNumber('#.0')}%)";
            //maleSeries.sequencedInterpolation = true;

            var categoryLabel = maleSeries.bullets.push(new am4charts.LabelBullet());
            categoryLabel.label.text = "{age_group}";
            categoryLabel.locationX = 1;
            categoryLabel.label.fontSize = 15;
            categoryLabel.label.horizontalCenter = "middle";
            categoryLabel.label.dx = -40;
            categoryLabel.label.truncate = false;
            categoryLabel.label.fill = am4core.color("#000000");

            // chart.maskBullets = false;
            // chart.paddingRight = 30;

            var femaleChart = mainContainer.createChild(am4charts.XYChart);
            femaleChart.paddingLeft = 0;
            femaleChart.data = JSON.parse(JSON.stringify(usData));

            // Create axes
            var femaleCategoryAxis = femaleChart.yAxes.push(new am4charts.CategoryAxis());
            femaleCategoryAxis.renderer.opposite = true;
            femaleCategoryAxis.dataFields.category = "age_group";
            femaleCategoryAxis.renderer.grid.template.location = 0;
            femaleCategoryAxis.renderer.minLabelPosition = 0;
            femaleCategoryAxis.renderer.maxLabelPosition = 1.95;
            femaleCategoryAxis.renderer.minGridDistance = 15;

            femaleCategoryAxis.renderer.grid.template.location = 0;
            femaleCategoryAxis.renderer.labels.template.disabled = true;

            // var label = femaleCategoryAxis.renderer.labels.template;
            // label.wrap = false;
            // label.truncate = true;
            // label.maxWidth = 120;
            // label.tooltipText = "{age_group}";

            var femaleValueAxis = femaleChart.xAxes.push(new am4charts.ValueAxis());
            femaleValueAxis.min = 0;
            femaleValueAxis.max = 15;
            femaleValueAxis.strictMinMax = true;
            femaleValueAxis.numberFormatter = new am4core.NumberFormatter();
            femaleValueAxis.numberFormatter.numberFormat = "#.#'%'";
            femaleValueAxis.renderer.minLabelPosition = 0.01;
            femaleValueAxis.renderer.grid.template.above = true;

            // Create series
            var femaleSeries = femaleChart.series.push(new am4charts.ColumnSeries());
            femaleSeries.dataFields.valueX = "female";
            femaleSeries.dataFields.valueXShow = "percent";
            femaleSeries.calculatePercent = true;
            femaleSeries.fill = femaleChart.colors.getIndex(4);
            femaleSeries.stroke = femaleSeries.fill;
            //femaleSeries.sequencedInterpolation = true;
            femaleSeries.columns.template.tooltipText =
                "महिला, {categoryY}: {valueX} ({valueX.percent.formatNumber('#.0')}%)";
            femaleSeries.dataFields.categoryY = "age_group";
            femaleSeries.interpolationDuration = 1000;

            var categoryLabel = femaleSeries.bullets.push(new am4charts.LabelBullet());
            categoryLabel.label.text = "{age_group}";
            categoryLabel.locationX = 1;
            categoryLabel.label.fontSize = 15;
            categoryLabel.label.horizontalCenter = "middle";
            categoryLabel.label.dx = 40;
            // categoryLabel.label.valign = "middle";
            categoryLabel.label.strokeWidth = 0;
            categoryLabel.label.truncate = false;
            categoryLabel.label.fill = am4core.color("#000000");
            categoryLabel.renderer.grid.template.location = 0;


        }); // end am4core.ready()
    </script>

    {{-- //literacy pie chart --}}
    <script>
        // am4core.ready(function() {

        //     // Themes begin

        //     am4core.useTheme(am4themes_animated);

        //     // Themes end

        //     // Create chart instance
        //     var chart = am4core.create("literacy", am4charts.PieChart);

        //     // Add and configure Series
        //     var pieSeries = chart.series.push(new am4charts.PieSeries());
        //     pieSeries.dataFields.value = "total";
        //     pieSeries.dataFields.category = "title";
        //     pieSeries.labels.template.disabled = true;
        //     pieSeries.ticks.template.disabled = true;

        //     // Let's cut a hole in our Pie chart the size of 30% the radius
        //     chart.innerRadius = am4core.percent(50);

        //     // Put a thick white border around each Slice
        //     pieSeries.slices.template.stroke = am4core.color("#fff");
        //     pieSeries.slices.template.strokeWidth = 2;
        //     pieSeries.slices.template.strokeOpacity = 1;
        //     pieSeries.slices.template
        //         // change the cursor on hover to make it apparent the object can be interacted with
        //         .cursorOverStyle = [{
        //             "property": "cursor",
        //             "value": "pointer"
        //         }];

        //     pieSeries.alignLabels = false;
        //     pieSeries.labels.template.bent = true;
        //     pieSeries.labels.template.radius = 3;
        //     pieSeries.labels.template.padding(0, 0, 0, 0);

        //     pieSeries.ticks.template.disabled = true;

        //     // Create a base filter effect (as if it's not there) for the hover to return to
        //     var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
        //     shadow.opacity = 0;

        //     // Create hover state
        //     var hoverState = pieSeries.slices.template.states.getKey(
        //         "hover"); // normally we have to create the hover state, in this case it already exists
        //     // Slightly shift the shadow and make it more prominent on hover
        //     var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
        //     hoverShadow.opacity = 0.7;
        //     hoverShadow.blur = 5;

        //     // Add a legend
        //     chart.legend = new am4charts.Legend();

        //     let myData = {!! json_encode($literacy['pie']) !!};
        //     chart.data = myData;
        //     chart.responsive.enabled = true;
        //     chart.legend = new am4charts.Legend();
        //     chart.legend.position = "right";

        // }); // end am4core.ready()


        am5.ready(function() {

            // Create root element
            // https://www.amcharts.com/docs/v5/getting-started/#Root_element
            var root = am5.Root.new("chartdiv");

            // Set themes
            // https://www.amcharts.com/docs/v5/concepts/themes/
            root.setThemes([
                am5themes_Animated.new(root)
            ]);

            // Create chart
            // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
            // start and end angle must be set both for chart and series
            var chart = root.container.children.push(am5percent.PieChart.new(root, {
                startAngle: 180,
                endAngle: 360,
                layout: root.verticalLayout,
                innerRadius: am5.percent(70),
                radius: am5.percent(70)
            }));


            // Create series
            // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
            // start and end angle must be set both for chart and series
            var series = chart.series.push(am5percent.PieSeries.new(root, {
                startAngle: 180,
                endAngle: 360,
                valueField: "total",
                categoryField: "title",
                alignLabels: false
            }));

            series.states.create("hidden", {
                startAngle: 180,
                endAngle: 180
            });

            series.slices.template.setAll({
                cornerRadius: 5
            });

            series.ticks.template.setAll({
                forceHidden: true
            });

            series.labels.template.setAll({
                fontSize: 12,
                maxWidth: 150,
                oversizedBehavior: "wrap" // to truncate labels, use "truncate"
            });

            let myData = {!! json_encode($literacy['pie']) !!};

            // Set data
            // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data
            series.data.setAll(myData)

            series.appear(1000, 100);

        }); // end am5.ready()
    </script>

    {{-- /disease bargraph --}}

    <script>
        am4core.ready(function() {
            // Themes begin
            am4core.useTheme(am4themes_animated);

            // Themes end

            var chart = am4core.create("disease", am4charts.XYChart);
            chart.padding(40, 40, 40, 40);

            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.dataFields.category = "title";
            categoryAxis.renderer.minGridDistance = 1;
            categoryAxis.renderer.inversed = true;
            categoryAxis.renderer.grid.template.disabled = true;

            var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
            valueAxis.min = 0;

            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.categoryY = "title";
            series.dataFields.valueX = "total";
            series.tooltipText = "{valueX.value}"
            series.columns.template.strokeOpacity = 0;
            series.columns.template.column.cornerRadiusBottomRight = 5;
            series.columns.template.column.cornerRadiusTopRight = 5;

            var labelBullet = series.bullets.push(new am4charts.LabelBullet())
            labelBullet.label.horizontalCenter = "left";
            labelBullet.label.dx = 10;
            labelBullet.label.text = "{values.valueX.workingValue.formatNumber('#as')}";
            labelBullet.locationX = 1;

            // as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
            series.columns.template.adapter.add("fill", function(fill, target) {
                return chart.colors.getIndex(target.dataItem.index);
            });

            categoryAxis.sortBySeries = series;
            let myData = {!! json_encode($disease['pie']) !!};
            chart.data = myData;
            chart.responsive.enabled = true;

        }); // end am4core.ready()
    </script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function() {

            function fetch_data(page) {
                $.ajax({
                    url: BASE_URL + "/dashboard_ajax_bank/?page=" + page,
                    success: function(data) {
                        $('.dataT tbody').html(data);
                    }
                })
            }
            $(document).on('click', '.bank a', function(event) {
                event.preventDefault();
                // console.log('test');
                var page = $(this).attr('href').split('page=')[1];
                // alert(page);
                fetch_data(page);
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            function fetch_data(page) {
                $.ajax({
                    url: BASE_URL + "/dashboard_ajax_college/?page=" + page,
                    success: function(data) {
                        $('.collegeP tbody').html(data);
                    }
                })
            }
            $(document).on('click', '.college a', function(event) {
                event.preventDefault();
                // console.log('test');
                var page = $(this).attr('href').split('page=')[1];
                // alert(page);
                fetch_data(page);
            });
        });
    </script>


    <style>
        #caste {
            height: 200px;
            width: 100%;
        }

        .genderbar {
            height: 450px;
        }

        .agewise {
            width: 100%;
            height: 500px;
        }

        #chartdiv {
            width: auto;
            height: 150px;
        }

        .disease {
            width: 100%;
            height: 500px;
        }

    </style>
@endsection

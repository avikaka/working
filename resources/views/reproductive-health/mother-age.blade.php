@extends('template.index')
@section('title','पहिलो बच्चा जन्मदिँदाको उमेर')
@section('main-content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h3>वडाअनुसार पहिलो बच्चा जन्मदिँदा आमाको उमेर</h3>
        </div>
        <div class="col-xl-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-primary table-striped">
                            <thead>
                                <tr>
                                    <th>वडा</th>
                                    <th>१५ वर्षभन्दा कम</th>
                                    <th>१५ - १९ वर्ष</th>
                                    <th>२० - २४ वर्ष</th>
                                    <th>२५ - २९ वर्ष</th>
                                    <th>३० - ३४ वर्ष</th>
                                    <th>३५ वर्ष वा सोभन्दा बढी</th>
                                    <th>जम्मा</th>

                                </tr>
                            </thead>
                            <tbody>


                                @foreach($data as $data)
                                <tr>
                                    <td class="font-weight-bold">{{ $data->WARD }}</td>
                                    <td>{{ $data->a }}</td>
                                    <td>{{ $data->b }}</td>
                                    <td>{{ $data->c}}</td>
                                    <td>{{ $data->d }}</td>
                                    <td>{{ $data->e }}</td>
                                    <td>{{ $data->f }}</td>
                                    <td>{{ $data->total }}</td>
                                </tr>
                                @endforeach



                            </tbody>
                            {{-- <tfoot>
                          <th>Total</th>
                          <th>{{$totalMale}}</th>
                            <th>{{$totalFemale}}</th>
                            <th>{{$totalOther}}</th>
                            <th>{{$totalMale + $totalFemale + $totalOther}}</th>
                            </tfoot> --}}
                        </table>{{-- end of table --}}
                    </div> {{-- end of .table-responsive --}}
                </div>
            </div>
        </div>

    </div> {{-- row end --}}

</div>
{{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
@endsection

@section('header-style')
<style>
    .ward-population {
        height: 450px;
    }

</style>
@endsection

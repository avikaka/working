@extends('template.index')
@section('title', 'शिशुको तौल')
@section('main-content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <h3>वडाअनुसार जन्मदाको
                    शिशुको तौल</h3>
            </div>

            <div id="chartdiv"></div>

            <div class="col-12 ">
                <br>

                <div class="d-flex flex-row-reverse">
                    <div>
                        <label class="switch">
                            <input type="checkbox" id="toggle">
                            <span class="slider round"></span><span></span>
                        </label>
                    </div>
                    <div>
                        <label class="font-weight-bold text-center p-2">वार्ड अनुसार तालिका हेर्नुहोस</label>
                    </div>
                </div>
            </div>

            <div class="col grid-margin stretch-card">
                <div class="card" style="display: none;">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-primary table-striped">
                                <thead>
                                    <tr class="">
                                        <th>वडा</th>
                                        @foreach ($title as $column)
                                            <th> {{ $column }}</th>
                                        @endforeach
                                        <th>जम्मा</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($table as $row)
                                        <tr>
                                            <td class="font-weight-bold">{{ $row['ward'] }}</td>
                                            @for ($j = 0; $j < $count; $j++)
                                                <td>{{ $row[$keys[$j]] }}</td>
                                            @endfor
                                            <td>{{ $row['total'] }}</td>
                                        </tr>
                                    @endforeach

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>प्रतिशत</th>
                                        @foreach ($percentage as $per)
                                            <th> {{ $per }}%</th>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        <th>जम्मा</th>
                                        @foreach ($total as $column)
                                            <th> {{ $column }}</th>
                                        @endforeach
                                    </tr>
                                </tfoot>
                            </table>{{-- end of table --}}
                        </div> {{-- end of .table-responsive --}}
                    </div>
                </div>
            </div>
        </div> {{-- row end --}}
    </div>

    <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>

    <!-- Chart code -->

    <style>
        #chartdiv {
            width: 100%;
            height: 500px;
        }

    </style>

    <script>
        am5.ready(function() {

            // Create root element
            // https://www.amcharts.com/docs/v5/getting-started/#Root_element
            var root = am5.Root.new("chartdiv");


            // Set themes
            // https://www.amcharts.com/docs/v5/concepts/themes/
            root.setThemes([
                am5themes_Animated.new(root)
            ]);


            // Create chart
            // https://www.amcharts.com/docs/v5/charts/xy-chart/
            var chart = root.container.children.push(am5xy.XYChart.new(root, {
                panX: false,
                panY: false,
                wheelX: "panX",
                wheelY: "zoomX",
                layout: root.verticalLayout
            }));

            // Add legend
            // https://www.amcharts.com/docs/v5/charts/xy-chart/legend-xy-series/
            var legend = chart.children.push(
                am5.Legend.new(root, {
                    centerX: am5.p50,
                    x: am5.p50
                })
            );

            var myData = {!! json_encode($shipments['final_data']) !!};
            var data = myData;


            // Create axes
            // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
            var xAxis = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
                categoryField: "ward",
                renderer: am5xy.AxisRendererX.new(root, {
                    cellStartLocation: 0.1,
                    cellEndLocation: 0.9
                }),
                tooltip: am5.Tooltip.new(root, {})
            }));

            xAxis.data.setAll(data);

            var yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
                renderer: am5xy.AxisRendererY.new(root, {})
            }));

            // Add series
            // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
            function makeSeries(name, fieldName) {
                var series = chart.series.push(am5xy.ColumnSeries.new(root, {
                    name: name,
                    xAxis: xAxis,
                    yAxis: yAxis,
                    valueYField: fieldName,
                    categoryXField: "ward"
                }));

                series.columns.template.setAll({
                    tooltipText: "{name}, {categoryX}:{valueY}",
                    width: am5.percent(90),
                    tooltipY: 0
                });

                series.data.setAll(data);

                // Make stuff animate on load
                // https://www.amcharts.com/docs/v5/concepts/animations/
                series.appear();

                series.bullets.push(function() {
                    return am5.Bullet.new(root, {
                        locationY: 0,
                        sprite: am5.Label.new(root, {
                            text: "{valueY}",
                            fill: root.interfaceColors.get("alternativeText"),
                            centerY: 0,
                            centerX: am5.p50,
                            populateText: true
                        })
                    });
                });

                legend.data.push(series);
            }

            var title = {!! json_encode($shipments['title']) !!};
            // console.log(Object.values(title(0)));

            for (const key in title) {
                // alert(`${key}`);
                makeSeries(`${title[key]}`, `${key}`);
            }

            // Make stuff animate on load
            // https://www.amcharts.com/docs/v5/concepts/animations/
            chart.appear(1000, 100);


            // Make stuff animate on load
            // https://www.amcharts.com/docs/v5/concepts/animations/
            chart.appear(1000, 100);

        }); // end am5.ready()
    </script>

    <script>
        // $(document).ready(function() {
        $("#toggle").click(function() {
            $(".card").toggle();
        });
        // });
    </script>
    {{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}

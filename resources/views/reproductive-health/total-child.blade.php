@extends('template.index')
@section('title','सन्तानको जन्म संख्या')
@section('main-content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h3>वडाअनुसार सन्तानको जन्म संख्या</h3>
        </div>
        <div class="col-xl-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>वडा</th>
                                    <th>उल्लेख नभएको</th>
                                    <th>जम्मा</th>

                                </tr>
                            </thead>
                            <tbody>


                                @foreach($data as $data)
                                <tr>
                                    <td class="font-weight-bold">{{ $data->WARD }}</td>
                                    <td>{{ $data->cha }}</td>
                                    <td>{{ $data->chaina }}</td>
                                    <td>{{ $data->nul }}</td>
                                    <td>{{ $data->total }}</td>
                                </tr>
                                @endforeach



                            </tbody>
                            {{-- <tfoot>
                          <th>Total</th>
                          <th>{{$totalMale}}</th>
                            <th>{{$totalFemale}}</th>
                            <th>{{$totalOther}}</th>
                            <th>{{$totalMale + $totalFemale + $totalOther}}</th>
                            </tfoot> --}}
                        </table>{{-- end of table --}}
                    </div> {{-- end of .table-responsive --}}
                </div>
            </div>
        </div>

    </div> {{-- row end --}}

</div>
{{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
@endsection

@section('header-style')
<style>
.ward-population {
    height: 450px;
}
</style>
@endsection
@foreach ($data as $toilet)
    <tr class="odd gradeX">

        <td>{{ $toilet->HOUSEHOLD_ID }}</td>
        <td>{{ $toilet->TOLE }}</td>
        <td>{{ $toilet->HOUSEHOLD_HEAD }}</td>
        <td>{{ $toilet->HEAD_CONTACT }}</td>
        <td>{{ $toilet->MEMBERS }}</td>
        <td>
            <a href="{{ url('admin/insertbirthcertification/' . $toilet->HOUSEHOLD_ID) }}"
                class="btn btn-circle green"> Add member
                <i class="fa icon-user-follow"></i>
            </a>
        </td>
        <td>
            <a href="{{ url('admin/personsdetail/' . $toilet->HOUSEHOLD_ID) }}" class="btn btn-circle blue"> View
                members
                <i class="fa icon-users"></i>
            </a>
        </td>
    </tr>
@endforeach

<tr class="d-flex justify-content-centerr">
    <td class="cover page-link" colspan="8">
        {{ $data->links() }}

    </td>
</tr>
{{-- <div class="dataTables_paginate paging_bootstrap_full_number" id="sample_1_paginate">
    {{ $data->links() }}
</div> --}}

@extends('template.index')
@section('title','Dashboard')

@section('main-content')
    <div class="row">
      <div class="col">
        <div id="map"></div>
      </div>
    </div>
    <div class="row">
      @component('components.card',
        ['icon'=>'mdi-human-male-female',
        'iconColor'=>'text-danger',
        'value'=>$population['total'],
        'title'=>'जनसंख्या'])
        अक्सर बसोबास गर्ने
      @endcomponent
      @component('components.card',
        ['icon'=>'mdi-human-male-male',
        'value'=>$population['male'],
        'title'=>'पुरुष'])
        ({{ $population['male_percent'] }}%)
      @endcomponent  
      @component('components.card',
        ['icon'=>'mdi-human-female-female',
        'value'=>$population['female'],
        'title'=>'महिला'])
      ({{ $population['female_percent'] }}%)
      @endcomponent  
      @component('components.card',
        ['icon'=>'mdi-map-marker-path',
        'iconColor'=>'text-primary',
        'value'=>$area,
        'title'=>'क्षेत्रफल'])
      वर्ग किलोमिटर
      @endcomponent
      @component('components.card',
        ['icon'=>'mdi-pencil-box',
        'iconColor'=>'text-info',
        'value'=> $literacy.'%',
        'title'=>'साक्षरता'])
      १५-६० वर्ष उमेरसमूहमा
      @endcomponent
      @component('components.card',
        ['icon'=>'mdi-account-group',
        'iconColor'=>'text-success',
        'value'=>$population['density'],
        'title'=>'जनघनत्व'])
      प्रति वर्ग किलोमिटर
      @endcomponent
  </div>
  <div class="row">
      @component('components.card',
        ['icon'=>'mdi-human-male-female',
        'iconColor'=>'text-danger',
        'value'=>$household,
        'title'=>'घरधुरी'])
        घरधुरी संख्या
      @endcomponent
      @component('components.card',
        ['icon'=>'mdi-human-male-male',
        'value'=>$familySize,
        'title'=>'परिवार आकार'])
        प्रति परिवार औषत जनसंख्या
      @endcomponent 
      @component('components.card',
        ['icon'=>'mdi-human-male-female',
        'iconColor'=>'text-danger',
        'value'=>$absentees['total'],
        'title'=>'अनुपस्थित जनसंख्या'])
        अनुपस्थित जनसंख्या
      @endcomponent
      @component('components.card',
        ['icon'=>'mdi-human-male-male',
        'value'=>$absentees['male'],
        'title'=>'अनुपस्थित पुरुष'])
      (51.92%)
      @endcomponent
      @component('components.card',
        ['icon'=>'mdi-human-female-female',
        'value'=>$absentees['female'],
        'title'=>'अनुपस्थित महिला'])
      (51.92%)
      @endcomponent
      @component('components.card',
        ['icon'=>'mdi-account-group',
        'iconColor'=>'text-success',
        'value'=>$population['total']+$absentees['total'],
        'title'=>'जम्मा'])
      अक्सर बसोबास र अनुपस्थित जनसंख्या
      @endcomponent
  </div>
  @include('reports.inc.main')
  @include('reports.inc.sex-wise')
  @include('reports.inc.age-group')
    {{-- <iframe src="{{ route('ajax.dashboard',['ward'=> 1]) }}" frameborder="1" height="400" width="100%"></iframe> --}}
@endsection
{{-- end of main-content section --}}

@section('header-script')
<script src="{{ asset('js/leaflet/leaflet-src-1.6.0.js') }}"></script>
<link rel="stylesheet" href="{{ asset('js/leaflet/plugins/StyledLayerControl/styledLayerControl.css') }}">
<script src="{{ asset('js/leaflet/plugins/StyledLayerControl/styledLayerControl.js') }}"></script>
<script src="{{ asset('json/indrawati.geojson') }}"></script>
@endsection

@section('footer-script')
<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>

<script src="{{asset('js/leaflet/plugins/Leaflet.Control.Custom.js?v=1.0.1')}}"></script>
<script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js'></script>
<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css' rel='stylesheet' />
<script>
    var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
    var mapboxUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
    
    var grayscale = L.tileLayer(mapboxUrl, {id: 'mapbox/light-v9', tileSize: 512, zoomOffset: -1, attribution: mbAttr});
    
    
      var streets   = L.tileLayer(mapboxUrl, {id: 'mapbox/streets-v11', tileSize: 512, zoomOffset: -1, attribution: mbAttr});
    //Inatializing Map Object
    var map = L.map('map',{
        scrollWheelZoom: false,
        touchZoom: false,
        doubleClickZoom: true,
        boxZoom: true,
        zoomControl: true,
        dragging: true,
        layers: streets,
        center: [27.8090381,85.6202664],
        zoom: 12
    });
    
// Add full screen control to existing map:
map.addControl(new L.Control.Fullscreen());

    // loading geoJSON Data
    var lyrBoundary = L.geoJSON(boundary, {
      style: function (feature){
        return {
            fillColor:'skyblue',
            weight:1,
            color:'blue',
            opacity:1,
            // dashArray:3,
            fillOpacity:0.2
        }
      },
      onEachFeature: boundaryFeture
    }).addTo(map);
    
/* //Adding Custom Control on Map
L.control.custom({
    position: 'topright',
    content : '<button type="button" class="btn btn-default">'+
              '    <i class="fa fa-crosshairs"></i>'+
              '</button>'+
              '<button type="button" class="btn btn-info">'+
              '    <i class="fa fa-compass"></i>'+
              '</button>'+
              '<button type="button" class="btn btn-primary">'+
              '    <i class="fa fa-spinner fa-pulse fa-fw"></i>'+
              '</button>'+
              '<button type="button" class="btn btn-danger">'+
              '    <i class="fa fa-times"></i>'+
              '</button>'+
              '<button type="button" class="btn btn-success">'+
              '    <i class="fa fa-check"></i>'+
              '</button>'+
              '<button type="button" class="btn btn-warning">'+
              '    <i class="fa fa-exclamation-triangle"></i>'+
              '</button>',
    classes : 'btn-group-vertical btn-group-sm',
    style   :
    {
        margin: '10px',
        padding: '0px 0 0 0',
        cursor: 'pointer',
    },
    datas   :
    {
        'foo': 'bar',
    },
    events:
    {
        click: function(data)
        {
            console.log('wrapper div element clicked');
            console.log(data);
        },
        dblclick: function(data)
        {
            console.log('wrapper div element dblclicked');
            console.log(data);
        },
        contextmenu: function(data)
        {
            console.log('wrapper div element contextmenu');
            console.log(data);
        },
    }
})
.addTo(map); */
//end of Custom Control add
// l.layers[1].click();
var infoBox = L.control();

    infoBox.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
        this.update();
        return this._div;
    };
    
    // method that we will use to update the control based on feature properties passed
    infoBox.update = function (props) {
        this._div.innerHTML = '<h4>वडाको क्षेत्रफल</h4>' +  (props ?
            '<b>' + props.Name + '</b><br />' + props.Population
            : 'Hover over a ward');
    };

    //BindingToolTip as label
    lyrBoundary.eachLayer(function(layer){
        layer.bindTooltip(layer.feature.properties.Name, {permanent: true,direction:"center",className:"wd-label"}).openTooltip()});
    
    
    function boundaryFeture(feature, layer) {
        layer.on({
            // mouseover: highlightFeature,
            // mouseout: resetHighlight,
            click: clickFeature
        });
    }
    var clickedLayer;
    function clickFeature(e) {
      lyrBoundary.resetStyle();
        map.fitBounds(e.target.getBounds());
        e.target.setStyle({
            weight: 2,
            fillColor: 'deeppink',
            color: 'deeppink',
            // fillOpacity: 0.5,
            // zIndex: 9999
        });
        clickedLayer = e.target;
        e.target.bringToFront();
        // alert(e.target.feature.properties.Name);
        // getData(e.target.feature.properties.Value);
    }
    //Load data as per the ward supplied
    //fire the event while map layer clicked
    function loadData(ward=null){
        $.ajax({
          url: _url,
          type: "GET",
          success: function(data) {
              if(data) {
                $("#post_id").val(data.id);
                $("#title").val(data.title);
                $("#description").val(data.description);
                $('#post-modal').modal('show');
              }
          }
        });
    }
    function getData(ward=null){
        data={!! $location !!};
        for (item of data ){
          L.marker([item.lat, item.lon]).bindTooltip('Name: '+ item.name).addTo(map);
        }
    }
    function highlightFeature(e) {
        var layer = e.target;
        layer.setStyle({
            weight: 2,
            fillOpacity: 0.5
        });
    
        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }
        infoBox.update(layer.feature.properties);
    }
    function resetHighlight(e) {
        lyrBoundary.resetStyle(e.target);
        infoBox.update();
    }
    L.control.scale({maxWidth: 150, position: 'bottomleft'}).addTo(map);
    getData();
    map.addLayer();
    </script>
@endsection
@section('header-style')
<link rel="stylesheet" href="{{ asset('js/leaflet/leaflet-1.6.0.css') }}">
<style>
  .ward-population{
    height: 450px;
  }
/* for leaflet-icon */
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      #map{
        height: 350px;
    }
    /* for Ward Label */
    .wd-label{
				font-size: 15px;
				font-weight:bold;
				padding: 0px;
				background: none;
				border: none;
				color: #ec09a0;
				/* text-shadow: 0px 0px 10px black; */
				box-shadow: none;
			}
      .info {
        padding: 6px 8px;
        font: 14px/16px Arial, Helvetica, sans-serif;
        background: white;
        background: rgba(255,255,255,0.8);
        box-shadow: 0 0 15px rgba(0,0,0,0.2);
        border-radius: 5px;
	}
	.info h4 {
		margin: 0 0 5px;
		color: #777;
	}
</style>
@endsection
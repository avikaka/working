@extends('template.index')
@section('title', 'विद्यालय गइरहेका')
@section('main-content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h3>वडाअनुसार विद्यालय गइरहेका</h3>
        </div>
        <div class="col grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-primary table-striped">
                            <thead>
                                <tr class="">
                                    <th>वडा</th>
                                    @foreach ($title as $column)
                                        <th> {{ $column }}</th>
                                    @endforeach
                                    <th>जम्मा</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($table as $row)
                                    <tr>
                                        <td class="font-weight-bold">{{ $row['ward'] }}</td>
                                        @for ($j = 0; $j < $count; $j++)
                                            <td>{{ $row[$keys[$j]] }}</td>

                                        @endfor
                                        <td>{{ $row['total'] }}</td>
                                    </tr>

                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>प्रतिशत</th>
                                    @foreach ($percentage as $per)
                                        <th> {{ $per }}%</th>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>जम्मा</th>
                                    @foreach ($total as $column)
                                        <th> {{ $column }}</th>
                                    @endforeach
                                </tr>
                            </tfoot>
                        </table>{{-- end of table --}}
                    </div> {{-- end of .table-responsive --}}
                </div>
            </div>
        </div>


    </div> {{-- row end --}}



</div>


@endsection

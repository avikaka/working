@extends('template.admin')
@section('title', 'सडक संजालको अवस्था विवरण')
@section('page-title')
    सडक संजालको अवस्था विवरण
@endsection
@section('content')
    <div class="content-wrapper">


        @if (session('status') == 'success')
            <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Updated"
                data-message="Member Updated Succesfuly" data-type="success" data-allow-outside-click="true"
                data-confirm-button-class="btn-success"></button>
        @elseif (session('status') == 'delet')
            <button class="btn btn-danger mt-sweetalert gk-btn-success" id="gk-success" data-title="मेटाइयो"
                data-message="सार्वजनिक शौचालय मेटाइयो" data-type="error" data-allow-outside-click="true"
                data-confirm-button-class="btn-danger"></button>
        @elseif (session('status') == 'insert')
            <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Inserted"
                data-message="Member Insert Succesfuly" data-type="success" data-allow-outside-click="true"
                data-confirm-button-class="btn-success"></button>

        @endif

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ url('admin/home') }}">Dashboard</a>
                    <i class="fa fa-circle"></i>
                </li>

                <li>
                    <span>सडक संजालको अवस्था विवरण </span>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body"
                    data-placement="bottom" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>
        </div>
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>


                                <span class="caption-subject bold uppercase"> सडक संजालको अवस्था विवरण</span>
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a id="sample_editable_1_new" class="btn sbold green"
                                                href="{{ route('admin.road-network.create') }}"> नयाँ जोड्नुहोस्
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <table class="table table-striped table-bordered table-hover table-checkable order-column"
                                id="sample_1">
                                <thead>
                                    <tr>

                                        <th>सडक संजालको नाम</th>
                                        <th>सडकको वर्गिकरण</th>
                                        <th>प्रकार</th>
                                        <th>सतहको प्रकार</th>
                                        <th>अनुमानित लम्बाई</th>
                                        <th>सुरु बिन्दु</th>
                                        <th>अन्तिम बिन्दु</th>
                                        <th>सडकले छुने वस्तिहरु</th>
                                        <th>सार्वजनिक यातायातको अवस्था</th>
                                        <th>कैफियत</th>
                                        <th>Edit</th>
                                        <th>Delete</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($road as $data)
                                        <tr>

                                            <td>{{ $data->name }}</td>
                                            <td>{{ $data->category }}</td>
                                            <td>{{ $data->type }}</td>
                                            <td>{{ $data->surface }}</td>
                                            <td>{{ $data->length }}</td>
                                            <td>{{ $data->start }}</td>
                                            <td>{{ $data->end }}</td>
                                            <td>{{ $data->settlement }}</td>
                                            <td>{{ $data->public_transport }}</td>
                                            <td>{{ $data->remarks }}</td>


                                            <td>

                                                <a href="{{ route('admin.road-network.edit', $data->id) }}"
                                                    class="btn btn-circle green"> सम्पादन गर्नुहोस्
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>


                                            <td><a href="#" data-toggle="modal" data-target="#{{ $data->id }}"
                                                    class="btn btn-circle red"><i class="fa fa-remove"></i>मेटाउन</a>

                                                </a>
                                                <div id="{{ $data->id }}" class="modal fade" tabindex="-1"
                                                    data-backdrop="static" data-keyboard="false">
                                                    <div class="___class_+?38___">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                    data-dismiss="modal" aria-hidden="true"></button>
                                                                <h4 class="modal-title">पुष्टिकरण</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p> के तपाइँ यसलाई {{ $data->name }} मेटाउन चाहानुहुन्छ?
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" data-dismiss="modal"
                                                                    class="btn dark btn-outline">रद्द गर्नुहोस्</button>


                                                                <form class="form-sample" method="POST"
                                                                    action="{{ Route('admin.road-network.destroy', $data->id) }}">

                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit" class="btn green">हो</button>
                                                                </form>



                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>


                                    @endforeach





                                </tbody>
                                {{-- <tfoot>
                      <th>Total</th>
                      <th>{{$totalMale}}</th>
                        <th>{{$totalFemale}}</th>
                        <th>{{$totalOther}}</th>
                        <th>{{$totalMale + $totalFemale + $totalOther}}</th>
                        </tfoot> --}}
                            </table>
                            {{ $road->links() }}
                            {{-- end of table --}}
                        </div> {{-- end of .table-responsive --}}
                    </div>
                </div>
            </div>

        </section>
    </div>







    <!-- /.example-modal -->

@endsection
{{-- end main-content section --}}

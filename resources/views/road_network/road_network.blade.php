@extends('template.admin')
@section('title', 'सडक संजालको विद्यमान अवस्था थप ')
@section('page-title')
    सडक संजालको विद्यमान अवस्था थप गर्नुहोस
@endsection
@section('content')

    <div class="tab-pane" id="tab_2">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>सडक संजालको विद्यमान अवस्था थप गर्नुहोस
                </div>

            </div>
            <div class="portlet-body form">
                <form class="form-sample" method="POST"
                    action="{{ isset($data->id) ? Route('admin.road-network.update', $data->id) : Route('admin.road-network.store') }}">
                    @isset($data->id)
                        {{ method_field('PUT') }}
                    @endisset
                    @csrf


                    <div class="form-body">
                        <div class="row">


                            <div class="col-md-6">
                                {!! inputField('name', 'सडकको नाम', 'सडक संजालको नाम', isset($data) ? $data->name : old('name')) !!}
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>


                            <div class="col-md-6">
                                {!! selectField('category', 'सडकको वर्गीकरण', [0 => 'राजमार्ग', 'ख', 'ग', 'घ', 'ङ'], '', isset($data) ? $data->category : old('category')) !!}

                                @error('category')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>

                        <div class="row">


                            <div class="col-md-6">
                                {!! selectField('type', 'प्रकार', [0 => 'संघीय', 'प्रादेशिका', 'स्थानीय', 'कृषि'], '', isset($data) ? $data->type : old('type')) !!}

                                @error('type')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>


                            <div class="col-md-6">
                                {!! selectField('surface', 'सहतको प्रकार', [0 => 'कालोपत्रे', 'ग्रभेल', 'धुलो'], '', isset($data) ? $data->surface : old('surface')) !!}

                                @error('surface')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">



                            <div class="col-md-6">
                                {!! latField('length', 'अनुमानित लम्बाई', 'अनुमानित लम्बाई', isset($data) ? $data->length : old('length')) !!}
                                @error('length')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>






                            <div class="col-md-6">
                                {!! inputField('start', 'सुरु बिन्दु', 'सुरु बिन्दु', isset($data) ? $data->start : old('start')) !!}
                                @error('start')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">



                            <div class="col-md-6">
                                {!! inputField('end', 'अन्तिम बिन्दु', 'अन्तिम बिन्दु', isset($data) ? $data->end : old('end')) !!}
                                @error('end')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>




                            <div class="col-md-6">
                                {!! inputField('settlement', 'सडकले छुने बस्तिहरु', 'सडकले छुने बस्तिहरु', isset($data) ? $data->settlement : old('settlement')) !!}
                                @error('settlement')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">



                            <div class="col-md-6">
                                {!! selectField('public_transport', 'सार्वजनिक यातायात सञ्चालनको अवस्था', [0 => 'बाह्रै महिना चल्ने', 'हिउँदमा मात्र चल्ने', 'कहिले नचल्ने'], '', isset($data) ? $data->public_transport : old('public_transport')) !!}

                                @error('public_transport')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                {!! latField('latitude', 'अक्षांश', 'अक्षांश', isset($data) ? $data->latitude : old('latitude')) !!}
                                @error('latitude')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                {!! latField('longitude', 'देशान्तर', 'देशान्तर', isset($data) ? $data->longitude : old('longitude')) !!}
                                @error('longitude')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                        </div>
                    </div>


                    <div class="row">


                        <!--/span-->

                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="control-label "> कैफियत
                                </label>
                                <div class="col-md-12">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <textarea id="editor1"
                                            name="remarks">{{ old('remarks', $data->remarks) }} </textarea>
                                        @if ($errors->has('remarks'))
                                            <span class="text-danger font-weight-danger">
                                                {{ $errors->first('remarks') }}
                                            </span>

                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div>
                        <button type="button"
                            class=" btn
                                                                                                                                                                                                                                                                                                                                                                                                        default"
                            href="{{ url()->previous() }}">Cancel</button>
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i> Save</button>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        })
    </script>


@endsection

@extends('template.index')
@section('title', 'बैँक')
@section('main-content')
    <div class="page-body">
        <div class="container-fluid">

            <!-- Container-fluid starts-->
            <div class="container-fluid">
                <div class="row">
                    @foreach ($cdata as $data)
                        <div class="col-xl-2 ">
                            <div class="card gradient-primary o-hidden ">
                                <div class="b-r-3 card-body">

                                    <div class="media static-top-widget" style="background-color: white">
                                        <div class="align-self-center text-center">
                                        </div>
                                        <a href="{{ url('collegedetails/' . $data->type) }}">
                                            <div class="media-body"><span
                                                    class="m-0 text-black">{{ $data->type }}</span>
                                                <h4>{{ $data->count }}</h4><i class="icon-bg"></i>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <div class="row">
                    <div class="col grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-primary table-striped">
                                        <thead>
                                            <tr>
                                                <th class="text-black">वडा</th>
                                                <th class="text-black">क्याम्पसको नाम</th>
                                                <th class="text-black">ठेगाना</th>
                                                <th class="text-black">सञ्चालित कार्यक्रम तथा तह</th>
                                                <th class="text-black">छात्रा</th>
                                                <th class="text-black">छात्र</th>
                                                <th class="text-black">सम्पर्क व्यक्ति</th>
                                                <th class="text-black">सम्पर्क नं.</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($college as $row)
                                                <tr>
                                                    <td>{{ $row->ward }}</td>
                                                    <td>{{ $row->name }}</td>
                                                    <td>{{ $row->address }}</td>
                                                    <td>{{ $row->programmes }}</td>
                                                    <td>{{ $row->girls }}</td>
                                                    <td>{{ $row->boys }}</td>
                                                    <td>{{ $row->contact_person }}</td>
                                                    <td>{{ $row->contact_no }}</td>

                                                </tr>
                                            @endforeach
                                        </tbody>

                                    </table>{{-- end of table --}}
                                </div> {{-- end of .table-responsive --}}
                            </div>
                        </div>
                    </div>


                </div> {{-- row end --}}



            </div>


        @endsection

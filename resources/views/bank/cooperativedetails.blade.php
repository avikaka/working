@extends('template.index')
@section('title', 'बैँक')
@section('main-content')
    <div class="page-body">
        <div class="container-fluid">

            <!-- Container-fluid starts-->
            <div class="container-fluid">
                <div class="row">
                    @foreach ($cdata as $data)
                        <div class="col-xl-2 ">
                            <div class="card gradient-primary o-hidden ">
                                <div class="b-r-3 card-body">

                                    <div class="media static-top-widget" style="background-color: white">
                                        <div class="align-self-center text-center">
                                        </div>
                                        <a href="{{ url('collegedetails/' . $data->type) }}">
                                            <div class="media-body"><span
                                                    class="m-0 text-black">{{ $data->type }}</span>
                                                <h4>{{ $data->count }}</h4><i class="icon-bg"></i>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <div class="row">
                    <div class="col grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-primary table-striped">
                                        <thead>
                                            <tr>
                                                <th>सहकारीको नाम</th>
                                                <th>वडा</th>
                                                <th>ठेगाना</th>
                                                <th>प्रकार</th>
                                                <th>बचत</th>
                                                <th>ऋण लगानी</th>
                                                <th>लगानीका मुख्य क्षेत्र</th>
                                                <th>पुरूष</th>
                                                <th>महिला</th>
                                                <th>सम्पर्क वयक्ति</th>
                                                <th>सम्पर्क नं.</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($cooperative as $row)
                                                <tr>
                                                    <td>{{ $row->name }}</td>
                                                    <td>{{ $row->ward }}</td>
                                                    <td>{{ $row->address }}</td>
                                                    <td>{{ $row->type }}</td>
                                                    <td>{{ $row->saving }}</td>
                                                    <td>{{ $row->loans }}</td>
                                                    <td>{{ $row->loan_area }}</td>
                                                    <td>{{ $row->male }}</td>
                                                    <td>{{ $row->female }}</td>
                                                    <td>{{ $row->contact_person }}</td>
                                                    <td>{{ $row->contact_no }}</td>

                                                </tr>
                                            @endforeach
                                        </tbody>

                                    </table>{{-- end of table --}}
                                </div> {{-- end of .table-responsive --}}
                            </div>
                        </div>
                    </div>


                </div> {{-- row end --}}



            </div>


        @endsection

@extends('template.index')
@section('title', 'बैँक')
@section('main-content')
    <div class="page-body">
        <div class="container-fluid">

            <!-- Container-fluid starts-->
            <div class="container-fluid">
                <div class="row">
                    @foreach ($bdata as $data)
                        <div class="col-xl-2 ">
                            <div class="card gradient-primary o-hidden ">
                                <div class="b-r-3 card-body">

                                    <div class="media static-top-widget" style="background-color: white">
                                        <div class="align-self-center text-center">
                                        </div>
                                        <a href="{{ url('bankdetails/' . $data->type) }}">
                                            <div class="media-body"><span
                                                    class="m-0 text-black">{{ $data->type }}</span>
                                                <h4>{{ $data->count }}</h4><i class="icon-bg"></i>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <div class="row">
                    <div class="col grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <th class=" text-black">बैँक तथा वित्तीयसंस्थाको नाम</th>
                                                <th class="text-black">ठेगाना</th>
                                                <th class="text-black">प्रकार</th>
                                                <th class="text-black">शाखाको प्रकार</th>
                                                <th class="text-black">सम्पर्क व्यक्ति</th>
                                                <th class="text-black">सम्पर्क नं.</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($bank as $row)
                                                <tr>
                                                    <td>{{ $row->name }}</td>
                                                    <td>{{ $row->address }}</td>
                                                    <td>{{ $row->type }}</td>
                                                    <td>{{ $row->branch_type }}</td>
                                                    <td>{{ $row->contact_person }}</td>
                                                    <td>{{ $row->contact_number }}</td>

                                                </tr>
                                            @endforeach
                                        </tbody>

                                    </table>{{-- end of table --}}
                                </div> {{-- end of .table-responsive --}}
                            </div>
                        </div>
                    </div>


                </div> {{-- row end --}}



            </div>


        @endsection

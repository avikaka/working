@extends('template.index')
@section('title','लिङ्गअनुसार अनुपस्थित')
@section('main-content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h3>Question</h3>
        </div>
        <div class="col-xl-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Feild_id</th>
                                    <th>Question</th>
                                    <th>Edit</th>
                                    <th>Del</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($data as $data)
                                <tr>
                                    <td class="font-weight-bold">{{ $data->field_name }}</td>
                                    <td style="white-space: nowrap; width: 5px; overflow: hidden; text-overflow: ----; border: 1px solid #000000;">{{ $data->Question }}</td>
                                    <td><a><button class="btn btn-primary">Edit</button></a></td>
                                    <td><a><button class="btn btn-danger">Delete</button></a></td>
                                    <td><a><button class="btn btn-outline-primary">Options</button></a></td>
                                </tr>
                                @endforeach





                            </tbody>
                            {{-- <tfoot>
                          <th>Total</th>
                          <th>{{$totalMale}}</th>
                            <th>{{$totalFemale}}</th>
                            <th>{{$totalOther}}</th>
                            <th>{{$totalMale + $totalFemale + $totalOther}}</th>
                            </tfoot> --}}
                        </table>{{-- end of table --}}
                    </div> {{-- end of .table-responsive --}}
                </div>
            </div>
        </div>

    </div> {{-- row end --}}

</div>
{{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
@endsection

@section('header-style')
<style>
.ward-population {
    height: 450px;
}
</style>
@endsection
@extends('template.index')
@section('title', 'साधन सुविधाहरु')
@section('main-content')
    <div class="page-body">
        <div class="row">
            <div class="col-12">
                <h3>वडाअनुसार घरमा उपल्बध साधन सुविधाहरु</h3>
            </div>
            <div class="col grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-primary table-striped">
                                <thead>
                                    <tr class="">
                                        <th>वडा</th>
                                        <th>रेडियो</th>
                                        <th>टेलिभिजन</th>
                                        <th>कम्प्युटर/ल्यापटप</th>
                                        <th>इन्टरनेट</th>
                                        <th>टेलिफोन</th>
                                        <th>मोबाईल फोन</th>
                                        <th>स्मार्ट फोन</th>
                                        <th>कार/जीप</th>
                                        <th>मोटरसाईकल/स्कुटर</th>
                                        <th>साईकल</th>
                                        <th>फ्रिज</th>
                                        <th>वासिङ मेसिन</th>
                                        <th>एयर कण्डिसन</th>
                                        <th>फ्यान</th>
                                        <th>माईक्रो ओवन</th>
                                        <th>राष्ट्रिय दैनिक</th>
                                        <th>केहि नभयको</th>
                                        <th>जम्मा</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($forTotal as $row)
                                        <tr>
                                            <td class="font-weight-bold">{{ $row['WARD'] }}</td>
                                            <td>{{ $row['Radio'] }}</td>
                                            <td>{{ $row['Tv'] }}</td>
                                            <td>{{ $row['Computer'] }}</td>
                                            <td>{{ $row['Net'] }}</td>
                                            <td>{{ $row['LandLine'] }}</td>
                                            <td>{{ $row['Mobile'] }}</td>
                                            <td>{{ $row['SmartPhone'] }}</td>
                                            <td>{{ $row['Car'] }}</td>
                                            <td>{{ $row['Bike'] }}</td>
                                            <td>{{ $row['Cycle'] }}</td>
                                            <td>{{ $row['Fridge'] }}</td>
                                            <td>{{ $row['WashingM'] }}</td>
                                            <td>{{ $row['AC'] }}</td>
                                            <td>{{ $row['Fan'] }}</td>
                                            <td>{{ $row['MicroOven'] }}</td>
                                            <td>{{ $row['Newspaper'] }}</td>
                                            <td>{{ $row['Nothing'] }}</td>
                                            <td>{{ $row['Total'] }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>

                                <tfoot class="">
                                    <tr>
                                        <th>प्रतिशत</th>
                                        @foreach ($per as $per)
                                            <th> {{ $per }}%</th>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        <th>जम्मा</th>
                                        @foreach ($total as $column)
                                            <th> {{ $column }}</th>
                                        @endforeach
                                    </tr>
                                </tfoot>
                                {{-- <tfoot>
                          <th>Total</th>
                          <th>{{$totalMale}}</th>
                            <th>{{$totalFemale}}</th>
                            <th>{{$totalOther}}</th>
                            <th>{{$totalMale + $totalFemale + $totalOther}}</th>
                            </tfoot> --}}
                            </table>{{-- end of table --}}
                        </div> {{-- end of .table-responsive --}}
                    </div>
                </div>
            </div>
        </div> {{-- row end --}}

    </div>
    {{-- content-wrapper ends --}}
@endsection

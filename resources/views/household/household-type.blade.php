@extends('template.index')
@section('title', 'घरको प्रकार')
@section('main-content')
    <div class="page-body">
        <div class="row">
            <div class="col-12">
                <h3>वडा अनुसार घरको प्रकार</h3>

            </div>
            <!-- HTML -->
            <div id="chartdiv"></div>

            <div class="col-12 ">
                <br>

                <div class="d-flex flex-row-reverse">
                    <div>
                        <label class="switch">
                            <input type="checkbox" id="toggle">
                            <span class="slider round"></span><span></span>
                        </label>
                    </div>
                    <div>
                        <label class="font-weight-bold text-center p-2">वार्ड अनुसार तालिका हेर्नुहोस</label>
                    </div>
                </div>
            </div>

            <div class="col grid-margin stretch-card">
                <div class="card" style="display: none;">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-primary table-striped">
                                <thead>
                                    <tr class="">
                                        <th>वडा</th>
                                        @foreach ($title as $column)
                                            <th> {{ $column }}</th>
                                        @endforeach
                                        <th>जम्मा</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($table as $row)
                                        <tr>
                                            <td class="font-weight-bold">{{ $row['ward'] }}</td>
                                            @for ($j = 0; $j < $count; $j++)
                                                <td>{{ $row[$keys[$j]] }}</td>
                                            @endfor
                                            <td>{{ $row['total'] }}</td>
                                        </tr>

                                    @endforeach

                                </tbody>
                                <tfoot class="">
                                    <tr>
                                        <th>प्रतिशत</th>
                                        @foreach ($percentage as $per)
                                            <th> {{ $per }}%</th>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        <th>जम्मा</th>
                                        @foreach ($total as $column)
                                            <th> {{ $column }}</th>
                                        @endforeach
                                    </tr>
                                </tfoot>
                            </table>{{-- end of table --}}
                        </div> {{-- end of .table-responsive --}}
                    </div>
                </div>
            </div>
        </div> {{-- row end --}}



    </div>
    {{-- <style>
        #chartdiv {
            width: 100%;
            height: 500px;
        }

    </style>

    <!-- Resources -->
    <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>

    <!-- Chart code -->
    <script>
        am5.ready(function() {

            // Create root element
            // https://www.amcharts.com/docs/v5/getting-started/#Root_element
            var root = am5.Root.new("chartdiv");


            // Set themes
            // https://www.amcharts.com/docs/v5/concepts/themes/
            root.setThemes([
                am5themes_Animated.new(root)
            ]);


            // Create chart
            // https://www.amcharts.com/docs/v5/charts/xy-chart/
            var chart = root.container.children.push(am5xy.XYChart.new(root, {
                panX: false,
                panY: false,
                wheelX: "panX",
                wheelY: "zoomX",
                layout: root.verticalLayout
            }));

            // Add scrollbar
            // https://www.amcharts.com/docs/v5/charts/xy-chart/scrollbars/
            chart.set("scrollbarX", am5.Scrollbar.new(root, {
                orientation: "horizontal"
            }));
            var myData = {!! json_encode($shipments['final_data']) !!};
            var data = myData;


            // Create axes
            // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
            var xAxis = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
                categoryField: "ward",
                renderer: am5xy.AxisRendererX.new(root, {}),
                tooltip: am5.Tooltip.new(root, {})
            }));

            xAxis.data.setAll(data);

            var yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
                min: 0,
                renderer: am5xy.AxisRendererY.new(root, {})
            }));


            // Add legend
            // https://www.amcharts.com/docs/v5/charts/xy-chart/legend-xy-series/
            var legend = chart.children.push(am5.Legend.new(root, {
                centerX: am5.p50,
                x: am5.p50
            }));


            // Add series
            // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
            function makeSeries(name, fieldName) {
                var series = chart.series.push(am5xy.ColumnSeries.new(root, {
                    name: name,
                    stacked: true,
                    xAxis: xAxis,
                    yAxis: yAxis,
                    valueYField: fieldName,
                    categoryXField: "ward"
                }));
                console.log(series);

                series.columns.template.setAll({
                    tooltipText: "{name}, {categoryX}: {valueY}",
                    tooltipY: am5.percent(10)
                });
                series.data.setAll(data);

                // Make stuff animate on load
                // https://www.amcharts.com/docs/v5/concepts/animations/
                series.appear();

                series.bullets.push(function() {
                    return am5.Bullet.new(root, {
                        sprite: am5.Label.new(root, {
                            text: "{valueY}",
                            fill: root.interfaceColors.get("alternativeText"),
                            centerY: am5.p50,
                            centerX: am5.p50,
                            populateText: true
                        })
                    });
                });

                legend.data.push(series);
            }

            makeSeries("Pakki", "brick\/stone attached to the soil");
            makeSeries("Semi-pakki", "cement attached brick\/stone");
            makeSeries("Kachchi", "with sloping pillars");
            makeSeries("Tahara", "a wooden pole is buried");
            // makeSeries("other", "other");
            // makeSeries("not_stated", "not_stated");
            // makeSeries("total", "total");


            // Make stuff animate on load
            // https://www.amcharts.com/docs/v5/concepts/animations/
            chart.appear(1000, 100);

        }); // end am5.ready()
    </script> --}}

    <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>

    <!-- Chart code -->

    <style>
        #chartdiv {
            width: 100%;
            height: 500px;
        }

    </style>

    <script>
        am5.ready(function() {

            // Create root element
            // https://www.amcharts.com/docs/v5/getting-started/#Root_element
            var root = am5.Root.new("chartdiv");


            // Set themes
            // https://www.amcharts.com/docs/v5/concepts/themes/
            root.setThemes([
                am5themes_Animated.new(root)
            ]);


            // Create chart
            // https://www.amcharts.com/docs/v5/charts/xy-chart/
            var chart = root.container.children.push(am5xy.XYChart.new(root, {
                panX: false,
                panY: false,
                wheelX: "panX",
                wheelY: "zoomX",
                layout: root.verticalLayout
            }));


            // Add legend
            // https://www.amcharts.com/docs/v5/charts/xy-chart/legend-xy-series/
            var legend = chart.children.push(
                am5.Legend.new(root, {
                    centerX: am5.p50,
                    x: am5.p50
                })
            );

            var myData = {!! json_encode($shipments['final_data']) !!};
            var data = myData;


            // Create axes
            // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
            var xAxis = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
                categoryField: "ward",
                renderer: am5xy.AxisRendererX.new(root, {
                    cellStartLocation: 0.1,
                    cellEndLocation: 0.9
                }),
                tooltip: am5.Tooltip.new(root, {})
            }));

            xAxis.data.setAll(data);

            var yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
                renderer: am5xy.AxisRendererY.new(root, {})
            }));


            // Add series
            // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
            function makeSeries(name, fieldName) {
                var series = chart.series.push(am5xy.ColumnSeries.new(root, {
                    name: name,
                    xAxis: xAxis,
                    yAxis: yAxis,
                    valueYField: fieldName,
                    categoryXField: "ward"
                }));

                series.columns.template.setAll({
                    tooltipText: "{name}, {categoryX}:{valueY}",
                    width: am5.percent(90),
                    tooltipY: 0
                });

                series.data.setAll(data);

                // Make stuff animate on load
                // https://www.amcharts.com/docs/v5/concepts/animations/
                series.appear();

                series.bullets.push(function() {
                    return am5.Bullet.new(root, {
                        locationY: 0,
                        sprite: am5.Label.new(root, {
                            text: "{valueY}",
                            fill: root.interfaceColors.get("alternativeText"),
                            centerY: 0,
                            centerX: am5.p50,
                            populateText: true
                        })
                    });
                });

                legend.data.push(series);
            }

            var title = {!! json_encode($shipments['title']) !!};
            // console.log(Object.values(title(0)));

            for (const key in title) {
                // alert(`${key}`);
                makeSeries(`${title[key]}`, `${key}`);
            }

            // Make stuff animate on load
            // https://www.amcharts.com/docs/v5/concepts/animations/
            chart.appear(1000, 100);

        }); // end am5.ready()
    </script>

    <script>
        // $(document).ready(function() {
        $("#toggle").click(function() {
            $(".card").toggle();
        });
        // });
    </script>



@endsection

<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> | Admin</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/simple-line-icons/simple-line-icons.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}"
        rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('cd-admin/avi/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet"
        type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('cd-admin/avi/global/css/components.min.css') }}" rel="stylesheet" id="style_components"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ asset('cd-admin/avi/pages/css/login-2.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="icon" type="image/x-icon" />
</head>
<!-- END HEAD -->

<body class=" login">
    <!-- BEGIN LOGO -->
    <div class="logo">

        <img style="height: 100px;" /> </a>
    </div>
    <!-- END LOGO -->
    @if (session('status') == 'fail')
        <button class="btn btn-danger mt-sweetalert gk-btn-success" id="gk-success" data-title="Delete"
            data-message="Email doesnt match" data-type="error" data-allow-outside-click="true"
            data-confirm-button-class="btn-danger"></button>

    @elseif (session('status')=='insert')
        <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Inserted"
            data-message="Please check your email" data-type="success" data-allow-outside-click="true"
            data-confirm-button-class="btn-success"></button>

    @endif
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form class="login-form" action="{{ route('login') }}" method="post">
            @csrf
            <div class="form-title">
                <span class="form-title">नमस्कार! सुरु गरौं</span>
                <br>
                <span class="form-subtitle">जारी राख्नको लागी साइन इन गर्नुहोस्।</span>
            </div>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span> दर्ता ईमेल र पासवर्ड प्रविष्ट गर्नुहोस्। </span>
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">ईमेल</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off"
                    placeholder="Email" name="email" />


                @if ($errors->has('email'))
                    <span class="alert alert-danger help-block">
                        <span> ईमेल वा पासवर्ड गलत छ। </span>
                @endif


            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="control-label visible-ie8 visible-ie9">पासवर्ड</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off"
                    placeholder="Password" name="password" />
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>
                            पासवर्ड
                            मेल खाएन</strong>
                    </span>
                @endif

            </div>
            <div class="form-actions">
                <button type="submit" class="btn red btn-block uppercase">लग - इन</button>
                <div class="pull-right forget-password-block">
                    <a href="{{ url('forgetpassword') }}" id="forget-password" class="forget-password">
                        पासवर्ड भुल्नु भयो ?</a>
                </div>
            </div>





        </form>


        <!-- END LOGIN FORM -->
        <!-- BEGIN FORGOT PASSWORD FORM -->


        <!-- END FORGOT PASSWORD FORM -->


    </div>
    <div class="page-footer">
        <div class="page-footer-inner" align="center"> {{ date('Y') }} &copy; Manufacture
            <a target="_blank" href="#">Deltrox</a>
        </div>
        <!-- END LOGIN -->
        <!--[if lt IE 9]>
<script src="{{ url('cd-admin/avi/global/plugins/respond.min.js') }}"></script>
<script src="{{ url('cd-admin/avi/global/plugins/excanvas.min.js') }}"></script>
<script src="{{ url('cd-admin/avi/global/plugins/ie8.fix.min.js') }}"></script>
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ asset('cd-admin/avi/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('cd-admin/avi/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript">
        </script>
        <script src="{{ asset('cd-admin/avi/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('cd-admin/avi/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"
                type="text/javascript"></script>
        <script src="{{ asset('cd-admin/avi/global/plugins/jquery.blockui.min.js') }}" type="text/javascript">
        </script>
        <script src="{{ asset('cd-admin/avi/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"
                type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ asset('cd-admin/avi/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"
                type="text/javascript"></script>
        <script src="{{ asset('cd-admin/avi/global/plugins/jquery-validation/js/additional-methods.min.js') }}"
                type="text/javascript"></script>
        <script src="{{ asset('cd-admin/avi/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript">
        </script>
        <script src="{{ asset('cd-admin/avi/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}"
                type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ asset('cd-admin/avi/global/scripts/app.min.js') }}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ asset('cd-admin/avi/pages/scripts/login.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('cd-admin/avi/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript">
        </script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function() {
                $('#clickmewow').click(function() {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
</body>

</html>s

@if(count($college)>0)
<div class="row" id="college-table">
  <div class="col-12">
      <h3>विश्वविद्यालय तथा क्याम्पसहरु</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="college-data">
            <thead>
              <tr>
                  <th>वडा</th>
                  <th>क्याम्पसको नाम</th>
                  <th>ठेगाना</th>
                  <th>सञ्चालित कार्यक्रम तथा तह</th>
                  <th>छात्रा</th>
                  <th>छात्र</th>
                  <th>सम्पर्क वयक्ति</th>
                  <th>सम्पर्क नं.</th>
              </tr>
            </thead>
            <tbody>
              @foreach($college as $comunitybuilding)
              <tr>
                <td class="text-center">{{ $comunitybuilding->ward }}</td>
                <td class="text-center">{{ $comunitybuilding->name }}</td>
                <td class="text-center">{{ $comunitybuilding->address }}</td>
                <td class="text-center">{{ $comunitybuilding->programmes }}</td>
                <td class="text-center">{{ $comunitybuilding->girls }}</td>
                <td class="text-center">{{ $comunitybuilding->boys }}</td>
                <td class="text-center">{{ $comunitybuilding->contact_person }}</td>
                <td class="text-center">{{ $comunitybuilding->contact_no }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                  <th>क्याम्पसको नाम</th>
                  <th>ठेगाना</th>
                  <th>सञ्चालित कार्यक्रम तथा तह</th>
                  <th>छात्रा</th>
                  <th>छात्र</th>
                  <th>सम्पर्क वयक्ति</th>
                  <th>सम्पर्क नं.</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
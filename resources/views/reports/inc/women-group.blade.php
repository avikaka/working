@if(count($womenGroup)>0)
<div class="row" id="women-group-table">
  <div class="col-12">
      <h3>आमा समूहको विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="women-group-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>आमा / महिला समूहको नाम</th>
                <th>ठेगाना</th>
                <th>सदस्य संख्या</th>
                <th>बचत रकम</th>
                <th>लगानी रकम</th>
                <th>लगानीको मुख्य क्षेत्र</th>
                <th>सम्पर्क वयक्ति</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </thead>
            <tbody>
              @foreach($womenGroup as $comunitybuilding)
              <tr>
                <td class="text-center">{{ $comunitybuilding->ward }}</td>
                <td class="text-center">{{ $comunitybuilding->name }}</td>
                <td class="text-center">{{ $comunitybuilding->address }}</td>
                <td class="text-center">{{ $comunitybuilding->members }}</td>
                <td class="text-center">{{ $comunitybuilding->savings }}</td>
                <td class="text-center">{{ $comunitybuilding->loans }}</td>
                <td class="text-center">{{ $comunitybuilding->loan_area }}</td>
                <td class="text-center">{{ $comunitybuilding->contact_person }}</td>
                <td class="text-center">{{ $comunitybuilding->contact_no }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>आमा / महिला समूहको नाम</th>
                <th>ठेगाना</th>
                <th>सदस्य संख्या</th>
                <th>बचत रकम</th>
                <th>लगानी रकम</th>
                <th>लगानीको मुख्य क्षेत्र</th>
                <th>सम्पर्क वयक्ति</th>
                <th>सम्पर्क नं.</th>   
             </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
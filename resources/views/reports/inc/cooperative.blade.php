@if(count($cooperatives)>0)
<div class="row" id="cooperative-table">
  <div class="col-12">
      <h3>सहकारी संस्था</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="cooperative-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>सहकारी संस्थाको नाम</th>
                <th>प्रकार</th>
                <th>ऋण लगानी क्षेत्र</th>
                <th>पुरुष</th>
                <th>महिला</th>
                <th>सम्पर्क व्यक्ति</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </thead>
            <tbody>
              @foreach($cooperatives as $cooperative)
              <tr>
                <td class="text-center">{{ $cooperative->ward }}</td>
                <td class="text-center">{{ $cooperative->name }}</td>
                <td class="text-center">{{ $cooperative->type }}</td>
                <td class="text-center">{{ $cooperative->loan_area }}</td>
                <td class="text-center">{{ $cooperative->male }}</td>
                <td class="text-center">{{ $cooperative->female }}</td>
                <td class="text-center">{{ $cooperative->contact_person }}</td>
                <td class="text-center">{{ $cooperative->contact_no }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>सहकारी संस्थाको नाम</th>
                <th>प्रकार</th>
                <th>ऋण लगानी क्षेत्र</th>
                <th>पुरुष</th>
                <th>महिला</th>
                <th>सम्पर्क व्यक्ति</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
@if(count($religious)>0)
<div class="row" id="religious-table">
  <div class="col-12">
      <h3>धार्मिक स्थलहरुको विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="religious-data">
            <thead>
              <tr>
                  <th>वडा</th>
                  <th>धार्मिक स्थलको नाम</th>
                  <th>ठेगाना</th>
                  <th>धर्म/समुदाय</th>
                  <th>सडकको पहुँच</th>
                  <th>विशेषता</th>
                  <th>मेला लाग्छ?</th>
                  <th>मेला लाग्ने समय/अवसर</th>
                  <th>कैफियत</th>
              </tr>
            </thead>
            <tbody>
              @foreach($religious as $comunitybuilding)
              <tr>
                <td class="text-center">{{ $comunitybuilding->ward }}</td>
                <td class="text-center">{{ $comunitybuilding->name }}</td>
                <td class="text-center">{{ $comunitybuilding->address }}</td>
                <td class="text-center">{{ $comunitybuilding->access_road }}</td>
                <td class="text-center">{{ $comunitybuilding->community }}</td>
                <td class="text-center">{{ $comunitybuilding->features }}</td>
                <td class="text-center">{{ $comunitybuilding->festive }}</td>
                <td class="text-center">{{ $comunitybuilding->occasion }}</td>
                <td class="text-center">{{ $comunitybuilding->remarks }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>धार्मिक स्थलको नाम</th>
                <th>ठेगाना</th>
                <th>धर्म/समुदाय</th>
                <th>सडकको पहुँच</th>
                <th>विशेषता</th>
                <th>मेला लाग्छ?</th>
                <th>मेला लाग्ने समय/अवसर</th>
                <th>कैफियत</th>
                
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
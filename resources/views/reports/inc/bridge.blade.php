@if(count($bridges)>0)
<div class="row" id="bridge-table">
  <div class="col-12">
      <h3>पुल पुलेसा सम्बन्धी विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="bridge-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>पुलको नाम</th>
                <th>नदी/खोला/खोल्सीको नाम</th>
                <th>पुलले जोडने वस्तीहरु</th>                                    
                <th>अवस्था</th>                                    
                <th>कैफियत</th>
              </tr>
            </thead>
            <tbody>
              @foreach($bridges as $bridge)
              <tr>
                <td class="text-center">{{ $bridge->ward }}</td>
                <td class="text-center">{{ $bridge->name }}</td>
                <td class="text-center">{{ $bridge->river_name }}</td>
                <td class="text-center">{{ $bridge->connecting_settlements }}</td>
                <td class="text-center">{{ $bridge->status }}</td>
                <td class="text-center">{{ $bridge->remarks }}</td>
                
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>पुलको नाम</th>
                <th>नदी/खोला/खोल्सीको नाम</th>
                <th>पुलले जोडने वस्तीहरु</th>                                    
                <th>अवस्था</th>                                    
                <th>कैफियत</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
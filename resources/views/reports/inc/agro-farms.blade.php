@if(count($agroFarms)>0)
<div class="row" id="agro-farm-table">
  <div class="col-12">
      <h3>कृषि तथा पशु फाराम</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="agro-farm-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>कृषि तथा पशु फारामको नाम</th>
                <th>प्रकार</th>
                <th>ठेगाना</th>
                <th>सम्पर्क व्यक्ति</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </thead>
            <tbody>
              @foreach($agroFarms as $agroFarm)
              <tr>
                <td class="text-center">{{ $agroFarm->ward }}</td>
                <td class="text-center">{{ $agroFarm->name }}</td>
                <td class="text-center">{{ $agroFarm->type }}</td>
                <td class="text-center">{{ $agroFarm->address }}</td>
                <td class="text-center">{{ $agroFarm->contact_person }}</td>
                <td class="text-center">{{ $agroFarm->contact_no }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>कृषि तथा पशु फारामको नाम</th>
                <th>प्रकार</th>
                <th>ठेगाना</th>
                <th>सम्पर्क व्यक्ति</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
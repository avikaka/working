@if(count($childrenClub)>0)
<div class="row" id="children-club-table">
  <div class="col-12">
      <h3>बालक्लबहरुको विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="children-club-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>बालक्लबको नाम</th>
                <th>ठेगाना</th>
                <th>प्रकार</th>
                <th>बालिका सदस्य</th>
                <th>बालक सदस्य</th>
              </tr>
            </thead>
            <tbody>
              @foreach($childrenClub as $comunitybuilding)
              <tr>
                <td class="text-center">{{ $comunitybuilding->ward }}</td>
                <td class="text-center">{{ $comunitybuilding->name }}</td>
                <td class="text-center">{{ $comunitybuilding->address }}</td>
                <td class="text-center">{{ $comunitybuilding->type }}</td>
                <td class="text-center">{{ $comunitybuilding->girls }}</td>
                <td class="text-center">{{ $comunitybuilding->boys }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>बालक्लबको नाम</th>
                <th>ठेगाना</th>
                <th>प्रकार</th>
                <th>बालिका सदस्य</th>
                <th>बालक सदस्य</th>           
             </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
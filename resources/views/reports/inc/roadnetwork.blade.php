@if(count($roadnetworks)>0)
<div class="row" id="roadnetwork-table">
  <div class="col-12">
      <h3>सडक संजालको  अवस्था विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="roadnetwork-data">
            <thead>
              <tr>
                <th>वडा</th>
               <th>सडक संजालको नाम</th>
                <th>सडकको वर्गिकरण</th>
                <th>प्रकार</th>                                    
                <th>सतहको प्रकार</th>
                <th>अनुमानित लम्बाई</th>
                <th>सुरु बिन्दु</th>
                <th>अन्तिम बिन्दु</th>
                <th>सडकले छुने वस्तिहरु</th>
                <th>सार्वजनिक यातायातको अवस्था</th>
                <th>कैफियत</th>
              </tr>
            </thead>
            <tbody>
              @foreach($roadnetworks as $roadnetwork)
              <tr>
                <td class="text-center">{{ $roadnetwork->ward }}</td>
                <td class="text-center">{{ $roadnetwork->name }}</td>
                <td class="text-center">{{ $roadnetwork->category }}</td>
                <td class="text-center">{{ $roadnetwork->type }}</td>
                <td class="text-center">{{ $roadnetwork->surface }}</td>
                <td class="text-center">{{ $roadnetwork->length }}</td>
                <td class="text-center">{{ $roadnetwork->start }}</td>
                <td class="text-center">{{ $roadnetwork->end }}</td>
                <td class="text-center">{{ $roadnetwork->settlements }}</td>
                <td class="text-center">{{ $roadnetwork->public_transport }}</td>
                <td class="text-center">{{ $roadnetwork->remarks }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
               <th>सडक संजालको नाम</th>
                <th>सडकको वर्गिकरण</th>
                <th>प्रकार</th>                                    
                <th>सतहको प्रकार</th>
                <th>अनुमानित लम्बाई</th>
                <th>सुरु बिन्दु</th>
                <th>अन्तिम बिन्दु</th>
                <th>सडकले छुने वस्तिहरु</th>
                <th>सार्वजनिक यातायातको अवस्था</th>
                <th>कैफियत</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
@if(count($agroVet)>0)
<div class="row" id="agro-vet-table">
  <div class="col-12">
      <h3>कृषक तथा पशुपालन समूहको विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="agro-vet-data">
            <thead>
              <tr>
                  <th>वडा</th>
                  <th>कृषक तथा पशुपालन समूहको नाम</th>
                  <th>ठेगाना</th>
                  <th>प्रकार</th>
                  <th>सम्पर्क वयक्ति</th>
                  <th>सम्पर्क नं.</th>
              </tr>
            </thead>
            <tbody>
              @foreach($agroVet as $agroFarm)
              <tr>
                <td class="text-center">{{ $agroFarm->ward }}</td>
                <td class="text-center">{{ $agroFarm->name }}</td>
                <td class="text-center">{{ $agroFarm->address }}</td>
                <td class="text-center">{{ $agroFarm->type }}</td>
                <td class="text-center">{{ $agroFarm->contact_person }}</td>
                <td class="text-center">{{ $agroFarm->contact_no }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>कृषक तथा पशुपालन समूहको नाम</th>
                <th>ठेगाना</th>
                <th>प्रकार</th>
                <th>सम्पर्क वयक्ति</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
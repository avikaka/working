@if(count($youthClub)>0)
<div class="row" id="youth-club-table">
  <div class="col-12">
      <h3>यूवा क्लब तथा खेलकुद क्लब सम्बन्धी विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="youth-club-data">
            <thead>
              <tr>
                  <th>वडा</th>
                  <th>क्लबको नाम</th>
                  <th>ठेगाना</th>
                  <th>प्रकार</th>
                  <th>सदस्य संख्या</th>
                  <th>सम्पर्क वयक्ति</th>
                  <th>सम्पर्क नं.</th>
                  <th>कैफियत</th>
              </tr>
            </thead>
            <tbody>
              @foreach($youthClub as $agroFarm)
              <tr>
                <td class="text-center">{{ $agroFarm->ward }}</td>
                <td class="text-center">{{ $agroFarm->name }}</td>
                <td class="text-center">{{ $agroFarm->address }}</td>
                <td class="text-center">{{ $agroFarm->type }}</td>
                <td class="text-center">{{ $agroFarm->members }}</td>
                <td class="text-center">{{ $agroFarm->contact_person }}</td>
                <td class="text-center">{{ $agroFarm->contact_no }}</td>
                <td class="text-center">{{ $agroFarm->remarks }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>क्लबको नाम</th>
                <th>ठेगाना</th>
                <th>प्रकार</th>
                <th>सदस्य संख्या</th>
                <th>सम्पर्क वयक्ति</th>
                <th>सम्पर्क नं.</th>
                <th>कैफियत</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
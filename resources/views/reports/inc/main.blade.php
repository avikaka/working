<div class="row">
        <div class="col-12">
            <h3>वडा तथा लिङ्गअनुसार जनसंख्या</h3>
        </div>
        <div class="col-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-hover table-bordered">
                  <thead>
                    <tr>
                      <th style="width:9%">वडा</th>
                      <th style="width:12%">पुरूष</th>
                      <th style="width:12%">महिला</th>
                      <th style="width:12%">जम्मा</th>
                      <th style="width:11%">परिवार संख्या</th>
                      <th style="width:11%">क्षेत्रफल</th>
                      <th style="width:11%">जनघनत्व प्रति वर्ग कि.मि.</th>
                      <th style="width:11%">औषत परिवार आकार</th>
                      <th style="width:11%">लैङ्गिक अनुपात</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-center">1</td>
                      <td class="text-center">1456</td>
                      <td class="text-center">1405</td>
                      <td class="text-center">2861</td>
                      <td class="text-center">619</td>
                      <td class="text-center">18.36</td>
                      <td class="text-center">155.83</td>
                      <td class="text-center">4.62</td>
                      <td class="text-center">103.63</td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th class="text-center">जम्मा</th>
                      <th class="text-center">13700</th>
                      <th class="text-center">14792</th>
                      <th class="text-center">28492</th>
                      <th class="text-center">7331</th>
                      <th class="text-center">105.09</th>
                      <th class="text-center">271.12</th>
                      <th class="text-center">3.89</th>
                      <th class="text-center">92.62</th>
                    </tr>
                  </tfoot>

                </table>{{-- end of table --}}
              </div> {{-- end of .table-responsive --}}
            </div>
          </div>
        </div>
      </div> {{-- row end --}}

@if(count($socialHome)>0)
<div class="row" id="social-home-table">
  <div class="col-12">
      <h3>महिला सुरक्षा गृह/ बाल गृह/ बाल स्याहार केन्द्र /विद्धाश्राम वा सत्सङ्ग केन्द्र</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="social-home-data">
            <thead>
              <tr>
                  <th>वडा</th>
                  <th>गृह / केन्द्रको नाम</th>
                  <th>ठेगाना</th>
                  <th>प्रकार</th>
                  <th>सम्पर्क वयक्ति</th>
                  <th>सम्पर्क नं.</th>
              </tr>
            </thead>
            <tbody>
              @foreach($socialHome as $comunitybuilding)
              <tr>
                <td class="text-center">{{ $comunitybuilding->ward }}</td>
                <td class="text-center">{{ $comunitybuilding->name }}</td>
                <td class="text-center">{{ $comunitybuilding->address }}</td>
                <td class="text-center">{{ $comunitybuilding->type }}</td>
                <td class="text-center">{{ $comunitybuilding->contact_person }}</td>
                <td class="text-center">{{ $comunitybuilding->contact_number }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>गृह / केन्द्रको नाम</th>
                <th>ठेगाना</th>
                <th>प्रकार</th>
                <th>सम्पर्क वयक्ति</th>
                <th>सम्पर्क नं.</th>           
             </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
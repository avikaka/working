@if(count($pondslakeswetlands)>0)
<div class="row" id="pondslakeswetland-table">
  <div class="col-12">
      <h3>सार्वजनिक निजी पोखरी, ताल, सिमसारको विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="pondslakeswetland-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>पोखरी, ताल, सिमसारको नाम</th>                
                <th>ठेगाना</th>
                <th>प्रकार</th>
                <th>स्वमित्व</th>
                <th>क्षेत्रफल</th>
                <th>सुख्खाको कारण सुक्नेखतरा</th>
              </tr>
            </thead>
            <tbody>
              @foreach($pondslakeswetlands as $pondslakeswetland)
              <tr>
                <td class="text-center">{{ $pondslakeswetland->ward }}</td>
                <td class="text-center">{{ $pondslakeswetland->name }}</td>
                <td class="text-center">{{ $pondslakeswetland->address }}</td>
                <td class="text-center">{{ $pondslakeswetland->type }}</td>
                <td class="text-center">{{ $pondslakeswetland->ownership }}</td>
                <td class="text-center">{{ $pondslakeswetland->area }}</td>
                <td class="text-center">{{ $pondslakeswetland->drying }}</td>                
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>पोखरी, ताल, सिमसारको नाम</th>                
                <th>ठेगाना</th>
                <th>प्रकार</th>
                <th>स्वमित्व</th>
                <th>क्षेत्रफल</th>
                <th>सुख्खाको कारण सुक्नेखतरा</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
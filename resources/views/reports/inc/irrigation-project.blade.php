@if(count($irrigatinprojects)>0)
<div class="row" id="irrigation-project-table">
  <div class="col-12">
      <h3>सिंचाई आयोजनाहरुको विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="irrigation-project-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>आयोजनको नाम</th>                
                <th>ठेगाना</th>
                <th>सिंचाइको माध्यम</th>
                <th>सिंचित क्षेत्र</th>
                <th>सिंचाइको उपलब्धता</th>
                <th>लाभान्वित घरधुरी</th>
              </tr>
            </thead>
            <tbody>
              @foreach($irrigatinprojects as $irrigatinproject)
              <tr>
                <td class="text-center">{{ $irrigatinproject->ward }}</td>
                <td class="text-center">{{ $irrigatinproject->name }}</td>
                <td class="text-center">{{ $irrigatinproject->address }}</td>
                <td class="text-center">{{ $irrigatinproject->medium }}</td>
                <td class="text-center">{{ $irrigatinproject->area }}</td>
                <td class="text-center">{{ $irrigatinproject->type }}</td>
                <td class="text-center">{{ $irrigatinproject->beneficiary }}</td>                
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>आयोजनको नाम</th>                
                <th>ठेगाना</th>
                <th>सिंचाइको माध्यम</th>
                <th>सिंचित क्षेत्र</th>
                <th>सिंचाइको उपलब्धता</th>
                <th>लाभान्वित घरधुरी</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
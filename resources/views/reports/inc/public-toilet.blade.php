@if(count($public_toilets)>0)
<div class="row" id="public-toilet-table">
  <div class="col-12">
      <h3>सार्वजनिक शौचालय सम्बन्धी विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="public-toilet-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>शौचालय नाम</th>
                <th>ठेगाना</th>
                <th>संख्या</th>
                <th>अवस्था</th>
                <th>महिलाको लागि छुट्टै भएको</th>
                <th>कैफियत</th>
              </tr>
            </thead>
            <tbody>
              @foreach($public_toilets as $public_toilet)
              <tr>
                <td class="text-center">{{ $public_toilet->ward }}</td>
                <td class="text-center">{{ $public_toilet->name }}</td>
                <td class="text-center">{{ $public_toilet->address }}</td>
                <td class="text-center">{{ $public_toilet->numbers }}</td>
                <td class="text-center">{{ $public_toilet->status }}</td>
                <td class="text-center">{{ $public_toilet->for_women }}</td>
                <td class="text-center">{{ $public_toilet->remarks }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>शौचालय नाम</th>
                <th>ठेगाना</th>
                <th>संख्या</th>
                <th>अवस्था</th>
                <th>महिलाको लागि छुट्टै भएको</th>
                <th>कैफियत</th>
                
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
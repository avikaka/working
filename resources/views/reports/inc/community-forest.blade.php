@if(count($communityforests)>0)
<div class="row" id="communityforest-table">
  <div class="col-12">
      <h3>सामुदायिक वनसम्बन्धी विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="communityforest-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>सामुदायिक वनको नाम </th>                
                <th>जंगलको क्षेत्रफल हेक्टरमा</th>
                <th>ओगटेको क्षेत्रफल</th>
                <th>सदस्य घरधुरी</th>                                    
              </tr>
            </thead>
            <tbody>
              @foreach($communityforests as $communityforest)
              <tr>
                <td class="text-center">{{ $communityforest->ward }}</td>
                <td class="text-center">{{ $communityforest->name }}</td>
                <td class="text-center">{{ $communityforest->forest_area }}</td>
                <td class="text-center">{{ $communityforest->occupied_area }}</td>
                <td class="text-center">{{ $communityforest->member_household }}</td>                
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>सामुदायिक वनको नाम </th>                
                <th>जंगलको क्षेत्रफल हेक्टरमा</th>
                <th>ओगटेको क्षेत्रफल</th>
                <th>सदस्य घरधुरी</th> 
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
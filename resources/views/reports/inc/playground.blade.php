@if(count($playgrounds)>0)
<div class="row" id="playground-table">
  <div class="col-12">
      <h3>खेलमैदान</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="playground-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>खेलमैदानको नाम</th>
                <th>क्षेत्रफल</th>
                <th>ठेगाना</th>
                <th>प्रकार</th>
                <th>कैफियत</th>
              </tr>
            </thead>
            <tbody>
              @foreach($playgrounds as $playground)
              <tr>
                <td class="text-center">{{ $playground->ward }}</td>
                <td class="text-center">{{ $playground->name }}</td>
                <td class="text-center">{{ $playground->area }}</td>
                <td class="text-center">{{ $playground->address }}</td>
                <td class="text-center">{{ $playground->type }}</td>
                <td class="text-center">{{ $playground->remarks }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>खेलमैदानको नाम</th>
                <th>क्षेत्रफल</th>
                <th>ठेगाना</th>
                <th>प्रकार</th>
                <th>कैफियत</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
<div class="row">
        <div class="col-12">
            <h3>वडा तथा लिङ्गअनुसार जनसंख्या</h3>
        </div>
        <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
							<tr class="table-info">
								<th class="text-center">उमेर समूह</th>
								@for($i=1;$i<=TOTAL_WARD;$i++)
								<th class="text-center">{{ $i }}</th>
								@endfor
								<th class="text-center">जम्मा</th>
                            </tr>
                        </thead>
                        <tbody>
							@php for($i=1;$i<= TOTAL_WARD;$i++){${'grandTotal'.$i}=0;}
							$grandTotal=0; @endphp
							@foreach($ageGroup as $row)
							@php $total=0; @endphp
							<tr>
								<td class="text-center">{{ getAgeGroup($row['age_group']) }}</td>
								@for($i=1;$i<=TOTAL_WARD;$i++)
								<td class="text-center">{{ $row['ward'.$i] }}</td>
								@php $total += $row['ward'.$i] @endphp
								@endfor
								<td class="text-center">{{ $total }}</td>
							</tr>
							@php for($i=1;$i<= TOTAL_WARD;$i++){
								${'grandTotal'.$i}+=$row['ward'.$i];
							} 
							@endphp
							@endforeach
                        </tbody>
                        <tfoot>
							<tr>
								<th>जम्मा</th>
								@for($i=1;$i<=TOTAL_WARD; $i++)
								<th>{{ ${'grandTotal'.$i} }}</th>
								@php $grandTotal += ${'grandTotal'.$i} @endphp
								@endfor
								<th>{{ $grandTotal }}</th>
                            </tr>
                        </tfoot>
						</table>
            </div> 
            </div>
        </div>
        </div>
    </div> 
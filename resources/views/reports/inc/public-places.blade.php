@if(count($publicPlace)>0)
<div class="row" id="public-place-table">
  <div class="col-12">
      <h3>सार्वजनिक पाटीपैवा र चौतारा सम्न्धी विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="public-place-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>पाटीपैवा र चौताराको नाम</th>
                <th>ठेगाना</th>
                <th>किसिम</th>
                <th>उपयोग</th>
              </tr>
            </thead>
            <tbody>
              @foreach($publicPlace as $agroFarm)
              <tr>
                <td class="text-center">{{ $agroFarm->ward }}</td>
                <td class="text-center">{{ $agroFarm->name }}</td>
                <td class="text-center">{{ $agroFarm->address }}</td>
                <td class="text-center">{{ $agroFarm->type }}</td>
                <td class="text-center">{{ $agroFarm->uses }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>पाटीपैवा र चौताराको नाम</th>
                <th>ठेगाना</th>
                <th>किसिम</th>
                <th>उपयोग</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
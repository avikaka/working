@if(count($comunitybuildings)>0)
<div class="row" id="community-building-table">
  <div class="col-12">
      <h3>सरकारी तथा सामुदायिक भवन सम्बन्धी विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="community-building-data">
            <thead>
              <tr>
                <th>वडा</th>                
                <th>सरकारी तथा सामुदायिक भवनको नाम</th>                
                <th>ठेगाना</th>
                <th>भवनको प्रकार</th>
                <th>अवस्था</th>
                <th>कैफियत</th>
              </tr>
            </thead>
            <tbody>
              @foreach($comunitybuildings as $comunitybuilding)
              <tr>
                <td class="text-center">{{ $comunitybuilding->ward }}</td>
                <td class="text-center">{{ $comunitybuilding->name }}</td>
                <td class="text-center">{{ $comunitybuilding->address }}</td>
                <td class="text-center">{{ $comunitybuilding->type }}</td>
                <td class="text-center">{{ $comunitybuilding->status }}</td>
                <td class="text-center">{{ $comunitybuilding->remarks }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>                
                <th>सरकारी तथा सामुदायिक भवनको नाम</th>
                <th>ठेगाना</th>
                <th>भवनको प्रकार</th>
                <th>अवस्था</th>
                <th>कैफियत</th>
                
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
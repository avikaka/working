@if(count($tourism)>0)
<div class="row" id="bank-table">
  <div class="col-12">
      <h3>बैँक तथा बित्तीयसंस्थाको विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="bank-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>बैँक तथा बित्तीयसंस्थाको नाम</th>
                <th>ठेगाना</th>
                <th>प्रकार</th>
                <th>साखाको प्रकार</th>
                <th>सम्पर्क वयक्ति.</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </thead>
            <tbody>
              @foreach($bank as $comunitybuilding)
              <tr>
                <td class="text-center">{{ $comunitybuilding->ward }}</td>
                <td class="text-center">{{ $comunitybuilding->name }}</td>
                <td class="text-center">{{ $comunitybuilding->address }}</td>
                <td class="text-center">{{ $comunitybuilding->type }}</td>
                <td class="text-center">{{ $comunitybuilding->branch_type }}</td>
                <td class="text-center">{{ $comunitybuilding->contact_person }}</td>
                <td class="text-center">{{ $comunitybuilding->contact_number }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>बैँक तथा बित्तीयसंस्थाको नाम</th>
                <th>ठेगाना</th>
                <th>प्रकार</th>
                <th>साखाको प्रकार</th>
                <th>सम्पर्क वयक्ति.</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
@if(count($healthInstitution)>0)
<div class="row" id="health-institution-table">
  <div class="col-12">
      <h3>स्वास्थ्य संथाको विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="health-institution-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>स्वास्थ्य संथाको नाम</th>
                <th>ठेगाना</th>
                <th>प्रकार</th>
                <th>स्वास्थ्य संस्थामा उपलब्ध सेवाहरु</th>
                <th>सम्पर्क वयक्ति</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </thead>
            <tbody>
              @foreach($healthInstitution as $comunitybuilding)
              <tr>
                <td class="text-center">{{ $comunitybuilding->ward }}</td>
                <td class="text-center">{{ $comunitybuilding->name }}</td>
                <td class="text-center">{{ $comunitybuilding->address }}</td>
                <td class="text-center">{{ $comunitybuilding->type }}</td>
                <td class="text-center">{{ $comunitybuilding->services }}</td>
                <td class="text-center">{{ $comunitybuilding->contact_person }}</td>
                <td class="text-center">{{ $comunitybuilding->contact_no }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>स्वास्थ्य संथाको नाम</th>
                <th>ठेगाना</th>
                <th>प्रकार</th>
                <th>स्वास्थ्य संस्थामा उपलब्ध सेवाहरु</th>
                <th>सम्पर्क वयक्ति</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
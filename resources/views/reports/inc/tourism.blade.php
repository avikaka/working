@if (count($tourism) > 0)
    <div class="row" id="tourism-table">
        <div class="col-12">
            <h3>पर्यटकीय स्थलहरुको विवरण (धार्मिक/ऐतिहासिक/पुरातात्विक)</h3>
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="tourism-data">
                            <thead>
                                <tr>
                                    <th>वडा</th>
                                    <th>पर्यटकीय स्थलको नाम</th>
                                    <th>ठेगाना</th>
                                    <th>सडकको पहुँच</th>
                                    <th>स्वामित्व</th>
                                    <th>विशेषता</th>
                                    <th>आगन्तुक संख्या</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tourism as $comunitybuilding)
                                    <tr>
                                        <td class="text-center">{{ $comunitybuilding->ward }}</td>
                                        <td class="text-center">{{ $comunitybuilding->name }}</td>
                                        <td class="text-center">{{ $comunitybuilding->address }}</td>
                                        <td class="text-center">{{ $comunitybuilding->access_road }}</td>
                                        <td class="text-center">{{ $comunitybuilding->ownership }}</td>
                                        <td class="text-center">{{ $comunitybuilding->features }}</td>
                                        <td class="text-center">{{ $comunitybuilding->yearly_tourist_number }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>वडा</th>
                                    <th>पर्यटकीय स्थलको नाम</th>
                                    <th>ठेगाना</th>
                                    <th>सडकको पहुँच</th>
                                    <th>स्वामित्व</th>
                                    <th>विशेषता</th>
                                    <th>आगन्तुक संख्या</th>

                                </tr>
                            </tfoot>

                        </table>{{-- end of table --}}
                    </div> {{-- end of .table-responsive --}}
                </div>
            </div>
        </div>
    </div> {{-- row end --}}
@endif

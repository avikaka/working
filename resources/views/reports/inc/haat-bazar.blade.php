@if(count($haatbazars)>0)
<div class="row" id="haatbazar-table">
  <div class="col-12">
      <h3>कृषि तथा हाट बजार सम्बन्धी विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="haatbazar-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>हाट बजारस्थलको नाम</th>                
                <th>ठेगाना</th>
                <th>हाट बजार लाग्ने दिन</th>
                <th>हाट लाग्ने अनतराल</th>                                    
                <th>उपलब्ध प्रमुख पूर्वाधार</th>
                <th>कैफियत</th>
              </tr>
            </thead>
            <tbody>
              @foreach($haatbazars as $haatbazar)
              <tr>
                <td class="text-center">{{ $haatbazar->ward }}</td>
                <td class="text-center">{{ $haatbazar->name }}</td>
                <td class="text-center">{{ $haatbazar->address }}</td>
                <td class="text-center">{{ $haatbazar->type }}</td>
                <td class="text-center">{{ $haatbazar->day }}</td>
                <td class="text-center">{{ $haatbazar->interval }}</td>
                <td class="text-center">{{ $haatbazar->infrastructure }}</td>
                <td class="text-center">{{ $haatbazar->remarks }}</td>                
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>हाट बजारस्थलको नाम</th>                
                <th>ठेगाना</th>
                <th>हाट बजार लाग्ने दिन</th>
                <th>हाट लाग्ने अनतराल</th>                                    
                <th>उपलब्ध प्रमुख पूर्वाधार</th>
                <th>कैफियत</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
@if(count($playgrounds)>0)
<div class="row" id="tlo-table">
  <div class="col-12">
      <h3>टोल विकास संस्था</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="tlo-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>संस्थाको नाम</th>
                <th>सदस्य संख्या</th>
                <th>समेटिएका गाउँहरू</th>
                <th>सम्पर्क व्यक्ति</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </thead>
            <tbody>
              @foreach($tlos as $tlo)
              <tr>
                <td class="text-center">{{ $tlo->ward }}</td>
                <td class="text-center">{{ $tlo->name }}</td>
                <td class="text-center">{{ $tlo->families }}</td>
                <td class="text-center">{{ $tlo->villages }}</td>
                <td class="text-center">{{ $tlo->contact_person }}</td>
                <td class="text-center">{{ $tlo->contact_no }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>संस्थाको नाम</th>
                <th>सदस्य संख्या</th>
                <th>समेटिएका गाउँहरू</th>
                <th>सम्पर्क व्यक्ति</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
@if(count($drinkwaters)>0)
<div class="row" id="drinkwater-table">
  <div class="col-12">
      <h3>खानेपानी आयोजनाहरूको विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="drinkwater-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>नाम</th>                
                <th>मुहान</th>
                <th>प्रकार</th>
                <th>लाभान्वित घरधुरी</th>                                    
                <th>सेवा प्राप्त गने</th>
                <th>सम्पर्क व्यक्ति</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </thead>
            <tbody>
              @foreach($drinkwaters as $drinkwater)
              <tr>
                <td class="text-center">{{ $drinkwater->ward }}</td>
                <td class="text-center">{{ $drinkwater->name }}</td>
                <td class="text-center">{{ $drinkwater->source }}</td>
                <td class="text-center">{{ $drinkwater->type }}</td>
                <td class="text-center">{{ $drinkwater->beneficiary }}</td>
                <td class="text-center">{{ $drinkwater->beneficiary_settlement }}</td>
                <td class="text-center">{{ $drinkwater->contact_person }}</td>
                <td class="text-center">{{ $drinkwater->contact_no }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>नाम</th>                
                <th>मुहान</th>
                <th>प्रकार</th>
                <th>लाभान्वित घरधुरी</th>                                    
                <th>सेवा प्राप्त गने</th>
                <th>सम्पर्क व्यक्ति</th>
                <th>सम्पर्क नं.</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
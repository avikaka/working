@if(count($agrovetpockets)>0)
<div class="row" id="agrovetpocket-table">
  <div class="col-12">
      <h3>कृषि/पशु पंक्षी पकेट क्षेत्रको विवरण विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="agrovetpocket-data">
            <thead>
              <tr>
                <th>वडा</th>
                <th>कृषि/पशु पंक्षी पकेट क्षेत्रको नाम</th>                
                <th>ठेगाना</th>
                <th>क्षेत्र</th>
                <th>मुख्य बाली वा पशु पंक्षीको जात</th>
                <th>कार्यन्वयन बजेटको श्रोत</th>
              </tr>
            </thead>
            <tbody>
              @foreach($agrovetpockets as $agrovetpocket)
              <tr>
                <td class="text-center">{{ $agrovetpocket->ward }}</td>
                <td class="text-center">{{ $agrovetpocket->name }}</td>
                <td class="text-center">{{ $agrovetpocket->address }}</td>
                <td class="text-center">{{ $agrovetpocket->sector }}</td>
                <td class="text-center">{{ $agrovetpocket->type }}</td>
                <td class="text-center">{{ $agrovetpocket->project_level }}</td>
                
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>
                <th>कृषि/पशु पंक्षी पकेट क्षेत्रको नाम</th>                
                <th>ठेगाना</th>
                <th>क्षेत्र</th>
                <th>मुख्य बाली वा पशु पंक्षीको जात</th>
                <th>कार्यन्वयन बजेटको श्रोत</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
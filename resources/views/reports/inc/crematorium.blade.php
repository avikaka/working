@if(count($crematoriums)>0)
<div class="row" id="crematorium-table">
  <div class="col-12">
      <h3>शवदाहस्थल, चिहानस्थलहरु तथा कव्रस्थानहरुको विवरण</h3>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="crematorium-data">
            <thead>
              <tr>
                <th>वडा</th>                
                <th>नाम</th>                
                <th>ठेगाना</th>
                <th>जातजार्त/समुदाय</th>
                <th>भौतिक पूर्वाधार</th>                                    
                <th>कैफियत</th>
                
              </tr>
            </thead>
            <tbody>
              @foreach($crematoriums as $crematorium)
              <tr>
                <td class="text-center">{{ $crematorium->ward }}</td>
                <td class="text-center">{{ $crematorium->name }}</td>
                <td class="text-center">{{ $crematorium->address }}</td>
                <td class="text-center">{{ $crematorium->community }}</td>
                <td class="text-center">{{ $crematorium->infrastructure }}</td>
                <td class="text-center">{{ $crematorium->remarks}}</td>                
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>वडा</th>                
                <th>नाम</th>                
                <th>ठेगाना</th>
                <th>जातजार्त/समुदाय</th>
                <th>भौतिक पूर्वाधार</th>                                    
                <th>कैफियत</th>
              </tr>
            </tfoot>

          </table>{{-- end of table --}}
        </div> {{-- end of .table-responsive --}}
      </div>
    </div>
  </div>
</div> {{-- row end --}}
@endif
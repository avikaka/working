<div class="row">
        <div class="col-12">
            <h3>वडा तथा लिङ्गअनुसार जनसंख्या</h3>
        </div>
        <div class="col-xl-7 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                    <th>वडा</th>
                    <th>पुरुष</th>
                    <th>महिला</th>
                    <th>अन्य लिङ्गी</th>
                    <th>जम्मा</th>
                    </tr>
                </thead>
                <tbody>
                                    
                    <tr>
                    <td>1</td>
                    <td>1455</td>
                    <td> 1401</td>
                    <td>5</td>
                    <td>2861</td>
                    </tr>                      
                    <tr>
                    <td>2</td>
                    <td>924</td>
                    <td> 1079</td>
                    <td>0</td>
                    <td>2003</td>
                    </tr>                      
                    <tr>
                    <td>3</td>
                    <td>844</td>
                    <td> 907</td>
                    <td>3</td>
                    <td>1754</td>
                    </tr>                      
                    <tr>
                    <td>4</td>
                    <td>1795</td>
                    <td> 1836</td>
                    <td>1</td>
                    <td>3632</td>
                    </tr>                      
                    <tr>
                    <td>5</td>
                    <td>1419</td>
                    <td> 1546</td>
                    <td>2</td>
                    <td>2967</td>
                    </tr>                      
                    <tr>
                    <td>6</td>
                    <td>1098</td>
                    <td> 1163</td>
                    <td>1</td>
                    <td>2262</td>
                    </tr>                      
                    <tr>
                    <td>7</td>
                    <td>1136</td>
                    <td> 1222</td>
                    <td>1</td>
                    <td>2359</td>
                    </tr>                      
                    <tr>
                    <td>8</td>
                    <td>712</td>
                    <td> 787</td>
                    <td>0</td>
                    <td>1499</td>
                    </tr>                      
                    <tr>
                    <td>9</td>
                    <td>743</td>
                    <td> 873</td>
                    <td>1</td>
                    <td>1617</td>
                    </tr>                      
                    <tr>
                    <td>10</td>
                    <td>1230</td>
                    <td> 1396</td>
                    <td>0</td>
                    <td>2626</td>
                    </tr>                      
                    <tr>
                    <td>11</td>
                    <td>853</td>
                    <td> 917</td>
                    <td>2</td>
                    <td>1772</td>
                    </tr>                      
                    <tr>
                    <td>12</td>
                    <td>1480</td>
                    <td> 1644</td>
                    <td>1</td>
                    <td>3125</td>
                    </tr>                     
                </tbody>
                <tfoot>
                    <th>Total</th>
                    <th>13689</th>
                    <th>14771</th>
                    <th>17</th>
                    <th>28477</th>
                </tfoot>
                </table>
            </div> 
            </div>
        </div>
        </div>
        <div class="col-xl-5 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <div class="chart-ward-sex ward-population">
            </div>
            </div>
        </div>
        </div>
    </div> 
@extends('template.index')
@section('title','Report Home')
@section('main-content')
        <div class="content-wrapper">
          <div class="row">
            <div class="col-12">
                <h3>वडा तथा लिङ्गअनुसार जनसंख्या</h3>
            </div>
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                      <thead>
                        <tr>
                          <th style="width:9%">वडा</th>
                          <th style="width:12%">पुरूष</th>
                          <th style="width:12%">महिला</th>
                          <th style="width:12%">जम्मा</th>
                          <th style="width:11%">परिवार संख्या</th>
                          <th style="width:11%">क्षेत्रफल</th>
                          <th style="width:11%">जनघनत्व प्रति वर्ग कि.मि.</th>
                          <th style="width:11%">औषत परिवार आकार</th>
                          <th style="width:11%">लैङ्गिक अनुपात</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="text-center">1</td>
                          <td class="text-center">1456</td>
                          <td class="text-center">1405</td>
                          <td class="text-center">2861</td>
                          <td class="text-center">619</td>
                          <td class="text-center">18.36</td>
                          <td class="text-center">155.83</td>
                          <td class="text-center">4.62</td>
                          <td class="text-center">103.63</td>
                        </tr>
                        <tr>
                          <td class="text-center">2</td>
                          <td class="text-center">924</td>
                          <td class="text-center">1079</td>
                          <td class="text-center">2003</td>
                          <td class="text-center">587</td>
                          <td class="text-center">6.98</td>
                          <td class="text-center">286.96</td>
                          <td class="text-center">3.41</td>
                          <td class="text-center">85.63</td>
                        </tr>
                        <tr>
                          <td class="text-center">3</td>
                          <td class="text-center">848</td>
                          <td class="text-center">911</td>
                          <td class="text-center">1759</td>
                          <td class="text-center">526</td>
                          <td class="text-center">7.08</td>
                          <td class="text-center">248.45</td>
                          <td class="text-center">3.34</td>
                          <td class="text-center">93.08</td>
                        </tr>
                        <tr>
                          <td class="text-center">4</td>
                          <td class="text-center">1796</td>
                          <td class="text-center">1839</td>
                          <td class="text-center">3635</td>
                          <td class="text-center">921</td>
                          <td class="text-center">16.67</td>
                          <td class="text-center">218.06</td>
                          <td class="text-center">3.95</td>
                          <td class="text-center">97.66</td>
                        </tr>
                        <tr>
                          <td class="text-center">5</td>
                          <td class="text-center">1419</td>
                          <td class="text-center">1548</td>
                          <td class="text-center">2967</td>
                          <td class="text-center">805</td>
                          <td class="text-center">11.42</td>
                          <td class="text-center">259.81</td>
                          <td class="text-center">3.69</td>
                          <td class="text-center">91.67</td>
                        </tr>
                        <tr>
                          <td class="text-center">6</td>
                          <td class="text-center">1098</td>
                          <td class="text-center">1164</td>
                          <td class="text-center">2262</td>
                          <td class="text-center">541</td>
                          <td class="text-center">8.66</td>
                          <td class="text-center">261.20</td>
                          <td class="text-center">4.18</td>
                          <td class="text-center">94.33</td>
                        </tr>
                        <tr>
                          <td class="text-center">7</td>
                          <td class="text-center">1136</td>
                          <td class="text-center">1223</td>
                          <td class="text-center">2359</td>
                          <td class="text-center">666</td>
                          <td class="text-center">4.73</td>
                          <td class="text-center">498.73</td>
                          <td class="text-center">3.54</td>
                          <td class="text-center">92.89</td>
                        </tr>
                        <tr>
                          <td class="text-center">8</td>
                          <td class="text-center">712</td>
                          <td class="text-center">787</td>
                          <td class="text-center">1499</td>
                          <td class="text-center">394</td>
                          <td class="text-center">5.70</td>
                          <td class="text-center">262.98</td>
                          <td class="text-center">3.80</td>
                          <td class="text-center">90.47</td>
                        </tr>
                        <tr>
                          <td class="text-center">9</td>
                          <td class="text-center">743</td>
                          <td class="text-center">874</td>
                          <td class="text-center">1617</td>
                          <td class="text-center">439</td>
                          <td class="text-center">5.82</td>
                          <td class="text-center">277.83</td>
                          <td class="text-center">3.68</td>
                          <td class="text-center">85.01</td>
                        </tr>
                        <tr>
                          <td class="text-center">10</td>
                          <td class="text-center">1230</td>
                          <td class="text-center">1396</td>
                          <td class="text-center">2626</td>
                          <td class="text-center">719</td>
                          <td class="text-center">8.81</td>
                          <td class="text-center">298.07</td>
                          <td class="text-center">3.65</td>
                          <td class="text-center">88.11</td>
                        </tr>
                        <tr>
                          <td class="text-center">11</td>
                          <td class="text-center">858</td>
                          <td class="text-center">921</td>
                          <td class="text-center">1779</td>
                          <td class="text-center">409</td>
                          <td class="text-center">3.53</td>
                          <td class="text-center">503.97</td>
                          <td class="text-center">4.35</td>
                          <td class="text-center">93.16</td>
                        </tr>
                        <tr>
                          <td class="text-center">12</td>
                          <td class="text-center">1480</td>
                          <td class="text-center">1645</td>
                          <td class="text-center">3125</td>
                          <td class="text-center">705</td>
                          <td class="text-center">7.33</td>
                          <td class="text-center">426.33</td>
                          <td class="text-center">4.43</td>
                          <td class="text-center">89.97</td>
                        </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th class="text-center">जम्मा</th>
                          <th class="text-center">13700</th>
                          <th class="text-center">14792</th>
                          <th class="text-center">28492</th>
                          <th class="text-center">7331</th>
                          <th class="text-center">105.09</th>
                          <th class="text-center">271.12</th>
                          <th class="text-center">3.89</th>
                          <th class="text-center">92.62</th>
                        </tr>
                      </tfoot>

                    </table>{{-- end of table --}}
                  </div> {{-- end of .table-responsive --}}
                </div>
              </div>
            </div>
          </div> {{-- row end --}}

          <div class="row">
            <div class="col-12">
                <h3>वडा तथा लिङ्गअनुसार जनसंख्या</h3>
            </div>
            <div class="col-xl-7 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                      <thead>
                        <tr>
                          <th>वडा</th>
                          <th>पुरुष</th>
                          <th>महिला</th>
                          <th>अन्य लिङ्गी</th>
                          <th>जम्मा</th>
                        </tr>
                      </thead>
                      <tbody>
                    @php 
                        $totalMale=0;
                        $totalFemale=0;
                        $totalOther =0;
                    @endphp
                    @foreach($wardWiseSex as $person) 
                        <tr>
                          <td>{{ $person->Ward}}</td>
                          <td>{{ $person->Male}}</td>
                          <td> {{ $person->Female}}</td>
                          <td>{{ $person->Other}}</td>
                          <td><?php $total = $person->Other + $person->Male + $person->Female; echo $total; ?></td>
                        </tr> @php
                                $totalMale += $person->Male;
                                $totalFemale += $person->Female;
                                $totalOther += $person->Other;
                              @endphp
                    @endforeach

                      </tbody>
                      <tfoot>
                          <th>Total</th>
                          <th>{{$totalMale}}</th>
                          <th>{{$totalFemale}}</th>
                          <th>{{$totalOther}}</th>
                          <th>{{$totalMale + $totalFemale + $totalOther}}</th>
                      </tfoot>
                    </table>{{-- end of table --}}
                  </div> {{-- end of .table-responsive --}}
                </div>
              </div>
            </div>
            <div class="col-xl-5 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="chart-ward-sex ward-population">
                  </div>
                </div>
              </div>
            </div>
          </div> {{-- row end --}}

          <div class="row">
              <div class="col-12">
                  <h3>उमेर समूहअनुसार वडागत जनसंख्या</h3>
              </div>
              <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered">
                        <thead>
                          <tr>
                            <th>उमेर समूह</th>
                            @for($i=1;$i<=12;$i++) <th class="text-center">{{$i}}</th>
                            @endfor <th class="text-center">जम्मा</th>
                          </tr>
                        </thead>
                        <tbody>
                      @php
                          $arrTotal = array();
                      @endphp
                      @foreach(getAgeGroup() as $key=>$value) <tr>
                            <td>{{ $value }}</td>@php 
                            $result = getByAgeGroup($key);
                            $totalAgeGroup=0;
                            $ward = 1;
                            $maxWard = $result[count($result)-1]->ward;
                            $col = 0;
                            @endphp
                        @if($result)
                          @foreach($result as $row)
                            @if($row->ward == ($ward) && $ward <= 12) 
                            <td class="text-center">{{$row->number}}</td> @php $ward++; $col++; @endphp
                            @else
                            @for($j = 0; $j < ($row->ward - $ward); $j++)
                            <td class="text-center">0</td>
                            @php $col++; @endphp
                            @endfor
                            @php $ward += $j+1; @endphp 
                            <td class="text-center">{{$row->number}}</td>
                            @endif
                            @php 
                              $totalAgeGroup += $row->number;
                            @endphp
                          @endforeach
                          @if($maxWard < 12 )
                              @for($k=$maxWard; $k<12; $k++)
                              <td class="text-center">0</td>
                              @endfor
                          @endif
                        <td class="text-center"><strong>{{$totalAgeGroup}}</strong></td>
                        @else
                          <td colspan="13">No records found for the given Age Group</td>
                          <td>&nbsp;</td>
                        @endif
                          </tr>
                      @endforeach
  
                        </tbody>
                        <tfoot>
                            <th>Total</th>
                            <th>Col2</th>
                            <th>col3</th>
                            <th>col4</th>
                            <th>col5</th>
                        </tfoot>
                      </table>{{-- end of table --}}
                    </div> {{-- end of .table-responsive --}}
                  </div>
                </div>
              </div>
          </div> {{-- row end --}}

        </div>
         {{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
@endsection

@section('header-style')
<style>
  .ward-population{
    height: 450px;
  }
</style>
@endsection
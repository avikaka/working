@extends('template.index')
@section('title','Report Home')
@section('main-content')
        <div class="content-wrapper">
          @include('reports.inc.main')
          
          <div class="row">
            <div class="col-12">
                <h3>वडा तथा लिङ्गअनुसार जनसंख्या</h3>
            </div>
            <div class="col-xl-7 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                      <thead>
                        <tr>
                          <th>वडा</th>
                          <th>पुरुष</th>
                          <th>महिला</th>
                          <th>अन्य लिङ्गी</th>
                          <th>जम्मा</th>
                        </tr>
                      </thead>
                      <tbody>
                    @php 
                        $totalMale=0;
                        $totalFemale=0;
                        $totalOther =0;
                    @endphp
                    @foreach($wardWiseSex as $person) 
                        <tr>
                          <td>{{ $person->Ward}}</td>
                          <td>{{ $person->Male}}</td>
                          <td> {{ $person->Female}}</td>
                          <td>{{ $person->Other}}</td>
                          <td><?php $total = $person->Other + $person->Male + $person->Female; echo $total; ?></td>
                        </tr> @php
                                $totalMale += $person->Male;
                                $totalFemale += $person->Female;
                                $totalOther += $person->Other;
                              @endphp
                    @endforeach

                      </tbody>
                      <tfoot>
                          <th>Total</th>
                          <th>{{$totalMale}}</th>
                          <th>{{$totalFemale}}</th>
                          <th>{{$totalOther}}</th>
                          <th>{{$totalMale + $totalFemale + $totalOther}}</th>
                      </tfoot>
                    </table>{{-- end of table --}}
                  </div> {{-- end of .table-responsive --}}
                </div>
              </div>
            </div>
            <div class="col-xl-5 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="chart-ward-sex ward-population">
                  </div>
                </div>
              </div>
            </div>
          </div> {{-- row end --}}

          <div class="row">
              <div class="col-12">
                  <h3>उमेर समूहअनुसार वडागत जनसंख्या</h3>
              </div>
              <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered">
                        <thead>
                          <tr>
                            <th>उमेर समूह</th>
                            @for($i=1;$i<=12;$i++) <th class="text-center">{{$i}}</th>
                            @endfor <th class="text-center">जम्मा</th>
                          </tr>
                        </thead>
                        <tbody>
                      @php
                          $arrTotal = array();
                      @endphp
                      @foreach(getAgeGroup() as $key=>$value) <tr>
                            <td>{{ $value }}</td>@php 
                            $result = getByAgeGroup($key);
                            $totalAgeGroup=0;
                            $ward = 1;
                            $maxWard = $result[count($result)-1]->ward;
                            $col = 0;
                            @endphp
                        @if($result)
                          @foreach($result as $row)
                            @if($row->ward == ($ward) && $ward <= 12) 
                            <td class="text-center">{{$row->number}}</td> @php $ward++; $col++; @endphp
                            @else
                            @for($j = 0; $j < ($row->ward - $ward); $j++)
                            <td class="text-center">0</td>
                            @php $col++; @endphp
                            @endfor
                            @php $ward += $j+1; @endphp 
                            <td class="text-center">{{$row->number}}</td>
                            @endif
                            @php 
                              $totalAgeGroup += $row->number;
                            @endphp
                          @endforeach
                          @if($maxWard < 12 )
                              @for($k=$maxWard; $k<12; $k++)
                              <td class="text-center">0</td>
                              @endfor
                          @endif
                        <td class="text-center"><strong>{{$totalAgeGroup}}</strong></td>
                        @else
                          <td colspan="13">No records found for the given Age Group</td>
                          <td>&nbsp;</td>
                        @endif
                          </tr>
                      @endforeach
  
                        </tbody>
                        <tfoot>
                            <th>Total</th>
                            <th>Col2</th>
                            <th>col3</th>
                            <th>col4</th>
                            <th>col5</th>
                        </tfoot>
                      </table>{{-- end of table --}}
                    </div> {{-- end of .table-responsive --}}
                  </div>
                </div>
              </div>
          </div> {{-- row end --}}


        </div>
         {{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
@endsection

@section('header-style')
<style>
  .ward-population{
    height: 450px;
  }
</style>
@endsection
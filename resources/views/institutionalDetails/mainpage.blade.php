@extends('template.index')
@section('title', 'परिवार को बैँक खाता')
@section('main-content')
    <div class="page-body">
        <div class="row">
            <div class="col-12">
                <h3>संस्थागत विवरण हेर्नुहोस्</h3>
            </div>

            @if (isset($errorMessageDuration))
                <div class="col-12">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ $errorMessageDuration }}
                </div>
            @else
                <div class="col grid-margin stretch-card">


                    <div class="table-responsive">
                        <table class="table table-bordered table-primary table-striped">
                            <thead>
                                <tr class="">
                                    @foreach ($keyss as $column)
                                        <th> {{ $column }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($data as $row)
                                    <tr>
                                        @foreach (array_values($row) as $value)
                                            <td>{{ $value }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>{{-- end of table --}}
                    </div> {{-- end of .table-responsive --}}

                </div>
            @endif
        </div> {{-- row end --}}
    </div>

@endsection

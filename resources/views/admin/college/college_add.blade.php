@extends('template.admin')
@section('title', 'शैक्षिक संस्थाको विवरण सम्बन्धी विवरण')
@section('page-title')
    शैक्षिक संस्थाको विवरण
@endsection
@section('content')

    <div class="tab-pane" id="tab_2">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>शैक्षिक संस्थाको विवरण
                </div>

            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->

                <form class="form-sample" method="POST"
                    action="{{ isset($data->id) ? Route('admin.college.update', $data->id) : Route('admin.college.store') }}">
                    @isset($data->id)
                        {{ method_field('PUT') }}
                    @endisset
                    @csrf


                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                {!! inputField('name', 'शैक्षिक संस्थाको नाम', 'शैक्षिक संस्थाको नाम', isset($data) ? $data->name : old('name')) !!}
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-3">

                                <div class="form-group">
                                    <label class="control-label">वडा नं.</label>
                                    {!! getWardCombo(0) !!}
                                    @error('ward')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                            </div>

                            <div class="col-md-3">
                                {!! selectField('type', 'शैक्षिक संस्थाको प्रकार', [0 => 'आधारभुत', 'प्राथमिक', 'माध्यमिक', 'निजी', 'मदरसा', 'विश्वविद्यालय', 'क्याम्पस'], '', isset($data) ? $data->type : old('type')) !!}
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                {!! inputField('address', 'ठेगाना', 'ठेगाना', isset($data) ? $data->address : old('address')) !!}
                                @if ($errors->has('address'))
                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                {!! inputField('programmes', 'सञ्चालित कार्यक्रम तथा तह', 'सञ्चालित कार्यक्रम तथा तह', isset($data) ? $data->programmes : old('programmes')) !!}
                                @if ($errors->has('programmes'))
                                    <span class="text-danger">{{ $errors->first('programmes') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                {!! latField('girls', 'छात्रा', 'छात्रा', isset($data) ? $data->girls : old('girls')) !!}
                                @if ($errors->has('girls'))
                                    <span class="text-danger">{{ $errors->first('girls') }}</span>
                                @endif
                            </div>


                            <div class="col-md-6">
                                {!! latField('boys', 'छात्र', 'छात्र', isset($data) ? $data->boys : old('boys')) !!}
                                @if ($errors->has('boys'))
                                    <span class="text-danger">{{ $errors->first('boys') }}</span>
                                @endif
                            </div>

                        </div>
                        <!-- <p class="card-description">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    Address
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                </p> -->
                        <div class="row">
                            <div class="col-md-6">
                                {!! inputField('contact_person', 'सम्पर्क व्यक्ति', 'सम्पर्क व्यक्ति', isset($data) ? $data->contact_person : old('contact_person')) !!}
                                @if ($errors->has('contact_person'))
                                    <span class="text-danger">{{ $errors->first('contact_person') }}</span>
                                @endif
                            </div>

                            <div class="col-md-6">
                                {!! latField('contact_no', 'सम्पर्क नं', 'सम्पर्क नं', isset($data) ? $data->contact_no : old('contact_no')) !!}
                                @if ($errors->has('contact_no'))
                                    <span class="text-danger">{{ $errors->first('contact_no') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                {!! latField('latitude', 'अक्षांश', 'अक्षांश', '', 'show', isset($data) ? $data->latitude : old('latitude')) !!}
                                @error('latitude')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                {!! latField('longitude', 'देशान्तर', 'देशान्तर', '', 'show', isset($data) ? $data->longitude : old('longitude')) !!}
                                @error('longitude')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                        </div>
                        <div id="map" class="map" style="height:180px; width:100%; "></div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-9 text-center">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            @if ($data->id == null)
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                                                    alt="" />
                                            @else
                                                <img src={{ url('uploads/college/' . $data->photo) }} alt="" />
                                            @endif
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                            style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> फोटो
                                                    छान्नुहोस </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image" value="{{ $data->photo }}"
                                                    accept="image/*"></span>
                                            <a href="javascript:;" class="btn red fileinput-exists"
                                                data-dismiss="fileinput">
                                                Remove </a>
                                            @error('photo')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!--/span-->

                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="control-label "> कैफियत
                                </label>
                                <div class="col-md-12">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <textarea id="editor1"
                                            name="remarks">{{ old('remarks', $data->remarks) }} </textarea>
                                        @if ($errors->has('remarks'))
                                            <span class="text-danger font-weight-danger">
                                                {{ $errors->first('remarks') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <br>

                    <div>
                        <button type="button"
                            class=" btn                                                                                                                                                                                                                                                                                                                                                                              default"
                            href="{{ url()->previous() }}">Cancel</button>
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i> Save</button>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>

    <script>
        tinymce.init({
            selector: 'textarea'
        })
    </script>

    {{-- <script>
        // $('#geoloc').leafletLocationPicker();
        console.log(intersects());

        $('#geoloc2').leafletLocationPicker({
            locationFormat: '{lat}@{lng}#WGS84',
            position: 'bottomleft'
        });
    </script> --}}


    <script>
        $(document).ready(function() {
            $('#show').click(function() {
                $('.map').toggle("slide");
            });
        });

        var mymap = L.map('map').setView([51.509, -0.08], 15);

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(mymap);

        var popup = L.popup();

        function onMapClick(e) {
            popup
                .setLatLng(e.latlng)
                .setContent("You clicked the map at " + e.latlng.toString())
                .openOn(mymap);
        }
        // console.log(marker);

        // var marker = L.marker([27.954912, 83.5413758]).addTo(mymap);
        var polygon = L.polygon([
            [51.509, -0.08],
            [51.503, -0.06],
            [51.51, -0.047]
        ]).addTo(mymap);


        mymap.on('click', onMapClick);
    </script>



@endsection

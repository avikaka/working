@foreach ($data as $toilet)
    <tr class="odd gradeX">
        <td>
            {{ $toilet->HOUSEHOLD_ID }}</td>
        <td>{{ $toilet->NAME }}</td>
        <td>{{ $toilet->AGE }}</td>
        <td>{{ $toilet->SEX }}</td>
        <td>
            @if ($toilet->death) <a><span
                        class="label label-sm label-danger">मृत्यु</span>
            </a> @else
                <a> <span class="label label-sm label-success"> जीवित </span> </a>
            @endif
        </td>


        <td>

            <a href="{{ url('admin/insertdeath/' . $toilet->NAME . '/' . $toilet->HOUSEHOLD_ID) }}"
                class="btn btn-circle blue"> Edit
                Person
                <i class="fa icon-users"></i>
            </a>
        </td>

    </tr>
@endforeach
<tr class="d-flex justify-content-centerr">
    <td class="cover page-link " colspan='6'>
        {{ $data->links() }}

    </td>
</tr>

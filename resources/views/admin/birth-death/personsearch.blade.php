@extends('template.admin')
@section('title', 'व्यक्ति खोज')
@section('page-title')
    व्यक्ति खोज
@endsection
@section('content')

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('cd-admin/home') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span>व्यक्ति खोज </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body"
                data-placement="bottom" data-original-title="Change dashboard date range">
                <i class="icon-calendar"></i>&nbsp;
                <span class="thin uppercase hidden-xs"></span>&nbsp;
                <i class="fa fa-angle-down"></i>
            </div>
        </div>
    </div>

    <div class="row">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">व्यक्ति खोज</span>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group has-success">
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="glyphicon glyphicon-credit-card"></i>
                            </span>
                            <input type="text" id="searchName" name="search" class="form-control" placeholder="नाम">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group has-success">
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="glyphicon glyphicon-home"></i>
                            </span>
                            <input type="text" id="searchHouse" class="form-control" placeholder="घर नं.">
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" id="show" class="btn btn-secondary">Advanced Search</button>
                </div>
            </div>

            <div class="filterSearch row" id="filterSearch" style="display: none">
                <form>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! getWardCombo(0) !!}
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            {!! getCaste() !!}
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            {!! getReligion() !!}
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            {!! getEducation() !!}
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <button type="button" id="filter" class="btn btn-primary">Apply</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="dataT portlet-body">
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead>
                        <tr>
                            <th>घर नं.</th>
                            <th>नाम</th>
                            <th>उमेर</th>
                            <th>वार्ड</th>
                            <th>Status</th>
                            <th>Edit</th>

                        </tr>
                    </thead>
                    <tbody id="dynamicTable">
                        @include('admin.birth-death.person_pag')
                    </tbody>
                </table>


            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#filter').click(function(e) {
                e.preventDefault();

                var ward = $("#ward").val();
                var caste = $("#caste").val();
                var religion = $("#religion").val();
                var education = $("#education").val();

                // alert(ward);

                $.ajax({
                    method: 'POST',
                    url: "{{ route('admin.filter-search') }}",
                    dataType: 'json',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        ward: ward,
                        caste: caste,
                        religion: religion,
                        education: education
                    },
                    success: function(res) {
                        var newTable = '';

                        $('#dynamicTable').html('');

                        // console.log(pag);

                        $.each(res.data, function(index, value) {
                            // console.log(value);

                            newTable =
                                '<tr><td class = "font-weight-bold">' +
                                value
                                .HOUSEHOLD_ID +
                                '</td>  <td>' + value.NAME +
                                ' </td> <td>' + value.AGE +
                                ' </td> <td>' + value.WARD +
                                '  </td > <td><a> <span class="label label-sm label-success"> जीवित </span> </a></td> <td><a href="/admin/insertdeath/' +
                                value
                                .NAME + '/' + value
                                .HOUSEHOLD_ID +
                                '" class="btn btn-circle blue">Edit Person<i class="fa icon-users"></i></a></td></tr> ';

                            var pag =
                                `'<tr class="d-flex justify-content-center"><td class="cover page-link" colspan="6">' +
                                {{ $data->links() }} + '</td></tr>'`;


                            $('#dynamicTable').append(newTable);
                        });

                    }
                });
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $('#show').click(function() {
                $('.filterSearch').toggle("slide");
            });
        });
    </script>

    <script>
        var BASE_URL = "{{ URL::to('/admin/person_ajax/?page=') }}";
        $(document).ready(function() {
            function fetch_data(page) {
                $.ajax({
                    url: BASE_URL + page,
                    success: function(data) {
                        $('.dataT tbody').html(data);
                    }
                })
            }
            $(document).on('click', '.cover a', function(event) {
                event.preventDefault();
                // console.log('test');
                var page = $(this).attr('href').split('page=')[1];
                // alert(BASE_URL);
                fetch_data(page);
            });
        });
    </script>

    <script>
        $('body').on('keypress', function(e) {
            if (e.keyCode == 13) {

                var nameReq = $("#searchName").val();
                var houseReq = $("#searchHouse").val();

                $.ajax({
                    method: 'POST',
                    url: "{{ route('admin.search-house') }}",
                    url: "{{ route('admin.search-person') }}",

                    dataType: 'json',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        nameReq: nameReq,
                        houseReq: houseReq,

                    },
                    success: function(res) {
                        var newTable = '';

                        $('#dynamicTable').html('');

                        $.each(res, function(index, value) {

                            if (value.death) {
                                newTable =
                                    '<tr><td class = "font-weight-bold">' +
                                    value
                                    .HOUSEHOLD_ID +
                                    '</td>  <td>' + value.NAME +
                                    ' </td> <td>' + value.AGE +
                                    ' </td> <td>' + value.WARD +
                                    '  </td > <td><a><span class="label label-sm label-danger">मृत्यु</span></a></td> <td><a href="/admin/insertdeath/' +
                                    value
                                    .NAME + '/' + value
                                    .HOUSEHOLD_ID +
                                    '" class="btn btn-circle blue">Edit Person<i class="fa icon-users"></i></a></td></tr> ';

                                $('#dynamicTable').append(newTable);

                            } else {
                                newTable =
                                    '<tr><td class = "font-weight-bold">' +
                                    value
                                    .HOUSEHOLD_ID +
                                    '</td>  <td>' + value.NAME +
                                    ' </td> <td>' + value.AGE +
                                    ' </td> <td>' + value.WARD +
                                    '  </td > <td><a> <span class="label label-sm label-success"> जीवित </span> </a></td> <td><a href="/admin/insertdeath/' +
                                    value
                                    .NAME + '/' + value
                                    .HOUSEHOLD_ID +
                                    '" class="btn btn-circle blue">Edit Person<i class="fa icon-users"></i></a></td></tr> ';

                                $('#dynamicTable').append(newTable);
                            };

                        });
                    }
                });
            }
        });
    </script>


    {{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}

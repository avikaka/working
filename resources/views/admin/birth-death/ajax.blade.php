@extends('template.admin')
@section('title', 'घरेलु खोज')
@section('page-title')
    परिवारको विवरण
@endsection
@section('content')

    <div>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ url('cd-admin/home') }}">Dashboard</a>
                    <i class="fa fa-circle"></i>
                </li>

                <li>
                    <span>परिवारीक विवरण</span>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body"
                    data-placement="bottom" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>
        </div>

        <div>
            <div class="row">

                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>


                            <span class="caption-subject bold uppercase"> घरेलु खोज</span>
                        </div>

                    </div>


                    <div class="col-md-3">
                        <div class="form-group has-success">

                            <div class="input-group">
                                <span class="input-group-addon input-circle-left">
                                    <i class="glyphicon glyphicon-user"></i>
                                </span>
                                <input type="text" id="searchTole" class="form-control"
                                    placeholder="टोल को नाम लेख्नुहोस्......" name="name">
                            </div>

                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group has-success">

                            <div class="input-group">
                                <span class="input-group-addon input-circle-left">
                                    <i class="glyphicon glyphicon-home"></i>
                                </span>
                                <input type="text" id="searchPosts" name="search" class="form-control"
                                    placeholder="घर धनि को नाम लेख्नुहोस्......">
                            </div>

                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="dataT table table-bordered table-striped table-condensed flip-content">
                            <thead>
                                <tr>
                                    <th>घर नं.</th>
                                    <th>टोल</th>
                                    <th>घर धनि</th>
                                    <th>सम्पर्क नं.</th>
                                    <th>जम्म परिवार संख्या</th>
                                    <th>Add Member</th>
                                    <th>View all memebers
                                        {{-- <th>Edit</th>
                                <th>Delete</th> --}}

                                </tr>
                            </thead>
                            <tbody id="dynamicTable">
                                @include('ajax_pag')
                            </tbody>
                        </table>


                    </div>

                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>

            <script>
                var BASE_URL = "{{ URL::to('/admin/cover_ajax/?page=') }}";
                $(document).ready(function() {
                    function fetch_data(page) {
                        $.ajax({
                            url: BASE_URL + page,
                            success: function(data) {
                                $('.dataT tbody').html(data);
                            }
                        })
                    }
                    $(document).on('click', '.cover a', function(event) {
                        event.preventDefault();
                        // console.log('test');
                        var page = $(this).attr('href').split('page=')[1];
                        // alert(page);
                        fetch_data(page);
                    });
                });
            </script>

            <script>
                $('body').on('keypress', function(e) {
                    if (e.keyCode == 13) {

                        var searchReq = $("#searchPosts").val();
                        var toleReq = $("#searchTole").val();
                        var roadReq = $("#searchRoad").val();

                        document


                        $.ajax({
                            method: 'POST',
                            url: "{{ route('admin.search-posts') }}",
                            dataType: 'json',
                            data: {
                                '_token': '{{ csrf_token() }}',
                                searchReq: searchReq,
                                toleReq: toleReq,
                                roadReq: roadReq,


                            },
                            success: function(res) {
                                var newTable = '';

                                $('#dynamicTable').html('');

                                $.each(res, function(index, value) {
                                    console.log(value);
                                    newTable = '<tr><td class = "font-weight-bold">' + value
                                        .HOUSEHOLD_ID +
                                        '</td> <td>' + value.TOLE + '</td> <td>' + value.RESPONDENT +
                                        '  <td > ' + value
                                        .RES_PHONE +
                                        ' <td > ' + value.MEMBERS +
                                        ' <td><a href="/admin/insertbirthcertification/' + value
                                    newTable = '<tr class="odd gradeX"><td>' + value
                                        .HOUSEHOLD_ID +
                                        '</td> <td>' + value.TOLE + '</td>  <td>' + value
                                        .HOUSEHOLD_HEAD +
                                        ' </td> <td > ' + value
                                        .HEAD_CONTACT +
                                        ' </td > <td> ' + value.MEMBERS +
                                        '</td>  <td><a href="/admin/insertbirthcertification/' +
                                        value.HOUSEHOLD_ID +
                                        '"  class="btn btn-circle green"> Add member <i class="fa icon-user-follow" ></i></td>    <td> <a href="/admin/personsdetail/' +
                                        value
                                        .HOUSEHOLD_ID +
                                        '" class="btn btn-circle blue"> View members<i class="fa icon-users"></i></a> </td></tr> ';

                                    $('#dynamicTable').append(newTable);
                                });
                            }
                        });
                    }
                });
            </script>

            {{-- <script src="{{ asset('js/jquery-3.5.1.min.js') }}">
            </script> --}}

            {{-- <script>
                $(document).ready(function() {

                    function fetch_data(page) {
                        $.ajax({
                            url: BASE_URL + "/admin/cover_ajax/?page=" + page,
                            success: function(data) {
                                $('.dataT tbody').html(data);
                            }
                        })
                    }
                    $(document).on('click', '.cover a', function(event) {
                        event.preventDefault();
                        // console.log('test');
                        var page = $(this).attr('href').split('page=')[1];
                        // alert(page);
                        console.log(page);
                        fetch_data(page);
                    });
                });
            </script> --}}
        </div>
    </div> {{-- content-wrapper ends --}}
@endsection

@extends('template.index')
@section('title', ' विवरण')
@section('main-content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-xl-12 col-sm-12 grid-margin stretch-card">
                <div class="card">

                    <div class="card-body">

                        @if (Session::has('msg'))
                            <div class="col-md-6">
                                <div class="alert alert-success">{{ Session::get('msg') }}</div>
                            </div>
                        @endif
                        <main role="main" class="container">
                            <div class="starter-template">
                                <h4>घरको विवरण</h4>
                                <br>
                                <table class="table table-striped table-sm">
                                    <tbody>
                                        <tr>
                                            <td class="font-weight-bold">वडा नं.</td>
                                            <td>{{ $household->WARD }}</td>
                                            <td class="font-weight-bold"> घर नं.</td>
                                            <td>{{ $household->HOUSEHOLD_ID }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold"> गाउँ/टोल/वस्तीको नाम</td>
                                            <td>{{ $household->TOLE }}</td>
                                            <td class="font-weight-bold"> टोल विकास संस्था/समितिको नाम</td>
                                            @if ($household->TLO_NAME == 0 || null)
                                                <td>-</td>
                                            @else
                                                <td>{{ $household->TLO_NAME }}</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold"> परिवारमूलीको नाम</td>
                                            <td>{{ $household->RESPONDENT }}</td>
                                            <td class="font-weight-bold"> फोन/मोबाइल नं.</td>
                                            <td>{{ $household->RES_PHONE }}</td>
                                        </tr>


                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <div>

                                <table class="table table-striped table-striped table-sm">
                                    <thead class="flip-content">
                                        <tr>
                                            <th>सदस्यको नाम</th>
                                            <th>उमेर</th>
                                            <th>लिङ्ग</th>
                                            <th>परिवारमूलीको नाता</th>
                                            <th>मातृभाषा</th>
                                            <th>विवरण</th>
                                        </tr>
                                    </thead>
                                    <tbody id="dynamicTable">
                                        @foreach ($data as $toilet)
                                            {{-- {{ dd($toilet) }} --}}
                                            @if ($toilet->death)
                                                <tr bgcolor='#c4201d'>
                                                    <td>{{ $toilet->NAME }}</td>
                                                    <td>{{ $toilet->AGE }}</td>
                                                    @if ($toilet->SEX == 1)
                                                        <td>पुरुष</td>
                                                    @elseif ($toilet->SEX == 2)
                                                        <td>महिला</td>
                                                    @endif

                                                    <td><a href="{{ url('admin/personsdetail/' . $toilet->HOUSEHOLD_ID . '/' . $toilet->NAME) }}"
                                                            class="btn btn-primary" data-toggle="modal"
                                                            data-target="#exampleModalCenter"> विवरण हेर्नुहोस्
                                                        </a></td>

                                                </tr>
                                            @else
                                                <tr>
                                                    <td>{{ $toilet->NAME }}</td>
                                                    <td>{{ $toilet->AGE }}</td>
                                                    @if ($toilet->SEX == 1)
                                                        <td>पुरुष</td>
                                                    @elseif ($toilet->SEX == 2)
                                                        <td>महिला</td>
                                                    @endif
                                                    @if ($toilet->RELATIONSHIP == 1)
                                                        <td>परिवारमूली</td>
                                                    @elseif ($toilet->RELATIONSHIP == 2)
                                                        <td>श्रीमान/श्रीमती</td>
                                                    @elseif ($toilet->RELATIONSHIP == 3)
                                                        <td>छोरा/बुहारी</td>
                                                    @elseif ($toilet->RELATIONSHIP == 4)
                                                        <td>छोरी/ज्वाइँ</td>
                                                    @elseif ($toilet->RELATIONSHIP == 5)
                                                        <td>बाबु/आमा</td>
                                                    @elseif ($toilet->RELATIONSHIP == 6)
                                                        <td>सासू/ससुरा</td>
                                                    @elseif ($toilet->RELATIONSHIP == 7)
                                                        <td>हजुरबुवा/हजुरआमा</td>
                                                    @elseif ($toilet->RELATIONSHIP == 8)
                                                        <td>नाति/नातिनी</td>
                                                    @else
                                                        <td>अन्य</td>
                                                    @endif

                                                    {{-- {{ $toilet->id }} --}}

                                                    <td>{{ $keys[$toilet->MOTHER_TONGUE - 1] }}</td>

                                                    <td><a id="{{ $toilet->id }}" class="btn btn-primary detailBtn"
                                                            data-toggle="modal" data-target="#exampleModalCenter"> विवरण
                                                            हेर्नुहोस्
                                                        </a></td>
                                                </tr>
                                            @endif
                                        @endforeach

                                    </tbody>
                                </table>{{-- end of table --}}

                                <!-- Modal -->
                                <div id="modalShow">
                                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">
                                                        व्यक्तिको विवरण हेर्नुहोस्</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-striped table-sm">
                                                        <tbody>
                                                            <tr>
                                                                <td class="font-weight-bold">वडा नं.</td>
                                                                <td>45</td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>'
                                </div>
                            </div> {{-- end of .table-responsive --}}
                    </div>
                </div>
            </div>

        </div> {{-- row end --}}

    </div>

    <script>
        $(document).ready(function() {
            $('.detailBtn').click(function(e) {
                e.preventDefault();
                var id = e.target.id;

                console.log(id);

                $.ajax({
                    method: 'POST',
                    url: '/admin/getDetail/' + id,
                    dataType: 'HTML',
                    data: {
                        '_token': '{{ csrf_token() }}',

                    },
                    success: function(res) {
                        console.log(res);
                        var newTable = '';

                        $('#modalShow').html(res);

                        console.log(res);

                        $.each(res, function(index, value) {
                            console.log(value.id);

                            newTable =
                                `'<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalCenterTitle" aria-hidden="true"> <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">
                                                    व्यक्तिको विवरण हेर्नुहोस्</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table table-striped table-sm">
                                                    <tbody>
                                                        <tr>
                                                            <td class="font-weight-bold">वडा नं.</td>
                                                            <td>${value.AGE}</td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>'`;

                            $('#modalShow').append(newTable);

                        });

                    }
                });
            });
        });
    </script>

    {{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')



@endsection

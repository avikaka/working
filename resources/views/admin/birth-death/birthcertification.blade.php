@extends('template.index')
@section('title', 'व्यक्ति थप्नुहोस्')
@section('main-content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <h3>व्यक्ति थप्नुहोस्</h3>
            </div>
            <div class="col grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ '/admin/storebirthcertification' }}" class="form-sample"
                            enctype="multipart/form-data">

                            @csrf

                            <div class="form-body">

                                {{-- {{dd($data)}} --}}
                                <div class="col-md-9">
                                    <div class="form-group row">
                                        <label class="control-label">घरेलु आईडी</label>
                                        <input type="text" class="form-control input-circle" name="household_id"
                                            value="{{ $data }} " readonly>

                                    </div>
                                </div>

                                <div class="row">

                                    <div class=" col-md-6">

                                        <label class="control-label">
                                            नाम :</label>
                                        <input type="text" class="form-control input-circle" placeholder="पुरा नाम"
                                            name="name" required>


                                    </div>

                                    <div class="col-md-3">

                                        <label class="control-label">

                                            जन्म मिति :</label>

                                        <input type="text" id="nepali-datepicker" class="form-control nepali-datepicker"
                                            placeholder="Please Choose Date...." जन्म मिति />

                                        <input type="hidden" id="english-date" name="date" />


                                    </div>

                                </div>

                                <br>

                                <div class="row">
                                    <div class=" col-md-12">
                                        <label class="control-label">
                                            लिङ्ग :</label>
                                        <div class="row">
                                            <div class="col-2">
                                                <input type="radio" class="form-check" value="1" name="sex">पुरुस
                                            </div>

                                            <div class="col-2">
                                                <input type="radio" class="form-check" value="2" name="sex">महिला
                                            </div>

                                            <div class="col-2">
                                                <input type="radio" class="form-check" value="3" name="sex">अन्य
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div class="ml-4">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{ url()->previous() }}" class="btn btn-secondary">Cancel</a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div> {{-- row end --}}

    </div>
    </div> {{-- row end --}}

    </div>
    {{-- content-wrapper ends --}}

    <script src="http://nepalidatepicker.sajanmaharjan.com.np/nepali.datepicker/js/nepali.datepicker.v3.6.min.js"
        type="text/javascript"></script>
    <link href="http://nepalidatepicker.sajanmaharjan.com.np/nepali.datepicker/css/nepali.datepicker.v3.6.min.css"
        rel="stylesheet" type="text/css" />





    <script type="text/javascript">
        window.onload = function() {
            var mainInput = document.getElementById("nepali-datepicker");
            mainInput.nepaliDatePicker({
                language: "english"
            });
            $('#nepali-datepicker').nepaliDatePicker({
                ndpEnglishInput: 'english-date'
            });
        };
    </script>




@endsection
{{-- end main-content section --}}

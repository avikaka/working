@extends('template.index')
@section('title', 'परिवारको विवरण')
@section('main-content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <h3>
                    परिवारको विवरण</h3>
            </div>
            <div class="col-xl-12 grid-margin stretch-card">
                <div class="card">

                    <div class="card-body">

                        @if (Session::has('msg'))
                            <div class="col-md-6">
                                <div class="alert alert-success">{{ Session::get('msg') }}</div>
                            </div>
                        @endif
                        <div class="row">

                            <div class="col-3">

                                <input type="text" id="searchTole" class="form-control"
                                    placeholder="टोल को नाम लेख्नुहोस्......">

                            </div>

                            <div class="col-3">

                                <input type="text" id="searchPosts" name="search" class="form-control"
                                    placeholder="घर धनि को नाम लेख्नुहोस्......">

                            </div>

                            <div class="col-3">

                                <input type="text" id="searchRoad" class="form-control"
                                    placeholder="सडक को नाम लेख्नुहोस्......">

                            </div>



                            <table class="dataT table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>घर नं.</th>
                                        <th width="20%">टोल</th>
                                        <th>रोड</th>
                                        <th>घर धनि</th>
                                        <th>सम्पर्क नं.</th>
                                        <th>जम्म परिवार संख्या</th>
                                        <th>Add Member</th>
                                        <th>View all memebers
                                            {{-- <th>Edit</th>
                                        <th>Delete</th> --}}

                                    </tr>
                                </thead>
                                <tbody id="dynamicTable">
                                    @include('ajax_pag')
                                </tbody>
                            </table>
                        </div> {{-- end of .table-responsive --}}
                    </div>
                </div>
            </div>
        </div> {{-- row end --}}
    </div>


    <script>
        $('body').on('keyup', function() {
            var searchReq = $("#searchPosts").val();
            var toleReq = $("#searchTole").val();
            var roadReq = $("#searchRoad").val();

            document


            $.ajax({
                method: 'POST',
                url: "{{ route('admin.search-posts') }}",
                dataType: 'json',
                data: {
                    '_token': '{{ csrf_token() }}',
                    searchReq: searchReq,
                    toleReq: toleReq,
                    roadReq: roadReq,


                },
                success: function(res) {
                    var newTable = '';

                    $('#dynamicTable').html('');

                    $.each(res, function(index, value) {
                        console.log(value);
                        newTable = '<tr><td class = "font-weight-bold">' + value.household_id +
                            '</td> <td>' + value.TOLE + '</td> <td>' + value.ROAD +
                            ' </td> <td>' + value.RESPONDENT + '  <td > ' + value.RES_PHONE +
                            ' <td > ' + value.MEMBERS +
                            ' <td><a href="/admin/insertbirthcertification/' + value
                            .household_id +
                            '" class = "btn btn-outline-primary btn-icon user"><i class = "mdi mdi-pencil mdi-18px" ></i></td>'
                        ' <td><a href="/admin/personsdetail/' + value
                            .household_id +
                            '" class = "btn btn-outline-primary btn-icon user"><i class = "mdi mdi-pencil mdi-18px" ></i></td></tr> ';

                        $('#dynamicTable').append(newTable);
                    });
                }
            });
        });
    </script>

    <script>
        $(document).ready(function() {

            function fetch_data(page) {
                $.ajax({
                    url: BASE_URL + "/admin/cover_ajax/?page=" + page,
                    success: function(data) {
                        $('.dataT tbody').html(data);
                    }
                })
            }
            $(document).on('click', '.cover a', function(event) {
                event.preventDefault();
                // console.log('test');
                var page = $(this).attr('href').split('page=')[1];
                // alert(page);
                fetch_data(page);
            });
        });
    </script>
    {{-- content-wrapper ends --}}





    {{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')




@endsection

@section('header-style')
    <style>
        .ward-population {
            height: 450px;
        }

    </style>

    <style>
        .ward-population {
            height: 450px;
        }

    </style>

@endsection

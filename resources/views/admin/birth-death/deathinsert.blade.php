@extends('template.index')
@section('title', 'स्वास्थ्य स्थिति')
@section('main-content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <h3>स्वास्थ्य स्थिति</h3>
            </div>
            <div class="col grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ '/admin/storedeath/' . $data->NAME . '/' . $data->HOUSEHOLD_ID }}"
                            class="form-sample" enctype="multipart/form-data">

                            @csrf


                            <div class="form-body">

                                <div class="col-md-9">
                                    <div class="form-group row">
                                        <label class="control-label">व्यक्ति आईडी</label>
                                        <input type="text" class="form-control input-circle" name="household_id"
                                            value="{{ $data->NAME }} " readonly>

                                    </div>
                                </div>

                                <div class="row">

                                    <div class=" col-md-6">
                                        {{-- <div class="form-group row"> --}}
                                        <label class="control-label">
                                            नाम :</label>
                                        <input type="text" class="form-control input-circle" placeholder="पुरा नाम"
                                            value="{{ $data->NAME }} " readonly>

                                        {{-- </div> --}}
                                    </div>

                                    <div class="col-md-3">
                                        {{-- <div class="form-group row"> --}}
                                        <label class="control-label">

                                            मृत्यु मिति :</label>

                                        <input type="text" id="nepali-datepicker" class="form-control nepali-datepicker"
                                            placeholder="Please Choose Date...." जन्म मिति />

                                        <input type="hidden" id="english-date" name="date" />

                                        {{-- </div> --}}
                                    </div>

                                </div>

                                <br>

                                <div class="row">
                                    <div class=" col-md-12">
                                        <label class="control-label">
                                            स्थिति :</label>
                                        <div class="row">
                                            <div class="col-2">
                                                <input type="radio" class="form-check" value="1" name="death">मृत्यु
                                            </div>

                                            <div class="col-2">
                                                <input type="radio" class="form-check" value="0" name="death">जीवित
                                            </div>


                                        </div>
                                    </div>
                                </div>










                            </div>
                            <br>


                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{ url()->previous() }}" class="btn btn-secondary">Cancel</a>

                        </form>
                    </div>
                </div>
            </div>
        </div> {{-- row end --}}

    </div>
    </div> {{-- row end --}}

    </div>
    {{-- content-wrapper ends --}}

    <script src="http://nepalidatepicker.sajanmaharjan.com.np/nepali.datepicker/js/nepali.datepicker.v3.6.min.js"
        type="text/javascript"></script>
    <link href="http://nepalidatepicker.sajanmaharjan.com.np/nepali.datepicker/css/nepali.datepicker.v3.6.min.css"
        rel="stylesheet" type="text/css" />





    <script type="text/javascript">
        window.onload = function() {
            var mainInput = document.getElementById("nepali-datepicker");
            mainInput.nepaliDatePicker({
                language: "english"
            });
            $('#nepali-datepicker').nepaliDatePicker({
                ndpEnglishInput: 'english-date'
            });
        };
    </script>




@endsection
{{-- end main-content section --}}

@extends('template.admin')
@section('title', 'कृषक तथा पशुपालन समूहको विवरण')
@section('page-title')
कृषक तथा पशुपालन समूह
@endsection
@section('content')

<div class="tab-pane" id="tab_2">
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>कृषक तथा पशुपालन समूह
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form method="POST"
                action="{{ isset($data->id) ?  Route('admin.agro-vet.update', ['agro_vet' => $data->id]) : Route('admin.agro-vet.store') }}"
                enctype="multipart/form-data" class="horizontal-form">
                @isset($data->id)
                {{ method_field('PUT') }}
                @endisset
                @csrf
                <div class="form-body">
                    {{-- <h3 class="form-section">सरकारी तथा सामुदायिक भवन</h3> --}}

                    <div class="row">
                        <div class="col-md-6">
                            {!! inputField('name', 'कृषक तथा पशुपालन समूहको नाम', 'कृषक/पशुपालन समूहको नाम',
                            isset($data) ? $data->name : old('name'))
                            !!}
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            {!! inputField('address', 'ठेगाना', 'ठेगाना', isset($data) ? $data->address :
                            old('address')) !!}
                            @if ($errors->has('address'))
                            <span class="help-block">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">वडा नं.</label>
                                {!! getWardCombo(0) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            {!! selectField(
                            'type',
                            'प्रकार',
                            [
                            0 => 'कृषक',
                            'पशुपालन',
                            'दुवै'
                            ],
                            '',
                            isset($data) ? $data->type : old('type'),
                            ) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            {!! inputField('contact_person', 'सम्पर्क वयक्ति', 'सम्पर्क वयक्ति', isset($data) ?
                            $data->contact_person : old('contact_person')) !!}
                        </div>
                        <div class="col-md-6">
                            {!! inputField('contact_no', 'सम्पर्क नं', 'सम्पर्क नं', isset($data) ?
                            $data->contact_no : old('contact_no')) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            {!! latField('latitude', 'अक्षांश', 'अक्षांश', isset($data) ? $data->latitude :
                            old('latitude')) !!}
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            {!! latField('longitude', 'देशान्तर', 'देशान्तर', isset($data) ? $data->longitude :
                            old('longitude')) !!}
                        </div>
                        <!--/span-->
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-9 text-center">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        @if ($data->id == null)
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                                            alt="" />
                                        @else
                                        <img src={{ url('uploads/communitybuilding/' . $data->photo) }} alt="" />
                                        @endif
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                        style="max-width: 200px; max-height: 150px;"> </div>
                                    <div>
                                        <span class="btn default btn-file">
                                            <span class="fileinput-new"> फोटो
                                                छान्नुहोस </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" name="image" value="{{ $data->photo }}"
                                                accept="image/*"></span>
                                        <a href="javascript:;" class="btn red fileinput-exists"
                                            data-dismiss="fileinput">
                                            Remove </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--/span-->

                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('remarks') ? ' has-error' : '' }} ">
                            <label class="control-label "> कैफियत
                            </label>
                            <div class="col-md-12">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <textarea id="editor1"
                                        name="remarks">{{ old('remarks', $data->remarks) }} </textarea>
                                    @if ($errors->has('remarks'))
                                    <span class="text-danger font-weight-danger">
                                        {{ $errors->first('remarks') }}
                                    </span>

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br>

                <div>
                    <button type="button" class=" btn default" href="{{ url()->previous() }}">Cancel</button>
                    <button type="submit" class="btn blue">
                        <i class="fa fa-check"></i> Save</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js"
    referrerpolicy="origin"></script>
<script>
    tinymce.init({
        selector: 'textarea'
    })

</script>


@endsection

@extends('template.admin')
@section('title', 'सरकारी तथा सामुदायिक भवन सम्बन्धी विवरण')
@section('page-title')
    सरकारी तथा सामुदायिक भवन
@endsection
@section('content')

    <div class="tab-pane" id="tab_2">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>सरकारी तथा सामुदायिक भवन
                </div>

            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->

                <form class="form-sample" method="POST"
                    action="{{ isset($data->id) ? Route('admin.health-institution.update', $data->id) : Route('admin.health-institution.store') }}">
                    @isset($data->id)
                        {{ method_field('PUT') }}
                    @endisset
                    @csrf
                    <div class="form-body">
                        <div class="row">

                            <div class="col-md-6">
                                {!! inputField('name', 'स्वास्थ्य संथाको नाम', 'स्वास्थ्य संथाको नाम', isset($data) ? $data->name : old('name')) !!}
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">वडा नं.</label>
                                    {!! getWardCombo(0) !!}
                                    @error('ward')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                {!! inputField('address', 'ठेगाना', 'ठेगाना', isset($data) ? $data->address : old('address')) !!}
                                @if ($errors->has('address'))
                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                @endif
                            </div>


                            <div class="col-md-6">
                                {!! selectField('services', 'स्वास्थ्य संस्थामा उपलब्ध सेवाहरु', [0 => 'परिवार नियोजन', 'सुरक्षित मातृत्व`', 'प्रजनन् सवास्थ्य', 'खोप सेवा', 'आकश्मिक सेवा', 'कुष्ठरोग', 'हाडजोर्नी', 'रक्तसन्चार', 'बाल सवास्थ्य', 'छयरोग', 'एड्स', 'पर्योगशाला', 'सिटीस्क्यान', 'शल्यकृिया', 'अल्ट्रासाउण्ड'], '', isset($data) ? $data->services : old('services')) !!}

                                @error('services')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">



                            <div class="col-md-6">
                                {!! selectField(
    'type',
    'प्रकार',
    [
        0 => '
                                स्वास्थ्य चाैकी',
        'उप-स्वास्थ्य चाैकी',
        '
प्राथमिक स्वास्थ्य केन्द्र',
        'सामान्य अस्पताल',
        '
निजी अस्पताल',
        'जिल्ला अस्पताल',
        'अञ्चल अस्पताल',
        'केन्द्रीय अस्पताल',
    ],
    '',
    isset($data) ? $data->type : old('type'),
) !!}

                                @error('type')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                {!! inputField('contact_person', 'सम्पर्क वयक्ति.', 'सम्पर्क वयक्ति.', isset($data) ? $data->contact_person : old('contact_person')) !!}
                                @error('contact_person')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                {!! latField('contact_no', 'सम्पर्क नं.', 'सम्पर्क नं.', isset($data) ? $data->contact_no : old('contact_no')) !!}
                                @error('contact_no')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                {!! latField('latitude', 'अक्षांश', 'अक्षांश', isset($data) ? $data->latitude : old('latitude')) !!}
                                @error('latitude')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                {!! latField('longitude', 'देशान्तर', 'देशान्तर', isset($data) ? $data->longitude : old('longitude')) !!}
                                @error('longitude')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-9 text-center">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            @if ($data->id == null)
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                                                    alt="" />
                                            @else
                                                <img src={{ url('uploads/cooperative/' . $data->photo) }} alt="" />
                                            @endif
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                            style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> फोटो
                                                    छान्नुहोस </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image" value="{{ $data->photo }}"
                                                    accept="image/*"></span>
                                            <a href="javascript:;" class="btn red fileinput-exists"
                                                data-dismiss="fileinput">
                                                Remove </a>
                                            @error('photo')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!--/span-->

                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="control-label "> कैफियत
                                </label>
                                <div class="col-md-12">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <textarea id="editor1"
                                            name="remarks">{{ old('remarks', $data->remarks) }} </textarea>
                                        @if ($errors->has('remarks'))
                                            <span class="text-danger font-weight-danger">
                                                {{ $errors->first('remarks') }}
                                            </span>

                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div>
                        <button type="button"
                            class=" btn
                                                                                                                                                                                                                                                                                                                                                                                                        default"
                            href="{{ url()->previous() }}">Cancel</button>
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i> Save</button>
                    </div>
                </form>
                <!-- END FORM-->

            </div>
        </div>
    </div>

    <script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        })
    </script>


@endsection

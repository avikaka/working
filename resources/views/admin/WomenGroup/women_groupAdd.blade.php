@extends('template.admin')
@section('title', 'आमा समूहको विवरण')
@section('page-title')
    आमा समूहको विवरण थप
@endsection
@section('content')

    <div class="tab-pane" id="tab_2">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>आमा समूहको विवरण थप गर्नुहोस
                </div>

            </div>

            <div class="portlet-body form">
                <form class="form-sample" method="POST"
                    action="{{ isset($data->id) ? Route('admin.women-group.update', $data->id) : Route('admin.women-group.store') }}">
                    @isset($data->id)
                        {{ method_field('PUT') }}
                    @endisset
                    @csrf

                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                {!! inputField('name', 'आमा / महिला समूहको नाम', 'आमा / महिला समूहको नाम', isset($data) ? $data->name : old('name')) !!}
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                {!! inputField('address', 'ठेगाना', 'ठेगाना', isset($data) ? $data->address : old('address')) !!}
                                @if ($errors->has('address'))
                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">वडा नं.</label>
                                    {!! getWardCombo(0) !!}
                                    @error('ward')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                {!! inputField('savings', 'बचत रकम', 'बचत रकम', isset($data) ? $data->savings : old('savings')) !!}
                                @if ($errors->has('savings'))
                                    <span class="text-danger">{{ $errors->first('savings') }}</span>
                                @endif
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                {!! inputField('members', 'सदस्य संख्या', 'सदस्य संख्या', isset($data) ? $data->members : old('members')) !!}
                                @error('members')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                {!! inputField('loans', 'लगानी रकम', 'लगानी रकम', isset($data) ? $data->loans : old('loans')) !!}
                                @if ($errors->has('loans'))
                                    <span class="text-danger">{{ $errors->first('loans') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                {!! inputField('loan_area', 'लगानीको मुख्य क्षेत्र', 'लगानीको मुख्य क्षेत्र', isset($data) ? $data->loan_area : old('loan_area')) !!}
                                @error('loan_area')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                {!! inputField('contact_person', 'सम्पर्क वयक्ति', 'सम्पर्क वयक्ति', isset($data) ? $data->contact_person : old('contact_person')) !!}
                                @if ($errors->has('contact_person'))
                                    <span class="text-danger">{{ $errors->first('contact_person') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                {!! inputField('contact_no', 'सम्पर्क नं.', 'सम्पर्क नं', isset($data) ? $data->contact_no : old('contact_no')) !!}
                                @error('contact_no')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                {!! latField('latitude', 'अक्षांश', 'अक्षांश', isset($data) ? $data->latitude : old('latitude')) !!}
                                @error('latitude')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                {!! latField('longitude', 'देशान्तर', 'देशान्तर', isset($data) ? $data->longitude : old('longitude')) !!}
                                @error('longitude')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                        </div>

                        <div class="row">
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="control-label "> कैफियत
                                    </label>
                                    <div class="col-md-12">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <textarea id="editor1"
                                                name="remarks">{{ old('remarks', $data->remarks) }} </textarea>
                                            @if ($errors->has('remarks'))
                                                <span class="text-danger font-weight-danger">
                                                    {{ $errors->first('remarks') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <br>

                    <div>
                        <button type="button"
                            class=" btn                                                                                                                                                                                                                                                                                                                                                                                                        default"
                            href="{{ url()->previous() }}">Cancel</button>
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i> Save</button>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        })
    </script>


@endsection












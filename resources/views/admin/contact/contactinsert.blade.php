@extends('template.admin')
@section('title', 'महत्वपूर्ण सम्पर्क थप्नुहोस्')
@section('page-title')
    महत्वपूर्ण सम्पर्क थप्नुहोस्
@endsection
@section('content')

    <div class="tab-pane" id="tab_2">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>महत्वपूर्ण सम्पर्क थप्नुहोस्
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form method="POST"
                    action="{{ isset($data->id) ? '/admin/updatecontact/' . $data->id : '/admin/storecontact' }}"
                    enctype="multipart/form-data" class="horizontal-form">
                    @isset($data->id)
                        {{ method_field('PUT') }}
                    @endisset
                    @csrf
                    <div class="form-body">
                        {{-- <h3 class="form-section">सरकारी तथा सामुदायिक भवन</h3> --}}
                        <div class="row">
                            <div class="col-md-6">
                                {!! inputField('name', 'नाम :', 'नाम', isset($data) ? $data->name : old('name')) !!}
                                @if ($errors->has('name'))
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                {!! inputField('address', 'ठेगाना', 'ठेगाना', isset($data) ? $data->address : old('address')) !!}
                                @if ($errors->has('address'))
                                    <span class="help-block">{{ $errors->first('address') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                {!! inputField('post', 'पद :', 'पद', isset($data) ? $data->post : old('post')) !!}
                                @if ($errors->has('post'))
                                    <span class="help-block">{{ $errors->first('post') }}</span>
                                @endif
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                {!! inputField('contact', 'सम्पर्क नं :', 'सम्पर्क नं', isset($data) ? $data->contact : old('contact')) !!}
                                @if ($errors->has('contact'))
                                    <span class="help-block">{{ $errors->first('contact') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                {!! inputField('website', 'वेब साइट :', 'वेब साइट', isset($data) ? $data->website : old('website')) !!}
                                @if ($errors->has('website'))
                                    <span class="help-block">{{ $errors->first('website') }}</span>
                                @endif
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                {!! inputField('email', 'ईमेल :', 'ईमेल', isset($data) ? $data->email : old('email')) !!}
                                @if ($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-md-9 text-center">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                @if ($data->id == null)
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                                                        alt="" />
                                                @else
                                                    <img src={{ url('uploads/contact/' . $data->photo) }} alt="" />
                                                @endif
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail"
                                                style="max-width: 200px; max-height: 150px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> फोटो
                                                        छान्नुहोस </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="photo" value="{{ $data->photo }}"
                                                        accept="image/*"></span>
                                                <a href="javascript:;" class="btn red fileinput-exists"
                                                    data-dismiss="fileinput">
                                                    Remove </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <!--/span-->

                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                    <label for="inputUserType3" class="form-label lead">सक्रिय :</label>
                                    <div class="row d-inline-flex p-5 m-5">
                                        @if ($data->id == null)
                                            <div class="col-3 md-radio form-control btn">
                                                <input type="radio" id="radio14" name="status" value="1"
                                                    class="md-radiobtn">
                                                <label for="radio14">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> छ </label>
                                            </div>
                                            <div class=" col-3 md-radio has-error form-control btn">
                                                <input type="radio" id="radio15" name="status" value="0"
                                                    class="md-radiobtn">
                                                <label for="radio15">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> छैन </label>
                                            </div>

                                        @else
                                            <div class="col-3 md-radio form-control btn">
                                                <input type="radio" id="radio14" name="status" value="1"
                                                    {{ $data->status == 1 ? 'checked' : '' }} class="md-radiobtn">
                                                <label for="radio14">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> छ </label>
                                            </div>
                                            <div class="col-3 md-radio has-error form-control btn">
                                                <input type="radio" id="radio15" name="status" value="0"
                                                    {{ $data->status == 0 ? 'checked' : '' }} class="md-radiobtn">
                                                <label for="radio15">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> छैन </label>
                                            </div>
                                        @endif
                                        @if ($errors->has('status'))
                                            <span class="help-block">{{ $errors->first('status') }}</span>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div>
                            <button type="button" class=" btn default" href="{{ url()->previous() }}">Cancel</button>
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Save</button>
                        </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        })
    </script>
@endsection

@extends('template.admin')
@section('title', 'हिंडोला')
@section('page-title')
    हिंडोला
@endsection
@section('content')


    @if (session('status') == 'success')
        <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Updated"
            data-message="Updated Succesfully" data-type="success" data-allow-outside-click="true"
            data-confirm-button-class="btn-success"></button>
    @elseif (session('status') == 'delet')
        <button class="btn btn-danger mt-sweetalert gk-btn-success" id="gk-success" data-title="Delete"
            data-message="Deleted Successfully" data-type="error" data-allow-outside-click="true"
            data-confirm-button-class="btn-danger"></button>
    @elseif (session('status') == 'insert')
        <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Inserted"
            data-message="Insert Succesfuly" data-type="success" data-allow-outside-click="true"
            data-confirm-button-class="btn-success"></button>
    @elseif (session('status') == 'change')
        <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Updated"
            data-message="Status Updated Succesfully" data-type="success" data-allow-outside-click="true"
            data-confirm-button-class="btn-success"></button>
    @endif

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('cd-admin/home') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span>हिंडोला</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body"
                data-placement="bottom" data-original-title="Change dashboard date range">
                <i class="icon-calendar"></i>&nbsp;
                <span class="thin uppercase hidden-xs"></span>&nbsp;
                <i class="fa fa-angle-down"></i>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>


                        <span class="caption-subject bold uppercase">हिंडोलाहरुको विवरण</span>
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a id="sample_editable_1_new" class="btn sbold green" href="/admin/insertcarousel"> Add
                                        New
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-checkable order-column"
                        id="sample_1">
                        {{-- <thead class="flip-content"> --}}
                        <tr class="d-flex">
                            <th class="col-4">Image</th>
                            <th class="col-3">status</th>
                            <th class="col-3">Edit</th>
                            <th class="col-2">Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($carousel as $value)
                                <tr class="odd gradeX">
                                    <td><img src="{{ url('uploads/carousel/' . $value->image) }}" width="100"
                                            height="100"></td>
                                    <td>
                                        @if ($value->status) <a
                                                href="{{ url('admin/changecarouselStatus/' . $value->id) }}">
                                                <span class="label label-sm label-success"> Active </span> </a>
                                        @else
                                            <a href="{{ url('admin/changecarouselStatus/' . $value->id) }}"><span
                                                    class="label label-sm label-danger">Inactive</span> </a>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="/admin/carouseledit/{{ $value->id }}" class="btn btn-circle green">
                                            सम्पादन गर्नुहोस्
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td><a href="#" data-toggle="modal" data-target="#{{ $value->id }}"
                                            class="btn btn-circle red"><i class="fa fa-remove"></i>मेटाउन</a>
                                        </a>
                                        <div id="{{ $value->id }}" class="modal fade" tabindex="-1"
                                            data-backdrop="static" data-keyboard="false">
                                            <div class="___class_+?38___">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true"></button>
                                                        <h4 class="modal-title">पुष्टिकरण</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p> के तपाइँ {{ $value->id }} लाई मेटाउन चाहानुहुन्छ?
                                                        </p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal"
                                                            class="btn dark btn-outline">रद्द गर्नुहोस्</button>
                                                        <form class="form-sample" method="POST"
                                                            action="/admin/carouseldeleteform/{{ $value->id }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn green">हो</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $carousel->links() }}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection

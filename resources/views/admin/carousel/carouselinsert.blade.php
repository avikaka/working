@extends('template.admin')
@section('title', 'हिंडोला')
@section('page-title')
हिंडोला
@endsection
@section('content')

<div class="tab-pane" id="tab_2">
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>हिंडोला थप्नुहोस्
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form method="POST"
                action="{{ isset($data->id) ? '/admin/updatecarousel/' . $data->id : '/admin/storecarousel' }}"
                enctype="multipart/form-data" class="horizontal-form">
                @isset($data->id)
                {{ method_field('PUT') }}
                @endisset
                @csrf
                <div class="form-body">
                    {{-- {{dd($data)}} --}}
                    {{-- <h3 class="form-section">सरकारी तथा सामुदायिक भवन</h3> --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row{{ $errors->has('image') ? ' has-error' : '' }}">
                                <label class="form-label" for="customFile">Choose Image</label>
                                <input type="file" name="image"
                                    class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" />

                                @if ($errors->has('image'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                                <label class="form-label" for="heading">Caption Heading</label>
                                <input type="text" name="heading"
                                    class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" />

                                <label class="form-label" for="caption">Caption</label>
                                <input type="text" name="caption"
                                    class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" />
                            </div>
                        </div>
                        <!--/span-->
                    </div>



                <div class="row">
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="inputUserType3" class="form-label lead">सक्रिय :</label>
                            <div class="row d-inline-flex p-5 m-5">
                                @if ($data->id == null)
                                    <div class="col-3 md-radio form-control btn">
                                        <input type="radio" id="radio14" name="status" value="1" class="md-radiobtn">
                                        <label for="radio14">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> छ </label>
                                    </div>
                                    <div class=" col-3 md-radio has-error form-control btn">
                                        <input type="radio" id="radio15" name="status" value="0" class="md-radiobtn">
                                        <label for="radio15">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> छैन </label>
                                    </div>

                                @else
                                    <div class="col-3 md-radio form-control btn">
                                        <input type="radio" id="radio14" name="status" value="1"
                                            {{ $data->status == 1 ? 'checked' : '' }} class="md-radiobtn">
                                        <label for="radio14">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> छ </label>
                                    </div>
                                    <div class="col-3 md-radio has-error form-control btn">
                                        <input type="radio" id="radio15" name="status" value="0"
                                            {{ $data->status == 0 ? 'checked' : '' }} class="md-radiobtn">
                                        <label for="radio15">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> छैन </label>
                                    </div>
                                @endif
                                @if ($errors->has('status'))
                                    <span class="help-block">{{ $errors->first('status') }}</span>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>

                <br>

                <div>
                    <button type="button" class=" btn default" href="{{ url()->previous() }}">Cancel</button>
                    <button type="submit" class="btn blue">
                        <i class="fa fa-check"></i> Save</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js"
    referrerpolicy="origin"></script>
<script>
    tinymce.init({
        selector: 'textarea'
    })
</script>
@endsection

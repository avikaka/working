{{-- @if (Auth::user()) --}}
<div class="page-sidebar navbar-collapse collapse" style="position: fixed">

    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-fixed " data-keep-expanded="false"
        data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px; ">

        <li class="sidebar-toggler-wrapper hide">
            <div class="sidebar-toggler">
                <span></span>
            </div>
        </li>


        <li class="nav-item {{ request()->is('admin/home') ? 'active' : '' }}  open">
            <a href="{{ url('/admin/home') }}">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
                <span class="selected"></span>

            </a>
        </li>

        {{-- @if (\Auth::user()->status == 'super-admin' || 'admin') --}}
        <li class="heading">
            <h3 class="uppercase">-----------------------------------------</h3>
        </li>


        <li class="nav-item start">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="glyphicon glyphicon-hdd"></i>
                <span class="title">वेबसाइट सामग्री</span>
                <span class="arrow"></span>
            </a>

            {{-- @endif --}}

            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{ url('admin/viewcontact') }}">
                        <i class="fa fa-users"></i>
                        <span class="title">
                            महत्त्वपूर्ण सम्पर्क</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/viewdescription') }}">
                        <i class="fa fa-text-width"></i>
                        <span class="title">
                            संक्षिप्त परिचय</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/viewcarousel') }}">
                        <i class="fa fa-image"></i>
                        <span class="title">हिंडोला</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/viewlogo') }}">
                        <i class="fa fa-file-image-o"></i>
                        <span class="title">लोगो</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="heading">
            <h3 class="uppercase">-----------------------------------------</h3>
        </li>

        <li class="nav-item">
            <a href="{{ url('admin/viewuser') }}" class="nav-link">
                <i class="glyphicon glyphicon-hdd"></i>
                <span class="title">Users</span>
            </a>
        </li>


        <li class="heading">
            <h3 class="uppercase">-----------------------------------------</h3>
        </li>


        <li class="nav-item">
            <a href="{{ url('admin/personsearch') }}" class="nav-link">
                <i class="glyphicon glyphicon-hdd"></i>
                <span class="title">व्यक्ति विवरण</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ url('admin/householdsearch') }}" class="nav-link">
                <i class="glyphicon glyphicon-hdd"></i>
                <span class="title">घरधुरि विवरण</span>
            </a>
        </li>

        <li class="heading">
            <h3 class="uppercase">-----------------------------------------</h3>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.community-building.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">सामुदायिक भवन</span>
            </a>
        </li>
        <li class="nav-item ">
            <a href="{{ route('admin.community-forest.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">सामुदायिक वन</span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="{{ route('admin.public-toilet.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">सार्वजनिक शौचालय</span>
            </a>
        </li>

        <li class="nav-item  ">
            <a href="{{ route('admin.pond-lake-wetland.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">सार्वजनिक पोखरी</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.irrigation.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">सिंचाई आयोजना</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.agro-vet.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">कृषि/पशुपालन समूह</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.tourism.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">पर्यटकीय
                    क्षेत्र</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.agro-vet-pocket.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">कृषि/पशु
                    पंक्षी
                    पकेट
                    क्षेत्र</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.road-network.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">सडक
                    संजालको
                    अवस्था</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.bridge.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">पुल
                    पुलेसा</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.drinking-water.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">खानेपानी
                    आयोजना</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.haat-bazar.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">कृषि
                    तथा हाट
                    बजार</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.religious.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">धार्मिकस्थल</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.tlo.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">टोल विकास
                    संस्था</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.cooperative.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">सहकारी
                    संस्था</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.bank.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">बैँक</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.health-institution.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">स्वास्थ्य
                    संस्था</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.college.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">विद्यालय/क्याम्पस</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.playground.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">खेल
                    मैदान/व्यायामशाला</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.agro-farm.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">कृषि/पशुपालन
                    फार्म</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.children-club.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">बाल
                    क्लब</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.public-place.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">पाटी/पौवा
                    चौतारा</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.crematorium.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">चिहानस्थलहरु</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.women-group.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">आमा
                    समूह</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.social-home.index') }}">
                <i class="fa fa-file-image-o"></i>
                <span class="title">महिला
                    सुरक्षा</span>
            </a>
        </li>
    </ul>

    {{-- @else --}}
    <!-- END SIDEBAR MENU -->
    <!-- END SIDEBAR MENU -->
</div>
{{-- @endif --}}
<!-- END SIDEBAR -->

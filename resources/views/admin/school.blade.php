@extends('template.index')
@section('title','साक्षरताको अवस्था')
@section('main-content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h3>वडाअनुसार साक्षरताको अवस्था</h3>
        </div>
        <div class="col grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form class="form-sample">
                        <p class="card-description">
                          Personal info
                        </p>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group row">
                              {{-- <label class="col-lg-3 col-sm-4 col-form-label" for="name">विद्यालयको नाम</label>
                              <div class="col-lg-9 col-sm-8">
                                <input type="text" id="name" class="form-control" name="name" />
                              </div> --}}
                              {!! getInputField('name','विद्यालयको नाम','विद्यालयको नाम') !!}
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group row">le
                              <label class="col-lg-3 col-sm-4 col-form-label" for="ward">वडा नं.</label>
                              <div class="col-sm-9">
                                {{-- @php echo(getWardCombo(5)) @endphp --}}
                                {!! getWardCombo(2) !!}
                                
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group row">
                                {!! getInputField('adress','ठेगाना','ठेगाना') !!}
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group row">
                                {!! getInputField('phone','फोन','फोन') !!}
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group row">
                                {!! getInputField('email','इमेल','इमेल') !!}
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group row">
                              {!! getSelectField('level','सञ्चालित तह',
                              array(0=>'Kindergarten','Basic Level (1-3)', 'Basic Level (1-5)', 'Secondary Level','Bachelors', 9=>'Masters' ),'') !!} 
                            </div>
                          </div>
                        </div>
                        <p class="card-description">
                          Address
                        </p>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group row">
                                {!! getInputField('remarks','कैफियत') !!}
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">State</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Address 2</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" />
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Postcode</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">City</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" />
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Country</label>
                              <div class="col-sm-9">
                                <select class="form-control">
                                  <option>America</option>
                                  <option>Italy</option>
                                  <option>Russia</option>
                                  <option>Britain</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                </div>
            </div>
            </div> 
        </div> {{-- row end --}}

    </div>
</div> {{-- row end --}}

</div>
{{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
@endsection

@section('header-style')
<style>
.ward-population {
    height: 450px;
}
</style>
@endsection
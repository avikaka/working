@extends('template.admin')
@section('title', 'सरकारी तथा सामुदायिक भवन सम्बन्धी विवरण')
@section('page-title')
    सरकारी तथा सामुदायिक भवन
@endsection
@section('content')

    @if (session('status') == 'success')
        <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Updated"
            data-message="Updated Succesfully" data-type="success" data-allow-outside-click="true"
            data-confirm-button-class="btn-success"></button>
    @elseif (session('status') == 'delet')
        <button class="btn btn-danger mt-sweetalert gk-btn-success" id="gk-success" data-title="Delete"
            data-message="Deleted Successfully" data-type="error" data-allow-outside-click="true"
            data-confirm-button-class="btn-danger"></button>
    @elseif (session('status') == 'insert')
        <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Inserted"
            data-message="Insert Succesfuly" data-type="success" data-allow-outside-click="true"
            data-confirm-button-class="btn-success"></button>
    @elseif (session('status') == 'change')
        <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Updated"
            data-message="Status Updated Succesfully" data-type="success" data-allow-outside-click="true"
            data-confirm-button-class="btn-success"></button>
    @endif

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('cd-admin/home') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span>सरकारी तथा सामुदायिक भवन </span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body"
                data-placement="bottom" data-original-title="Change dashboard date range">
                <i class="icon-calendar"></i>&nbsp;
                <span class="thin uppercase hidden-xs"></span>&nbsp;
                <i class="fa fa-angle-down"></i>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> सरकारी तथा सामुदायिक भवन सम्बन्धी विवरण</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a id="sample_editable_1_new" class="btn sbold green"
                                        href="{{ route('admin.community-building.create') }}"> Add New
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <table class="table table-bordered table-striped table-condensed flip-content">
                        <thead class="flip-content">
                            <tr>
                                <th>भवनको नाम</th>
                                <th>वडा</th>
                                <th>छवि</th>
                                <th>ठेगाना</th>
                                <th>भवनको प्रकार</th>
                                <th>अवस्था</th>
                                <th>कैफियत</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($communitybuilding as $building)
                                <tr class="odd gradeX">
                                    <td>{{ $building->name }}</td>
                                    <td>{{ $building->ward }}</td>
                                    @if (!empty($building->photo))
                                        <td><img src="{{ url('uploads/communitybuilding/' . $building->photo) }}"
                                                width="100" height="100">
                                        </td>
                                    @else
                                        <td><img src="{{ url('uploads/communitybuilding/buildingsamo123321.png') }}"
                                                width="100" height="100"></td>
                                    @endif
                                    <td>{{ $building->address }}</td>
                                    <td>{{ $building->type }}</td>
                                    <td>{{ $building->status }}</td>
                                    <td>{!! $building->remarks !!}</td>
                                    <td>
                                        <a href="/admin/community-building/{{ $building->id }}/edit"
                                            class="btn btn-circle green"> सम्पादन गर्नुहोस्
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td><a href="#" data-toggle="modal" data-target="#{{ $building->id }}"
                                            class="btn btn-circle red"><i class="fa fa-remove"></i>मेटाउन</a>
                                        </a>
                                        <div id="{{ $building->id }}" class="modal fade" tabindex="-1"
                                            data-backdrop="static" data-keyboard="false">
                                            <div class="___class_+?38___">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true"></button>
                                                        <h4 class="modal-title">पुष्टिकरण</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p> के तपाइँ {{ $building->name }} लाई मेटाउन चाहानुहुन्छ?
                                                        </p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal"
                                                            class="btn dark btn-outline">रद्द गर्नुहोस्</button>
                                                        <form class="form-sample" method="POST"
                                                            action="{{ Route('admin.community-building.destroy', $building->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn green">हो</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $communitybuilding->links() }}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection

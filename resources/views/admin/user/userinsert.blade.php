@extends('template.admin')

@section('page-title')
    User Insert
@endsection


@section('content')


    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>User Insert
            </div>

        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form autocomplete="off" action="{{ url('/admin/storeuser') }}" method="post" class="form-horizontal"
                enctype="multipart/form-data">
                @csrf
                <div class="form-body">

                    <div class="form-group">
                        <label class="col-md-3 control-label"> Name</label>
                        <div class="col-md-4">
                            <div class="input-icon">

                                <input type="text" class="form-control input-circle" placeholder="Name" name="name">
                            </div>
                            @if ($errors->has('name'))
                                <span class="help-block">{{ $errors->first('name') }}</span>
                            @endif
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> Email</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="envelope-o"></i>
                                <input type="text" class="form-control input-circle" placeholder="Email" name="email"
                                    autocomplete="off">
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">New Password</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="password" class="form-control input-circle-left" placeholder="New Password"
                                    name="new-password">
                                <span class="input-group-addon input-circle-right">
                                    <i class="fa fa-cog"></i>
                                </span>
                            </div>
                            @if ($errors->has('new-password'))
                                <span class="text-danger">{{ $errors->first('new-password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Confirm Password</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="password" class="form-control input-circle-left" placeholder="Confirm Password"
                                    name="new-password_confirmation">
                                <span class="input-group-addon input-circle-right">
                                    <i class="fa fa-cog"></i>
                                </span>

                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="inputUserType3" class="col-md-3 control-label">Role</label>
                        <div class="col-md-4">
                            <div class="md-radio">
                                <input type="radio" id="radio14" name="status" value="admin" class="md-radiobtn">
                                <label for="radio14">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Admin </label>
                            </div>
                            <div class="md-radio ">
                                <input type="radio" id="radio15" name="status" value="user" class="md-radiobtn">
                                <label for="radio15">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> User </label>
                            </div>
                            @if ($errors->has('status'))
                                <span class="help-block">{{ $errors->first('status') }}</span>
                            @endif

                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-circle green">Submit</button>
                            </div>
                        </div>
                    </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>


@endsection

@extends('template.admin')

@section('page-title')
    View All User
@endsection



@section('content')


    <div class="portlet box green">

    </div>
    <div class="card-body">

        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{ url('/newpassword', $user->id) }}" method="post" class="form-horizontal"
                enctype="multipart/form-data">
                @csrf
                <div class="form-body">

                    <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">Current Password</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="password" class="form-control input-circle-left" placeholder="Old Password"
                                    name="current-password">
                                <span class="input-group-addon input-circle-right">
                                    <i class="fa fa-cog"></i>
                                </span>
                                @if ($errors->has('current-password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('current-password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">New Password</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="password" class="form-control input-circle-left" placeholder="New Password"
                                    name="new-password">
                                <span class="input-group-addon input-circle-right">
                                    <i class="fa fa-cog"></i>
                                </span>
                                @if ($errors->has('new-password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('new-password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Confirm Password</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="password" class="form-control input-circle-left" placeholder="Confirm Password"
                                    name="new-password_confirmation">
                                <span class="input-group-addon input-circle-right">
                                    <i class="fa fa-cog"></i>
                                </span>

                            </div>
                        </div>
                    </div>



                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn btn-circle green">Submit</button>

                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>


@endsection

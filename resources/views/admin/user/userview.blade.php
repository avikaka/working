@extends('template.admin')

@section('page-title')
    View All User
@endsection


@section('content')
    @if (session('status') == 'success')
        <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Updated"
            data-message="User Updated Succesfuly" data-type="success" data-allow-outside-click="true"
            data-confirm-button-class="btn-success"></button>
    @elseif (session('status') == 'delet')
        <button class="btn btn-danger mt-sweetalert gk-btn-success" id="gk-success" data-title="Delete"
            data-message="User Deleted" data-type="error" data-allow-outside-click="true"
            data-confirm-button-class="btn-danger"></button>
    @elseif (session('status') == 'insert')
        <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Inserted"
            data-message="User Insert Succesfuly" data-type="success" data-allow-outside-click="true"
            data-confirm-button-class="btn-success"></button>
    @elseif (session('status') == 'change')
        <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Updated"
            data-message="User Status Updated" data-type="success" data-allow-outside-click="true"
            data-confirm-button-class="btn-success"></button>
    @endif
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('admin/home') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Users</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body"
                data-placement="bottom" data-original-title="Change dashboard date range">
                <i class="icon-calendar"></i>&nbsp;
                <span class="thin uppercase hidden-xs"></span>&nbsp;
                <i class="fa fa-angle-down"></i>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> User Table</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">

                                <div class="btn-group">
                                    <a id="sample_editable_1_new" class="btn sbold green"
                                        href="{{ url('admin/insertuser') }}"> Add New
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>

                            </div>

                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable order-column"
                        id="sample_1">
                        <thead>
                            <tr>

                                <th> Username </th>
                                <th> Email </th>
                                <th> Role </th>
                                <th> Actions </th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($user as $value)
                                <tr class="odd gradeX">

                                    <td> {{ $value->name }} </td>
                                    <td>
                                        {{ $value->email }}
                                    </td>

                                    <td>
                                        {{ $value->status }}

                                    </td>

                                    <td>
                                        <a class="btn red btn-outline sbold uppercase" data-target="#{{ $value->id }}"
                                            data-toggle="modal"> Delete </a>
                                        </li>
                                    </td>

                                    <td>
                                        <a class="btn btn-primary btn-outline sbold uppercase"
                                            href="{{ url('admin/changePassword/' . $value->id) }}">
                                            Change Password </a>
                                        </li>
                                    </td>

                                </tr>



                            @endforeach




                        </tbody>

                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    @foreach ($user as $value)
        <div id="ajax-modal" class="modal fade" tabindex="-1"> </div>
        <div id="{{ $value->id }}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
            <div class="modal-body">
                <p> Do You like To Delete It? </p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
                <a href="{{ url("/admin/userdeleteform/{$value->id}") }}" class="btn red">Delete</a>

            </div>
        </div>



        </object>
        </div>


        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>

        </div>
        </div>
    @endforeach
@endsection

@extends('template.admin')

@section('page-title')
    Change Password
@endsection


@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Change password
            </div>

        </div>
    </div>
    <div class="card-body">
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{ url('/admin/changePassworduser/' . $data->id) }}" method="POST" class="form-horizontal">

                @csrf
                @if (isset($data->id))
                    {{ method_field('PUT') }}
                @endif

                <div class="form-body">
                    {{-- <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">Current Password</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="password" class="form-control input-circle-left" placeholder="Old Password"
                                    name="current-password" value="{{ $data->password }}" disabled>
                                <span class="input-group-addon input-circle-right">
                                    <i class="fa fa-cog"></i>
                                </span>
                                @if ($errors->has('current-password'))
                                    <span class="text-danger">{{ $errors->first('current-password') }}</span>
                                @endif
                            </div>
                        </div>
                    </div> --}}

                    <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">New Password</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="password" class="form-control input-circle-left" placeholder="New Password"
                                    name="new-password">
                                <span class="input-group-addon input-circle-right">
                                    <i class="fa fa-cog"></i>
                                </span>
                            </div>
                            @if (session()->has('message'))
                                <div class="alert alert-danger">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Confirm Password</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="password" class="form-control input-circle-left" placeholder="Confirm Password"
                                    name="new-password_confirmation">
                                <span class="input-group-addon input-circle-right">
                                    <i class="fa fa-cog"></i>
                                </span>

                            </div>
                        </div>
                    </div>



                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn btn-circle green">Submit</button>
                            <button href="{{ url()->previous() }}"
                                class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>


@endsection

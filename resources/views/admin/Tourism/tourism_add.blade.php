@extends('template.admin')
@section('title', 'सरकारी तथा सामुदायिक भवन सम्बन्धी विवरण')
@section('page-title')
    पर्यटकीय स्थलहरुको विवरण
@endsection
@section('content')

    <div class="tab-pane" id="tab_2">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>पर्यटकीय स्थलहरुको विवरण (धार्मिक/ऐतिहासिक/पुरातात्विक)
                </div>

            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->

                <form method="POST"
                    action="{{ isset($data->id) ? Route('admin.tourism.update', $data->id) : Route('admin.tourism.store') }}"
                    enctype="multipart/form-data" class="horizontal-form">
                    @isset($data->id)
                        {{ method_field('PUT') }}
                    @endisset
                    @csrf
                    <div class="form-body">
                        <div class="row">

                            <div class="col-md-6">
                                {!! inputField('name', 'पर्यटकीय स्थलको नाम', 'पर्यटकीय स्थलको नाम', isset($data) ? $data->name : old('name'), $errors) !!}
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <div class="col-md-6">
                                {!! inputField('address', 'ठेगाना', 'ठेगाना', isset($data) ? $data->address : old('address')) !!}
                                @if ($errors->has('address'))
                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                @endif
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">वडा नं.</label>
                                    {!! getWardCombo(0) !!}
                                    @if ($errors->has('ward'))
                                        <span class="text-danger">{{ $errors->first('ward') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                {!! selectField(
    'access',
    'सडकको पहुँच',
    [
        1 => 'पुगेको',
        'नपुगेको',
    ],
    '',
    isset($data) ? $data->access : old('access'),
) !!}
                                @if ($errors->has('access'))
                                    <span class="text-danger">{{ $errors->first('access') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                {!! selectField(
    'ownership',
    'भवनको प्रकार',
    [
        1 => 'निजी',

        'पब्लिक',
        'सामुदायिक',
        'अन्य',
    ],
    '',
    isset($data) ? $data->ownership : old('ownership'),
) !!}
                                @if ($errors->has('ownership'))
                                    <span class="text-danger">{{ $errors->first('ownership') }}</span>
                                @endif
                            </div>

                            <div class="col-md-6">
                                {!! selectField(
    'features',
    'विशेषता',
    [
        1 => 'धार्मिक',

        'सांस्कृतिक',
        'एतिहासिक',
        'पर्यटकिय',
        'अन्य',
    ],
    '',
    isset($data) ? $data->features : old('features'),
) !!}
                                @if ($errors->has('features'))
                                    <span class="text-danger">{{ $errors->first('features') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                {!! inputField('yearly_tourist_number', 'अनुमानित बार्षिक आगन्तुक संख्या', 'अनुमानित बार्षिक आगन्तुक संख्या', isset($data) ? $data->yearly_tourist_number : old('yearly_tourist_number'), $errors) !!}
                                @if ($errors->has('yearly_tourist_number'))
                                    <span class="text-danger">{{ $errors->first('yearly_tourist_number') }}</span>
                                @endif
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                {!! latField('latitude', 'अक्षांश', 'अक्षांश', isset($data) ? $data->latitude : old('latitude')) !!}
                                @error('latitude')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                {!! latField('longitude', 'देशान्तर', 'देशान्तर', isset($data) ? $data->longitude : old('longitude')) !!}
                                @error('longitude')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--/span-->
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-9 text-center">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            @if ($data->id == null)
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                                                    alt="" />
                                            @else
                                                <img src={{ url('uploads/toilet/' . $data->photo) }} alt="" />
                                            @endif
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                            style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> फोटो
                                                    छान्नुहोस </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image" value="{{ $data->photo }}"
                                                    accept="image/*"></span>
                                            <a href="javascript:;" class="btn red fileinput-exists"
                                                data-dismiss="fileinput">
                                                Remove </a>
                                            @error('photo')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!--/span-->

                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="control-label "> कैफियत
                                </label>
                                <div class="col-md-12">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <textarea id="editor1"
                                            name="remarks">{{ old('remarks', $data->remarks) }} </textarea>
                                        @if ($errors->has('remarks'))
                                            <span class="text-danger font-weight-danger">
                                                {{ $errors->first('remarks') }}
                                            </span>

                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div>
                        <button type="button"
                            class=" btn
                                                                                                                                                                                                                                                                                                                                                                                                        default"
                            href="{{ url()->previous() }}">Cancel</button>
                        <button type="submit" class="btn blue">
                            <i class="fa fa-check"></i> Save</button>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        })
    </script>


@endsection

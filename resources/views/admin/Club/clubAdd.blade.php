{{-- @extends('template.index')
@section('title', 'यूवा क्लब तथा खेलकुद क्लब सम्बन्धी विवरण')
@section('main-content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h3>यूवा क्लब तथा खेलकुद क्लब सम्बन्धी विवरण</h3>
        </div>
        <div class="col grid-margin stretch-card">
            <div class="card">
                <div class="card-body"> --}}
@extends('template.admin')
@section('title', 'सरकारी तथा सामुदायिक भवन सम्बन्धी विवरण')
@section('page-title')
    सरकारी तथा सामुदायिक भवन
@endsection
@section('content')

    <div class="tab-pane" id="tab_2">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>सरकारी तथा सामुदायिक भवन
                </div>

            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form form method="POST" action='@if (isset($data))  /admin/youth-club/{{ $data->id }} @else /admin/youth-club @endif' enctype="multipart/form-data">
                    @csrf
                    @if (isset($data))
                        @method('PUT')
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                {!! getInputField('name', 'यूवा क्लब तथा खेलकुद क्लबको नाम', 'यूवा/खेलकुद क्लबको नाम', isset($data) ? $data->name : old('name')) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-lg-3 col-sm-4 col-form-label" for="ward">वडा नं.</label>
                                <div class="col-sm-9">
                                    {{-- @php echo(getWardCombo(5)) @endphp --}}
                                    {!! getWardCombo(1) !!}

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                {!! getInputField('address', 'ठेगाना', 'ठेगाना', isset($data) ? $data->address : old('address')) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="type">प्रकार</label>
                                <div class="col-sm-9">
                                    <select name="type" class="form-control">
                                        <option>छान्नुहोस</option>
                                        <option value="1">सामाजिक</option>
                                        <option value="2">खेलकुद</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                {!! getInputField('members', 'सदस्य संख्या', 'सदस्य संख्या', isset($data) ? $data->members : old('members')) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                {!! getInputField('contact_person', 'सम्पर्क वयक्ति', 'सम्पर्क वयक्ति', isset($data) ? $data->contact_person : old('contact_person')) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group row">
                                {!! getInputField('contact_no', 'सम्पर्क नं.', 'सम्पर्क नं', isset($data) ? $data->contact_no : old('contact_no')) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="remarks">कैफियत</label>
                                <div class="col-sm-9">
                                    <textarea type="name" name="remarks" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                {!! getInputField('latitude', 'Latitude', 'Latitude', isset($data) ? $data->latitude : old('latitude')) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                {!! getInputField('longitude', 'Longitude', 'Longitude', isset($data) ? $data->longitude : old('longitude')) !!}
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="/admin/youth-club" class="btn btn-secondary">Cancel</a>


                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        })
    </script>


@endsection

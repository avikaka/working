@extends('template.index')
@section('title', 'यूवा क्लब तथा खेलकुद क्लब सम्बन्धी विवरण')
@section('main-content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <h3>यूवा क्लब तथा खेलकुद क्लब सम्बन्धी विवरण</h3>
            </div>
            <div class="col-xl-12 grid-margin stretch-card">
                <div class="card">

                    <div class="card-body">

                        @if (Session::has('msg'))
                            <div class="col-md-12">
                                <div class="alert alert-success">{{ Session::get('msg') }}</div>
                            </div>
                        @endif

                        <a href="{{ route('admin.youth-club.create') }}" class="btn btn-primary"
                            style="float:right;">ADD</a>
                        <div class="table-responsive">

                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                    <tr>
                                        <th>क्र.स.</th>
                                        <th>क्लबको नाम</th>
                                        <th>वडा</th>
                                        <th>ठेगाना</th>
                                        <th>प्रकार</th>
                                        <th>सदस्य संख्या</th>
                                        <th>सम्पर्क वयक्ति</th>
                                        <th>सम्पर्क नं.</th>
                                        <th>कैफियत</th>
                                        <th>Edit</th>
                                        <th>Delete</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($club as $toilet)
                                        <tr>
                                            <td class="font-weight-bold">{{ $toilet->id }}</td>
                                            <td>{{ $toilet->name }}</td>
                                            <td>{{ $toilet->ward }}</td>
                                            <td>{{ $toilet->address }}</td>
                                            <td>{{ $toilet->type }}</td>
                                            <td>{{ $toilet->members }}</td>
                                            <td>{{ $toilet->contact_person }}</td>
                                            <td>{{ $toilet->contact_no }}</td>
                                            <td>{{ $toilet->remarks }}</td>
                                            <td><a href="/admin/youth-club/{{ $toilet->id }}/edit"
                                                    class="btn btn-outline-primary btn-icon edit"><i
                                                        class="mdi mdi-pencil mdi-18px"></i><a></td>

                                            <td>
                                                <form action="/admin/youth-club/{{ $toilet->id }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class='btn btn-outline-danger btn-icon delete ml-1'><i
                                                            class="mdi mdi-delete mdi-18px"></i></button>
                                                </form>
                                            </td>

                                    @endforeach





                                </tbody>
                                {{-- <tfoot>
                          <th>Total</th>
                          <th>{{$totalMale}}</th>
                            <th>{{$totalFemale}}</th>
                            <th>{{$totalOther}}</th>
                            <th>{{$totalMale + $totalFemale + $totalOther}}</th>
                            </tfoot> --}}
                            </table>
                            {{ $club->links() }}
                            {{-- end of table --}}
                        </div> {{-- end of .table-responsive --}}
                    </div>
                </div>
            </div>

        </div> {{-- row end --}}

    </div>
    {{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
    <script src="{{ asset('amcharts4/core.js') }}"></script>
    <script src="{{ asset('amcharts4/charts.js') }}"></script>
    <script src="{{ asset('amcharts4/themes/animated.js') }}"></script>
    <script src="{{ asset('amcharts4/population.js?v=0.1') }}"></script>
@endsection

@section('header-style')
    <style>
        .ward-population {
            height: 450px;
        }

    </style>

@endsection
{{-- <script>
  function deleteRecord($id) {
    if(confirm('Are you sure want to delete ?'))
    {
        window.location.href='{{ url('toilet-deletes') }}/'+$id;
}
alert('ID'+$id);
};
</script> --}}

@extends('template.admin')
@section('title', 'सार्वजनिक शौचालय सम्बन्धी विवरण')
@section('page-title')
    सार्वजनिक शौचालय सम्बन्धी विवरण
@endsection
@section('content')
    <div class="content-wrapper">


        @if (session('status') == 'success')
            <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Updated"
                data-message="Member Updated Succesfuly" data-type="success" data-allow-outside-click="true"
                data-confirm-button-class="btn-success"></button>
        @elseif (session('status') == 'delet')
            <button class="btn btn-danger mt-sweetalert gk-btn-success" id="gk-success" data-title="मेटाइयो"
                data-message="सार्वजनिक शौचालय मेटाइयो" data-type="error" data-allow-outside-click="true"
                data-confirm-button-class="btn-danger"></button>
        @elseif (session('status') == 'insert')
            <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Inserted"
                data-message="Member Insert Succesfuly" data-type="success" data-allow-outside-click="true"
                data-confirm-button-class="btn-success"></button>

        @endif

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ url('admin/home') }}">Dashboard</a>
                    <i class="fa fa-circle"></i>
                </li>

                <li>
                    <span>सार्वजनिक शौचालय सम्बन्धी विवरण </span>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body"
                    data-placement="bottom" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>
        </div>
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>


                                <span class="caption-subject bold uppercase"> सार्वजनिक शौचालय सम्बन्धी विवरण</span>
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a id="sample_editable_1_new" class="btn sbold green"
                                                href="{{ route('admin.public-toilet.create') }}"> नयाँ जोड्नुहोस्
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <table class="table table-striped table-bordered table-hover table-checkable order-column"
                                id="sample_1">
                                <thead>
                                    <tr>

                                        <th>शौचालय नाम</th>
                                        <th>वडा</th>
                                        <th>
                                            छवि</th>
                                        <th>ठेगाना</th>
                                        <th>संख्या</th>
                                        <th>अवस्था</th>
                                        <th>महिलाको लागि छुट्टै भएको</th>
                                        <th>कैफियत</th>
                                        <th>Edit</th>
                                        <th>Delete</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($publictoilet as $data)
                                        <tr>
                                            <td>{{ $data->name }}</td>
                                            <td>{{ $data->ward }}</td>
                                            @if (!empty($data->photo))
                                                <td><img src="{{ url('uploads/toilet/' . $data->photo) }}" width="100"
                                                        height="100">
                                                </td>
                                            @else
                                                <td><img src="{{ url('uploads/toilet/toiletstable1.jfif') }}" width="100"
                                                        height="100"></td>
                                            @endif
                                            <td>{{ $data->address }}</td>
                                            <td>{{ $data->numbers }}</td>
                                            <td>{{ $data->status }}</td>
                                            <td>{{ $data->for_women }}</td>
                                            <td>{!! $data->remarks !!}</td>
                                            <td>
                                                <a href="{{ route('admin.public-toilet.edit', $data->id) }}"
                                                    class="btn btn-circle green"> सम्पादन गर्नुहोस्
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                            <td><a href="#" data-toggle="modal" data-target="#{{ $data->id }}"
                                                    class="btn btn-circle red"><i class="fa fa-remove"></i>मेटाउन</a>
                                                </a>
                                                <div id="{{ $data->id }}" class="modal fade" tabindex="-1"
                                                    data-backdrop="static" data-keyboard="false">
                                                    <div class="___class_+?38___">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                    data-dismiss="modal" aria-hidden="true"></button>
                                                                <h4 class="modal-title">पुष्टिकरण</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p> के तपाइँ {{ $data->name }} लाई मेटाउन चाहानुहुन्छ?
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" data-dismiss="modal"
                                                                    class="btn dark btn-outline">रद्द गर्नुहोस्</button>
                                                                <form class="form-sample" method="POST"
                                                                    action="{{ Route('admin.public-toilet.destroy', $data->id) }}">

                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit" class="btn green">हो</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>


                                    @endforeach





                                </tbody>
                                {{-- <tfoot>
                          <th>Total</th>
                          <th>{{$totalMale}}</th>
                            <th>{{$totalFemale}}</th>
                            <th>{{$totalOther}}</th>
                            <th>{{$totalMale + $totalFemale + $totalOther}}</th>
                            </tfoot> --}}
                            </table>
                            {{ $publictoilet->links() }}
                            {{-- end of table --}}
                        </div> {{-- end of .table-responsive --}}
                    </div>
                </div>
            </div>

        </section>
    </div>







    <!-- /.example-modal -->

@endsection
{{-- end main-content section --}}

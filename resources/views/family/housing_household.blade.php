@extends('template.index')
@section('title','घर तथा घरपरिवार विवरण')
@section('main-content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h3>घर तथा घरपरिवार विवरण</h3>
        </div>
        <div class="col grid-margin stretch-card">
            <div class="card" style="width:450px"> >
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">

                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif

                            <form method="POST" action="/admin/housing_household">
                                @csrf
                                <div class="form-group row">
                                    <label for="Household_id" class="col-sm-10 col-form-label">Household ID</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="householdid" name="householdId"
                                            value="" placeholder="Household ID">
                                    </div>
                                </div>
                                {{-- @method('PUT') --}}
                                <table class="table">
                                    @foreach($datas as $data)
                                    <tr>
                                        <td>{{$data['row']->id}}</td>

                                        <td style="background:#D1D0CE;">
                                            {{$data['row']->question}}</td>
                                    </tr>
                                    <tr>
                                        <td style="background:white;"> &nbsp;</td>
                                        <td>
                                            <table>
                                                @foreach($data['values'] as $value)

                                                <tr>

                                                    <td style="background:white;"><input type="radio"
                                                            name="que_ans[{{$data['row']->field_name }}]"
                                                            value="{{$value->Value}}">{{$value->label_NE}}</input></td>
                                                </tr>

                                                @endforeach
                                            </table>
                                        </td>
                                        <td>
                                    </tr>

                                    @endforeach
                                    <tr><button type="submit" class="btn btn-primary">Accept</button></td>
                                    </tr>
                                </table>{{-- end of table --}}
                            </form>
                        </div>
                    </div>
                </div>
            </div> {{-- row end --}}

        </div>
    </div> {{-- row end --}}

</div>

{{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
@endsection

@section('header-style')
<style>
    .ward-population {
        height: 450px;
    }

</style>
@endsection

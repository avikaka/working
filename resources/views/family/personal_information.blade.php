@extends('template.index')
@section('title', 'परिचयात्मक विवरण')
@section('main-content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <h3>सामान्य व्यक्तिगत विवरण</h3>
            </div>

        </div>

    </div> {{-- row end --}}
    <div class="card">
        <div class="card-body">
            <form method="POST" action="/admin/PersonalInof">
                @csrf
                <div class="form-group row">
                    <div class="col-sm-10">
                        <label for="Household_id" class="col-sm-10 col-form-label">हाउस होल्ड आई. डी.</label>

                        <input type="text" class="form-control" id="householdid" name="householdId" value=""
                            placeholder="Household ID">
                    </div>
                    <div class="col-sm-10">
                        <label for="name" class="col-sm-10 col-form-label">नाम</label>
                        <input type="text" class="form-control" id="name" name="name" value="">
                    </div>
                </div>
                {{-- @method('PUT') --}}
                @foreach ($datas as $data)
                    @if ($data['row']->show_hide == 1)
                        <div class="form-group col-md-12" id="div-{{ $data['row']->field_name }}">
                            <label for="{{ $data['row']->field_name }}"><b>{{ $data['row']->id }}.
                                    {{ $data['row']->question }}</b></label>
                            @if ($data['row']->field_type == 'option')
                                <select class="form-control" id="{{ $data['row']->field_name }}"
                                    name="{{ $data['row']->field_name }}">
                                    @foreach ($data['values'] as $value)
                                        <option value="{{ $value->Value }}">{{ $value->label_NE }}</option>
                                    @endforeach
                                </select>
                            @elseif($data['row']->field_type == 'input')
                                <input class="form-control" id="{{ $data['row']->field_name }}"
                                    name="{{ $data['row']->field_name }}" />
                            @endif
                        </div>
                    @endif
                @endforeach
                <tr><button type="submit" class="btn btn-primary">Accept</button></td>
                </tr>
                </table>{{-- end of table --}}
            </form>


        </div>
    </div>
    </div>
    </div> {{-- row end --}}

    </div>
    {{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
    <script src=" {{ asset('amcharts4/core.js') }}">
    </script>
    <script src="{{ asset('amcharts4/charts.js') }}"></script>
    <script src="{{ asset('amcharts4/themes/animated.js') }}"></script>
    <script src="{{ asset('amcharts4/population.js?v=0.1') }}"></script>
    <script>
        $('#G07').on('change', function() {
            if ($(this).val() == 1) {
                // alert($(this).val() + ' Selected')
                $('#div-G09, #div-G08').css('display', 'block')
            } else {
                $('#div-G09, #div-G08').css('display', 'none')
            }
        })
    </script>

@endsection

@section('header-style')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


    <style>
        .ward-population {
            height: 450px;
        }

    </style>
@endsection

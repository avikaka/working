@extends('template.index')
@section('title','घर तथा घरपरिवार विवरण')
@section('main-content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h3>घर तथा घरपरिवार विवरण</h3>
        </div>
        <div class="col-xl-12 grid-margin stretch-card">
            <div class="card">

                <div class="card-body">
                    <div class="form-row">
                        <div class="col-4">
                            <input type="text" class="form-control" id="search" name="name" placeholder="Name">
                        </div>
                        <div class="col-2">
                            <select name="ward" id="search-ward">
                                <option>All</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>
                        <div class="col-4"><input type="text" class="form-control" id="house-no" placeholder="House No"
                                name="tole"></div>
                        <div class="col-2"><a href="/admin/cover_create" class="btn btn-primary"
                                style="float:right;">ADD</a>
                        </div>






                    </div>
                    @if(Session::has('msg'))
                    <div class="col-md-12">
                        <div class="alert alert-success">{{Session::get('msg')}}</div>
                    </div>
                    @endif

                    <div class="table-responsive">

                        <table class="table table-hover" id="my-table">
                            <thead>
                                <tr>

                                    <th>घर नं.</th>
                                    <th width="20%">टोल</th>
                                    <th>रोड</th>
                                    <th>घर धनि</th>
                                    <th>सम्पर्क नं.</th>
                                    <th>जम्म परिवार संख्या</th>
                                    <th>View</th>
                                    <th>Edit</th>
                                    <th>Delete</th>

                                </tr>
                            </thead>
                            <tbody>

                                @foreach($datas as $data)
                                <tr>
                                    <td class="font-weight-bold" id="{{ $data->household_id }}">
                                        {{ $data->household_id }}</td>
                                    <td>{{ $data->TOLE }}</td>
                                    <td>{{ $data->ROAD }}</td>
                                    <td>{{ $data->RESPONDENT }}</td>
                                    <td>{{ $data->RES_PHONE }}</td>
                                    <td>{{ $data->MEMBERS }}</td>
                                    <!-- 
                                    <td><button class="view-details btn btn-outline-primary btn-small 
                                            btn-icon edit" id="view-details"
                                            data-household-id="{{ $data->household_id }}"><i
                                                class="btn-block mdi mdi-eye mdi-24px"></i><a></td> -->
                                    <td>
                                        <a href="{{route('admin.details.edit', ['detail'=>$data->household_id])}}"
                                            class="btn btn-outline-primary btn-small btn-icon edit"><i
                                                class="btn-block mdi mdi-eye mdi-14px"></i><a>
                                    </td>
                                    <td><a href="{{ route('admin.drinking-water.edit', ['drinking_water'=>$data->id] )  }}"
                                            class="btn btn-outline-primary btn-small btn-icon edit"><i
                                                class="mdi mdi-pencil"></i><a></td>

                                    <td><a href="" onclick="deleteRecord({{ $data->id }})"
                                            class="btn btn-outline-danger btn-icon btn-small delete ml-1"><i
                                                class="mdi mdi-delete"></i></a></td>

                                    @endforeach





                            </tbody>




                        </table>{{-- end of table --}}

                    </div>
                    <div> {{$datas->links()}}</div>
                </div>
            </div>
        </div>

    </div> {{-- row end --}}

</div>
{{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script>
$(document).ready(function() {
    $('#search').on('keyup', function(e) {
        if (e.keyCode == 13) {
            var houseNo = $("#house-no").val();
            var name = $("#search").val();
            var data = {
                houseNo: houseNo,
                name: name
            }
            getName(data);
        }
    })

    $('#house-no').on('keyup', function(e) {
        if (e.keyCode == 13) {
            var houseNo = $("#house-no").val();
            var name = $("#search").val();
            var data = {
                houseNo: houseNo,
                name: name
            }
            getName(data);
        }
    })

    function getName(formData = {}) {
        $.ajax({
            url: "/admin/search",
            type: "GET",
            data: formData,
            success: function(data) {
                $('#my-table tbody').html(data.table_data);
            }
        })
    }
    $('.view-details').on('click', function(e) {
        var householdid = $(this).data('household-id');

        // console.log('Attr : ' + $(this).attr('data-household-id'));

        $.ajax({
            url: "/admin/details",
            type: "GET",
            data: {
                value: householdid
            },
            success: function(data) {


            }
        });
    });
});
</script>

<script type="text/javascript">
// function deleteRecord($id) {
//     if (confirm('Are you sure want to delete ?')) {
//         window.location.href = '{{ url('
//         admin / toilets - deletes ') }}/' + $id;


//     }

// };
</script>


@endsection

@section('header-style')
<style>
.ward-population {
    height: 450px;
}

.btn.btn-small {
    padding: 0.2rem 0.5rem;
    font-size: 0.625rem;
}
</style>

@endsection
@extends('template.index')
@section('title','घर तथा घरपरिवार विवरण')
@section('main-content')


<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h3>सम्पूर्ण विवरण</h3>
        </div>
        <div class="col-xl-12 grid-margin stretch-card">
            <div class="col-xl-12 grid-margin stretch-card">
                <div class="card">

                    <div class="card-body">
                        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
                            <a class="navbar-brand" href="#">Navbar</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault"
                                aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                                <ul class="navbar-nav mr-auto">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Link</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link disabled" href="#" tabindex="-1"
                                            aria-disabled="true">Disabled</a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="dropdown01"
                                            data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">Dropdown</a>
                                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </li>
                                </ul>
                                <form class="form-inline my-2 my-lg-0">
                                    <input class="form-control mr-sm-2" type="text" placeholder="Search"
                                        aria-label="Search">
                                    <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
                                </form>
                            </div>
                        </nav>

                        <main role="main" class="container">

                            <div class="starter-template">
                                <h4>परिचयात्मक विवरण</h4>
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>प्रदेश</td>
                                            <td>{{ $household->PROV }}</td>
                                        </tr>

                                        <tr>
                                            <td>जिल्ला</td>
                                            <td>{{ $household->DIST }}</td>
                                        </tr>
                                        <tr>
                                            <td>पालिका</td>
                                            <td>{{ $household->MUN }}</td>
                                        </tr>
                                        <tr>
                                            <td>वडा नं.</td>
                                            <td>{{ $household->WARD }}</td>
                                        </tr>
                                        <tr>
                                            <td>६. घर क्रम सङ्ख्या</td>
                                            <td>{{ $household->EA }}</td>
                                        </tr>
                                        <tr>
                                            <td>७. परिवार क्रम सङ्ख्या</td>
                                            <td>{{ $household->DIST }}</td>
                                        </tr>
                                        <tr>
                                            <td>८. गाउँ/टोल/वस्तीको नाम</td>
                                            <td>{{ $household->TOLE }}</td>
                                        </tr>
                                        <tr>
                                            <td>९. टोल विकास संस्था/समितिको नाम</td>
                                            <td>{{ $household->ROAD }}</td>
                                        </tr>
                                        <tr>
                                            <td>११. उत्तरदाताको नाम</td>
                                            <td>{{ $household->RESPONDENT }}</td>
                                        </tr>
                                        <tr>
                                            <td>१२. उत्तरदाताको फोन/मोबाइल नं.</td>
                                            <td>{{ $household->RES_PHONE }}</td>
                                        </tr>
                                        <tr>
                                            <td>१३. घरको प्रयोजन</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h4>Section A : घरधुरीसम्बन्धी विवरण</h4>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>प्रश्न</td>
                                            <td>उत्तर</td>
                                        </tr>
                                        {!! getQuestionAnswer('household','A01',$household->A01)!!}
                                        {!! getQuestionAnswer('household','A02',$household->A02)!!}
                                        {!! getQuestionAnswer('household','A03',$household->A03)!!}

                                        {!! getQuestionAnswer('household','A08',$household->A08)!!}
                                        {!! getQuestionAnswer('household','A09',$household->A09)!!}
                                        {!! getQuestionAnswer('household','A10',$household->A10)!!}
                                        {!! getQuestionAnswer('household','A11',$household->A11)!!}

                                        {!! getQuestionAnswer('household','A13',$household->A13)!!}
                                        {!! getQuestionAnswer('household','A14',$household->A14)!!}
                                        {!! getQuestionAnswer('household','A15',$household->A15)!!}
                                        {!! getQuestionAnswer('household','A16',$household->A16)!!}
                                        {!! getQuestionAnswer('household','A17',$household->A17)!!}
                                        {!! getQuestionAnswer('household','A18',$household->A18)!!}

                                        {!! getQuestionAnswer('household','A20',$household->A20)!!}
                                        {!! getQuestionAnswer('household','A21',$household->A21)!!}
                                        {!! getQuestionAnswer('household','A22',$household->A22)!!}
                                        {!! getQuestionAnswer('household','A22',$household->A22)!!}
                                        {!! getQuestionAnswer('household','A22',$household->A22)!!}
                                        {!! getQuestionAnswer('household','A24',$household->A24)!!}
                                        {!! getQuestionAnswer('household','A25_01',$household->A25_01)!!}
                                        {!! getQuestionAnswer('household','A25_02',$household->A25_02)!!}
                                        {!! getQuestionAnswer('household','A25_03',$household->A25_03)!!}
                                        {!! getQuestionAnswer('household','A25_04',$household->A25_04)!!}
                                        {!! getQuestionAnswer('household','A25_05',$household->A25_05)!!}
                                        {!! getQuestionAnswer('household','A25_06',$household->A25_06)!!}
                                        {!! getQuestionAnswer('household','A25_07',$household->A25_07)!!}
                                        {!! getQuestionAnswer('household','A25_08',$household->A25_08)!!}
                                        {!! getQuestionAnswer('household','A25_09',$household->A25_09)!!}
                                        {!! getQuestionAnswer('household','A25_10',$household->A25_10)!!}
                                        {!! getQuestionAnswer('household','A25_11',$household->A25_11)!!}
                                        {!! getQuestionAnswer('household','A25_12',$household->A25_12)!!}
                                        {!! getQuestionAnswer('household','A25_13',$household->A25_13)!!}
                                        {!! getQuestionAnswer('household','A25_14',$household->A25_14)!!}
                                        {!! getQuestionAnswer('household','A25_15',$household->A25_15)!!}
                                        {!! getQuestionAnswer('household','A25_16',$household->A25_16)!!}
                                        {!! getQuestionAnswer('household','A25_17',$household->A25_17)!!}
                                        {!! getQuestionAnswer('household','A27',$household->A27)!!}
                                        {!! getQuestionAnswer('household','A29',$household->A29)!!}
                                        {!! getQuestionAnswer('household','A31',$household->A31)!!}
                                        {!! getQuestionAnswer('household','A42',$household->A42)!!}
                                        {!! getQuestionAnswer('household','A43',$household->A43)!!}
                                        {!! getQuestionAnswer('household','A44',$household->A44)!!}
                                        {!! getQuestionAnswer('household','A48',$household->A48)!!}
                                        {!! getQuestionAnswer('household','A49',$household->A49)!!}

                                        {!! getQuestionAnswer('household','I01',$household->I01)!!}
                                        {{-- @foreach(getQuestions() as $row)
                                        <tr>
                                            <td>{{$row->field_name }}. {{ $row->question}}</td>
                                        <td>{{ ($row->field_type == 'option' || $row->field_type == 'select')? (getValueLabel($row->field_name, $data->{$row->field_name})):($data->{$row->field_name})}}
                                        </td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td>A01. {{ getQuestionText('household','A01') }}
                                            </td>
                                            <td>{{ getValueLabel('A01', $data->A01) }}</td>
                                        </tr>--}}
                                        <tr>
                                            <td>A02. तपाईँको परिवारले प्रयोग गरेको घर रहेको जग्गाको स्वामित्व कस्तो
                                                हो?
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A03. तपाईको परिवारले प्रयोग गरेको घरको प्रकार कस्तो हो ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A04. तपाईँको परिवारले प्रयोग गरेको घरको बाहिरी गारो कस्तो प्रकारको छ
                                                ?
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A05. तपाईँको परिवारले प्रयोग गरेको घरको छानो कस्तो प्रकारको छ ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A06. तपाईँको परिवार बसोबास गरेको वस्ती के कस्तो प्राकृतिक प्रकोपको
                                                जोखिममा छ ?<br>
                                                (बहुउत्तर सम्भव छ । बढीमा ५ ओटा)</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A07. तपाईँको परिवारले घर तथा कोठाहरू भाडामा दिनु भएको छ ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A08. तपाईँको परिवारले कतिवटा कोठा प्रयोग गरेको छ ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A09. तपाईँको परिवारमा भान्साकोठा र सुत्नेकोठा अलग्गै छ ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A10.1 तपाईँको परिवारको स्वामित्वमा यस बाहेक अन्य स्थानमा घर छ ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A10.2 तपाईँको परिवारको स्वामित्वमा यस बाहेक अन्य स्थानमा भएको घर
                                                संख्या
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A12. तपाईँको परिवारले पिउने पानीको मुख्य स्रोत कुन हो ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A13. पानीको मुख्य श्रोतसम्म पुग्न लाग्ने समय (घण्टा र मिनेटमा उल्लेख
                                                गर्नुहोस्)</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A14. तपाईँको परिवारलाई पानीको मुख्य श्रोतबाट प्राप्त खानेपानीको
                                                पर्याप्तता कस्तो छ ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A15. तपाईँको परिवारले पिउने पानीको गुणस्तर कस्तो छ ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A16. तपाईँको परिवारले खानेपानी शुद्धीकरण गरेर खानु हुन्छ भने कसरी
                                                शुद्धीकरण गर्नुहुन्छ ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A17. तपाईँको परिवारमा खाना पकाउन अक्सर (मुख्य रूपमा) कुन इन्धन
                                                प्रयोग
                                                हुन्छ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A18. तपाईँको परिवारमा खाना पकाउन प्रयोग हुने सहायक इन्धनहरू उल्लेख
                                                गर्नुहोस् - (बहुउत्तर सम्भव छ)</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A19. तपाईँको परिवारमा बत्ती बाल्न प्रयोग हुने उर्जाको मुख्य श्रोत
                                                कुन
                                                इन्धन प्रयोग गर्नु हुन्छ ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A20. बत्ती बाल्न मुख्य ऊर्जा स्रोत बाहेक अन्य कुन कुन ऊर्जा स्रोत
                                                प्रयोग
                                                गर्नु हुन्छ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A21. तपाईँको परिवारले प्रयोग गर्ने चर्पी कस्तो छ ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A22. तपाईँको परिवारले घरबाट निस्कने ठोस ‌फोहोरमैला अक्सर कहाँ
                                                व्यवस्थापन
                                                गर्नुहुन्छ ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A23. तपाईँको घरबाट निस्कने फोहोर पानी कहाँ व्यवस्थापन गर्नु हुन्छ ?
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A24. तपाईँको घरसम्म पुग्ने बाटोको अवस्था कस्तो छ ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A25.1 तपाईँको परिवार बसोबास गरेको घरबाट सार्वजनिक यातायातको
                                                पहुँचसम्म
                                                पुग्न कति समय लाग्छ ? (मिनेटमा उल्लेख गर्नुहोस्)</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A25.2 तपाईँको परिवार बसोबास गरेको घरबाट नजिकको बजार केन्द्रसम्म
                                                पुग्न
                                                कति समय लाग्छ ? (मिनेटमा उल्लेख गर्नुहोस्)</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A26. तपाईँको घरबाट बाह्रै महिना सञ्चालन योग्य सडक कति टाढा छ ?</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>A27. तपाईँको परिवारमा तल उल्लेखित कुन कुन साधन/सुविधा उपलब्ध छन् ?
                                                (उपलब्ध भएका साधन/सुविधाहरूमा टिक लगाउनुहोस् )</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-white">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>क्र.सं.</th>
                                                            <th>साधन/सुविधा</th>
                                                            <th>छ/छैन</th>
                                                        </tr>
                                                    </thead>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>रेडियो</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>टेलिभिजन</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>केबल/डिस च्यानल</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>कम्प्युटर/ल्यापटप/ट्याबलेट</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>ईन्टरनेट</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>ल्याण्डलाइन फोन</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>मोबाइल फोन</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>मोटरसाइकल/स्कूटर</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>कार/जीप/भ्यान (निजी प्रयोगको लागि)</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>ढुवानी साधन (बस, ट्रक, ट्रिपर, भ्यान, जीप ...)</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>अन्य सवारी (टाँगा, ठेला, गाडा, ...)</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>ट्याम्पु / ई-रिक्सा</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>साइकल / रिक्सा</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>पावर ट्रिलर / पम्पसेट</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>फ्रिज</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>एयर कन्डिसनर / कुलर</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>वासिङ मेशिन</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>गिजर/सोलार (नुहाउन तातो पानीको लागि)</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>पंखा / हिटर</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>

                                    </tbody>
                                </table>
                                <div class="starter-template">
                                    <!-- <h4>Section B : - कृषिसम्बन्धी विवरण</h2> -->
                                    <table class="table table-sm table-hover table-bordered">
                                        <tbody>
                                            <tr>
                                                <td>B01. कृषि प्रयोजनको लागि यस परिवारले यस गा.पा./न.पा.भित्र चलन
                                                    गरेको
                                                    वा अरूलाई चलन गर्न दिएको जग्गा/जमिन छ ?</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table table-sm table-hover table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td rowspan="2">क्रसं.</td>
                                                                <td rowspan="2">विवरण</td>
                                                                <td rowspan="2">छ/छैन</td>
                                                                <td rowspan="2">जग्गा / जमिनको एकाइ</td>
                                                                <td colspan="3">सिंचित क्षेत्रफल</td>
                                                                <td rowspan="2">सिँचाइको प्रकार</td>
                                                                <td rowspan="2">सिँचित अवधि</td>
                                                                <td colspan="3">असिंचित क्षेत्रफल</td>
                                                            </tr>
                                                            <tr>
                                                                <td>बिघा</td>
                                                                <td>कट्ठा</td>
                                                                <td>धुर</td>
                                                                <td>बिघा</td>
                                                                <td>कट्ठा</td>
                                                                <td>धुर</td>
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>आफ्नो जग्गा आफैँले चलन गरेको</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td>अरूको जग्गा आफूले चलन गरेको</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>3</td>
                                                                <td>आफ्नो जग्गा अरूले चलन गरेको</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>B03.1 तपाईँको परिवारको स्वामित्वमा यस गा.पा./न.पा. बाहेक अन्य
                                                    स्थानमा जग्गा/जमिन छ ?</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>B03.2 अन्य स्थानमा रहेको जग्गा/जमिनको संख्या </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table table-sm table-hover table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td rowspan="2">क्रसं.</td>
                                                                <td colspan="2">जग्गा / जमिन रहेको स्थान</td>
                                                                <td rowspan="2">जग्गा / जमिनको एकाइ</td>
                                                                <td colspan="3">सिंचित क्षेत्रफल</td>
                                                            </tr>
                                                            <tr>
                                                                <td>जिल्ला</td>
                                                                <td>गा.पा./ न.पा.</td>
                                                                <td>बिघा</td>
                                                                <td>कट्ठा</td>
                                                                <td>धुर</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>B05. तपाईँको परिवार कृषि पेशामा संलग्न छ ?</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="table table-sm table-hover table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td>सि.नं.</td>
                                                                <td>बालीको नाम</td>
                                                                <td>खेती गरे नगरेको</td>
                                                                <td>उत्पादन इकाई</td>
                                                                <td>उत्पादन परिमाण</td>
                                                                <td>बिक्री इकाई</td>
                                                                <td>बिक्री परिमाण</td>
                                                                <td>बिक्री रकम</td>
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>धान</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td>मकै</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>B07. तपाईँको परिवारले कृषि प्रयोजनको लागि पशुपंक्षी पाल्नु भएको
                                                    छ ?
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="table table-sm table-hover table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td>सि.नं.</td>
                                                                <td>विवरण</td>
                                                                <td>हालको संख्या</td>
                                                                <td>गत वर्ष विक्री संख्या</td>
                                                                <td>बिक्रीबाट आम्दानी</td>
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>गाई/गोरु</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td>भैँसी/राँगा</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="table table-sm table-hover table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td>सि.नं.</td>
                                                                <td>पशुपंक्षी जन्य उत्पादन</td>
                                                                <td>उत्पादनको अवस्था</td>
                                                                <td>उत्पादन इकाई</td>
                                                                <td>उत्पादन परिमाण</td>
                                                                <td>बिक्री परिमाण</td>
                                                                <td>बिक्रीबाट आम्दानी रु.</td>
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>दूध</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td>दूधजन्य वस्तु (घ्यू, चिज, मखन आदि)</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>B10. आफ्नो कृषि उत्पादनले तपाईको परिवारलाई कति महिना खान पुग्छ ?
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="starter-template">
                                    <h4>Section B : मृत्युसम्बन्धी विवरण</h4>
                                    <p>विगत ५ वर्षमा यस परिवारमा कसैको मृत्यू भएको थियो ?</p>
                                    <table class="table table-sm table-hover table-bordered table-responsive">
                                        <tbody>
                                            <tr>
                                                <td>क्रसं.</td>
                                                <td>मृत्यू भएको व्यक्तिको पूरा नाम</td>
                                                <td>परिवारमूली सँगको नाता</td>
                                                <td>लिङ्ग</td>
                                                <td>मृत्यू हुँदाको उमेर (वर्षमा)</td>
                                                <td>मृत्यूको कारण</td>
                                                <td>मृतक 15-49 वर्षको महिला भए मृत्यू हुँदाको अवस्था</td>
                                                <td>मृत्यू भएको साल</td>
                                                <td>मृत्यू दर्ताको अवस्था</td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>C03</td>
                                                <td>C04</td>
                                                <td>C05</td>
                                                <td>C06</td>
                                                <td>C07</td>
                                                <td>C08</td>
                                                <td>C09</td>
                                                <td>C10</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <h4>Section C : अनुपस्थित व्यक्ति तथा विप्रेषण</h4>
                                <p>&nbsp;</p>
                                <p>यस परिवारबाट कुनै व्यक्ति घर छाडेर बाहिर जानु भएको छ? </p>
                                <p>छ भने संख्या</p>
                                <table class="table table-sm table-hover table-bordered table-responsive">
                                    <tbody>
                                        <tr>
                                            <td>क्रसं.</td>
                                            <td>अनुपस्थित व्यक्तिको पूरा नाम</td>
                                            <td>लिङ्ग</td>
                                            <td>अनुपस्थित हुँदाको उमेर (वर्षमा)</td>
                                            <td>अनुपस्थित हुँदा उत्तीर्ण गरेको तह</td>
                                            <td>अनुपस्थित भएको समय (वर्षमा)</td>
                                            <td>अनुपस्थित हुनुको मुख्य कारण</td>
                                            <td>हाल बसिरहेको स्थान</td>
                                            <td>&nbsp;</td>
                                            <td>विगत १२ महिनामा नगर र जिन्सी गरी पठाएको रकम</td>
                                        </tr>
                                        @foreach($absentees as $absent)
                                        <tr>
                                            <td class="sticky-col first-col">1</td>
                                            <td class="sticky-col second-col">{{ $absent->C03}}</td>
                                            <td class="sticky-col second-col">{{ $absent->C04}}
                                            </td>
                                            <td class="sticky-col second-col">{{ $absent->C06}}</td>
                                            <td class="sticky-col second-col">{{ $absent->C07}}</td>
                                            <td class="sticky-col second-col">{{ $absent->C08}}</td>
                                            <td class="sticky-col second-col">{{ $absent->C09}}</td>
                                            <td class="sticky-col second-col">{{ $absent->C10}}</td>
                                            <td class="sticky-col second-col">{{ $absent->C11}}</td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <p>D11 प्राप्त विप्रेषण खर्च भएकोक्षेत्र- </p>
                                <table class="table table-sm table-hover table-bordered">
                                    <tbody>
                                        <tr>
                                            <th> विप्रेषण खर्च भएको पहिलो क्षेत्र</th>
                                            <th>विप्रेषण खर्च भएको दोस्रो क्षेत्र</th>
                                            <th>विप्रेषण खर्च भएको तेस्रो क्षेत्र</th>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p>&nbsp;</p>
                                <h4>Section E : सामान्य व्यक्तिगत विवरण</h4>
                                <table class="table table-sm table-hover table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <th class="sticky-col first-col">&nbsp;</th>
                                            <th class="sticky-col second-col">&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th colspan="3">१० वर्ष वा सोभन्दा बढी उमेरका लागि</th>
                                            <th colspan="3">५ वर्ष वा सोभन्दा बढी उमेरका लागि</th>
                                            <th colspan="4">५-२४ वर्षसम्मको लागि मात्र</th>
                                            <th colspan="5">१० वर्ष वा सोभन्दा बढी उमेरका लागि</th>
                                            <th colspan="5">जन्मस्थान र बसाइसराइ सम्बन्धी</th>
                                            <th colspan="3">अपाङ्गता सम्बन्धी</th>
                                            <th colspan="2">५ वर्ष भन्दा कम उमेरका लागि</th>
                                            <th colspan="3">दीर्घरोग सम्बन्धी</th>
                                            <th colspan="7">हालसम्म कति जीवित सन्तानलाई जन्म दिनु भयो?</th>
                                            <th colspan="4">विगत १२ महिनामा</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                        <tr>
                                            <th rowspan="2" class="sticky-col first-col">क्रसं.</th>
                                            <th rowspan="2" class="sticky-col second-col">पूरा नाम</th>
                                            <th rowspan="2">लिङ</th>
                                            <th rowspan="2">उमेर</th>
                                            <th rowspan="2">परिवारमूली सँगको नाता</th>
                                            <th rowspan="2">राष्ट्रियता</th>
                                            <th rowspan="2">जात/जाति</th>
                                            <th rowspan="2">मातृभाषा</th>
                                            <th rowspan="2">धर्म</th>
                                            <th rowspan="2">वैवाहिक स्थिति</th>
                                            <th rowspan="2">पहिलो पटक विवाह गर्दाको उमेर</th>
                                            <th rowspan="2">१ वर्षमा आय आर्जन हुने कार्यमा संलग्न महिना</th>
                                            <th rowspan="2">पढ्न लेख्न जानेको</th>
                                            <th rowspan="2">उत्तीर्ण गरेको तह</th>
                                            <th rowspan="2">उत्तीर्ण गरको मुख्य विषय</th>
                                            <th rowspan="2">हाल स्कूल / कलेज गइरहनु भएको छ</th>
                                            <th rowspan="2">कहिल्यै स्कूल / कलेज जानु भएको थियो</th>
                                            <th rowspan="2">हाल अध्ययन गरिरहेको स्कूल / कलेज कस्तो किसिमको हो</th>
                                            <th rowspan="2">हाल स्कूल / कलेज नजानुको कारण</th>
                                            <th rowspan="2">कुनै औपचारिक वा अनौपचारिक तालिम लिएको छ</th>
                                            <th rowspan="2">तालिमको मुख्य विषय</th>
                                            <th rowspan="2">तालिमको अवधि (महिनामा)</th>
                                            <th rowspan="2">मुख्य शिप वा क्षमता</th>
                                            <th rowspan="2">इन्टरनेटमा पहुँच</th>
                                            <th rowspan="2">जन्मस्थान</th>
                                            <th rowspan="2">जन्म जिल्ला</th>
                                            <th rowspan="2">जन्म देश</th>
                                            <th rowspan="2">यहाँ बसोबास गरेको वर्ष</th>
                                            <th rowspan="2">यहाँ बसोबास गर्नुको कारण</th>
                                            <th rowspan="2">कुनै किसिमको अपाङ्गता</th>
                                            <th rowspan="2">अपाङ्ग हुनुको कारण</th>
                                            <th rowspan="2">उपलब्ध अपाङ्गता सहायता सामग्री</th>
                                            <th rowspan="2">जन्म दर्ता</th>
                                            <th rowspan="2">हालसम्म दिनुपर्ने सबै खोप दिए नदिएको</th>
                                            <th rowspan="2">दीर्घरोग छ?</th>
                                            <th rowspan="2">दीर्घरोगको किसिम</th>
                                            <th rowspan="2">रोगबाट पिडित वर्ष</th>
                                            <th rowspan="2">जीवित शिशुलाई जन्म दिएको ?</th>
                                            <th colspan="2">हालसम्म जीवित सन्तान</th>
                                            <th colspan="2">जीवित जन्मिएर मरेका सन्तान</th>
                                            <th colspan="2">हालसम्म जन्म दिएका जम्मा सन्तान</th>
                                            <th rowspan="2">जीवित शिशुलाई जन्म दिएको ?</th>
                                            <th colspan="2">कति जना जीवित सन्तान जन्म दिनु भयो?</th>
                                            <th rowspan="2">सुत्केरी गराएको स्थान</th>
                                            <th rowspan="2">पहिलो सन्तान जन्मदिँदाको तपाईँको उमेर? (वर्षमा)</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                        <tr>
                                            <th>छोरा</th>
                                            <th>छोरी</th>
                                            <th>छोरा</th>
                                            <th>छोरी</th>
                                            <th>छोरा</th>
                                            <th>छोरी</th>
                                            <th>छोरा</th>
                                            <th>छोरी</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($persons as $person)
                                        <tr>
                                            <td class="sticky-col first-col">{{$person->LINE}}</td>
                                            <td class="sticky-col second-col">
                                                {{ $person->NAME}}
                                            </td>
                                            <td>
                                                {{ getValueLabel('SEX',$person->SEX)}}
                                            </td>
                                            <td>{{ $person->AGE}}</td>
                                            <td>
                                                {{ getValueLabel('RELATIONSHIP',$person->RELATIONSHIP)}}
                                            </td>
                                            <td>
                                                {{ getValueLabel('NATIONALITY',$person->NATIONALITY)}}
                                            </td>
                                            <td>
                                                {{ $person->MOTHER_STATUS}}
                                            </td>
                                            <td>
                                                {{ getValueLabel('CASTE',$person->CASTE)}}
                                            </td>
                                            <td>
                                                {{ getValueLabel('RELIGION',$person->RELIGION)}}
                                            </td>
                                            <td>
                                                {{ getValueLabel('MARITAL_STATUS',$person->MARITAL_STATUS)}}
                                            </td>
                                            <td>{{ $person->AGE_AT_FIRST_MARRIAGE}}</td>
                                            <td>
                                                {{ getValueLabel('LITERATE',$person->LITERATE)}}
                                            </td>
                                            <td>
                                                {{ getValueLabel('EDU_LEVEL',$person->EDU_LEVEL)}}
                                            </td>
                                            <td>{{ $person->ATTENDING_SCHOOL}}</td>
                                            <td>{{ $person->EVER_ATTEND}}</td>
                                            <td>
                                                {{ getValueLabel('SCHOOL_TYPE',$person->SCHOOL_TYPE)}}</td>
                                            <td>{{ $person->YEARLY_FEE}}</td>






                                        </tr>
                                        @endforeach
                                    </tbody>

                                </table>

                                <p>तपाईको टोल, छिमेक तथा समाजको लागि गाउँपालिका/नगरपालिकाले के कुरामा सहयोग, काम गरिदिए
                                    हुन्थ्यो जस्तो लाग्छ अर्थात् गा.पा./न.पा. सँग तपाईको अपेक्षा तथा गा.पा./न.पा.लाई
                                    तपाईको
                                    सुझाव के छ ? <em>(बहुउत्तर सम्भव छ-बढीमा 5 ओटा)</em></p>
                                <p>&nbsp;</p>

                                <h4>Section F : बसाई सराइ र आर्थिक क्रियाकलाप </h4>
                                <table class="table table-sm table-hover table-bordered table-responsive">
                                    <thead>

                                        <tr>
                                            <th rowspan="2" class="sticky-col first-col">क्रसं.</th>
                                            <th rowspan="2" class="sticky-col second-col">पुरा नाम</th>
                                            <th>जन्मस्थान</th>
                                            <th rowspan="2">जन्मस्थान जिल्ला </th>
                                            <th rowspan="2">जन्मस्थान तत्कालिन गाविस/गापा वा न.पा.</th>
                                            <th rowspan="2">जन्मस्थान देश </th>
                                            <th rowspan="2">बसोबास गरेको कति बर्ष भयो </th>
                                            <th rowspan="2">बसोबास गर्नुको मुख्य कारण</th>
                                            <th rowspan="2">५ वर्षअघि वसोवास कहाँ थियो</th>
                                            <th rowspan="2">५ वर्षअघि बसोवास गरेको जिल्ला </th>
                                            <th rowspan="2">५ वर्षअघि वसोवास गरेको स्थान तत्कालिन गाविस/गापा वा न.पा.
                                            </th>
                                            <th rowspan="2">५ वर्ष अघि बसोवास गरेको देश</th>

                                        </tr>

                                    </thead>
                                    <tbody>
                                        @foreach($persons as $person)
                                        <tr>
                                            <td class="sticky-col first-col">{{$person->LINE}}</td>
                                            <td class="sticky-col second-col">{{ $person->NAME}}</td>
                                            <td>{{ getValueLabel('F01',$person->F01)}}
                                            </td>
                                            <td>{{ $person->F02}}</td>
                                            <td>{{ $person->F03}}</td>
                                            <td>{{ $person->F04}}</td>
                                            <td>{{ $person->F05}}</td>
                                            <td>{{ $person->F06}}</td>
                                            <td>{{ getValueLabel('F07',$person->F07)}}
                                            </td>
                                            <td>{{ $person->F08}}</td>
                                            <td>{{ $person->F09}}</td>
                                            <td>{{ $person->F10}}</td>

                                        </tr>
                                        @endforeach
                                    </tbody>

                                </table>
                                <p>&nbsp;</p>

                                <h4>Section G : सामाजिक सुरक्षा र स्वास्थ्य </h4>
                                <table class="table table-sm table-hover table-bordered table-responsive">
                                    <thead>

                                        <tr>
                                            <th class="sticky-col first-col">क्रसं.</th>
                                            <th class="sticky-col second-col">पुरा नाम</th>
                                            <th>कुनै किसिमको शारिरीक वा मानसिक
                                                अपाङ्गता</th>
                                            <th>कति वर्ष अगाडि देखि अपाङ्ग हुनु भएको </th>
                                            <th>कस्तो किसिमको परिचयपत्र पाउनु भएको</th>
                                            <th>विगत १२ महिनामा कुन प्रकारको सामाजिक सुरक्षा भत्ता पाउनु
                                                भएको </th>
                                            <th>जन्मदर्ता गर्नु भएको </th>
                                            <th>सवै खोप दिनुभएको</th>
                                            <th>कुनै दीर्घरोगले पिडित हुनुभएको</th>
                                            <th>मुख्यतया कुन दीर्घ रोगले सताएको </th>
                                            <th>कति वर्ष अगाडिदेखि यो रोगबाट पिडित हुनु भएको</th>
                                            <th>स्वास्थ्य सम्बन्धी समस्याहरू</th>
                                            <th>मुख्यतया कस्तो प्रकारको बिरामी</th>
                                            <th>गत ३० दिनमा विरामी भई वा चोट पटक लागि कसै सँग जंचाउनुभयो</th>
                                            <th>पछिल्लो पटक जाँच गराउन कहाँ जानुभयो</th>
                                            <th>पछिल्लो पटक कसलाई जँचाउनु भयो</th>
                                            <th>कसैसँग पनि नजँचाउनुको मुख्य कारण</th>


                                        </tr>

                                    </thead>
                                    <tbody>
                                        @foreach($persons as $person)
                                        <tr>
                                            <td class="sticky-col first-col">{{$person->LINE}}</td>
                                            <td class="sticky-col second-col">{{ $person->NAME}}</td>
                                            <td>{{ getValueLabel('G01',$person->G01)}}
                                            </td>
                                            <td>{{ $person->G02}}</td>
                                            <td>{{ $person->DISABILITY_ID}}</td>
                                            <td>{{ $person->SOCIAL_SECURITY}}</td>
                                            <td>{{ $person->BIRTH_CERTIFICATE}}</td>
                                            <td>{{ $person->VACINATION}}</td>
                                            <td>{{ getValueLabel('G07',$person->G07)}}
                                            </td>
                                            <td>{{ $person->G08}}</td>
                                            <td>{{ $person->G09}}</td>
                                            <td>{{ $person->G10}}</td>
                                            <td>{{ $person->G11}}</td>
                                            <td>{{ $person->G12}}</td>
                                            <td>{{ $person->G13}}</td>
                                            <td>{{ $person->G14}}</td>
                                            <td>{{ $person->G15}}</td>


                                        </tr>
                                        @endforeach
                                    </tbody>

                                </table>

                                <p>&nbsp;</p>

                                <h4>Section H : प्रजनन तथा प्रजनन स्वास्थ्य </h4>
                                <table class="table table-sm table-hover table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <th class="sticky-col first-col">&nbsp;</th>
                                            <th class="sticky-col second-col">&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th colspan="4">हालसम्म कतिजना जिवित बच्चलाई जन्मदिनु भयो</th>
                                            <th colspan="3">बितेको १२ महिनामा </th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>


                                        </tr>
                                        <tr>
                                            <th class="sticky-col first-col">क्रसं.</th>
                                            <th class="sticky-col second-col">पुरा नाम</th>
                                            <th>हालसम्म जिवित शिशुलाई जन्म दिनु भएका
                                            </th>
                                            <th>छोरा</th>
                                            <th>छोरी</th>
                                            <th>छोरा</th>
                                            <th>छोरी</th>
                                            <th>छोरा</th>
                                            <th>छोरी</th>
                                            <th>छोरा</th>
                                            <th>छोरी</th>
                                            <th>जिवित बच्चा जन्माउनु भएको</th>
                                            <th>कति जना जिवित बच्चा जन्माउनु भयो</th>
                                            <th>जन्माएको बच्चाको जन्मदा तौल कति थियो</th>
                                            <th>कहाँ सुत्केरी गराउनु भयो</th>
                                            <th>गर्भवती हुँदा प्रोटोकल अनुसार ४ पटक गर्भ जाँच गराउनु भयो</th>
                                            <th>पहिलो बच्चालाई जन्मदिंदा कति वर्षको हुनुहुन्थ्यो</th>

                                        </tr>

                                    </thead>
                                    <tbody>
                                        @foreach($persons as $person)

                                        <tr>
                                            <td class="sticky-col first-col">{{$person->LINE}}</td>
                                            <td class="sticky-col second-col">{{ $person->NAME}}</td>
                                            <td>{{ $person->H01}}</td>
                                            <td>{{ $person->H02A}}</td>
                                            <td>{{ $person->H02B}}</td>
                                            <td>{{ $person->H03A}}</td>
                                            <td>{{ $person->H03A}}</td>
                                            <td>{{ $person->H04A}}</td>
                                            <td>{{ $person->H04A}}</td>
                                            <td>{{ $person->H05A}}</td>
                                            <td>{{ $person->H05B}}</td>
                                            <td>{{ $person->H06}}</td>
                                            <td>{{ $person->H07A}}</td>
                                            <td>{{ $person->H08}}</td>
                                            <td>{{ $person->H09}}</td>
                                            <td>{{ $person->H10}}</td>
                                            <td>{{ $person->H11}}</td>

                                        </tr>
                                        @endforeach
                                    </tbody>

                                </table>
                            </div>

                        </main><!-- /.container -->

                    </div>
                </div>
            </div>
        </div>

    </div> {{-- row end --}}

</div>
{{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
</script>
<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="bootstrap-4.5.3/js/bootstrap.bundle.min.js"></script>

<script src="{{asset('amcharts4/core.js')}}"></script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


@endsection

@section('header-style')
<style type="text/css">
.sticky-col {
    position: -webkit-sticky;
    position: sticky;
    background-color: white;
    /*			border: 1px solid #A73A3C !important;*/
}

.first-col {
    width: 40px;
    min-width: 40px;
    max-width: 40px;
    left: 0px;
}

.second-col {
    width: 200px;
    min-width: 200px;
    max-width: 200px;
    left: 40px;
}

.sticky-col1 {
    position: -webkit-sticky;
    position: sticky;
    background-color: white;
}
</style>

@endsection
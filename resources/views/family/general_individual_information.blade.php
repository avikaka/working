@extends('template.index')
@section('title','परिचयात्मक विवरण')
@section('main-content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h3>परिचयात्मक विवरण</h3>
        </div>
        <div class="col grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                    @endif
                    <form class="form-sample" method="POST"
                        action="{{ isset($data)? '/admin/cover/$id': "/admin/cover"}}">
                        @if(isset($data))
                        @method('PUT')
                        @endif
                        @csrf

                        <div class="form-row">
                            <div class="form-group col-md-2" hidden>
                                <label for="province">प्रदेश:</label>
                                <input type="text" value="3" name="province" class="form-control">
                            </div>

                            <div class="form-group col-md-3" hidden>
                                <label for="district">जिल्ला:</label>
                                <input type="text" value="23" name="district" class="form-control">
                            </div>

                            <div class="form-group col-md-4" hidden>
                                <label for="municipality">स्थानीय तहको नाम:</label>
                                <input type="text" value="6" name="municipality" class="form-control">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="ward">वडा नं.:</label>
                                {!! getWardCombo(0) !!}
                            </div>
                            <div class="form-group col-md-6">
                                {!! inputField('members','पारिवारिक सं. :',
                                'पारिवारिक क्र.सं.',isset($data)?$data->members :
                                old('members')) !!}
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6" hidden>
                                {!! inputField('householdId','घर क्र.सं (नम्बर) :',
                                'घर क्र.सं (नम्बर)',isset($data)?$data->householdId :
                                old('householdId')) !!}
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                {!! inputField('tole','गाउँ/टोल/बस्तीको नाम :',
                                'गाउँ/टोल/बस्तीको नाम',isset($data)?$data->tole :
                                old('tole')) !!}
                            </div>
                            <div class="form-group col-md-4">
                                {!! inputField('','आवद्ध टोल विकास संस्थाको नाम :',
                                'आवद्ध टोल विकास संस्थाको नाम',isset($data)?$data->tlo :
                                old('tlo')) !!}
                            </div>
                            <div class="form-group col-md-4">
                                {!! inputField('road','सडकको नाम :',
                                'सडकको नाम ',isset($data)?$data->road :
                                old('road')) !!}
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-3">
                                {!! inputField('latitude','Latitude :',
                                'Latitude',isset($data)?$data->latitude :
                                old('latitude')) !!}
                            </div>
                            <div class="form-group col-md-3">
                                {!! inputField('longitude','Longitude :',
                                'Longitude',isset($data)?$data->longitude :
                                old('longitude')) !!}
                            </div>
                            <div class="form-group col-md-3">
                                {!! inputField('accuracy','Accuracy Level :',
                                'Accuracy Level ',isset($data)?$data->accuracy :
                                old('accuracy')) !!}
                            </div>
                            <div class="form-group col-md-3">
                                {!! inputField('satellites','Satellites :',
                                'Satellites ',isset($data)?$data->satellites :
                                old('satellites')) !!}
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                {!! inputField('respondent','उत्तरदाताको पूरा नाम, थर :',
                                'उत्तरदाताको पूरा नाम, थर',isset($data)?$data->respondent :
                                old('respondent')) !!}
                            </div>
                            <div class="form-group col-md-4">
                                <label for="res_sex">उत्तरदाताको लिङ्ग के हो ?</label>
                                <select name="res_sex" class="form-control">
                                    <option selected disabled>लिङ्ग छान्नुहोस</option>
                                    <option value="1">पुरुस</option>
                                    <option value="2">महिला</option>
                                    <option value="3">अन्य</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                {!! inputField('res_phone','उत्तरदाताको फोन नं. :',
                                'उत्तरदाताको फोन नं. ',isset($data)?$data->res_phone :
                                old('res_phone')) !!}
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="use_of_house">यस घरलाई तपाई (वा तपाईको परिवार)ले के को रुपमा प्रयोग गर्नु
                                    भएको छ
                                    ?</label>
                                <select name="use_of_household" class="form-control">
                                    <option selected disabled>उत्तर छान्नुहोस</option>
                                    <option value="1">आवास (बसोवास) को रुपमा मात्र</option>
                                    <option value="2">आवास (बसोवास) र व्यापारिक÷कार्यालय प्रयोजनको लागि प्रयोग गरेको
                                    </option>
                                    <option value="3">व्यापारिक÷कार्यालय प्रयोजनको लागि मात्र प्रयोग गरेका</option>
                                </select>
                            </div>
                        </div>

                        <div class="text-center">
                            <button class="btn btn-primary align-middle" type="submit">Submit</button>
                            <a href="/admin/bank" class="btn btn-secondary">Cancel</a>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div> {{-- row end --}}

</div>
</div> {{-- row end --}}

</div>
{{-- content-wrapper ends --}}
@endsection
{{-- end main-content section --}}
@section('footer-script')
<script src=" {{asset('amcharts4/core.js')}}">
</script>
<script src="{{asset('amcharts4/charts.js')}}"></script>
<script src="{{asset('amcharts4/themes/animated.js')}}"></script>
<script src="{{asset('amcharts4/population.js?v=0.1')}}"></script>
@endsection

@section('header-style')
<style>
    .ward-population {
        height: 450px;
    }

</style>
@endsection

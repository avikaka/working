@extends('template.admin')
@section('title', 'सामुदायिक वनसम्बन्धी विवरण')
@section('page-title')
सामुदायिक वनसम्बन्धी विवरण थप गर्नुहोस
@endsection
@section('content')

<div class="tab-pane" id="tab_2">
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>सामुदायिक वनसम्बन्धी विवरण थप
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form method="POST"
                action="{{ isset($data->id) ? Route('admin.community-forest.update', ['community_forest' => $data->id]) : Route('admin.community-forest.store') }}"
                enctype="multipart/form-data" class="horizontal-form">
                @isset($data->id)
                {{ method_field('PUT') }}
                @endisset
                @csrf
                <div class="form-body">
                    {{-- <h3 class="form-section">सरकारी तथा सामुदायिक भवन</h3> --}}
                    {{-- {{dd($errors->first('occupied_area'))}} --}}
                    <div class="row">
                        <div class="col-md-12">
                            {!! inputField('name', 'सामुदायिक वनको नाम', 'सामुदायिक वनको नाम', isset($data) ?
                            $data->name:old('name'))!!}
                            @if ($errors->has('name'))
                            <span class="help-block">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <!--/span-->
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            {!! inputField('occupied_area', 'ओगटेको क्षेत्रफल', 'ओगटेको क्षेत्रफल', isset($data)
                            ? $data->occupied_area : old('occupied_area')) !!}
                            @if ($errors->has('occupied_area'))
                            <span class="help-block">{{ $errors->first('occupied_area') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6">
                            {!! inputField('forest_area', 'जंगलको क्षेत्रफल हेक्टरमा', 'जंगलको क्षेत्रफल हेक्टरमा',
                            isset($data) ? $data->forest_area : old('forest_area')) !!}
                            @if ($errors->has('forest_area'))
                            <span class="help-block">{{ $errors->first('forest_area') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            {!! inputField('member_household', 'सदस्य घरधुरी', 'सदस्य घरधुरी', isset($data) ?
                            $data->member_household : old('member_household')) !!}
                            @if ($errors->has('member_household'))
                            <span class="help-block">{{ $errors->first('member_household') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">वडा नं.</label>
                                {!! getWardCombo(0) !!}
                                @if ($errors->has('ward'))
                                <span class="help-block">{{ $errors->first('ward') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {!! latField('latitude', 'अक्षांश', 'अक्षांश', isset($data) ? $data->latitude :
                            old('latitude')) !!}
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            {!! latField('longitude', 'देशान्तर', 'देशान्तर', isset($data) ? $data->longitude :
                            old('longitude')) !!}
                        </div>
                        <!--/span-->
                    </div>

                    <!--/span-->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('remarks') ? ' has-error' : '' }} ">
                                <label class="control-label "> कैफियत
                                </label>
                                <div class="col-md-12">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <textarea id="editor1"
                                            name="remarks">{{ old('remarks', $data->remarks) }} </textarea>
                                        @if ($errors->has('remarks'))
                                        <span class="text-danger font-weight-danger">
                                            {{ $errors->first('remarks') }}
                                        </span>

                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <br>

                <div>
                    <button type="button" class=" btn default" href="{{ url()->previous() }}">Cancel</button>
                    <button type="submit" class="btn blue">
                        <i class="fa fa-check"></i> Save</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js"
    referrerpolicy="origin"></script>
<script>
    tinymce.init({
        selector: 'textarea'
    })

</script>


@endsection

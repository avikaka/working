<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="Digital Profile for local goverment is proudly power by Deltrox IT Solutins.">
    <meta name="keywords"
        content="digital profile, municipal profile, local government profile, comprehensive database, digital basemap">
    <meta name="author" content="pixelstrap">
    {{-- {{ dd($logoname) }} --}}
    <link rel="icon" href="{{ url('uploads/logo/' . $logoimage) }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ url('uploads/logo/' . $logoimage) }}" type="image/x-icon">

    <title>{{ $logoname }}</title>
    <!-- Google font-->
    <link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,700,300" rel="stylesheet" type="text/css">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/icofont.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/icofont/icofont.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/icofont.css') }}">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>



    <!-- ico-font-->

    <!-- Themify icon-->
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/themify.css')}}"> --}}
    <!-- Flag icon-->
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/flag-icon.css')}}"> --}}
    <!-- Feather icon-->
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/feather-icon.css')}}"> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}"> --}}
    <!-- Plugins css start-->
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/chartist.css')}}"> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/date-picker.css')}}"> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/prism.css')}}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/material-design-icon.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pe7-icon.css') }}">
    <!-- Plugins css Ends-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/summernote.css') }}">
    <link href="{{ asset('bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css">

    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css?v=1.12') }}">
    <link id="color" rel="stylesheet" href="{{ asset('css/color-1.css" media="screen') }}">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('leaflet/leaflet-1.6.0.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pe7-icon.css') }}">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link id="color" rel="stylesheet" href="{{ asset('css/color-1.css" media="screen') }}">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('leaflet/leaflet-1.6.0.css') }}">
    @yield('header-style')

    <style type="text/css">
        #map {
            min-height: 500px;
        }

        .wd-label {
            font-size: 15px;
            font-weight: bold;
            padding: 0px;
            background: none;
            border: none;
            color: #ec09a0;
            /* text-shadow: 0px 0px 10px black; */
            box-shadow: none;
        }

    </style>
    <script type="text/javascript">
        BASE_URL = "<?php echo url(''); ?>";
    </script>
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    @yield('header-script')
</head>

<body>
    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        @include('template.top-nav')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper mt-5">

            <!-- partial -->
            <div class="main-panel formHeight">
                @include('template.content')
                <!-- content-wrapper ends -->

                <!-- partial:partials/_footer.html -->
                @include('template.footer')
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    <!-- latest jquery-->

    <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
    <!-- Bootstrap js-->
    <script src="{{ asset('js/bootstrap/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap/bootstrap.js') }}"></script>
    <!-- feather icon js-->
    <script src="{{ asset('js/icons/feather-icon/feather.min.js') }}"></script>
    <script src="{{ asset('js/icons/feather-icon/feather-icon.js') }}"></script>
    <!-- Sidebar jquery-->
    <script src="{{ asset('js/sidebar-menu.js') }}"></script>
    <script src="{{ asset('js/config.js') }}"></script>
    <!-- Plugins JS start-->
    <script src="{{ asset('js/typeahead/handlebars.js') }}"></script>
    <script src="{{ asset('js/typeahead/typeahead.bundle.js') }}"></script>
    <script src="{{ asset('js/typeahead/typeahead.custom.js') }}"></script>
    <script src="{{ asset('js/typeahead-search/handlebars.js') }}"></script>
    <script src="{{ asset('js/typeahead-search/typeahead-custom.js') }}"></script>
    <script src="{{ asset('js/chart/chartist/chartist.js') }}"></script>
    <script src="{{ asset('js/chart/chartist/chartist-plugin-tooltip.js') }}"></script>
    <script src="{{ asset('js/chart/apex-chart/apex-chart.js') }}"></script>
    <script src="{{ asset('js/chart/apex-chart/stock-prices.js') }}"></script>
    <script src="{{ asset('js/prism/prism.min.js') }}"></script>
    <script src="{{ asset('js/clipboard/clipboard.min.js') }}"></script>
    <script src="{{ asset('js/counter/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('js/counter/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('js/counter/counter-custom.js') }}"></script>
    <script src="{{ asset('js/custom-card/custom-card.js') }}"></script>
    <script src="{{ asset('js/notify/bootstrap-notify.min.js') }}"></script>
    <script src="{{ asset('js/dashboard/default.js') }}"></script>
    <script src="{{ asset('js/notify/index.js') }}"></script>
    <script src="{{ asset('js/datepicker/date-picker/datepicker.js') }}"></script>
    <script src="{{ asset('js/datepicker/date-picker/datepicker.en.js') }}"></script>
    <script src="{{ asset('js/datepicker/date-picker/datepicker.custom.js') }}"></script>
    <script src="{{ asset('js/chat-menu.js') }}"></script>
    <script src="{{ asset('js/editor/summernote/summernote.js') }}"></script>
    <script src="{{ asset('bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>

    <!-- Plugins JS Ends-->
    <!-- leaflet plugin -->

    <script src="{{ asset('leaflet/leaflet-src-1.6.0.js') }}"></script>
    <!-- Theme js-->
    <script src="{{ asset('js/script.js') }}"></script>

    <script src="{{ asset('js/jquery.drilldown.js') }}"></script>
    <script src="{{ asset('js/vertical-menu.js') }}"></script>
    <script src="{{ asset('js/megamenu.js') }}"></script>
    <script src="{{ asset('json/ward.geojson') }}"></script>
    <!-- login js-->
    <!-- Plugin used-->
    @yield('footer-script')

    <script>
        var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
        var mapboxUrl =
            'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

        var grayscale = L.tileLayer(mapboxUrl, {
            id: 'mapbox/light-v9',
            tileSize: 512,
            zoomOffset: -1,
            attribution: mbAttr
        });

        var streets = L.tileLayer(mapboxUrl, {
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            attribution: mbAttr
        });
        //Inatializing Map Object
        var map = L.map('map', {
            scrollWheelZoom: false,
            touchZoom: true,
            doubleClickZoom: true,
            boxZoom: true,
            zoomControl: true,
            dragging: true,
            layers: streets,
            center: [27.954912, 83.5413758],
            zoom: 12.5
        });

        // loading geoJSON Data
        var lyrBoundary = L.geoJSON(boundary, {
            style: function(feature) {
                return {
                    fillColor: 'skyblue',
                    weight: 1,
                    color: 'blue',
                    opacity: 1,
                    // dashArray:3,
                    fillOpacity: 0.2
                }
            },
            onEachFeature: boundaryFeture
        }).addTo(map);

        //BindingToolTip as label
        lyrBoundary.eachLayer(function(layer) {
            layer.bindTooltip(layer.feature.properties.Name, {
                permanent: true,
                direction: "center",
                className: "wd-label"
            }).openTooltip()
        });
        var infoBox = L.control();

        infoBox.onAdd = function(map) {
            this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
            this.update();
            return this._div;
        };

        // method that we will use to update the control based on feature properties passed
        infoBox.update = function(props) {
            this._div.innerHTML = '<h4>वडाको क्षेत्रफल</h4>' + (props ?
                '<b>' + props.Name + '</b><br />' + props.Population :
                'Hover over a ward');
        };

        function boundaryFeture(feature, layer) {
            layer.on({
                // mouseover: highlightFeature,
                // mouseout: resetHighlight,
                click: clickFeature
            });
            infoPop(feature, layer);
        }

        var clickedLayer;

        function clickFeature(e) {
            lyrBoundary.resetStyle();
            map.fitBounds(e.target.getBounds());
            e.target.setStyle({
                weight: 2,
                fillColor: 'deeppink',
                color: 'deeppink',
                // fillOpacity: 0.5,
                // zIndex: 9999
            });
            e.target.bringToFront();
            clickedLayer = e.target;
            // alert(e.target.feature.properties.Name);
        }

        function highlightFeature(e) {
            var layer = e.target;
            layer.setStyle({
                weight: 2,
                fillOpacity: 0.5
            });
            layer.bringToFront();
            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront();
            }
            infoBox.update(layer.feature.properties);
        }

        function resetHighlight(e) {
            lyrBoundary.resetStyle(e.target);
            infoBox.update();
        }
        L.control.scale({
            maxWidth: 150,
            position: 'bottomleft'
        }).addTo(map);

        map.addLayer()

        function infoPop(feature, layer) {
            layer.bindPopup(
                `<ul class="nav nav-tabs" id="icon-tab" role="tablist">
              <li class="nav-item"><a class="nav-link active" id="icon-home-tab" data-toggle="tab" href="#icon-home" role="tab" aria-controls="icon-home" aria-selected="true"><i class="icofont icofont-ui-home"></i>House</a></li>
              <li class="nav-item"><a class="nav-link" id="profile-icon-tab" data-toggle="tab" href="#profile-icon" role="tab" aria-controls="profile-icon" aria-selected="false"><i class="icofont icofont-man-in-glasses"></i>Individual</a></li>
              <li class="nav-item"><a class="nav-link" id="contact-icon-tab" data-toggle="tab" href="#contact-icon" role="tab" aria-controls="contact-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Agriculture</a></li>
            </ul>
            <div class="tab-content" id="icon-tabContent">
              <div class="tab-pane fade show active" id="icon-home" role="tabpanel" aria-labelledby="icon-home-tab">
                <p class="mb-0 m-t-30"><strong>${feature.properties.GaPa_NaPa} ${feature.properties.Name}</strong> Household Details goes here -- Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
              </div>
              <div class="tab-pane fade" id="profile-icon" role="tabpanel" aria-labelledby="profile-icon-tab">
                <p class="mb-0 m-t-30">Individual goes here -- Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
              </div>
              <div class="tab-pane fade" id="contact-icon" role="tabpanel" aria-labelledby="contact-icon-tab">
                <p class="mb-0 m-t-30">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the</p>
              </div>
            </div>`
            )
        }
    </script>

    <script>
        var g = document.getElementsByClassName('btnn');
        // console.log(g.length);
        for (var i = 0; i < (g.length); i++) {
            g[i].addEventListener("click", function() {
                var current = document.getElementsByClassName("active");
                current[0].className = current[0].className.replace("active", "");
                console.log(current[0]);
                this.className += " active";
            });
        }
    </script>

    <style type="text/css">
        .leaflet-popup-content {
            width: 450px !important;
        }

    </style>
    <script>
        CKEDITOR.replace('editor1');
    </script>
</body>

</html>

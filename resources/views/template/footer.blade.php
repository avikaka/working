<div class="footer navbar  bg-light">
    <div class="row">
        <div class="col-12">

            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © {{ date('Y') }}
                <a href="http://deltrox.com/" target="_blank">Deltrox IT Solutions Pvt. Ltd.</a>. All rights
                reserved.</span>
            {{-- <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                <i class="mdi mdi-heart text-danger"></i>
            </span> --}}

        </div>
    </div>
</div>

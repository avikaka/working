<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>@yield('page-title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports"
        name="description" />
    <meta content="" name="author" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.15.4/js/all.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.15.4/js/v4-shims.js"></script>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
        type="text/css" />

    <link href="{{ asset('cd-admin/avi/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/simple-line-icons/simple-line-icons.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}"
        rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link
        href="{{ asset('cd-admin/avi/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/jqvmap/jqvmap/jqvmap.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet"
        type="text/css" />
    <link
        href="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/css/jquery.fileupload.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css') }}"
        rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('cd-admin/avi/global/css/components.min.css') }}" rel="stylesheet" id="style_components"
        type="text/css" />
    <link href="{{ asset('cd-admin/avi/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ asset('cd-admin/avi/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/avi/layouts/layout/css/themes/darkblue.min.css') }}" rel="stylesheet"
        type="text/css" id="style_color" />
    <link href="{{ asset('cd-admin/avi/layouts/layout/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('cd-admin/css/style.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="icon" type="image/x-icon" href="{{ url('uploads/logo/' . $logoimage) }}" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    {{-- <link href="{{ asset('leaflet/plugins/location-picker/locationPicker.css') }}" rel="stylesheet"
        type="text/css" />
    <script src="{{ asset('leaflet/plugins/location-picker/locationPicker.js') }}" type="text/javascript"></script> --}}


</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <div class="page-wrapper">

        @include('admin.dashboard.header.header')

        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <div class="page-sidebar-wrapper">
                @include('admin.dashboard.header.sidebar')
            </div>

            <!-- BEGIN SIDEBAR -->
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">

                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    @yield('content')
                    {{-- @include('admin.dashboard.header.dashContent') --}}
                </div>

                <!-- END CONTENT BODY -->

            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->

        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner fixed-bottom"> {{ date('Y') }} &copy; निर्माण
                <a target="_blank" href="http://deltrox.com">Deltrox</a>
            </div>

            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>

        </div>
        <!-- END FOOTER -->
    </div>

</body>
<!-- BEGIN CORE PLUGINS -->

<script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('cd-admin/avi/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript">
</script>

<script src="{{ asset('cd-admin/avi/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('cd-admin/avi/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"
type="text/javascript"></script>

<script src="{{ asset('cd-admin/avi/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('cd-admin/avi/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"
type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('cd-admin/avi/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
type="text/javascript"></script>

<script src="{{ asset('cd-admin/avi/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
{{-- <script src="{{ asset('cd-admin/avi/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript">
</script> --}}
<script src="{{ asset('cd-admin/avi/global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/amcharts/amcharts/pie.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/amcharts/amcharts/radar.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/amcharts/amcharts/themes/patterns.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/amcharts/amcharts/themes/chalk.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/amcharts/ammap/ammap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/amcharts/ammap/maps/js/worldLow.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/amcharts/amstockcharts/amstock.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/horizontal-timeline/horizontal-timeline.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/jqvmap/jqvmap/jquery.vmap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js') }}"
type="text/javascript"></script>

<script src="{{ asset('cd-admin/avi/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/datatables/datatables.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
type="text/javascript"></script>

<script src="{{ asset('cd-admin/avi/global/plugins/bootstrap-summernote/summernote.min.js') }}"
type="text/javascript"></script>

<script src="{{ asset('cd-admin/avi/global/plugins/jquery-repeater/jquery.repeater.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}"
type="text/javascript"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{ asset('cd-admin/avi/global/scripts/app.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/pages/scripts/components-editors.min.js') }}" type="text/javascript">
</script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('cd-admin/avi/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/pages/scripts/form-repeater.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript">
</script>


<script src="{{ asset('cd-admin/avi/pages/scripts/ui-extended-modals.min.js') }}" type="text/javascript">
</script>

<script src="{{ asset('cd-admin/avi/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/pages/scripts/form-fileupload.min.js') }}" type="text/javascript"></script>



<script src="{{ asset('cd-admin/avi/global/plugins/fancybox/source/jquery.fancybox.pack.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/js/vendor/load-image.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js') }}"
type="text/javascript"></script>
<script
src="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/js/jquery.fileupload.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js') }}"
type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js') }}"
type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{ asset('cd-admin/avi/layouts/layout/scripts/layout.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/layouts/layout/scripts/demo.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('cd-admin/avi/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('cd-admin/avi/layouts/global/scripts/quick-nav.min.js') }}" type="text/javascript">
</script>
<!-- END THEME LAYOUT SCRIPTS -->
<script>
    $(document).ready(function() {
        $('#clickmewow').click(function() {
            $('#radio1003').attr('checked', 'checked');
        });
    })
</script>

<script src="{{ asset('cd-admin/js/js.js') }}" type="text/javascript"></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote();

    });
</script>
<script>
    $(document).ready(function() {
        $('#summernote1').summernote();

    });
</script>


</html>

<nav class="sidebar sidebar-offcanvas" id="sidebar">


    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#‌mnu-setting" aria-expanded="false"
            aria-controls="‌mnu-setting">
            <i class="menu-icon mdi mdi-settings"></i>
            <span class="menu-title">Settings</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="‌mnu-setting">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.community-building.index') }}">सामुदायिक भवन</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.community-forest.index') }}">सामुदायिक वनसम्बन्धी
                        विवरण</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.pond-lake-wetland.index') }}">सार्वजनिक निजी
                        पोखरी,
                        ताल, सिमसारको विवरण</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.irrigation.index') }}">सिंचाई आयोजनाहरु</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.agro-vet-pocket.index') }}">कृषि/पशु पंक्षी पकेट
                        क्षेत्र</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.road-network.index') }}">सडक संजालको अवस्था</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.bridge.index') }}">पुल पुलेसा</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.drinking-water.index') }}">खानेपानी आयोजना</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.haat-bazar.index') }}">कृषि तथा हाट बजार</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.public-toilet.index') }}">सार्वजनिक शौचालय</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.religious.index') }}">धार्मिकस्थल</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.tourism.index') }}">पर्यटकीय क्षेत्र</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.tlo.index') }}">टोल विकास संस्था</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.cooperative.index') }}">सहकारी संस्था</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.bank.index') }}">बैँक</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.health-institution.index') }}">स्वास्थ्य
                        संस्था</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.college.index') }}">विद्यालय/क्याम्पस</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.playground.index') }}">खेल मैदान/व्यायामशाला</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.agro-vet.index') }}">कृषि/पशुपालन समूह</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.agro-farm.index') }}">कृषि/पशुपालन फार्म</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.children-club.index') }}">बाल क्लब</a>
                </li>
                {{-- <li class="nav-item">
              <a class="nav-link" href="{{ route('admin.club.index') }}">युवा/खेलकुद क्लब</a>
    </li> --}}
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.public-place.index') }}">पाटी/पौवा चौतारा</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.crematorium.index') }}">चिहानस्थलहरु</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.women-group.index') }}">आमा समूह</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.social-home.index') }}">महिला सुरक्षा</a>
                </li>


            </ul>
        </div>
    </li>

    </ul>
</nav>

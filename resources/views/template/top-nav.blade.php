<!-- page-wrapper Start-->
<div>
    <!-- Page Header Start-->
    <div>

        <nav class="navbar fixed-top navbar-expand-lg navbar-bold " style="background: #f8f5fd !important">
            <div class="logo-wrapper"><a href="/">
                    <img src="{{ url('uploads/logo/' . $logoimage) }}" height="80px" alt="">
                </a>
            </div>
            <br>
            <div class="title ml-2">
                <span>{{ $logoname }}</span>
            </div>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" style="margin-left:6rem" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-4 mb-lg-0">

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active btnn" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            | <i class="icofont icofont-home"></i><span class="ml-2">घरधुरी</span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('household.house.ownership') }}">
                                <br> घरको
                                स्वामित्व </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('household.land.ownership') }}"> जग्गाको
                                स्वामित्व </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('household.house.type') }}"> घरको प्रकार
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('household.water.source') }}"> पिउने
                                पानीको मुख्यस्रोत </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('household.cooking.fuel') }}"> खाना
                                पकाउने मुख्य इन्धन </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('household.light.fuel') }}"> बत्ती बाल्ने
                                मुख्य उर्जा </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('household.toilet') }}"> चर्पीको
                                प्रकार</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('household.garbage') }}">फोहर व्यवस्थापन
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('household.amenities') }}"> उपलब्ध
                                साधन/सुविधा </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('household.access.road') }}"> बाटोको
                                अवस्था </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('household.women.assets') }}"> महिलाको
                                नाममा सम्पत्ति </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('household.bank.account') }}"> बैँक
                                खाता</a>
                        </div>
                    </li>


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle btnn" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            | <i class="icofont icofont-male"></i><span class="ml-2">जनसंख्या</span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('population.gender') }}">लिङ्गअनुसार</a>
                            {{-- <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('population.agewise') }}">उमेरसमूहअनुसार</a> --}}
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('population.religion') }}">धर्मअनुसार</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('population.caste') }}">जातजातिअनुसार</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('population.mother.tongue') }}">मातृभाषाअनुसार</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('population.disability') }}">अपाङ्गता
                                सम्बन्धी</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('population.marital.status') }}">वैवाहिक
                                स्थिति</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('population.marital.status.current.age') }}">हालको
                                उमेर
                                अनुसार वैवाहिक स्थिति</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('population.birth.place') }}">जन्मस्थान</a>
                        </div>
                    </li>



                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle btnn" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            | <i class="icofont icofont-education"></i><span
                                class="ml-2">शिक्षासम्बन्धी</span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('population.literacy') }}">साक्षरताको
                                अवस्था
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('education.level') }}">उत्तीर्ण
                                गरेको
                                तह
                            </a>

                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('education.school.not.attending.reasons') }}">विद्यालय
                                नजानुका कारण
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('education.training') }}">पेशागत
                                तालिम
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('education.skills') }}">मुख्य
                                शिप
                                वा
                                क्षमता
                            </a>
                        </div>
                    </li>


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle btnn" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            | <i class="icofont icofont-stethoscope"></i><span
                                class="ml-2">स्वास्थ्यसम्बन्धी</span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('health.life.long.disease') }}">दीर्घरोग</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('health.emergency.disease') }}">आकस्मिक
                                रोग</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('health.vaccination') }}">खोप</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('health.safe') }}">सुरक्षित
                                मातृत्व</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('health.child.weight') }}">शिशुको
                                तौल</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('health.delivery.place') }}">सुत्केरी
                                गराएको स्थान</a>
                        </div>
                    </li>



                    {{-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle btnn" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            | <i class="icofont icofont-baby"></i><span class="ml-2">प्रजनन्
                                स्वास्थ्यसम्बन्धी</span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            {{-- <a class="dropdown-item" class="nav-link" href="{{ route('health.total.birth') }}">सन्तान
                            जाय
                            जन्म</a>
                            <div class="dropdown-divider"></div> --}}
                    {{-- <a class="dropdown-item" class="nav-link"
                                href="{{ route('health.child.weight') }}">शिशुको
                                तौल</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('health.delivery.place') }}">सुत्केरी
                                गराएको स्थान</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('health.safe') }}">सुरक्षित
                                मातृत्व</a>
                        </div>
                    </li> --}}



                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle btnn" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            | <i class="icofont icofont-education"></i><span class="ml-2">अनुपस्थित</span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('absentees.gender') }}">लिङ्गअनुसार</a>
                            {{-- <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('absentees.agewise') }}">उमेरसमूहअनुसार</a> --}}
                            <div class="dropdown-divider"></div>
                            {{-- <a class="dropdown-item" class="nav-link"
                                href="{{ route('absentees.education') }}">शैक्षिक
                                स्तर</a>
                            <div class="dropdown-divider"></div> --}}
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('absentees.reasons') }}">अनुपस्थित
                                हुनुका
                                कारण</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" class="nav-link"
                                href="{{ route('absentees.currently.stayiing') }}">हाल
                                बसिरहेको स्थान</a>
                        </div>
                    </li>


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle btnn" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            | <i class="icofont icofont-stethoscope"></i><span class="ml-2">संस्थागत
                                विवरण</span>
                        </a>
                        <div class="dropdown-menu pre-scrollable" aria-labelledby="navbarDropdown">

                            <a class="nav-link dropdown-item" href="/communityBuilding">सामुदायिक
                                भवन</a>


                            <a class="nav-link dropdown-item" href="/communityForest">सामुदायिक
                                वनसम्बन्धी
                                विवरण</a>


                            <a class="nav-link dropdown-item" href="/waterLands">सार्वजनिक
                                निजी
                                पोखरी,
                                ताल, सिमसारको विवरण</a>


                            <a class="nav-link dropdown-item" href="/irrigation">सिंचाई
                                आयोजनाहरु</a>


                            <a class="nav-link dropdown-item" href="/agroPocket">कृषि/पशु
                                पंक्षी
                                पकेट
                                क्षेत्र</a>


                            <a class="nav-link dropdown-item" href="/roads">सडक
                                संजालको
                                अवस्था</a>


                            <a class="nav-link dropdown-item" href="/bridge">पुल
                                पुलेसा</a>


                            <a class="nav-link dropdown-item" href="/drinkingWater">खानेपानी
                                आयोजना</a>


                            <a class="nav-link dropdown-item" href="/haatBazar">कृषि तथा
                                हाट
                                बजार</a>


                            <a class="nav-link dropdown-item" href="/publicToilets">सार्वजनिक
                                शौचालय</a>


                            <a class="nav-link dropdown-item" href="/religious">धार्मिकस्थल</a>


                            <a class="nav-link dropdown-item" href="/tourisms">पर्यटकीय
                                क्षेत्र</a>


                            <a class="nav-link dropdown-item" href="/tlo">टोल विकास
                                संस्था</a>


                            <a class="nav-link dropdown-item" href="/cooperatives">सहकारी
                                संस्था</a>


                            <a class="nav-link dropdown-item" href="/banks">बैँक</a>


                            <a class="nav-link dropdown-item" href="/healthInstitutions">स्वास्थ्य
                                संस्था</a>


                            <a class="nav-link dropdown-item" href="/colleges">विद्यालय/क्याम्पस</a>


                            <a class="nav-link dropdown-item" href="/playground">खेल
                                मैदान/व्यायामशाला</a>


                            <a class="nav-link dropdown-item" href="/agroGroups">कृषि/पशुपालन
                                समूह</a>


                            <a class="nav-link dropdown-item" href="/agroFarms">कृषि/पशुपालन
                                फार्म</a>


                            <a class="nav-link dropdown-item" href="/childrenClubs">
                                क्लब</a>

                            {{-- <a class="nav-link dropdown-item" href="{{ route('admin.club.index') }}">युवा/खेलकुद क्लब</a> --}}

                            <a class="nav-link dropdown-item" href="/publicPlaces">पाटी/पौवा
                                चौतारा</a>


                            <a class="nav-link dropdown-item" href="/crematorioum">चिहानस्थलहरु</a>



                            <a class="nav-link dropdown-item" href="/womensGroup">आमा
                                समूह</a>


                            <a class="nav-link dropdown-item" href="/socialHome">महिला
                                सुरक्षा</a>

                        </div>
                    </li>

            </div>

            <div class="col-sm-2 d-sm-block d-none">
                <img src="/images/np_flag.gif" class="img-fluid float-right" style="height:95px; margin-bottom: 6px">

            </div>
        </nav>
    </div>
</div>

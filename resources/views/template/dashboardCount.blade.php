<div class="col-lg-4">
    <div class="box">
        <div class="row height-info">
            <div class="col-md-12 anchor">
                <h3 class="font-weight-normal text-center">प्रमुख वस्तुहरू</h3>
                <h4 class="mt-2 font-weight-light">सामुदाय</h4>
                <ul class="table-view mt-2">
                    <li>
                        <h4>कुल वडा</h4>
                        <p class="font-weight-bold">{{ TOTAL_WARD }}</p>
                    </li>
                    <li>
                        <a href="/communityBuilding">
                            <h4>सामुदायिक भवन</h4>
                        </a>
                        <p class="font-weight-bold">{!! itemsCount('communitybuilding_file') !!}</p>
                    </li>
                    <li>
                        <a href="/roads">
                            <h4>सडक ट्रयाक</h4>
                        </a>
                        <p class="font-weight-bold">{!! itemsCount('roadnetworkdetails_file') !!}</p>
                    </li>
                    <li>
                        <a href="/publicToilets">
                            <h4>सार्वजनिक शौचालय</h4>
                        </a>
                        <p class="font-weight-bold">{!! itemsCount('toiletdetails_file') !!}</p>
                    </li>
                    <li>
                        <a href="/publicPlaces">
                            <h4>सार्वजनिक चौतारा</h4>
                        </a>
                        <p class="font-weight-bold">{!! itemsCount('public-place_file') !!}</p>
                    </li>
                    <li>
                        <a href="/communityForest">
                            <h4>सामुदायिक वन</h4>
                        </a>
                        <p class="font-weight-bold">{!! itemsCount('communityforest_file') !!}</p>
                    </li>
                    <li>
                        <a href="/religious">
                            <h4>धार्मिक स्थल</h4>
                        </a>
                        <p class="font-weight-bold">{!! itemsCount('religious_file') !!}</p>
                    </li>
                    <li>
                        <a href="/playground">
                            <h4>पार्क तथा खेलमैदान</h4>
                        </a>
                        <p class="font-weight-bold">{!! itemsCount('playground_file') !!}</p>
                    </li>

                    <div class="clearfix"></div>
                </ul>

                <a href="healthInstitutions">
                    <h4 class="mt-2 font-weight-light">स्वास्थ्य</h4>
                </a>
                <ul class="table-view mt-2">
                    <li>
                        <a href="healthInstitutions">
                            <h4>सामान्य अस्पताल</h4>
                        </a>
                        <p class="font-weight-bold">
                            @if ($hos == 0)
                                -
                            @else
                                {{ $hos }}
                            @endif
                        </p>
                    </li>
                    <li>
                        <a href="healthInstitutions">
                            <h4>स्वास्थ्य चौकी</h4>
                        </a>
                        <p class="font-weight-bold">
                            @if ($hp == 0)
                                -
                            @else
                                {{ $hp }}
                            @endif
                        </p>
                    </li>
                    <li>
                        <h4>स्स्वास्थ्य इकाइ</h4>
                        <p class="font-weight-bold">
                            @if ($sk == 0)
                                -
                            @else
                                {{ $sk }}
                            @endif
                        </p>
                    </li>
                    <li>
                        <h4>निजी अस्पताल</h4>
                        <p class="font-weight-bold">
                            @if ($clinic == 0)
                                -
                            @else
                                {{ $clinic }}
                            @endif
                        </p>
                    </li>
                    <li>
                        <h4>केन्द्रीय अस्पताल</h4>
                        <p class="font-weight-bold">
                            @if ($birthC == 0)
                                -
                            @else
                                {{ $birthC }}
                            @endif
                        </p>
                    </li>
                    <div class="clearfix"></div>
                </ul>

                <a href="/colleges">
                    <h4 class="mt-2 font-weight-light">शिक्षा</h4>
                </a>
                <ul class="table-view mt-2">
                    <li>
                        <h4>विद्यालय (प्रा.बि)</h4>
                        <p class="font-weight-bold">
                            @if ($pra == 0)
                                -
                            @else
                                {{ $pra }}
                            @endif
                        </p>
                    </li>
                    <li>
                        <h4>विद्यालय (मा.बि)</h4>
                        <p class="font-weight-bold">
                            @if ($ma == 0)
                                -
                            @else
                                {{ $ma }}
                            @endif
                        </p>
                    </li>
                    <li>
                        <h4>आधारभूत</h4>
                        <p class="font-weight-bold">
                            @if ($ad == 0)
                                -
                            @else
                                {{ $ad }}
                            @endif
                        </p>
                    </li>
                    <li>
                        <h4>निजी</h4>
                        <p class="font-weight-bold">
                            @if ($priv == 0)
                                -
                            @else
                                {{ $priv }}
                            @endif
                        </p>
                    </li>
                    <li>
                        <h4>क्याम्पस</h4>
                        <p class="font-weight-bold">
                            @if ($col == 0)
                                -
                            @else
                                {{ $col }}
                            @endif
                        </p>
                    </li>
                    <li>
                        <h4>मदरसा</h4>
                        <p class="font-weight-bold">
                            @if ($mad == 0)
                                -
                            @else
                                {{ $mad }}
                            @endif
                        </p>
                    </li>
                    <li>
                        <h4>विश्वविद्यालय</h4>
                        <p class="font-weight-bold">
                            @if ($uni == 0)
                                -
                            @else
                                {{ $uni }}
                            @endif
                        </p>
                    </li>
                    <div class="clearfix"></div>
                </ul>

                <h4 class="mt-2 font-weight-light">संस्थाहरू</h4>
                <ul class="table-view mt-2">
                    <li>
                        <a href="/banks">
                            <h4>बैंक</h4>
                        </a>
                        <p class="font-weight-bold">
                            @if ($ban == 0)
                                -
                            @else
                                {{ $ban }}
                            @endif
                        </p>
                    </li>
                    <li>
                        <a href="/cooperatives">
                            <h4>सहकारी</h4>
                        </a>
                        <p class="font-weight-bold">{!! itemsCount('cooperativedetails_file') !!}</p>
                    </li>
                    <li>
                        <h4>वित्त</h4>
                        <p class="font-weight-bold">
                            @if ($fin == 0)
                                -
                            @else
                                {{ $fin }}
                            @endif
                        </p>
                    </li>
                    <li>
                        <a href="/womensGroup">
                            <h4>महिला समूह</h4>
                        </a>
                        <p class="font-weight-bold">{!! itemsCount('womenGroup_file') !!}</p>
                    </li>
                    <li>
                        <a href="/childrenClubs">
                            <h4>क्लब</h4>
                        </a>
                        <p class="font-weight-bold">{!! itemsCount('childclubdetails_file') !!}</p>
                    </li>
                    <li>
                        <a href="/tlo">
                            <h4>टोल विकास संस्था</h4>
                        </a>
                        <p class="font-weight-bold">{!! itemsCount('tlodetails_file') !!}</p>
                    </li>
                    <li>
                        <a href="/agroGroups">
                            <h4>कृषि समूह</h4>
                        </a>
                        <p class="font-weight-bold">{!! itemsCount('agro_group_file') !!}</p>
                    </li>
                    <li>
                        <a href="/agroFarms">
                            <h4>कृषि फार्म</h4>
                        </a>
                        <p class="font-weight-bold">{!! itemsCount('agrofarmdetails_file') !!}</p>
                    </li>
                    <div class="clearfix"></div>
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
